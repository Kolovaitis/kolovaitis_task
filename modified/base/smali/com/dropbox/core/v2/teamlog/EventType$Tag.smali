.class public final enum Lcom/dropbox/core/v2/teamlog/EventType$Tag;
.super Ljava/lang/Enum;
.source "EventType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/teamlog/EventType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Tag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/dropbox/core/v2/teamlog/EventType$Tag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum ACCOUNT_CAPTURE_CHANGE_AVAILABILITY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum ACCOUNT_CAPTURE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum ACCOUNT_CAPTURE_MIGRATE_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum ALLOW_DOWNLOAD_DISABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum ALLOW_DOWNLOAD_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum APP_LINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum APP_LINK_USER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum APP_UNLINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum APP_UNLINK_USER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum CAMERA_UPLOADS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum COLLECTION_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum CREATE_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_MOBILE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_UNLINK_ACTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_CHANGE_IP_DESKTOP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_CHANGE_IP_MOBILE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_CHANGE_IP_WEB:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_DELETE_ON_UNLINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_DELETE_ON_UNLINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_LINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_LINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_MANAGEMENT_DISABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_MANAGEMENT_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DEVICE_UNLINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DIRECTORY_RESTRICTIONS_ADD_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DISABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_INVITES_EMAIL_EXISTING_USERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum DOMAIN_VERIFICATION_REMOVE_DOMAIN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EMM_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EMM_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EMM_CREATE_EXCEPTIONS_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EMM_CREATE_USAGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EMM_ERROR:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EMM_REFRESH_AUTH_TOKEN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EMM_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum ENABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EXPORT_MEMBERS_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum EXTENDED_VERSION_HISTORY_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_ADD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_CHANGE_COMMENT_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_COMMENTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_EDIT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_GET_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_LIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_MOVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_PREVIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_REQUESTS_EMAILS_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_REQUEST_CHANGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_REQUEST_CLOSE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_REQUEST_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_REQUEST_RECEIVE_FILE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_RESTORE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_REVERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_ROLLBACK_CHANGES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_SAVE_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_UNLIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum FILE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GOOGLE_SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_ADD_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_CHANGE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_CHANGE_MANAGEMENT_TYPE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_DESCRIPTION_UPDATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_JOIN_POLICY_UPDATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_MOVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_REMOVE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum GROUP_USER_MANAGEMENT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum LOGIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum LOGIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum LOGOUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_ADD_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_CHANGE_ADMIN_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_CHANGE_EMAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_CHANGE_MEMBERSHIP_TYPE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_DELETE_MANUAL_CONTACTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SUGGEST:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_SUGGESTIONS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MEMBER_TRANSFER_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum NETWORK_CONTROL_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum NOTE_ACL_INVITE_ONLY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum NOTE_ACL_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum NOTE_ACL_TEAM_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum NOTE_SHARE_RECEIVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum OPEN_NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum OTHER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_ADMIN_EXPORT_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CHANGE_DEPLOYMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CHANGE_MEMBER_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_ADD_TO_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_ARCHIVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_REMOVE_FROM_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_CONTENT_RESTORE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_CHANGE_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_EDIT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_MENTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_OWNERSHIP_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_REVERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_SLACK_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_TRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_DOC_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_ENABLED_USERS_GROUP_ADDITION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_ENABLED_USERS_GROUP_REMOVAL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_ALLOW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_DEFAULT_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_FORBID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_FOLDER_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_FOLDER_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_FOLDER_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PAPER_FOLDER_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PASSWORD_CHANGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PASSWORD_RESET:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PASSWORD_RESET_ALL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum PERMANENT_DELETE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum RESELLER_SUPPORT_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum RESELLER_SUPPORT_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SECONDARY_MAILS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_ADD_GROUP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_EXTERNAL_INVITE_WARN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_FB_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_FB_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_FB_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_INVITE_GROUP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_TEAM_GRANT_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_TEAM_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_TEAM_JOIN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_TEAM_JOIN_FROM_OOB_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SF_TEAM_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_ADD_INVITEES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_ADD_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_ADD_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_INVITEE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_AUDIENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_CLAIM_INVITATION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_RELINQUISH_MEMBERSHIP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_INVITEES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_UNSHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_CONTENT_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_DECLINE_INVITATION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_MOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_NEST:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_TRANSFER_OWNERSHIP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_FOLDER_UNMOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_ADD_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_CHANGE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_CHANGE_VISIBILITY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_DISABLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_REMOVE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_LINK_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARED_NOTE_OPENED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARING_CHANGE_FOLDER_JOIN_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARING_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHARING_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHMODEL_GROUP_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_ACCESS_GRANTED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_ARCHIVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_CHANGE_DOWNLOAD_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_CHANGE_ENABLED_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_CREATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_EDITED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_FILE_ADDED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_FILE_REMOVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_FILE_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_PERMANENTLY_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_POST_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_RENAMED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_RESTORED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_TRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_TRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_UNTRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SHOWCASE_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SIGN_IN_AS_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SIGN_IN_AS_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SMART_SYNC_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SMART_SYNC_NOT_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SMART_SYNC_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_ADD_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_ADD_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_ADD_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_CHANGE_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_CHANGE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_CHANGE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_CHANGE_SAML_IDENTITY_MODE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_ERROR:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_REMOVE_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_REMOVE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum SSO_REMOVE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_ACTIVITY_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_FOLDER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_FOLDER_DOWNGRADE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_FOLDER_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_FOLDER_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_MERGE_FROM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_MERGE_TO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_PROFILE_ADD_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_PROFILE_REMOVE_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_SELECTIVE_SYNC_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TFA_ADD_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TFA_ADD_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TFA_CHANGE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TFA_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TFA_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TFA_REMOVE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TFA_REMOVE_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TFA_RESET:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum TWO_ACCOUNT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum VIEWER_INFO_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

.field public static final enum WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 41
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "APP_LINK_TEAM"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->APP_LINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 45
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "APP_LINK_USER"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->APP_LINK_USER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 49
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "APP_UNLINK_TEAM"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->APP_UNLINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 53
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "APP_UNLINK_USER"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->APP_UNLINK_USER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 57
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_ADD_COMMENT"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 62
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_CHANGE_COMMENT_SUBSCRIPTION"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_CHANGE_COMMENT_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 66
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_DELETE_COMMENT"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 70
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_EDIT_COMMENT"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 74
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_LIKE_COMMENT"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_LIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 78
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_RESOLVE_COMMENT"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 82
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_UNLIKE_COMMENT"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_UNLIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 86
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_UNRESOLVE_COMMENT"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 90
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_CHANGE_IP_DESKTOP"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_CHANGE_IP_DESKTOP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 94
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_CHANGE_IP_MOBILE"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_CHANGE_IP_MOBILE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 98
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_CHANGE_IP_WEB"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_CHANGE_IP_WEB:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 102
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_DELETE_ON_UNLINK_FAIL"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_DELETE_ON_UNLINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 106
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_DELETE_ON_UNLINK_SUCCESS"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_DELETE_ON_UNLINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 110
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_LINK_FAIL"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_LINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 114
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_LINK_SUCCESS"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_LINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 118
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_MANAGEMENT_DISABLED"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_MANAGEMENT_DISABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 122
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_MANAGEMENT_ENABLED"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_MANAGEMENT_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 126
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_UNLINK"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_UNLINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 131
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EMM_REFRESH_AUTH_TOKEN"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_REFRESH_AUTH_TOKEN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 136
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "ACCOUNT_CAPTURE_CHANGE_AVAILABILITY"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_CHANGE_AVAILABILITY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 140
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "ACCOUNT_CAPTURE_MIGRATE_ACCOUNT"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_MIGRATE_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 145
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 150
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 154
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DISABLED_DOMAIN_INVITES"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DISABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 158
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 162
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM"

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 167
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_INVITES_EMAIL_EXISTING_USERS"

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_EMAIL_EXISTING_USERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 171
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM"

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 176
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO"

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 181
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES"

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 185
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL"

    const/16 v15, 0x22

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 189
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS"

    const/16 v15, 0x23

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 193
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DOMAIN_VERIFICATION_REMOVE_DOMAIN"

    const/16 v15, 0x24

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_VERIFICATION_REMOVE_DOMAIN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 197
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "ENABLED_DOMAIN_INVITES"

    const/16 v15, 0x25

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ENABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 201
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "CREATE_FOLDER"

    const/16 v15, 0x26

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->CREATE_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 205
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_ADD"

    const/16 v15, 0x27

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_ADD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 209
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_COPY"

    const/16 v15, 0x28

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 213
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_DELETE"

    const/16 v15, 0x29

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 217
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_DOWNLOAD"

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 221
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_EDIT"

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_EDIT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 225
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_GET_COPY_REFERENCE"

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_GET_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 229
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_MOVE"

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_MOVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 233
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_PERMANENTLY_DELETE"

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 237
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_PREVIEW"

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_PREVIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 241
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_RENAME"

    const/16 v15, 0x30

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 245
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_RESTORE"

    const/16 v15, 0x31

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_RESTORE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 249
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_REVERT"

    const/16 v15, 0x32

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REVERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 253
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_ROLLBACK_CHANGES"

    const/16 v15, 0x33

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_ROLLBACK_CHANGES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 257
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_SAVE_COPY_REFERENCE"

    const/16 v15, 0x34

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_SAVE_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 261
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_REQUEST_CHANGE"

    const/16 v15, 0x35

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUEST_CHANGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 265
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_REQUEST_CLOSE"

    const/16 v15, 0x36

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUEST_CLOSE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 269
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_REQUEST_CREATE"

    const/16 v15, 0x37

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUEST_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 273
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_REQUEST_RECEIVE_FILE"

    const/16 v15, 0x38

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUEST_RECEIVE_FILE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 277
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_ADD_EXTERNAL_ID"

    const/16 v15, 0x39

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_ADD_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 281
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_ADD_MEMBER"

    const/16 v15, 0x3a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 285
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_CHANGE_EXTERNAL_ID"

    const/16 v15, 0x3b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_CHANGE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 289
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_CHANGE_MANAGEMENT_TYPE"

    const/16 v15, 0x3c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_CHANGE_MANAGEMENT_TYPE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 293
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_CHANGE_MEMBER_ROLE"

    const/16 v15, 0x3d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 297
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_CREATE"

    const/16 v15, 0x3e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 301
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_DELETE"

    const/16 v15, 0x3f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 305
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_DESCRIPTION_UPDATED"

    const/16 v15, 0x40

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_DESCRIPTION_UPDATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 309
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_JOIN_POLICY_UPDATED"

    const/16 v15, 0x41

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_JOIN_POLICY_UPDATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 313
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_MOVED"

    const/16 v15, 0x42

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_MOVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 317
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_REMOVE_EXTERNAL_ID"

    const/16 v15, 0x43

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_REMOVE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 321
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_REMOVE_MEMBER"

    const/16 v15, 0x44

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 325
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_RENAME"

    const/16 v15, 0x45

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 330
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EMM_ERROR"

    const/16 v15, 0x46

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_ERROR:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 334
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "LOGIN_FAIL"

    const/16 v15, 0x47

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->LOGIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 338
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "LOGIN_SUCCESS"

    const/16 v15, 0x48

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->LOGIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 342
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "LOGOUT"

    const/16 v15, 0x49

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->LOGOUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 346
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "RESELLER_SUPPORT_SESSION_END"

    const/16 v15, 0x4a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->RESELLER_SUPPORT_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 350
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "RESELLER_SUPPORT_SESSION_START"

    const/16 v15, 0x4b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->RESELLER_SUPPORT_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 354
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SIGN_IN_AS_SESSION_END"

    const/16 v15, 0x4c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SIGN_IN_AS_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 358
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SIGN_IN_AS_SESSION_START"

    const/16 v15, 0x4d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SIGN_IN_AS_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 363
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_ERROR"

    const/16 v15, 0x4e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_ERROR:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 367
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_ADD_NAME"

    const/16 v15, 0x4f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_ADD_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 371
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_CHANGE_ADMIN_ROLE"

    const/16 v15, 0x50

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_ADMIN_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 375
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_CHANGE_EMAIL"

    const/16 v15, 0x51

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_EMAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 380
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_CHANGE_MEMBERSHIP_TYPE"

    const/16 v15, 0x52

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_MEMBERSHIP_TYPE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 384
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_CHANGE_NAME"

    const/16 v15, 0x53

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 388
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_CHANGE_STATUS"

    const/16 v15, 0x54

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 392
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_DELETE_MANUAL_CONTACTS"

    const/16 v15, 0x55

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_DELETE_MANUAL_CONTACTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 396
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS"

    const/16 v15, 0x56

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 400
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA"

    const/16 v15, 0x57

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 404
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA"

    const/16 v15, 0x58

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 408
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_CHANGE_STATUS"

    const/16 v15, 0x59

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 412
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA"

    const/16 v15, 0x5a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 416
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SUGGEST"

    const/16 v15, 0x5b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SUGGEST:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 421
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_TRANSFER_ACCOUNT_CONTENTS"

    const/16 v15, 0x5c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_TRANSFER_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 425
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SECONDARY_MAILS_POLICY_CHANGED"

    const/16 v15, 0x5d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SECONDARY_MAILS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 429
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_ADD_MEMBER"

    const/16 v15, 0x5e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 433
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_ADD_TO_FOLDER"

    const/16 v15, 0x5f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_ADD_TO_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 437
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_ARCHIVE"

    const/16 v15, 0x60

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_ARCHIVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 441
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_CREATE"

    const/16 v15, 0x61

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 445
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_PERMANENTLY_DELETE"

    const/16 v15, 0x62

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 449
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_REMOVE_FROM_FOLDER"

    const/16 v15, 0x63

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_REMOVE_FROM_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 453
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_REMOVE_MEMBER"

    const/16 v15, 0x64

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 457
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_RENAME"

    const/16 v15, 0x65

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 461
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CONTENT_RESTORE"

    const/16 v15, 0x66

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_RESTORE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 465
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_ADD_COMMENT"

    const/16 v15, 0x67

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 469
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_CHANGE_MEMBER_ROLE"

    const/16 v15, 0x68

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 473
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_CHANGE_SHARING_POLICY"

    const/16 v15, 0x69

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_CHANGE_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 477
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_CHANGE_SUBSCRIPTION"

    const/16 v15, 0x6a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 481
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_DELETED"

    const/16 v15, 0x6b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 485
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_DELETE_COMMENT"

    const/16 v15, 0x6c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 489
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_DOWNLOAD"

    const/16 v15, 0x6d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 493
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_EDIT"

    const/16 v15, 0x6e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_EDIT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 497
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_EDIT_COMMENT"

    const/16 v15, 0x6f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 502
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_FOLLOWED"

    const/16 v15, 0x70

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 506
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_MENTION"

    const/16 v15, 0x71

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_MENTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 510
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_OWNERSHIP_CHANGED"

    const/16 v15, 0x72

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_OWNERSHIP_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 514
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_REQUEST_ACCESS"

    const/16 v15, 0x73

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 518
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_RESOLVE_COMMENT"

    const/16 v15, 0x74

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 522
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_REVERT"

    const/16 v15, 0x75

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_REVERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 526
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_SLACK_SHARE"

    const/16 v15, 0x76

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_SLACK_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 531
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_TEAM_INVITE"

    const/16 v15, 0x77

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 535
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_TRASHED"

    const/16 v15, 0x78

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_TRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 539
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_UNRESOLVE_COMMENT"

    const/16 v15, 0x79

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 543
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_UNTRASHED"

    const/16 v15, 0x7a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 547
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_DOC_VIEW"

    const/16 v15, 0x7b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 552
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_EXTERNAL_VIEW_ALLOW"

    const/16 v15, 0x7c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_EXTERNAL_VIEW_ALLOW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 557
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_EXTERNAL_VIEW_DEFAULT_TEAM"

    const/16 v15, 0x7d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_EXTERNAL_VIEW_DEFAULT_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 562
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_EXTERNAL_VIEW_FORBID"

    const/16 v15, 0x7e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_EXTERNAL_VIEW_FORBID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 566
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_FOLDER_CHANGE_SUBSCRIPTION"

    const/16 v15, 0x7f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_FOLDER_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 570
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_FOLDER_DELETED"

    const/16 v15, 0x80

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_FOLDER_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 575
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_FOLDER_FOLLOWED"

    const/16 v15, 0x81

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_FOLDER_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 580
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_FOLDER_TEAM_INVITE"

    const/16 v15, 0x82

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_FOLDER_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 584
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PASSWORD_CHANGE"

    const/16 v15, 0x83

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PASSWORD_CHANGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 588
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PASSWORD_RESET"

    const/16 v15, 0x84

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PASSWORD_RESET:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 592
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PASSWORD_RESET_ALL"

    const/16 v15, 0x85

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PASSWORD_RESET_ALL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 596
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EMM_CREATE_EXCEPTIONS_REPORT"

    const/16 v15, 0x86

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_CREATE_EXCEPTIONS_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 600
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EMM_CREATE_USAGE_REPORT"

    const/16 v15, 0x87

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_CREATE_USAGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 604
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EXPORT_MEMBERS_REPORT"

    const/16 v15, 0x88

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EXPORT_MEMBERS_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 608
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_ADMIN_EXPORT_START"

    const/16 v15, 0x89

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_ADMIN_EXPORT_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 612
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT"

    const/16 v15, 0x8a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 616
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_ACTIVITY_CREATE_REPORT"

    const/16 v15, 0x8b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_ACTIVITY_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 620
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "COLLECTION_SHARE"

    const/16 v15, 0x8c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->COLLECTION_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 625
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "NOTE_ACL_INVITE_ONLY"

    const/16 v15, 0x8d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_ACL_INVITE_ONLY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 630
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "NOTE_ACL_LINK"

    const/16 v15, 0x8e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_ACL_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 635
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "NOTE_ACL_TEAM_LINK"

    const/16 v15, 0x8f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_ACL_TEAM_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 639
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "NOTE_SHARED"

    const/16 v15, 0x90

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 643
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "NOTE_SHARE_RECEIVE"

    const/16 v15, 0x91

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_SHARE_RECEIVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 647
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "OPEN_NOTE_SHARED"

    const/16 v15, 0x92

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->OPEN_NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 651
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_ADD_GROUP"

    const/16 v15, 0x93

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_ADD_GROUP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 656
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS"

    const/16 v15, 0x94

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 661
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_EXTERNAL_INVITE_WARN"

    const/16 v15, 0x95

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_EXTERNAL_INVITE_WARN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 666
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_FB_INVITE"

    const/16 v15, 0x96

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_FB_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 671
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_FB_INVITE_CHANGE_ROLE"

    const/16 v15, 0x97

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_FB_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 676
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_FB_UNINVITE"

    const/16 v15, 0x98

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_FB_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 681
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_INVITE_GROUP"

    const/16 v15, 0x99

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_INVITE_GROUP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 686
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_TEAM_GRANT_ACCESS"

    const/16 v15, 0x9a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_GRANT_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 691
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_TEAM_INVITE"

    const/16 v15, 0x9b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 696
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_TEAM_INVITE_CHANGE_ROLE"

    const/16 v15, 0x9c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 701
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_TEAM_JOIN"

    const/16 v15, 0x9d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_JOIN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 706
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_TEAM_JOIN_FROM_OOB_LINK"

    const/16 v15, 0x9e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_JOIN_FROM_OOB_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 711
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SF_TEAM_UNINVITE"

    const/16 v15, 0x9f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 716
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_ADD_INVITEES"

    const/16 v15, 0xa0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_ADD_INVITEES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 720
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_ADD_LINK_EXPIRY"

    const/16 v15, 0xa1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_ADD_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 724
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_ADD_LINK_PASSWORD"

    const/16 v15, 0xa2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_ADD_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 728
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_ADD_MEMBER"

    const/16 v15, 0xa3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 732
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY"

    const/16 v15, 0xa4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 737
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_INVITEE_ROLE"

    const/16 v15, 0xa5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_INVITEE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 741
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_LINK_AUDIENCE"

    const/16 v15, 0xa6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_LINK_AUDIENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 745
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_LINK_EXPIRY"

    const/16 v15, 0xa7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 749
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_LINK_PASSWORD"

    const/16 v15, 0xa8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 753
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_MEMBER_ROLE"

    const/16 v15, 0xa9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 758
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY"

    const/16 v15, 0xaa

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 763
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_CLAIM_INVITATION"

    const/16 v15, 0xab

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CLAIM_INVITATION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 767
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_COPY"

    const/16 v15, 0xac

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 771
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_DOWNLOAD"

    const/16 v15, 0xad

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 775
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_RELINQUISH_MEMBERSHIP"

    const/16 v15, 0xae

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_RELINQUISH_MEMBERSHIP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 780
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_REMOVE_INVITEES"

    const/16 v15, 0xaf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REMOVE_INVITEES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 784
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_REMOVE_LINK_EXPIRY"

    const/16 v15, 0xb0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REMOVE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 788
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_REMOVE_LINK_PASSWORD"

    const/16 v15, 0xb1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REMOVE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 792
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_REMOVE_MEMBER"

    const/16 v15, 0xb2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 796
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_REQUEST_ACCESS"

    const/16 v15, 0xb3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 801
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_UNSHARE"

    const/16 v15, 0xb4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_UNSHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 805
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_CONTENT_VIEW"

    const/16 v15, 0xb5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 809
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_CHANGE_LINK_POLICY"

    const/16 v15, 0xb6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 814
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY"

    const/16 v15, 0xb7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 818
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY"

    const/16 v15, 0xb8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 822
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_CHANGE_MEMBERS_POLICY"

    const/16 v15, 0xb9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 826
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_CREATE"

    const/16 v15, 0xba

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 830
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_DECLINE_INVITATION"

    const/16 v15, 0xbb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_DECLINE_INVITATION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 834
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_MOUNT"

    const/16 v15, 0xbc

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_MOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 838
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_NEST"

    const/16 v15, 0xbd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_NEST:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 842
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_TRANSFER_OWNERSHIP"

    const/16 v15, 0xbe

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_TRANSFER_OWNERSHIP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 846
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_FOLDER_UNMOUNT"

    const/16 v15, 0xbf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_UNMOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 850
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_ADD_EXPIRY"

    const/16 v15, 0xc0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_ADD_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 854
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_CHANGE_EXPIRY"

    const/16 v15, 0xc1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_CHANGE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 858
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_CHANGE_VISIBILITY"

    const/16 v15, 0xc2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_CHANGE_VISIBILITY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 862
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_COPY"

    const/16 v15, 0xc3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 866
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_CREATE"

    const/16 v15, 0xc4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 870
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_DISABLE"

    const/16 v15, 0xc5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_DISABLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 874
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_DOWNLOAD"

    const/16 v15, 0xc6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 878
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_REMOVE_EXPIRY"

    const/16 v15, 0xc7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_REMOVE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 882
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_SHARE"

    const/16 v15, 0xc8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 886
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_LINK_VIEW"

    const/16 v15, 0xc9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 890
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARED_NOTE_OPENED"

    const/16 v15, 0xca

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_NOTE_OPENED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 894
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHMODEL_GROUP_SHARE"

    const/16 v15, 0xcb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHMODEL_GROUP_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 898
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_ACCESS_GRANTED"

    const/16 v15, 0xcc

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_ACCESS_GRANTED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 902
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_ADD_MEMBER"

    const/16 v15, 0xcd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 906
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_ARCHIVED"

    const/16 v15, 0xce

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_ARCHIVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 910
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_CREATED"

    const/16 v15, 0xcf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_CREATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 914
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_DELETE_COMMENT"

    const/16 v15, 0xd0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 918
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_EDITED"

    const/16 v15, 0xd1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_EDITED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 922
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_EDIT_COMMENT"

    const/16 v15, 0xd2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 926
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_FILE_ADDED"

    const/16 v15, 0xd3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_FILE_ADDED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 930
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_FILE_DOWNLOAD"

    const/16 v15, 0xd4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 934
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_FILE_REMOVED"

    const/16 v15, 0xd5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_FILE_REMOVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 938
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_FILE_VIEW"

    const/16 v15, 0xd6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_FILE_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 942
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_PERMANENTLY_DELETED"

    const/16 v15, 0xd7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_PERMANENTLY_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 946
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_POST_COMMENT"

    const/16 v15, 0xd8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_POST_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 950
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_REMOVE_MEMBER"

    const/16 v15, 0xd9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 954
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_RENAMED"

    const/16 v15, 0xda

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_RENAMED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 958
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_REQUEST_ACCESS"

    const/16 v15, 0xdb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 962
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_RESOLVE_COMMENT"

    const/16 v15, 0xdc

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 966
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_RESTORED"

    const/16 v15, 0xdd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_RESTORED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 970
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_TRASHED"

    const/16 v15, 0xde

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_TRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 975
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_TRASHED_DEPRECATED"

    const/16 v15, 0xdf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_TRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 979
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_UNRESOLVE_COMMENT"

    const/16 v15, 0xe0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 983
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_UNTRASHED"

    const/16 v15, 0xe1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 988
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_UNTRASHED_DEPRECATED"

    const/16 v15, 0xe2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_UNTRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 992
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_VIEW"

    const/16 v15, 0xe3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 996
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_ADD_CERT"

    const/16 v15, 0xe4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_ADD_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1000
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_ADD_LOGIN_URL"

    const/16 v15, 0xe5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_ADD_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1004
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_ADD_LOGOUT_URL"

    const/16 v15, 0xe6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_ADD_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1008
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_CHANGE_CERT"

    const/16 v15, 0xe7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1012
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_CHANGE_LOGIN_URL"

    const/16 v15, 0xe8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1016
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_CHANGE_LOGOUT_URL"

    const/16 v15, 0xe9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1020
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_CHANGE_SAML_IDENTITY_MODE"

    const/16 v15, 0xea

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_SAML_IDENTITY_MODE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1024
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_REMOVE_CERT"

    const/16 v15, 0xeb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_REMOVE_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1028
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_REMOVE_LOGIN_URL"

    const/16 v15, 0xec

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_REMOVE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1032
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_REMOVE_LOGOUT_URL"

    const/16 v15, 0xed

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_REMOVE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1036
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_FOLDER_CHANGE_STATUS"

    const/16 v15, 0xee

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1040
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_FOLDER_CREATE"

    const/16 v15, 0xef

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1044
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_FOLDER_DOWNGRADE"

    const/16 v15, 0xf0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_DOWNGRADE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1048
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_FOLDER_PERMANENTLY_DELETE"

    const/16 v15, 0xf1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1052
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_FOLDER_RENAME"

    const/16 v15, 0xf2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1056
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED"

    const/16 v15, 0xf3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1060
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "ACCOUNT_CAPTURE_CHANGE_POLICY"

    const/16 v15, 0xf4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1064
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "ALLOW_DOWNLOAD_DISABLED"

    const/16 v15, 0xf5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ALLOW_DOWNLOAD_DISABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1068
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "ALLOW_DOWNLOAD_ENABLED"

    const/16 v15, 0xf6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ALLOW_DOWNLOAD_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1072
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "CAMERA_UPLOADS_POLICY_CHANGED"

    const/16 v15, 0xf7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->CAMERA_UPLOADS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1077
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY"

    const/16 v15, 0xf8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1082
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY"

    const/16 v15, 0xf9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1087
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY"

    const/16 v15, 0xfa

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1092
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_APPROVALS_CHANGE_MOBILE_POLICY"

    const/16 v15, 0xfb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_APPROVALS_CHANGE_MOBILE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1097
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION"

    const/16 v15, 0xfc

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1102
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DEVICE_APPROVALS_CHANGE_UNLINK_ACTION"

    const/16 v15, 0xfd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_APPROVALS_CHANGE_UNLINK_ACTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1106
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DIRECTORY_RESTRICTIONS_ADD_MEMBERS"

    const/16 v15, 0xfe

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DIRECTORY_RESTRICTIONS_ADD_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1110
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS"

    const/16 v15, 0xff

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1114
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EMM_ADD_EXCEPTION"

    const/16 v15, 0x100

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1119
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EMM_CHANGE_POLICY"

    const/16 v15, 0x101

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1123
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EMM_REMOVE_EXCEPTION"

    const/16 v15, 0x102

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1127
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "EXTENDED_VERSION_HISTORY_CHANGE_POLICY"

    const/16 v15, 0x103

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EXTENDED_VERSION_HISTORY_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1131
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_COMMENTS_CHANGE_POLICY"

    const/16 v15, 0x104

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_COMMENTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1135
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_REQUESTS_CHANGE_POLICY"

    const/16 v15, 0x105

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1140
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_REQUESTS_EMAILS_ENABLED"

    const/16 v15, 0x106

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUESTS_EMAILS_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1145
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY"

    const/16 v15, 0x107

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1149
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GOOGLE_SSO_CHANGE_POLICY"

    const/16 v15, 0x108

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GOOGLE_SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1153
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "GROUP_USER_MANAGEMENT_CHANGE_POLICY"

    const/16 v15, 0x109

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_USER_MANAGEMENT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1157
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_REQUESTS_CHANGE_POLICY"

    const/16 v15, 0x10a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1161
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_ADD_EXCEPTION"

    const/16 v15, 0x10b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1165
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY"

    const/16 v15, 0x10c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1169
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_CHANGE_POLICY"

    const/16 v15, 0x10d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1174
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION"

    const/16 v15, 0x10e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1179
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MEMBER_SUGGESTIONS_CHANGE_POLICY"

    const/16 v15, 0x10f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SUGGESTIONS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1183
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY"

    const/16 v15, 0x110

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1187
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "NETWORK_CONTROL_CHANGE_POLICY"

    const/16 v15, 0x111

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NETWORK_CONTROL_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1192
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CHANGE_DEPLOYMENT_POLICY"

    const/16 v15, 0x112

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CHANGE_DEPLOYMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1197
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CHANGE_MEMBER_LINK_POLICY"

    const/16 v15, 0x113

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CHANGE_MEMBER_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1203
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CHANGE_MEMBER_POLICY"

    const/16 v15, 0x114

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1207
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_CHANGE_POLICY"

    const/16 v15, 0x115

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1211
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_ENABLED_USERS_GROUP_ADDITION"

    const/16 v15, 0x116

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_ENABLED_USERS_GROUP_ADDITION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1215
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PAPER_ENABLED_USERS_GROUP_REMOVAL"

    const/16 v15, 0x117

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_ENABLED_USERS_GROUP_REMOVAL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1220
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "PERMANENT_DELETE_CHANGE_POLICY"

    const/16 v15, 0x118

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PERMANENT_DELETE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1225
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARING_CHANGE_FOLDER_JOIN_POLICY"

    const/16 v15, 0x119

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARING_CHANGE_FOLDER_JOIN_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1230
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARING_CHANGE_LINK_POLICY"

    const/16 v15, 0x11a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARING_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1235
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHARING_CHANGE_MEMBER_POLICY"

    const/16 v15, 0x11b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARING_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1240
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_CHANGE_DOWNLOAD_POLICY"

    const/16 v15, 0x11c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_CHANGE_DOWNLOAD_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1244
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_CHANGE_ENABLED_POLICY"

    const/16 v15, 0x11d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_CHANGE_ENABLED_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1249
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY"

    const/16 v15, 0x11e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1253
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SMART_SYNC_CHANGE_POLICY"

    const/16 v15, 0x11f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SMART_SYNC_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1257
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SMART_SYNC_NOT_OPT_OUT"

    const/16 v15, 0x120

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SMART_SYNC_NOT_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1261
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SMART_SYNC_OPT_OUT"

    const/16 v15, 0x121

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SMART_SYNC_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1265
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "SSO_CHANGE_POLICY"

    const/16 v15, 0x122

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1269
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_SELECTIVE_SYNC_POLICY_CHANGED"

    const/16 v15, 0x123

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_SELECTIVE_SYNC_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1273
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TFA_CHANGE_POLICY"

    const/16 v15, 0x124

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1278
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TWO_ACCOUNT_CHANGE_POLICY"

    const/16 v15, 0x125

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TWO_ACCOUNT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1282
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "VIEWER_INFO_POLICY_CHANGED"

    const/16 v15, 0x126

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->VIEWER_INFO_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1287
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY"

    const/16 v15, 0x127

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1292
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY"

    const/16 v15, 0x128

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1296
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_MERGE_FROM"

    const/16 v15, 0x129

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_MERGE_FROM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1300
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_MERGE_TO"

    const/16 v15, 0x12a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_MERGE_TO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1304
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_PROFILE_ADD_LOGO"

    const/16 v15, 0x12b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_ADD_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1308
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE"

    const/16 v15, 0x12c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1312
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_PROFILE_CHANGE_LOGO"

    const/16 v15, 0x12d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_CHANGE_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1316
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_PROFILE_CHANGE_NAME"

    const/16 v15, 0x12e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1320
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TEAM_PROFILE_REMOVE_LOGO"

    const/16 v15, 0x12f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_REMOVE_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1324
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TFA_ADD_BACKUP_PHONE"

    const/16 v15, 0x130

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_ADD_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1328
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TFA_ADD_SECURITY_KEY"

    const/16 v15, 0x131

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_ADD_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1332
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TFA_CHANGE_BACKUP_PHONE"

    const/16 v15, 0x132

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_CHANGE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1336
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TFA_CHANGE_STATUS"

    const/16 v15, 0x133

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1340
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TFA_REMOVE_BACKUP_PHONE"

    const/16 v15, 0x134

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_REMOVE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1344
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TFA_REMOVE_SECURITY_KEY"

    const/16 v15, 0x135

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_REMOVE_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1348
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "TFA_RESET"

    const/16 v15, 0x136

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_RESET:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    .line 1357
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const-string v1, "OTHER"

    const/16 v15, 0x137

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventType$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->OTHER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v0, 0x138

    .line 37
    new-array v0, v0, [Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->APP_LINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->APP_LINK_USER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->APP_UNLINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->APP_UNLINK_USER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_CHANGE_COMMENT_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v8

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v9

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_LIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v10

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v11

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_UNLIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v12

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v13

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_CHANGE_IP_DESKTOP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    aput-object v1, v0, v14

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_CHANGE_IP_MOBILE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_CHANGE_IP_WEB:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_DELETE_ON_UNLINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_DELETE_ON_UNLINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_LINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_LINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_MANAGEMENT_DISABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_MANAGEMENT_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_UNLINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_REFRESH_AUTH_TOKEN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_CHANGE_AVAILABILITY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_MIGRATE_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DISABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_EMAIL_EXISTING_USERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DOMAIN_VERIFICATION_REMOVE_DOMAIN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ENABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->CREATE_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_ADD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_EDIT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_GET_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_MOVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_PREVIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_RESTORE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REVERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_ROLLBACK_CHANGES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_SAVE_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUEST_CHANGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUEST_CLOSE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUEST_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUEST_RECEIVE_FILE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_ADD_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_CHANGE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_CHANGE_MANAGEMENT_TYPE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_DESCRIPTION_UPDATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_JOIN_POLICY_UPDATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_MOVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_REMOVE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_ERROR:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->LOGIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->LOGIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->LOGOUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->RESELLER_SUPPORT_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->RESELLER_SUPPORT_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SIGN_IN_AS_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SIGN_IN_AS_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_ERROR:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_ADD_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_ADMIN_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_EMAIL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_MEMBERSHIP_TYPE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_DELETE_MANUAL_CONTACTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SUGGEST:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_TRANSFER_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SECONDARY_MAILS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_ADD_TO_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_ARCHIVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_REMOVE_FROM_FOLDER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CONTENT_RESTORE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_CHANGE_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_EDIT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_MENTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_OWNERSHIP_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_REVERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_SLACK_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_TRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_DOC_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_EXTERNAL_VIEW_ALLOW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_EXTERNAL_VIEW_DEFAULT_TEAM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_EXTERNAL_VIEW_FORBID:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_FOLDER_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_FOLDER_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_FOLDER_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_FOLDER_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PASSWORD_CHANGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PASSWORD_RESET:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PASSWORD_RESET_ALL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_CREATE_EXCEPTIONS_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_CREATE_USAGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EXPORT_MEMBERS_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_ADMIN_EXPORT_START:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_ACTIVITY_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->COLLECTION_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_ACL_INVITE_ONLY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_ACL_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_ACL_TEAM_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NOTE_SHARE_RECEIVE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->OPEN_NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_ADD_GROUP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_EXTERNAL_INVITE_WARN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_FB_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_FB_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_FB_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_INVITE_GROUP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_GRANT_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_JOIN:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_JOIN_FROM_OOB_LINK:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SF_TEAM_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_ADD_INVITEES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_ADD_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_ADD_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_INVITEE_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_LINK_AUDIENCE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_CLAIM_INVITATION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_RELINQUISH_MEMBERSHIP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REMOVE_INVITEES:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REMOVE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REMOVE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_UNSHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_CONTENT_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_DECLINE_INVITATION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_MOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_NEST:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_TRANSFER_OWNERSHIP:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_FOLDER_UNMOUNT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_ADD_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_CHANGE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_CHANGE_VISIBILITY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_COPY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_DISABLE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_REMOVE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_LINK_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARED_NOTE_OPENED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHMODEL_GROUP_SHARE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_ACCESS_GRANTED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_ARCHIVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_CREATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_EDITED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_FILE_ADDED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_FILE_REMOVED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_FILE_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_PERMANENTLY_DELETED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_POST_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_RENAMED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_RESTORED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_TRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_TRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_UNTRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_VIEW:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_ADD_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_ADD_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_ADD_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_SAML_IDENTITY_MODE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_REMOVE_CERT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_REMOVE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_REMOVE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_DOWNGRADE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_FOLDER_RENAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ACCOUNT_CAPTURE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ALLOW_DOWNLOAD_DISABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->ALLOW_DOWNLOAD_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->CAMERA_UPLOADS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_APPROVALS_CHANGE_MOBILE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DEVICE_APPROVALS_CHANGE_UNLINK_ACTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DIRECTORY_RESTRICTIONS_ADD_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0xff

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x100

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x101

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EMM_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x102

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->EXTENDED_VERSION_HISTORY_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x103

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_COMMENTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x104

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x105

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUESTS_EMAILS_ENABLED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x106

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x107

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GOOGLE_SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x108

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->GROUP_USER_MANAGEMENT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x109

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x10a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x10b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x10c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x10d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x10e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MEMBER_SUGGESTIONS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x10f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x110

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->NETWORK_CONTROL_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x111

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CHANGE_DEPLOYMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x112

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CHANGE_MEMBER_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x113

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x114

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x115

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_ENABLED_USERS_GROUP_ADDITION:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x116

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PAPER_ENABLED_USERS_GROUP_REMOVAL:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x117

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->PERMANENT_DELETE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x118

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARING_CHANGE_FOLDER_JOIN_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x119

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARING_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x11a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHARING_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x11b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_CHANGE_DOWNLOAD_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x11c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_CHANGE_ENABLED_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x11d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x11e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SMART_SYNC_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x11f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SMART_SYNC_NOT_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x120

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SMART_SYNC_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x121

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x122

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_SELECTIVE_SYNC_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x123

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x124

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TWO_ACCOUNT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x125

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->VIEWER_INFO_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x126

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x127

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x128

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_MERGE_FROM:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x129

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_MERGE_TO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x12a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_ADD_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x12b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x12c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_CHANGE_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x12d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x12e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TEAM_PROFILE_REMOVE_LOGO:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x12f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_ADD_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x130

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_ADD_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x131

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_CHANGE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x132

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x133

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_REMOVE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x134

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_REMOVE_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x135

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->TFA_RESET:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x136

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->OTHER:Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    const/16 v2, 0x137

    aput-object v1, v0, v2

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->$VALUES:[Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/core/v2/teamlog/EventType$Tag;
    .locals 1

    .line 37
    const-class v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    return-object p0
.end method

.method public static values()[Lcom/dropbox/core/v2/teamlog/EventType$Tag;
    .locals 1

    .line 37
    sget-object v0, Lcom/dropbox/core/v2/teamlog/EventType$Tag;->$VALUES:[Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    invoke-virtual {v0}, [Lcom/dropbox/core/v2/teamlog/EventType$Tag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/core/v2/teamlog/EventType$Tag;

    return-object v0
.end method

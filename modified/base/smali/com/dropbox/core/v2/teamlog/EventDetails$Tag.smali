.class public final enum Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;
.super Ljava/lang/Enum;
.source "EventDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/teamlog/EventDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Tag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_CHANGE_AVAILABILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_MIGRATE_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ALLOW_DOWNLOAD_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ALLOW_DOWNLOAD_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum APP_LINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum APP_LINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum APP_UNLINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum APP_UNLINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum CAMERA_UPLOADS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum COLLECTION_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum CREATE_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_MOBILE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_APPROVALS_CHANGE_UNLINK_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_CHANGE_IP_DESKTOP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_CHANGE_IP_MOBILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_CHANGE_IP_WEB_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_DELETE_ON_UNLINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_DELETE_ON_UNLINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_LINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_LINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_MANAGEMENT_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_MANAGEMENT_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DEVICE_UNLINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DIRECTORY_RESTRICTIONS_ADD_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DISABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_EMAIL_EXISTING_USERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum DOMAIN_VERIFICATION_REMOVE_DOMAIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_CREATE_EXCEPTIONS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_CREATE_USAGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_REFRESH_AUTH_TOKEN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EMM_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum ENABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EXPORT_MEMBERS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum EXTENDED_VERSION_HISTORY_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_ADD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_CHANGE_COMMENT_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_COMMENTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_GET_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_LIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_MOVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_PREVIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUESTS_EMAILS_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_CLOSE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REQUEST_RECEIVE_FILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_ROLLBACK_CHANGES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_SAVE_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_UNLIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum FILE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GOOGLE_SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_ADD_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_CHANGE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_CHANGE_MANAGEMENT_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_DESCRIPTION_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_JOIN_POLICY_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_MOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_REMOVE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum GROUP_USER_MANAGEMENT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LOGIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LOGIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum LOGOUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_ADD_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_ADMIN_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_EMAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_MEMBERSHIP_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_DELETE_MANUAL_CONTACTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SUGGESTIONS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_SUGGEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MEMBER_TRANSFER_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum MISSING_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NETWORK_CONTROL_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_ACL_INVITE_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_ACL_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_ACL_TEAM_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum NOTE_SHARE_RECEIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum OPEN_NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum OTHER:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_ADMIN_EXPORT_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CHANGE_DEPLOYMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CHANGE_MEMBER_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_ADD_TO_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_ARCHIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_REMOVE_FROM_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_CONTENT_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_CHANGE_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_MENTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_OWNERSHIP_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_SLACK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_DOC_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_ENABLED_USERS_GROUP_ADDITION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_ENABLED_USERS_GROUP_REMOVAL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_ALLOW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_DEFAULT_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_EXTERNAL_VIEW_FORBID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_FOLDER_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_FOLDER_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_FOLDER_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PAPER_FOLDER_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PASSWORD_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PASSWORD_RESET_ALL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PASSWORD_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum PERMANENT_DELETE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum RESELLER_SUPPORT_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum RESELLER_SUPPORT_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SECONDARY_MAILS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_ADD_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_EXTERNAL_INVITE_WARN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_FB_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_FB_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_FB_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_INVITE_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_GRANT_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_JOIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_JOIN_FROM_OOB_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SF_TEAM_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_ADD_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_ADD_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_ADD_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_INVITEE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_AUDIENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_CLAIM_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_RELINQUISH_MEMBERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_UNSHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_CONTENT_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_DECLINE_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_MOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_NEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_TRANSFER_OWNERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_FOLDER_UNMOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_ADD_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_CHANGE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_CHANGE_VISIBILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_DISABLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_REMOVE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_LINK_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARED_NOTE_OPENED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARING_CHANGE_FOLDER_JOIN_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARING_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHARING_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHMODEL_GROUP_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_ACCESS_GRANTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_ARCHIVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_CHANGE_DOWNLOAD_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_CHANGE_ENABLED_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_CREATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_EDITED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_FILE_ADDED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_FILE_REMOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_FILE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_PERMANENTLY_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_POST_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_RENAMED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_RESTORED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_TRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_UNTRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SHOWCASE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SIGN_IN_AS_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SIGN_IN_AS_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMART_SYNC_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMART_SYNC_NOT_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SMART_SYNC_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_ADD_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_ADD_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_ADD_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_CHANGE_SAML_IDENTITY_MODE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_REMOVE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_REMOVE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum SSO_REMOVE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_ACTIVITY_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_DOWNGRADE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_FOLDER_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_FROM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_MERGE_TO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_ADD_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_PROFILE_REMOVE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_SELECTIVE_SYNC_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_ADD_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_ADD_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_CHANGE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_REMOVE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_REMOVE_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TFA_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum TWO_ACCOUNT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum VIEWER_INFO_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

.field public static final enum WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 38
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "APP_LINK_TEAM_DETAILS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_LINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 39
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "APP_LINK_USER_DETAILS"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_LINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 40
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "APP_UNLINK_TEAM_DETAILS"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_UNLINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 41
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "APP_UNLINK_USER_DETAILS"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_UNLINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 42
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_ADD_COMMENT_DETAILS"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 43
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_CHANGE_COMMENT_SUBSCRIPTION_DETAILS"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_CHANGE_COMMENT_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 44
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_DELETE_COMMENT_DETAILS"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 45
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_EDIT_COMMENT_DETAILS"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 46
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_LIKE_COMMENT_DETAILS"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_LIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 47
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_RESOLVE_COMMENT_DETAILS"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 48
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_UNLIKE_COMMENT_DETAILS"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_UNLIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 49
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_UNRESOLVE_COMMENT_DETAILS"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 50
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_CHANGE_IP_DESKTOP_DETAILS"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_DESKTOP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 51
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_CHANGE_IP_MOBILE_DETAILS"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_MOBILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 52
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_CHANGE_IP_WEB_DETAILS"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_WEB_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 53
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_DELETE_ON_UNLINK_FAIL_DETAILS"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_DELETE_ON_UNLINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 54
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_DELETE_ON_UNLINK_SUCCESS_DETAILS"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_DELETE_ON_UNLINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 55
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_LINK_FAIL_DETAILS"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_LINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 56
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_LINK_SUCCESS_DETAILS"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_LINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 57
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_MANAGEMENT_DISABLED_DETAILS"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_MANAGEMENT_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 58
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_MANAGEMENT_ENABLED_DETAILS"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_MANAGEMENT_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 59
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_UNLINK_DETAILS"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_UNLINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 60
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EMM_REFRESH_AUTH_TOKEN_DETAILS"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_REFRESH_AUTH_TOKEN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 61
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "ACCOUNT_CAPTURE_CHANGE_AVAILABILITY_DETAILS"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_CHANGE_AVAILABILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 62
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "ACCOUNT_CAPTURE_MIGRATE_ACCOUNT_DETAILS"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_MIGRATE_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 63
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT_DETAILS"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 64
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT_DETAILS"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 65
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DISABLED_DOMAIN_INVITES_DETAILS"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DISABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 66
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM_DETAILS"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 67
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM_DETAILS"

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 68
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_INVITES_EMAIL_EXISTING_USERS_DETAILS"

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_EMAIL_EXISTING_USERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 69
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM_DETAILS"

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 70
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO_DETAILS"

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 71
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES_DETAILS"

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 72
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL_DETAILS"

    const/16 v15, 0x22

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 73
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS_DETAILS"

    const/16 v15, 0x23

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 74
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DOMAIN_VERIFICATION_REMOVE_DOMAIN_DETAILS"

    const/16 v15, 0x24

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_REMOVE_DOMAIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 75
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "ENABLED_DOMAIN_INVITES_DETAILS"

    const/16 v15, 0x25

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ENABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 76
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "CREATE_FOLDER_DETAILS"

    const/16 v15, 0x26

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CREATE_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 77
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_ADD_DETAILS"

    const/16 v15, 0x27

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ADD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 78
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_COPY_DETAILS"

    const/16 v15, 0x28

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 79
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_DELETE_DETAILS"

    const/16 v15, 0x29

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 80
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_DOWNLOAD_DETAILS"

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 81
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_EDIT_DETAILS"

    const/16 v15, 0x2b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 82
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_GET_COPY_REFERENCE_DETAILS"

    const/16 v15, 0x2c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_GET_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 83
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_MOVE_DETAILS"

    const/16 v15, 0x2d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_MOVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 84
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_PERMANENTLY_DELETE_DETAILS"

    const/16 v15, 0x2e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 85
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_PREVIEW_DETAILS"

    const/16 v15, 0x2f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_PREVIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 86
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_RENAME_DETAILS"

    const/16 v15, 0x30

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 87
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_RESTORE_DETAILS"

    const/16 v15, 0x31

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 88
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_REVERT_DETAILS"

    const/16 v15, 0x32

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 89
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_ROLLBACK_CHANGES_DETAILS"

    const/16 v15, 0x33

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ROLLBACK_CHANGES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 90
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_SAVE_COPY_REFERENCE_DETAILS"

    const/16 v15, 0x34

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_SAVE_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 91
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_REQUEST_CHANGE_DETAILS"

    const/16 v15, 0x35

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 92
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_REQUEST_CLOSE_DETAILS"

    const/16 v15, 0x36

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CLOSE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 93
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_REQUEST_CREATE_DETAILS"

    const/16 v15, 0x37

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 94
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_REQUEST_RECEIVE_FILE_DETAILS"

    const/16 v15, 0x38

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_RECEIVE_FILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 95
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_ADD_EXTERNAL_ID_DETAILS"

    const/16 v15, 0x39

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_ADD_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 96
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_ADD_MEMBER_DETAILS"

    const/16 v15, 0x3a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 97
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_CHANGE_EXTERNAL_ID_DETAILS"

    const/16 v15, 0x3b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 98
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_CHANGE_MANAGEMENT_TYPE_DETAILS"

    const/16 v15, 0x3c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_MANAGEMENT_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 99
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_CHANGE_MEMBER_ROLE_DETAILS"

    const/16 v15, 0x3d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 100
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_CREATE_DETAILS"

    const/16 v15, 0x3e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 101
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_DELETE_DETAILS"

    const/16 v15, 0x3f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 102
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_DESCRIPTION_UPDATED_DETAILS"

    const/16 v15, 0x40

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_DESCRIPTION_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 103
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_JOIN_POLICY_UPDATED_DETAILS"

    const/16 v15, 0x41

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_JOIN_POLICY_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 104
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_MOVED_DETAILS"

    const/16 v15, 0x42

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_MOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 105
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_REMOVE_EXTERNAL_ID_DETAILS"

    const/16 v15, 0x43

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_REMOVE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 106
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_REMOVE_MEMBER_DETAILS"

    const/16 v15, 0x44

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 107
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_RENAME_DETAILS"

    const/16 v15, 0x45

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 108
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EMM_ERROR_DETAILS"

    const/16 v15, 0x46

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 109
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "LOGIN_FAIL_DETAILS"

    const/16 v15, 0x47

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 110
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "LOGIN_SUCCESS_DETAILS"

    const/16 v15, 0x48

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 111
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "LOGOUT_DETAILS"

    const/16 v15, 0x49

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGOUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 112
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "RESELLER_SUPPORT_SESSION_END_DETAILS"

    const/16 v15, 0x4a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->RESELLER_SUPPORT_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 113
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "RESELLER_SUPPORT_SESSION_START_DETAILS"

    const/16 v15, 0x4b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->RESELLER_SUPPORT_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 114
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SIGN_IN_AS_SESSION_END_DETAILS"

    const/16 v15, 0x4c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SIGN_IN_AS_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 115
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SIGN_IN_AS_SESSION_START_DETAILS"

    const/16 v15, 0x4d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SIGN_IN_AS_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 116
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_ERROR_DETAILS"

    const/16 v15, 0x4e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 117
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_ADD_NAME_DETAILS"

    const/16 v15, 0x4f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_ADD_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 118
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_CHANGE_ADMIN_ROLE_DETAILS"

    const/16 v15, 0x50

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_ADMIN_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 119
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_CHANGE_EMAIL_DETAILS"

    const/16 v15, 0x51

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_EMAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 120
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_CHANGE_MEMBERSHIP_TYPE_DETAILS"

    const/16 v15, 0x52

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_MEMBERSHIP_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 121
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_CHANGE_NAME_DETAILS"

    const/16 v15, 0x53

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 122
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_CHANGE_STATUS_DETAILS"

    const/16 v15, 0x54

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 123
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_DELETE_MANUAL_CONTACTS_DETAILS"

    const/16 v15, 0x55

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_DELETE_MANUAL_CONTACTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 124
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS_DETAILS"

    const/16 v15, 0x56

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 125
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA_DETAILS"

    const/16 v15, 0x57

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 126
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA_DETAILS"

    const/16 v15, 0x58

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 127
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_CHANGE_STATUS_DETAILS"

    const/16 v15, 0x59

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 128
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA_DETAILS"

    const/16 v15, 0x5a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 129
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SUGGEST_DETAILS"

    const/16 v15, 0x5b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SUGGEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 130
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_TRANSFER_ACCOUNT_CONTENTS_DETAILS"

    const/16 v15, 0x5c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_TRANSFER_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 131
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SECONDARY_MAILS_POLICY_CHANGED_DETAILS"

    const/16 v15, 0x5d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SECONDARY_MAILS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 132
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_ADD_MEMBER_DETAILS"

    const/16 v15, 0x5e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 133
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_ADD_TO_FOLDER_DETAILS"

    const/16 v15, 0x5f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ADD_TO_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 134
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_ARCHIVE_DETAILS"

    const/16 v15, 0x60

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ARCHIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 135
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_CREATE_DETAILS"

    const/16 v15, 0x61

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 136
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_PERMANENTLY_DELETE_DETAILS"

    const/16 v15, 0x62

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 137
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_REMOVE_FROM_FOLDER_DETAILS"

    const/16 v15, 0x63

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_REMOVE_FROM_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 138
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_REMOVE_MEMBER_DETAILS"

    const/16 v15, 0x64

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 139
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_RENAME_DETAILS"

    const/16 v15, 0x65

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 140
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CONTENT_RESTORE_DETAILS"

    const/16 v15, 0x66

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 141
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_ADD_COMMENT_DETAILS"

    const/16 v15, 0x67

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 142
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_CHANGE_MEMBER_ROLE_DETAILS"

    const/16 v15, 0x68

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 143
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_CHANGE_SHARING_POLICY_DETAILS"

    const/16 v15, 0x69

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 144
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_CHANGE_SUBSCRIPTION_DETAILS"

    const/16 v15, 0x6a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 145
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_DELETED_DETAILS"

    const/16 v15, 0x6b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 146
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_DELETE_COMMENT_DETAILS"

    const/16 v15, 0x6c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 147
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_DOWNLOAD_DETAILS"

    const/16 v15, 0x6d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 148
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_EDIT_DETAILS"

    const/16 v15, 0x6e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 149
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_EDIT_COMMENT_DETAILS"

    const/16 v15, 0x6f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 150
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_FOLLOWED_DETAILS"

    const/16 v15, 0x70

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 151
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_MENTION_DETAILS"

    const/16 v15, 0x71

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_MENTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 152
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_OWNERSHIP_CHANGED_DETAILS"

    const/16 v15, 0x72

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_OWNERSHIP_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 153
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_REQUEST_ACCESS_DETAILS"

    const/16 v15, 0x73

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 154
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_RESOLVE_COMMENT_DETAILS"

    const/16 v15, 0x74

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 155
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_REVERT_DETAILS"

    const/16 v15, 0x75

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 156
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_SLACK_SHARE_DETAILS"

    const/16 v15, 0x76

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_SLACK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 157
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_TEAM_INVITE_DETAILS"

    const/16 v15, 0x77

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 158
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_TRASHED_DETAILS"

    const/16 v15, 0x78

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 159
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_UNRESOLVE_COMMENT_DETAILS"

    const/16 v15, 0x79

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 160
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_UNTRASHED_DETAILS"

    const/16 v15, 0x7a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 161
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_DOC_VIEW_DETAILS"

    const/16 v15, 0x7b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 162
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_EXTERNAL_VIEW_ALLOW_DETAILS"

    const/16 v15, 0x7c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_ALLOW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 163
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_EXTERNAL_VIEW_DEFAULT_TEAM_DETAILS"

    const/16 v15, 0x7d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_DEFAULT_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 164
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_EXTERNAL_VIEW_FORBID_DETAILS"

    const/16 v15, 0x7e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_FORBID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 165
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_FOLDER_CHANGE_SUBSCRIPTION_DETAILS"

    const/16 v15, 0x7f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 166
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_FOLDER_DELETED_DETAILS"

    const/16 v15, 0x80

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 167
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_FOLDER_FOLLOWED_DETAILS"

    const/16 v15, 0x81

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 168
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_FOLDER_TEAM_INVITE_DETAILS"

    const/16 v15, 0x82

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 169
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PASSWORD_CHANGE_DETAILS"

    const/16 v15, 0x83

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 170
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PASSWORD_RESET_DETAILS"

    const/16 v15, 0x84

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 171
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PASSWORD_RESET_ALL_DETAILS"

    const/16 v15, 0x85

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_RESET_ALL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 172
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EMM_CREATE_EXCEPTIONS_REPORT_DETAILS"

    const/16 v15, 0x86

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CREATE_EXCEPTIONS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 173
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EMM_CREATE_USAGE_REPORT_DETAILS"

    const/16 v15, 0x87

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CREATE_USAGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 174
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EXPORT_MEMBERS_REPORT_DETAILS"

    const/16 v15, 0x88

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EXPORT_MEMBERS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 175
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_ADMIN_EXPORT_START_DETAILS"

    const/16 v15, 0x89

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ADMIN_EXPORT_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 176
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT_DETAILS"

    const/16 v15, 0x8a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 177
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_ACTIVITY_CREATE_REPORT_DETAILS"

    const/16 v15, 0x8b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_ACTIVITY_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 178
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "COLLECTION_SHARE_DETAILS"

    const/16 v15, 0x8c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->COLLECTION_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 179
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "NOTE_ACL_INVITE_ONLY_DETAILS"

    const/16 v15, 0x8d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_INVITE_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 180
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "NOTE_ACL_LINK_DETAILS"

    const/16 v15, 0x8e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 181
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "NOTE_ACL_TEAM_LINK_DETAILS"

    const/16 v15, 0x8f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_TEAM_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 182
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "NOTE_SHARED_DETAILS"

    const/16 v15, 0x90

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 183
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "NOTE_SHARE_RECEIVE_DETAILS"

    const/16 v15, 0x91

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_SHARE_RECEIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 184
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "OPEN_NOTE_SHARED_DETAILS"

    const/16 v15, 0x92

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->OPEN_NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 185
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_ADD_GROUP_DETAILS"

    const/16 v15, 0x93

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_ADD_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 186
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS_DETAILS"

    const/16 v15, 0x94

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 187
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_EXTERNAL_INVITE_WARN_DETAILS"

    const/16 v15, 0x95

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_EXTERNAL_INVITE_WARN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 188
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_FB_INVITE_DETAILS"

    const/16 v15, 0x96

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 189
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_FB_INVITE_CHANGE_ROLE_DETAILS"

    const/16 v15, 0x97

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 190
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_FB_UNINVITE_DETAILS"

    const/16 v15, 0x98

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 191
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_INVITE_GROUP_DETAILS"

    const/16 v15, 0x99

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_INVITE_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 192
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_TEAM_GRANT_ACCESS_DETAILS"

    const/16 v15, 0x9a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_GRANT_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 193
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_TEAM_INVITE_DETAILS"

    const/16 v15, 0x9b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 194
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_TEAM_INVITE_CHANGE_ROLE_DETAILS"

    const/16 v15, 0x9c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 195
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_TEAM_JOIN_DETAILS"

    const/16 v15, 0x9d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_JOIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 196
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_TEAM_JOIN_FROM_OOB_LINK_DETAILS"

    const/16 v15, 0x9e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_JOIN_FROM_OOB_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 197
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SF_TEAM_UNINVITE_DETAILS"

    const/16 v15, 0x9f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 198
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_ADD_INVITEES_DETAILS"

    const/16 v15, 0xa0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 199
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_ADD_LINK_EXPIRY_DETAILS"

    const/16 v15, 0xa1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 200
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_ADD_LINK_PASSWORD_DETAILS"

    const/16 v15, 0xa2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 201
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_ADD_MEMBER_DETAILS"

    const/16 v15, 0xa3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 202
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY_DETAILS"

    const/16 v15, 0xa4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 203
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_INVITEE_ROLE_DETAILS"

    const/16 v15, 0xa5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_INVITEE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 204
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_LINK_AUDIENCE_DETAILS"

    const/16 v15, 0xa6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_AUDIENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 205
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_LINK_EXPIRY_DETAILS"

    const/16 v15, 0xa7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 206
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_LINK_PASSWORD_DETAILS"

    const/16 v15, 0xa8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 207
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_MEMBER_ROLE_DETAILS"

    const/16 v15, 0xa9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 208
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY_DETAILS"

    const/16 v15, 0xaa

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 209
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_CLAIM_INVITATION_DETAILS"

    const/16 v15, 0xab

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CLAIM_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 210
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_COPY_DETAILS"

    const/16 v15, 0xac

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 211
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_DOWNLOAD_DETAILS"

    const/16 v15, 0xad

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 212
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_RELINQUISH_MEMBERSHIP_DETAILS"

    const/16 v15, 0xae

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_RELINQUISH_MEMBERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 213
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_REMOVE_INVITEES_DETAILS"

    const/16 v15, 0xaf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 214
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_REMOVE_LINK_EXPIRY_DETAILS"

    const/16 v15, 0xb0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 215
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_REMOVE_LINK_PASSWORD_DETAILS"

    const/16 v15, 0xb1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 216
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_REMOVE_MEMBER_DETAILS"

    const/16 v15, 0xb2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 217
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_REQUEST_ACCESS_DETAILS"

    const/16 v15, 0xb3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 218
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_UNSHARE_DETAILS"

    const/16 v15, 0xb4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_UNSHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 219
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_CONTENT_VIEW_DETAILS"

    const/16 v15, 0xb5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 220
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_CHANGE_LINK_POLICY_DETAILS"

    const/16 v15, 0xb6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 221
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY_DETAILS"

    const/16 v15, 0xb7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 222
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY_DETAILS"

    const/16 v15, 0xb8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 223
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_CHANGE_MEMBERS_POLICY_DETAILS"

    const/16 v15, 0xb9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 224
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_CREATE_DETAILS"

    const/16 v15, 0xba

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 225
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_DECLINE_INVITATION_DETAILS"

    const/16 v15, 0xbb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_DECLINE_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 226
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_MOUNT_DETAILS"

    const/16 v15, 0xbc

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_MOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 227
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_NEST_DETAILS"

    const/16 v15, 0xbd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_NEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 228
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_TRANSFER_OWNERSHIP_DETAILS"

    const/16 v15, 0xbe

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_TRANSFER_OWNERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 229
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_FOLDER_UNMOUNT_DETAILS"

    const/16 v15, 0xbf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_UNMOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 230
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_ADD_EXPIRY_DETAILS"

    const/16 v15, 0xc0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_ADD_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 231
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_CHANGE_EXPIRY_DETAILS"

    const/16 v15, 0xc1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CHANGE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 232
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_CHANGE_VISIBILITY_DETAILS"

    const/16 v15, 0xc2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CHANGE_VISIBILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 233
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_COPY_DETAILS"

    const/16 v15, 0xc3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 234
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_CREATE_DETAILS"

    const/16 v15, 0xc4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 235
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_DISABLE_DETAILS"

    const/16 v15, 0xc5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_DISABLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 236
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_DOWNLOAD_DETAILS"

    const/16 v15, 0xc6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 237
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_REMOVE_EXPIRY_DETAILS"

    const/16 v15, 0xc7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_REMOVE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 238
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_SHARE_DETAILS"

    const/16 v15, 0xc8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 239
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_LINK_VIEW_DETAILS"

    const/16 v15, 0xc9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 240
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARED_NOTE_OPENED_DETAILS"

    const/16 v15, 0xca

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_NOTE_OPENED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 241
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHMODEL_GROUP_SHARE_DETAILS"

    const/16 v15, 0xcb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHMODEL_GROUP_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 242
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_ACCESS_GRANTED_DETAILS"

    const/16 v15, 0xcc

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ACCESS_GRANTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 243
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_ADD_MEMBER_DETAILS"

    const/16 v15, 0xcd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 244
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_ARCHIVED_DETAILS"

    const/16 v15, 0xce

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ARCHIVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 245
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_CREATED_DETAILS"

    const/16 v15, 0xcf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CREATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 246
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_DELETE_COMMENT_DETAILS"

    const/16 v15, 0xd0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 247
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_EDITED_DETAILS"

    const/16 v15, 0xd1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_EDITED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 248
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_EDIT_COMMENT_DETAILS"

    const/16 v15, 0xd2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 249
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_FILE_ADDED_DETAILS"

    const/16 v15, 0xd3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_ADDED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 250
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_FILE_DOWNLOAD_DETAILS"

    const/16 v15, 0xd4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 251
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_FILE_REMOVED_DETAILS"

    const/16 v15, 0xd5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_REMOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 252
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_FILE_VIEW_DETAILS"

    const/16 v15, 0xd6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 253
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_PERMANENTLY_DELETED_DETAILS"

    const/16 v15, 0xd7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_PERMANENTLY_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 254
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_POST_COMMENT_DETAILS"

    const/16 v15, 0xd8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_POST_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 255
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_REMOVE_MEMBER_DETAILS"

    const/16 v15, 0xd9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 256
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_RENAMED_DETAILS"

    const/16 v15, 0xda

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RENAMED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 257
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_REQUEST_ACCESS_DETAILS"

    const/16 v15, 0xdb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 258
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_RESOLVE_COMMENT_DETAILS"

    const/16 v15, 0xdc

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 259
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_RESTORED_DETAILS"

    const/16 v15, 0xdd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RESTORED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 260
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_TRASHED_DETAILS"

    const/16 v15, 0xde

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 261
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_TRASHED_DEPRECATED_DETAILS"

    const/16 v15, 0xdf

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_TRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 262
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_UNRESOLVE_COMMENT_DETAILS"

    const/16 v15, 0xe0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 263
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_UNTRASHED_DETAILS"

    const/16 v15, 0xe1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 264
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_UNTRASHED_DEPRECATED_DETAILS"

    const/16 v15, 0xe2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNTRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 265
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_VIEW_DETAILS"

    const/16 v15, 0xe3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 266
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_ADD_CERT_DETAILS"

    const/16 v15, 0xe4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 267
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_ADD_LOGIN_URL_DETAILS"

    const/16 v15, 0xe5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 268
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_ADD_LOGOUT_URL_DETAILS"

    const/16 v15, 0xe6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 269
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_CHANGE_CERT_DETAILS"

    const/16 v15, 0xe7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 270
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_CHANGE_LOGIN_URL_DETAILS"

    const/16 v15, 0xe8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 271
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_CHANGE_LOGOUT_URL_DETAILS"

    const/16 v15, 0xe9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 272
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_CHANGE_SAML_IDENTITY_MODE_DETAILS"

    const/16 v15, 0xea

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_SAML_IDENTITY_MODE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 273
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_REMOVE_CERT_DETAILS"

    const/16 v15, 0xeb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 274
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_REMOVE_LOGIN_URL_DETAILS"

    const/16 v15, 0xec

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 275
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_REMOVE_LOGOUT_URL_DETAILS"

    const/16 v15, 0xed

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 276
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_FOLDER_CHANGE_STATUS_DETAILS"

    const/16 v15, 0xee

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 277
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_FOLDER_CREATE_DETAILS"

    const/16 v15, 0xef

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 278
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_FOLDER_DOWNGRADE_DETAILS"

    const/16 v15, 0xf0

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_DOWNGRADE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 279
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_FOLDER_PERMANENTLY_DELETE_DETAILS"

    const/16 v15, 0xf1

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 280
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_FOLDER_RENAME_DETAILS"

    const/16 v15, 0xf2

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 281
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED_DETAILS"

    const/16 v15, 0xf3

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 282
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "ACCOUNT_CAPTURE_CHANGE_POLICY_DETAILS"

    const/16 v15, 0xf4

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 283
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "ALLOW_DOWNLOAD_DISABLED_DETAILS"

    const/16 v15, 0xf5

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ALLOW_DOWNLOAD_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 284
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "ALLOW_DOWNLOAD_ENABLED_DETAILS"

    const/16 v15, 0xf6

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ALLOW_DOWNLOAD_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 285
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "CAMERA_UPLOADS_POLICY_CHANGED_DETAILS"

    const/16 v15, 0xf7

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CAMERA_UPLOADS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 286
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY_DETAILS"

    const/16 v15, 0xf8

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 287
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY_DETAILS"

    const/16 v15, 0xf9

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 288
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY_DETAILS"

    const/16 v15, 0xfa

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 289
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_APPROVALS_CHANGE_MOBILE_POLICY_DETAILS"

    const/16 v15, 0xfb

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_MOBILE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 290
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION_DETAILS"

    const/16 v15, 0xfc

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 291
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DEVICE_APPROVALS_CHANGE_UNLINK_ACTION_DETAILS"

    const/16 v15, 0xfd

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_UNLINK_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 292
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DIRECTORY_RESTRICTIONS_ADD_MEMBERS_DETAILS"

    const/16 v15, 0xfe

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DIRECTORY_RESTRICTIONS_ADD_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 293
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS_DETAILS"

    const/16 v15, 0xff

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 294
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EMM_ADD_EXCEPTION_DETAILS"

    const/16 v15, 0x100

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 295
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EMM_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x101

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 296
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EMM_REMOVE_EXCEPTION_DETAILS"

    const/16 v15, 0x102

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 297
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "EXTENDED_VERSION_HISTORY_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x103

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EXTENDED_VERSION_HISTORY_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 298
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_COMMENTS_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x104

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_COMMENTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 299
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_REQUESTS_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x105

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 300
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_REQUESTS_EMAILS_ENABLED_DETAILS"

    const/16 v15, 0x106

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_EMAILS_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 301
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY_DETAILS"

    const/16 v15, 0x107

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 302
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GOOGLE_SSO_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x108

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GOOGLE_SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 303
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "GROUP_USER_MANAGEMENT_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x109

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_USER_MANAGEMENT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 304
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_REQUESTS_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x10a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 305
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_ADD_EXCEPTION_DETAILS"

    const/16 v15, 0x10b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 306
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY_DETAILS"

    const/16 v15, 0x10c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 307
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x10d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 308
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION_DETAILS"

    const/16 v15, 0x10e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 309
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MEMBER_SUGGESTIONS_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x10f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SUGGESTIONS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 310
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x110

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 311
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "NETWORK_CONTROL_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x111

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NETWORK_CONTROL_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 312
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CHANGE_DEPLOYMENT_POLICY_DETAILS"

    const/16 v15, 0x112

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_DEPLOYMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 313
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CHANGE_MEMBER_LINK_POLICY_DETAILS"

    const/16 v15, 0x113

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_MEMBER_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 314
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CHANGE_MEMBER_POLICY_DETAILS"

    const/16 v15, 0x114

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 315
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x115

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 316
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_ENABLED_USERS_GROUP_ADDITION_DETAILS"

    const/16 v15, 0x116

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ENABLED_USERS_GROUP_ADDITION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 317
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PAPER_ENABLED_USERS_GROUP_REMOVAL_DETAILS"

    const/16 v15, 0x117

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ENABLED_USERS_GROUP_REMOVAL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 318
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "PERMANENT_DELETE_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x118

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PERMANENT_DELETE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 319
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARING_CHANGE_FOLDER_JOIN_POLICY_DETAILS"

    const/16 v15, 0x119

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_FOLDER_JOIN_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 320
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARING_CHANGE_LINK_POLICY_DETAILS"

    const/16 v15, 0x11a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 321
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHARING_CHANGE_MEMBER_POLICY_DETAILS"

    const/16 v15, 0x11b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 322
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_CHANGE_DOWNLOAD_POLICY_DETAILS"

    const/16 v15, 0x11c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_DOWNLOAD_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 323
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_CHANGE_ENABLED_POLICY_DETAILS"

    const/16 v15, 0x11d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_ENABLED_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 324
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY_DETAILS"

    const/16 v15, 0x11e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 325
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SMART_SYNC_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x11f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 326
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SMART_SYNC_NOT_OPT_OUT_DETAILS"

    const/16 v15, 0x120

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_NOT_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 327
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SMART_SYNC_OPT_OUT_DETAILS"

    const/16 v15, 0x121

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 328
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "SSO_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x122

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 329
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_SELECTIVE_SYNC_POLICY_CHANGED_DETAILS"

    const/16 v15, 0x123

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_SELECTIVE_SYNC_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 330
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TFA_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x124

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 331
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TWO_ACCOUNT_CHANGE_POLICY_DETAILS"

    const/16 v15, 0x125

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TWO_ACCOUNT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 332
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "VIEWER_INFO_POLICY_CHANGED_DETAILS"

    const/16 v15, 0x126

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->VIEWER_INFO_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 333
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY_DETAILS"

    const/16 v15, 0x127

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 334
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY_DETAILS"

    const/16 v15, 0x128

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 335
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_MERGE_FROM_DETAILS"

    const/16 v15, 0x129

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_FROM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 336
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_MERGE_TO_DETAILS"

    const/16 v15, 0x12a

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_TO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 337
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_PROFILE_ADD_LOGO_DETAILS"

    const/16 v15, 0x12b

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_ADD_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 338
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE_DETAILS"

    const/16 v15, 0x12c

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 339
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_PROFILE_CHANGE_LOGO_DETAILS"

    const/16 v15, 0x12d

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 340
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_PROFILE_CHANGE_NAME_DETAILS"

    const/16 v15, 0x12e

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 341
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TEAM_PROFILE_REMOVE_LOGO_DETAILS"

    const/16 v15, 0x12f

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_REMOVE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 342
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TFA_ADD_BACKUP_PHONE_DETAILS"

    const/16 v15, 0x130

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_ADD_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 343
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TFA_ADD_SECURITY_KEY_DETAILS"

    const/16 v15, 0x131

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_ADD_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 344
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TFA_CHANGE_BACKUP_PHONE_DETAILS"

    const/16 v15, 0x132

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 345
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TFA_CHANGE_STATUS_DETAILS"

    const/16 v15, 0x133

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 346
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TFA_REMOVE_BACKUP_PHONE_DETAILS"

    const/16 v15, 0x134

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_REMOVE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 347
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TFA_REMOVE_SECURITY_KEY_DETAILS"

    const/16 v15, 0x135

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_REMOVE_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 348
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "TFA_RESET_DETAILS"

    const/16 v15, 0x136

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 353
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "MISSING_DETAILS"

    const/16 v15, 0x137

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MISSING_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    .line 362
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const-string v1, "OTHER"

    const/16 v15, 0x138

    invoke-direct {v0, v1, v15}, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->OTHER:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v0, 0x139

    .line 37
    new-array v0, v0, [Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_LINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_LINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_UNLINK_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->APP_UNLINK_USER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_CHANGE_COMMENT_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v8

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v9

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_LIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v10

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v11

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_UNLIKE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v12

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v13

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_DESKTOP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    aput-object v1, v0, v14

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_MOBILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_CHANGE_IP_WEB_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_DELETE_ON_UNLINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_DELETE_ON_UNLINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_LINK_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_LINK_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_MANAGEMENT_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_MANAGEMENT_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_UNLINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_REFRESH_AUTH_TOKEN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_CHANGE_AVAILABILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_MIGRATE_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DISABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_EMAIL_EXISTING_USERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DOMAIN_VERIFICATION_REMOVE_DOMAIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ENABLED_DOMAIN_INVITES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CREATE_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ADD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_GET_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_MOVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_PREVIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_ROLLBACK_CHANGES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_SAVE_COPY_REFERENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CLOSE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUEST_RECEIVE_FILE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_ADD_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_MANAGEMENT_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_DESCRIPTION_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_JOIN_POLICY_UPDATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_MOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_REMOVE_EXTERNAL_ID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGIN_FAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGIN_SUCCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->LOGOUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->RESELLER_SUPPORT_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->RESELLER_SUPPORT_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SIGN_IN_AS_SESSION_END_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SIGN_IN_AS_SESSION_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ERROR_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_ADD_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_ADMIN_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_EMAIL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_MEMBERSHIP_TYPE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_DELETE_MANUAL_CONTACTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SUGGEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_TRANSFER_ACCOUNT_CONTENTS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SECONDARY_MAILS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ADD_TO_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_ARCHIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_REMOVE_FROM_FOLDER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CONTENT_RESTORE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_ADD_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_EDIT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_MENTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_OWNERSHIP_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_REVERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_SLACK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_DOC_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_ALLOW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_DEFAULT_TEAM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_EXTERNAL_VIEW_FORBID_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_CHANGE_SUBSCRIPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_FOLLOWED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_FOLDER_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_CHANGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x84

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PASSWORD_RESET_ALL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x85

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CREATE_EXCEPTIONS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x86

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CREATE_USAGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x87

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EXPORT_MEMBERS_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x88

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ADMIN_EXPORT_START_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x89

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x8a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_ACTIVITY_CREATE_REPORT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x8b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->COLLECTION_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x8c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_INVITE_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x8d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x8e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_ACL_TEAM_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x8f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x90

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NOTE_SHARE_RECEIVE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x91

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->OPEN_NOTE_SHARED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x92

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_ADD_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x93

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x94

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_EXTERNAL_INVITE_WARN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x95

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x96

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x97

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_FB_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x98

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_INVITE_GROUP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x99

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_GRANT_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x9a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_INVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x9b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_INVITE_CHANGE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x9c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_JOIN_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x9d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_JOIN_FROM_OOB_LINK_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x9e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SF_TEAM_UNINVITE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x9f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_INVITEE_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_AUDIENCE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_MEMBER_ROLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xa9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xaa

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_CLAIM_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xab

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xac

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xad

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_RELINQUISH_MEMBERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xae

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_INVITEES_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xaf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_LINK_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_LINK_PASSWORD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_UNSHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_CONTENT_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CHANGE_MEMBERS_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xb9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xba

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_DECLINE_INVITATION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xbb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_MOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xbc

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_NEST_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xbd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_TRANSFER_OWNERSHIP_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xbe

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_FOLDER_UNMOUNT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xbf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_ADD_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CHANGE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CHANGE_VISIBILITY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_COPY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_DISABLE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_REMOVE_EXPIRY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_LINK_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xc9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARED_NOTE_OPENED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xca

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHMODEL_GROUP_SHARE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xcb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ACCESS_GRANTED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xcc

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ADD_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xcd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_ARCHIVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xce

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CREATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xcf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_DELETE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_EDITED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_EDIT_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_ADDED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_DOWNLOAD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_REMOVED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_FILE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_PERMANENTLY_DELETED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_POST_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_REMOVE_MEMBER_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xd9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RENAMED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xda

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_REQUEST_ACCESS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xdb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xdc

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_RESTORED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xdd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_TRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xde

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_TRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xdf

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNRESOLVE_COMMENT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNTRASHED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_UNTRASHED_DEPRECATED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_VIEW_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_ADD_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xe9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_SAML_IDENTITY_MODE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xea

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_CERT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xeb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_LOGIN_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xec

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_REMOVE_LOGOUT_URL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xed

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xee

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_CREATE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xef

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_DOWNGRADE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf0

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_PERMANENTLY_DELETE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf1

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_FOLDER_RENAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf2

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf3

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ACCOUNT_CAPTURE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf4

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ALLOW_DOWNLOAD_DISABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf5

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->ALLOW_DOWNLOAD_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf6

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->CAMERA_UPLOADS_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf7

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf8

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xf9

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xfa

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_MOBILE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xfb

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xfc

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DEVICE_APPROVALS_CHANGE_UNLINK_ACTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xfd

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DIRECTORY_RESTRICTIONS_ADD_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xfe

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0xff

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x100

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x101

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EMM_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x102

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->EXTENDED_VERSION_HISTORY_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x103

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_COMMENTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x104

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x105

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_EMAILS_ENABLED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x106

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x107

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GOOGLE_SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x108

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->GROUP_USER_MANAGEMENT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x109

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_REQUESTS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x10a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_ADD_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x10b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x10c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x10d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x10e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MEMBER_SUGGESTIONS_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x10f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x110

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->NETWORK_CONTROL_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x111

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_DEPLOYMENT_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x112

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_MEMBER_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x113

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x114

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x115

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ENABLED_USERS_GROUP_ADDITION_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x116

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PAPER_ENABLED_USERS_GROUP_REMOVAL_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x117

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->PERMANENT_DELETE_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x118

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_FOLDER_JOIN_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x119

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_LINK_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x11a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHARING_CHANGE_MEMBER_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x11b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_DOWNLOAD_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x11c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_ENABLED_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x11d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x11e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x11f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_NOT_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x120

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SMART_SYNC_OPT_OUT_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x121

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->SSO_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x122

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_SELECTIVE_SYNC_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x123

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x124

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TWO_ACCOUNT_CHANGE_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x125

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->VIEWER_INFO_POLICY_CHANGED_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x126

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x127

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x128

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_FROM_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x129

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_MERGE_TO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x12a

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_ADD_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x12b

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x12c

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x12d

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_CHANGE_NAME_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x12e

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TEAM_PROFILE_REMOVE_LOGO_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x12f

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_ADD_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x130

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_ADD_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x131

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x132

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_CHANGE_STATUS_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x133

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_REMOVE_BACKUP_PHONE_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x134

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_REMOVE_SECURITY_KEY_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x135

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->TFA_RESET_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x136

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->MISSING_DETAILS:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x137

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->OTHER:Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    const/16 v2, 0x138

    aput-object v1, v0, v2

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->$VALUES:[Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;
    .locals 1

    .line 37
    const-class v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    return-object p0
.end method

.method public static values()[Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;
    .locals 1

    .line 37
    sget-object v0, Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->$VALUES:[Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    invoke-virtual {v0}, [Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/core/v2/teamlog/EventDetails$Tag;

    return-object v0
.end method

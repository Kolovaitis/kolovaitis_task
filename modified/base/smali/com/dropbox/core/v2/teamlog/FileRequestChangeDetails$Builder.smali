.class public Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;
.super Ljava/lang/Object;
.source "FileRequestChangeDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected fileRequestId:Ljava/lang/String;

.field protected final newDetails:Lcom/dropbox/core/v2/teamlog/FileRequestDetails;

.field protected previousDetails:Lcom/dropbox/core/v2/teamlog/FileRequestDetails;


# direct methods
.method protected constructor <init>(Lcom/dropbox/core/v2/teamlog/FileRequestDetails;)V
    .locals 1

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 131
    iput-object p1, p0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;->newDetails:Lcom/dropbox/core/v2/teamlog/FileRequestDetails;

    const/4 p1, 0x0

    .line 132
    iput-object p1, p0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;->fileRequestId:Ljava/lang/String;

    .line 133
    iput-object p1, p0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;->previousDetails:Lcom/dropbox/core/v2/teamlog/FileRequestDetails;

    return-void

    .line 129
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value for \'newDetails\' is null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public build()Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails;
    .locals 4

    .line 181
    new-instance v0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails;

    iget-object v1, p0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;->newDetails:Lcom/dropbox/core/v2/teamlog/FileRequestDetails;

    iget-object v2, p0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;->fileRequestId:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;->previousDetails:Lcom/dropbox/core/v2/teamlog/FileRequestDetails;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails;-><init>(Lcom/dropbox/core/v2/teamlog/FileRequestDetails;Ljava/lang/String;Lcom/dropbox/core/v2/teamlog/FileRequestDetails;)V

    return-object v0
.end method

.method public withFileRequestId(Ljava/lang/String;)Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;
    .locals 2

    if-eqz p1, :cond_2

    .line 150
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_1

    const-string v0, "[-_0-9a-zA-Z]+"

    .line 153
    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 154
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "String \'fileRequestId\' does not match pattern"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 151
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "String \'fileRequestId\' is shorter than 1"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 157
    :cond_2
    :goto_0
    iput-object p1, p0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;->fileRequestId:Ljava/lang/String;

    return-object p0
.end method

.method public withPreviousDetails(Lcom/dropbox/core/v2/teamlog/FileRequestDetails;)Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/dropbox/core/v2/teamlog/FileRequestChangeDetails$Builder;->previousDetails:Lcom/dropbox/core/v2/teamlog/FileRequestDetails;

    return-object p0
.end method

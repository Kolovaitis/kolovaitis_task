.class public Lcom/evernote/clients/LinkedNoteStoreClient;
.super Ljava/lang/Object;
.source "LinkedNoteStoreClient.java"


# instance fields
.field private authenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

.field private linkedNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

.field private mainNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;


# direct methods
.method constructor <init>(Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/edam/userstore/AuthenticationResult;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->mainNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

    .line 61
    iput-object p2, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->linkedNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

    .line 62
    iput-object p3, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->authenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    return-void
.end method


# virtual methods
.method public createNote(Lcom/evernote/edam/type/Note;Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/Note;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 105
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object p2

    invoke-virtual {p2}, Lcom/evernote/clients/NoteStoreClient;->getSharedNotebookByAuth()Lcom/evernote/edam/type/SharedNotebook;

    move-result-object p2

    .line 106
    invoke-virtual {p2}, Lcom/evernote/edam/type/SharedNotebook;->getNotebookGuid()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/evernote/edam/type/Note;->setNotebookGuid(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/evernote/clients/NoteStoreClient;->createNote(Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method getAuthenticationResult()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->authenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    return-object v0
.end method

.method public getClient()Lcom/evernote/clients/NoteStoreClient;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->linkedNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

    return-object v0
.end method

.method public getCorrespondingNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/Notebook;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 131
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/clients/NoteStoreClient;->getSharedNotebookByAuth()Lcom/evernote/edam/type/SharedNotebook;

    move-result-object p1

    .line 132
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/type/SharedNotebook;->getNotebookGuid()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/evernote/clients/NoteStoreClient;->getNotebook(Ljava/lang/String;)Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    return-object p1
.end method

.method getPersonalClient()Lcom/evernote/clients/NoteStoreClient;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/evernote/clients/LinkedNoteStoreClient;->mainNoteStoreClient:Lcom/evernote/clients/NoteStoreClient;

    return-object v0
.end method

.method getToken()Ljava/lang/String;
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getAuthenticationResult()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNotebookWritable(Lcom/evernote/edam/type/LinkedNotebook;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 143
    invoke-virtual {p0, p1}, Lcom/evernote/clients/LinkedNoteStoreClient;->getCorrespondingNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    .line 144
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->getRestrictions()Lcom/evernote/edam/type/NotebookRestrictions;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/edam/type/NotebookRestrictions;->isNoCreateNotes()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public listNotebooks()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 119
    invoke-virtual {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->getPersonalClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/clients/NoteStoreClient;->listLinkedNotebooks()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

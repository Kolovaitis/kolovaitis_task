.class public Lcom/evernote/clients/UserStoreClient;
.super Ljava/lang/Object;
.source "UserStoreClient.java"


# instance fields
.field private final client:Lcom/evernote/edam/userstore/UserStore$Client;

.field private final token:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/evernote/thrift/protocol/TProtocol;Lcom/evernote/thrift/protocol/TProtocol;Ljava/lang/String;)V
    .locals 1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 64
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$Client;

    invoke-direct {v0, p1, p2}, Lcom/evernote/edam/userstore/UserStore$Client;-><init>(Lcom/evernote/thrift/protocol/TProtocol;Lcom/evernote/thrift/protocol/TProtocol;)V

    iput-object v0, p0, Lcom/evernote/clients/UserStoreClient;->client:Lcom/evernote/edam/userstore/UserStore$Client;

    .line 65
    iput-object p3, p0, Lcom/evernote/clients/UserStoreClient;->token:Ljava/lang/String;

    return-void

    .line 61
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "TProtocol and Token must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method constructor <init>(Lcom/evernote/thrift/protocol/TProtocol;Ljava/lang/String;)V
    .locals 1

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 55
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$Client;

    invoke-direct {v0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;-><init>(Lcom/evernote/thrift/protocol/TProtocol;)V

    iput-object v0, p0, Lcom/evernote/clients/UserStoreClient;->client:Lcom/evernote/edam/userstore/UserStore$Client;

    .line 56
    iput-object p2, p0, Lcom/evernote/clients/UserStoreClient;->token:Ljava/lang/String;

    return-void

    .line 52
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "TProtocol and Token must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public authenticate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 119
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/evernote/edam/userstore/UserStore$Client;->authenticate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public authenticateLongSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 132
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/evernote/edam/userstore/UserStore$Client;->authenticateLongSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public authenticateToBusiness()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 141
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$Client;->authenticateToBusiness(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method

.method public checkVersion(Ljava/lang/String;SS)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 101
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/evernote/edam/userstore/UserStore$Client;->checkVersion(Ljava/lang/String;SS)Z

    move-result p1

    return p1
.end method

.method public completeTwoFactorAuthentication(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 201
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/evernote/edam/userstore/UserStore$Client;->completeTwoFactorAuthentication(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    return-void
.end method

.method public getBootstrapInfo(Ljava/lang/String;)Lcom/evernote/edam/userstore/BootstrapInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 109
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->getBootstrapInfo(Ljava/lang/String;)Lcom/evernote/edam/userstore/BootstrapInfo;

    move-result-object p1

    return-object p1
.end method

.method getClient()Lcom/evernote/edam/userstore/UserStore$Client;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/evernote/clients/UserStoreClient;->client:Lcom/evernote/edam/userstore/UserStore$Client;

    return-object v0
.end method

.method public getNoteStoreUrl()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 182
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$Client;->getNoteStoreUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPremiumInfo()Lcom/evernote/edam/type/PremiumInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 174
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$Client;->getPremiumInfo(Ljava/lang/String;)Lcom/evernote/edam/type/PremiumInfo;

    move-result-object v0

    return-object v0
.end method

.method public getPublicUserInfo(Ljava/lang/String;)Lcom/evernote/edam/userstore/PublicUserInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 166
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;->getPublicUserInfo(Ljava/lang/String;)Lcom/evernote/edam/userstore/PublicUserInfo;

    move-result-object p1

    return-object p1
.end method

.method getToken()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/evernote/clients/UserStoreClient;->token:Ljava/lang/String;

    return-object v0
.end method

.method public getUser()Lcom/evernote/edam/type/User;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 157
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$Client;->getUser(Ljava/lang/String;)Lcom/evernote/edam/type/User;

    move-result-object v0

    return-object v0
.end method

.method public isBusinessUser()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 92
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$Client;->getUser(Ljava/lang/String;)Lcom/evernote/edam/type/User;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/edam/type/User;->isSetBusinessUserInfo()Z

    move-result v0

    return v0
.end method

.method public refreshAuthentication()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 149
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$Client;->refreshAuthentication(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method

.method public revokeLongSession()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 190
    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getClient()Lcom/evernote/edam/userstore/UserStore$Client;

    move-result-object v0

    invoke-virtual {p0}, Lcom/evernote/clients/UserStoreClient;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/userstore/UserStore$Client;->revokeLongSession(Ljava/lang/String;)V

    return-void
.end method

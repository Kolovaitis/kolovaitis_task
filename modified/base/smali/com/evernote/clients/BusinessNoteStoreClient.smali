.class public Lcom/evernote/clients/BusinessNoteStoreClient;
.super Lcom/evernote/clients/LinkedNoteStoreClient;
.source "BusinessNoteStoreClient.java"


# direct methods
.method constructor <init>(Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/edam/userstore/AuthenticationResult;)V
    .locals 0

    .line 56
    invoke-direct {p0, p1, p2, p3}, Lcom/evernote/clients/LinkedNoteStoreClient;-><init>(Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/edam/userstore/AuthenticationResult;)V

    return-void
.end method


# virtual methods
.method public createNotebook(Lcom/evernote/edam/type/Notebook;)Lcom/evernote/edam/type/LinkedNotebook;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 67
    invoke-virtual {p0}, Lcom/evernote/clients/BusinessNoteStoreClient;->getClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/evernote/clients/NoteStoreClient;->createNotebook(Lcom/evernote/edam/type/Notebook;)Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->getSharedNotebooks()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/edam/type/SharedNotebook;

    .line 71
    new-instance v1, Lcom/evernote/edam/type/LinkedNotebook;

    invoke-direct {v1}, Lcom/evernote/edam/type/LinkedNotebook;-><init>()V

    .line 72
    invoke-virtual {v0}, Lcom/evernote/edam/type/SharedNotebook;->getShareKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/evernote/edam/type/LinkedNotebook;->setShareKey(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/LinkedNotebook;->setShareName(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/evernote/clients/BusinessNoteStoreClient;->getAuthenticationResult()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getUser()Lcom/evernote/edam/type/User;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->getUsername()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/LinkedNotebook;->setUsername(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/evernote/clients/BusinessNoteStoreClient;->getAuthenticationResult()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getUser()Lcom/evernote/edam/type/User;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/edam/type/User;->getShardId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/LinkedNotebook;->setShardId(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/evernote/clients/BusinessNoteStoreClient;->getPersonalClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/evernote/clients/NoteStoreClient;->createLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object p1

    return-object p1
.end method

.method public listNotebooks()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 95
    invoke-super {p0}, Lcom/evernote/clients/LinkedNoteStoreClient;->listNotebooks()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/edam/type/LinkedNotebook;

    .line 96
    invoke-virtual {v2}, Lcom/evernote/edam/type/LinkedNotebook;->isSetBusinessId()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 97
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

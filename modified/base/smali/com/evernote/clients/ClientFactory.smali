.class public Lcom/evernote/clients/ClientFactory;
.super Ljava/lang/Object;
.source "ClientFactory.java"


# static fields
.field private static final CONSUMER_KEY_REGEX:Ljava/util/regex/Pattern;

.field private static final USER_AGENT_KEY:Ljava/lang/String; = "User-Agent"


# instance fields
.field private customHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private evernoteAuth:Lcom/evernote/auth/EvernoteAuth;

.field private userAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ":A=([^:]+):"

    .line 54
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/evernote/clients/ClientFactory;->CONSUMER_KEY_REGEX:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/evernote/auth/EvernoteAuth;)V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    .line 68
    iput-object p1, p0, Lcom/evernote/clients/ClientFactory;->evernoteAuth:Lcom/evernote/auth/EvernoteAuth;

    return-void

    .line 66
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "token must not be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public createBusinessNoteStoreClient()Lcom/evernote/clients/BusinessNoteStoreClient;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 148
    invoke-virtual {p0}, Lcom/evernote/clients/ClientFactory;->createNoteStoreClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object v0

    .line 149
    invoke-virtual {p0}, Lcom/evernote/clients/ClientFactory;->createUserStoreClient()Lcom/evernote/clients/UserStoreClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/evernote/clients/UserStoreClient;->authenticateToBusiness()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v1

    .line 151
    const-class v2, Lcom/evernote/clients/NoteStoreClient;

    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/evernote/clients/ClientFactory;->createStoreClient(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/clients/NoteStoreClient;

    .line 155
    new-instance v3, Lcom/evernote/clients/BusinessNoteStoreClient;

    invoke-direct {v3, v0, v2, v1}, Lcom/evernote/clients/BusinessNoteStoreClient;-><init>(Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/edam/userstore/AuthenticationResult;)V

    return-object v3
.end method

.method public createLinkedNoteStoreClient(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/clients/LinkedNoteStoreClient;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 119
    invoke-virtual {p0}, Lcom/evernote/clients/ClientFactory;->createNoteStoreClient()Lcom/evernote/clients/NoteStoreClient;

    move-result-object v0

    .line 120
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getShareKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/evernote/clients/NoteStoreClient;->authenticateToSharedNotebook(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v1

    .line 122
    const-class v2, Lcom/evernote/clients/NoteStoreClient;

    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, p1, v3}, Lcom/evernote/clients/ClientFactory;->createStoreClient(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/clients/NoteStoreClient;

    .line 126
    new-instance v2, Lcom/evernote/clients/LinkedNoteStoreClient;

    invoke-direct {v2, v0, p1, v1}, Lcom/evernote/clients/LinkedNoteStoreClient;-><init>(Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/clients/NoteStoreClient;Lcom/evernote/edam/userstore/AuthenticationResult;)V

    return-object v2
.end method

.method public createNoteStoreClient()Lcom/evernote/clients/NoteStoreClient;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/evernote/clients/ClientFactory;->evernoteAuth:Lcom/evernote/auth/EvernoteAuth;

    invoke-virtual {v0}, Lcom/evernote/auth/EvernoteAuth;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lcom/evernote/clients/ClientFactory;->createUserStoreClient()Lcom/evernote/clients/UserStoreClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/clients/UserStoreClient;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object v0

    .line 104
    iget-object v1, p0, Lcom/evernote/clients/ClientFactory;->evernoteAuth:Lcom/evernote/auth/EvernoteAuth;

    invoke-virtual {v1, v0}, Lcom/evernote/auth/EvernoteAuth;->setNoteStoreUrl(Ljava/lang/String;)V

    .line 106
    :cond_0
    const-class v1, Lcom/evernote/clients/NoteStoreClient;

    iget-object v2, p0, Lcom/evernote/clients/ClientFactory;->evernoteAuth:Lcom/evernote/auth/EvernoteAuth;

    invoke-virtual {v2}, Lcom/evernote/auth/EvernoteAuth;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/evernote/clients/ClientFactory;->createStoreClient(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/clients/NoteStoreClient;

    return-object v0
.end method

.method createStoreClient(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/transport/TTransportException;
        }
    .end annotation

    .line 168
    new-instance v0, Lcom/evernote/thrift/transport/THttpClient;

    invoke-direct {v0, p2}, Lcom/evernote/thrift/transport/THttpClient;-><init>(Ljava/lang/String;)V

    const-string p2, "User-Agent"

    .line 170
    invoke-virtual {p0}, Lcom/evernote/clients/ClientFactory;->generateUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/evernote/thrift/transport/THttpClient;->setCustomHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object p2, p0, Lcom/evernote/clients/ClientFactory;->customHeaders:Ljava/util/Map;

    if-eqz p2, :cond_0

    .line 172
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 173
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/evernote/thrift/transport/THttpClient;->setCustomHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 177
    :cond_0
    new-instance p2, Lcom/evernote/thrift/protocol/TBinaryProtocol;

    invoke-direct {p2, v0}, Lcom/evernote/thrift/protocol/TBinaryProtocol;-><init>(Lcom/evernote/thrift/transport/TTransport;)V

    const/4 v0, 0x3

    .line 179
    :try_start_0
    new-array v1, v0, [Ljava/lang/Class;

    const-class v2, Lcom/evernote/thrift/protocol/TProtocol;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/evernote/thrift/protocol/TProtocol;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-class v2, Ljava/lang/String;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    invoke-virtual {p1, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p2, v0, v3

    aput-object p2, v0, v4

    aput-object p3, v0, v5

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p2

    .line 182
    new-instance p3, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t create "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " due to the error."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p3
.end method

.method public createUserStoreClient()Lcom/evernote/clients/UserStoreClient;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/transport/TTransportException;
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/evernote/clients/ClientFactory;->evernoteAuth:Lcom/evernote/auth/EvernoteAuth;

    invoke-virtual {v0}, Lcom/evernote/auth/EvernoteAuth;->getUserStoreUrl()Ljava/lang/String;

    move-result-object v0

    .line 86
    const-class v1, Lcom/evernote/clients/UserStoreClient;

    iget-object v2, p0, Lcom/evernote/clients/ClientFactory;->evernoteAuth:Lcom/evernote/auth/EvernoteAuth;

    invoke-virtual {v2}, Lcom/evernote/auth/EvernoteAuth;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lcom/evernote/clients/ClientFactory;->createStoreClient(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/clients/UserStoreClient;

    return-object v0
.end method

.method generateUserAgent()Ljava/lang/String;
    .locals 4

    .line 191
    iget-object v0, p0, Lcom/evernote/clients/ClientFactory;->userAgent:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 193
    sget-object v0, Lcom/evernote/clients/ClientFactory;->CONSUMER_KEY_REGEX:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/evernote/clients/ClientFactory;->evernoteAuth:Lcom/evernote/auth/EvernoteAuth;

    invoke-virtual {v1}, Lcom/evernote/auth/EvernoteAuth;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 196
    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    .line 202
    :cond_1
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getImplementationVersion()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, "1.25"

    :cond_2
    const-string v2, "java.version"

    .line 207
    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 208
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " / "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "; Java / "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setCustomHeaders(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 226
    iput-object p1, p0, Lcom/evernote/clients/ClientFactory;->customHeaders:Ljava/util/Map;

    return-void
.end method

.method public setUserAgent(Ljava/lang/String;)V
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/evernote/clients/ClientFactory;->userAgent:Ljava/lang/String;

    return-void
.end method

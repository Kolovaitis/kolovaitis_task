.class public Lcom/evernote/auth/EvernoteAuth;
.super Ljava/lang/Object;
.source "EvernoteAuth.java"


# static fields
.field private static final CHARSET:Ljava/lang/String; = "UTF-8"

.field private static final NOTESTORE_REGEX:Ljava/util/regex/Pattern;

.field private static final TOKEN_REGEX:Ljava/util/regex/Pattern;

.field private static final USERID_REGEX:Ljava/util/regex/Pattern;

.field private static final USER_STORE_PATH:Ljava/lang/String; = "/edam/user"

.field private static final WEBAPI_REGEX:Ljava/util/regex/Pattern;


# instance fields
.field private noteStoreUrl:Ljava/lang/String;

.field private service:Lcom/evernote/auth/EvernoteService;

.field private token:Ljava/lang/String;

.field private userId:I

.field private webApiUrlPrefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "oauth_token=([^&]+)"

    .line 42
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/evernote/auth/EvernoteAuth;->TOKEN_REGEX:Ljava/util/regex/Pattern;

    const-string v0, "edam_noteStoreUrl=([^&]+)"

    .line 44
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/evernote/auth/EvernoteAuth;->NOTESTORE_REGEX:Ljava/util/regex/Pattern;

    const-string v0, "edam_webApiUrlPrefix=([^&]+)"

    .line 46
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/evernote/auth/EvernoteAuth;->WEBAPI_REGEX:Ljava/util/regex/Pattern;

    const-string v0, "edam_userId=([^&]+)"

    .line 48
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/evernote/auth/EvernoteAuth;->USERID_REGEX:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/evernote/auth/EvernoteService;Ljava/lang/String;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 62
    iput-object p1, p0, Lcom/evernote/auth/EvernoteAuth;->service:Lcom/evernote/auth/EvernoteService;

    .line 63
    iput-object p2, p0, Lcom/evernote/auth/EvernoteAuth;->token:Ljava/lang/String;

    return-void

    .line 59
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "EvernoteService and token must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public constructor <init>(Lcom/evernote/auth/EvernoteService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    .line 73
    iput-object p1, p0, Lcom/evernote/auth/EvernoteAuth;->service:Lcom/evernote/auth/EvernoteService;

    .line 74
    iput-object p2, p0, Lcom/evernote/auth/EvernoteAuth;->token:Ljava/lang/String;

    .line 75
    iput-object p3, p0, Lcom/evernote/auth/EvernoteAuth;->noteStoreUrl:Ljava/lang/String;

    .line 76
    iput-object p4, p0, Lcom/evernote/auth/EvernoteAuth;->webApiUrlPrefix:Ljava/lang/String;

    .line 77
    iput p5, p0, Lcom/evernote/auth/EvernoteAuth;->userId:I

    return-void

    .line 70
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "EvernoteService, token, noteStoreUrl and webApiUrlPrefix must not be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private static extract(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;
    .locals 2

    .line 108
    invoke-virtual {p1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p1

    .line 109
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 111
    :try_start_0
    invoke-virtual {p1, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    const-string p1, "UTF-8"

    invoke-static {p0, p1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    .line 113
    new-instance p1, Lcom/evernote/auth/AuthException;

    const-string v0, "Charset not found while decoding string: UTF-8"

    invoke-direct {p1, v0, p0}, Lcom/evernote/auth/AuthException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw p1

    .line 117
    :cond_0
    new-instance p1, Lcom/evernote/auth/AuthException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Response body is incorrect. Can\'t extract token and secret from this: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\'"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/evernote/auth/AuthException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw p1
.end method

.method static extractNoteStoreUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 96
    sget-object v0, Lcom/evernote/auth/EvernoteAuth;->NOTESTORE_REGEX:Ljava/util/regex/Pattern;

    invoke-static {p0, v0}, Lcom/evernote/auth/EvernoteAuth;->extract(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static extractToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 92
    sget-object v0, Lcom/evernote/auth/EvernoteAuth;->TOKEN_REGEX:Ljava/util/regex/Pattern;

    invoke-static {p0, v0}, Lcom/evernote/auth/EvernoteAuth;->extract(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static extractUserId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 104
    sget-object v0, Lcom/evernote/auth/EvernoteAuth;->USERID_REGEX:Ljava/util/regex/Pattern;

    invoke-static {p0, v0}, Lcom/evernote/auth/EvernoteAuth;->extract(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static extractWebApiUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 100
    sget-object v0, Lcom/evernote/auth/EvernoteAuth;->WEBAPI_REGEX:Ljava/util/regex/Pattern;

    invoke-static {p0, v0}, Lcom/evernote/auth/EvernoteAuth;->extract(Ljava/lang/String;Ljava/util/regex/Pattern;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static parseOAuthResponse(Lcom/evernote/auth/EvernoteService;Ljava/lang/String;)Lcom/evernote/auth/EvernoteAuth;
    .locals 7

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 86
    new-instance v6, Lcom/evernote/auth/EvernoteAuth;

    invoke-static {p1}, Lcom/evernote/auth/EvernoteAuth;->extractToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/evernote/auth/EvernoteAuth;->extractNoteStoreUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/evernote/auth/EvernoteAuth;->extractWebApiUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/evernote/auth/EvernoteAuth;->extractUserId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/evernote/auth/EvernoteAuth;-><init>(Lcom/evernote/auth/EvernoteService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v6

    .line 83
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "EvernoteService and response must not be null."

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public getNoteStoreUrl()Ljava/lang/String;
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/evernote/auth/EvernoteAuth;->noteStoreUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/evernote/auth/EvernoteAuth;->token:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .line 163
    iget v0, p0, Lcom/evernote/auth/EvernoteAuth;->userId:I

    return v0
.end method

.method public getUserStoreUrl()Ljava/lang/String;
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/evernote/auth/EvernoteAuth;->service:Lcom/evernote/auth/EvernoteService;

    const-string v1, "/edam/user"

    invoke-virtual {v0, v1}, Lcom/evernote/auth/EvernoteService;->getEndpointUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWebApiUrlPrefix()Ljava/lang/String;
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/evernote/auth/EvernoteAuth;->webApiUrlPrefix:Ljava/lang/String;

    return-object v0
.end method

.method public setNoteStoreUrl(Ljava/lang/String;)V
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/evernote/auth/EvernoteAuth;->noteStoreUrl:Ljava/lang/String;

    return-void
.end method

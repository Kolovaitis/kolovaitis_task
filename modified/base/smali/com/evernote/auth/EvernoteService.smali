.class public final enum Lcom/evernote/auth/EvernoteService;
.super Ljava/lang/Enum;
.source "EvernoteService.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/evernote/auth/EvernoteService;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/evernote/auth/EvernoteService;

.field public static final enum PRODUCTION:Lcom/evernote/auth/EvernoteService;

.field public static final enum SANDBOX:Lcom/evernote/auth/EvernoteService;

.field public static final enum YINXIANG:Lcom/evernote/auth/EvernoteService;


# instance fields
.field private host:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 31
    new-instance v0, Lcom/evernote/auth/EvernoteService;

    const-string v1, "PRODUCTION"

    const-string v2, "https://www.evernote.com"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/auth/EvernoteService;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/evernote/auth/EvernoteService;->PRODUCTION:Lcom/evernote/auth/EvernoteService;

    .line 32
    new-instance v0, Lcom/evernote/auth/EvernoteService;

    const-string v1, "SANDBOX"

    const-string v2, "https://sandbox.evernote.com"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/evernote/auth/EvernoteService;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/evernote/auth/EvernoteService;->SANDBOX:Lcom/evernote/auth/EvernoteService;

    .line 33
    new-instance v0, Lcom/evernote/auth/EvernoteService;

    const-string v1, "YINXIANG"

    const-string v2, "https://app.yinxiang.com"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/evernote/auth/EvernoteService;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/evernote/auth/EvernoteService;->YINXIANG:Lcom/evernote/auth/EvernoteService;

    const/4 v0, 0x3

    .line 29
    new-array v0, v0, [Lcom/evernote/auth/EvernoteService;

    sget-object v1, Lcom/evernote/auth/EvernoteService;->PRODUCTION:Lcom/evernote/auth/EvernoteService;

    aput-object v1, v0, v3

    sget-object v1, Lcom/evernote/auth/EvernoteService;->SANDBOX:Lcom/evernote/auth/EvernoteService;

    aput-object v1, v0, v4

    sget-object v1, Lcom/evernote/auth/EvernoteService;->YINXIANG:Lcom/evernote/auth/EvernoteService;

    aput-object v1, v0, v5

    sput-object v0, Lcom/evernote/auth/EvernoteService;->$VALUES:[Lcom/evernote/auth/EvernoteService;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput-object p3, p0, Lcom/evernote/auth/EvernoteService;->host:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/evernote/auth/EvernoteService;
    .locals 1

    .line 29
    const-class v0, Lcom/evernote/auth/EvernoteService;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/evernote/auth/EvernoteService;

    return-object p0
.end method

.method public static values()[Lcom/evernote/auth/EvernoteService;
    .locals 1

    .line 29
    sget-object v0, Lcom/evernote/auth/EvernoteService;->$VALUES:[Lcom/evernote/auth/EvernoteService;

    invoke-virtual {v0}, [Lcom/evernote/auth/EvernoteService;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/evernote/auth/EvernoteService;

    return-object v0
.end method


# virtual methods
.method public getAccessTokenEndpoint()Ljava/lang/String;
    .locals 1

    const-string v0, "/oauth"

    .line 50
    invoke-virtual {p0, v0}, Lcom/evernote/auth/EvernoteService;->getEndpointUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAuthorizationUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "/OAuth.action?oauth_token=%s"

    .line 54
    invoke-virtual {p0, v0}, Lcom/evernote/auth/EvernoteService;->getEndpointUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getEndpointUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/evernote/auth/EvernoteService;->host:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/evernote/auth/EvernoteService;->host:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestTokenEndpoint()Ljava/lang/String;
    .locals 1

    const-string v0, "/oauth"

    .line 46
    invoke-virtual {p0, v0}, Lcom/evernote/auth/EvernoteService;->getEndpointUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final enum Lcom/evernote/edam/error/EDAMErrorCode;
.super Ljava/lang/Enum;
.source "EDAMErrorCode.java"

# interfaces
.implements Lcom/evernote/thrift/TEnum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/evernote/edam/error/EDAMErrorCode;",
        ">;",
        "Lcom/evernote/thrift/TEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum AUTH_EXPIRED:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum BAD_DATA_FORMAT:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum DATA_CONFLICT:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum DATA_REQUIRED:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum ENML_VALIDATION:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum INTERNAL_ERROR:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum INVALID_AUTH:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum LEN_TOO_LONG:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum LEN_TOO_SHORT:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum LIMIT_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum PERMISSION_DENIED:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum QUOTA_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum RATE_LIMIT_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum SHARD_UNAVAILABLE:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum TAKEN_DOWN:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum TOO_FEW:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum TOO_MANY:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum UNKNOWN:Lcom/evernote/edam/error/EDAMErrorCode;

.field public static final enum UNSUPPORTED_OPERATION:Lcom/evernote/edam/error/EDAMErrorCode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 64
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->UNKNOWN:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 65
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "BAD_DATA_FORMAT"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->BAD_DATA_FORMAT:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 66
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "PERMISSION_DENIED"

    const/4 v4, 0x3

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->PERMISSION_DENIED:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 67
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "INTERNAL_ERROR"

    const/4 v5, 0x4

    invoke-direct {v0, v1, v4, v5}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->INTERNAL_ERROR:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 68
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "DATA_REQUIRED"

    const/4 v6, 0x5

    invoke-direct {v0, v1, v5, v6}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->DATA_REQUIRED:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 69
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "LIMIT_REACHED"

    const/4 v7, 0x6

    invoke-direct {v0, v1, v6, v7}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->LIMIT_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 70
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "QUOTA_REACHED"

    const/4 v8, 0x7

    invoke-direct {v0, v1, v7, v8}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->QUOTA_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 71
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "INVALID_AUTH"

    const/16 v9, 0x8

    invoke-direct {v0, v1, v8, v9}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->INVALID_AUTH:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 72
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "AUTH_EXPIRED"

    const/16 v10, 0x9

    invoke-direct {v0, v1, v9, v10}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->AUTH_EXPIRED:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 73
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "DATA_CONFLICT"

    const/16 v11, 0xa

    invoke-direct {v0, v1, v10, v11}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->DATA_CONFLICT:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 74
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "ENML_VALIDATION"

    const/16 v12, 0xb

    invoke-direct {v0, v1, v11, v12}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->ENML_VALIDATION:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 75
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "SHARD_UNAVAILABLE"

    const/16 v13, 0xc

    invoke-direct {v0, v1, v12, v13}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->SHARD_UNAVAILABLE:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 76
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "LEN_TOO_SHORT"

    const/16 v14, 0xd

    invoke-direct {v0, v1, v13, v14}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->LEN_TOO_SHORT:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 77
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "LEN_TOO_LONG"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v14, v15}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->LEN_TOO_LONG:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 78
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "TOO_FEW"

    const/16 v14, 0xf

    invoke-direct {v0, v1, v15, v14}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->TOO_FEW:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 79
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "TOO_MANY"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v14, v15}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->TOO_MANY:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 80
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "UNSUPPORTED_OPERATION"

    const/16 v14, 0x11

    invoke-direct {v0, v1, v15, v14}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->UNSUPPORTED_OPERATION:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 81
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "TAKEN_DOWN"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v14, v15}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->TAKEN_DOWN:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 82
    new-instance v0, Lcom/evernote/edam/error/EDAMErrorCode;

    const-string v1, "RATE_LIMIT_REACHED"

    const/16 v14, 0x12

    const/16 v15, 0x13

    invoke-direct {v0, v1, v14, v15}, Lcom/evernote/edam/error/EDAMErrorCode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->RATE_LIMIT_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    const/16 v0, 0x13

    .line 63
    new-array v0, v0, [Lcom/evernote/edam/error/EDAMErrorCode;

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->UNKNOWN:Lcom/evernote/edam/error/EDAMErrorCode;

    const/4 v14, 0x0

    aput-object v1, v0, v14

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->BAD_DATA_FORMAT:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->PERMISSION_DENIED:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->INTERNAL_ERROR:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->DATA_REQUIRED:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->LIMIT_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->QUOTA_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->INVALID_AUTH:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->AUTH_EXPIRED:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v9

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->DATA_CONFLICT:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v10

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->ENML_VALIDATION:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v11

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->SHARD_UNAVAILABLE:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v12

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->LEN_TOO_SHORT:Lcom/evernote/edam/error/EDAMErrorCode;

    aput-object v1, v0, v13

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->LEN_TOO_LONG:Lcom/evernote/edam/error/EDAMErrorCode;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->TOO_FEW:Lcom/evernote/edam/error/EDAMErrorCode;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->TOO_MANY:Lcom/evernote/edam/error/EDAMErrorCode;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->UNSUPPORTED_OPERATION:Lcom/evernote/edam/error/EDAMErrorCode;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->TAKEN_DOWN:Lcom/evernote/edam/error/EDAMErrorCode;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/evernote/edam/error/EDAMErrorCode;->RATE_LIMIT_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->$VALUES:[Lcom/evernote/edam/error/EDAMErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 87
    iput p3, p0, Lcom/evernote/edam/error/EDAMErrorCode;->value:I

    return-void
.end method

.method public static findByValue(I)Lcom/evernote/edam/error/EDAMErrorCode;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 140
    :pswitch_0
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->RATE_LIMIT_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 138
    :pswitch_1
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->TAKEN_DOWN:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 136
    :pswitch_2
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->UNSUPPORTED_OPERATION:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 134
    :pswitch_3
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->TOO_MANY:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 132
    :pswitch_4
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->TOO_FEW:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 130
    :pswitch_5
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->LEN_TOO_LONG:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 128
    :pswitch_6
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->LEN_TOO_SHORT:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 126
    :pswitch_7
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->SHARD_UNAVAILABLE:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 124
    :pswitch_8
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->ENML_VALIDATION:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 122
    :pswitch_9
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->DATA_CONFLICT:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 120
    :pswitch_a
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->AUTH_EXPIRED:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 118
    :pswitch_b
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->INVALID_AUTH:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 116
    :pswitch_c
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->QUOTA_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 114
    :pswitch_d
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->LIMIT_REACHED:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 112
    :pswitch_e
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->DATA_REQUIRED:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 110
    :pswitch_f
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->INTERNAL_ERROR:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 108
    :pswitch_10
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->PERMISSION_DENIED:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 106
    :pswitch_11
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->BAD_DATA_FORMAT:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    .line 104
    :pswitch_12
    sget-object p0, Lcom/evernote/edam/error/EDAMErrorCode;->UNKNOWN:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/evernote/edam/error/EDAMErrorCode;
    .locals 1

    .line 63
    const-class v0, Lcom/evernote/edam/error/EDAMErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/evernote/edam/error/EDAMErrorCode;

    return-object p0
.end method

.method public static values()[Lcom/evernote/edam/error/EDAMErrorCode;
    .locals 1

    .line 63
    sget-object v0, Lcom/evernote/edam/error/EDAMErrorCode;->$VALUES:[Lcom/evernote/edam/error/EDAMErrorCode;

    invoke-virtual {v0}, [Lcom/evernote/edam/error/EDAMErrorCode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/evernote/edam/error/EDAMErrorCode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 94
    iget v0, p0, Lcom/evernote/edam/error/EDAMErrorCode;->value:I

    return v0
.end method

.class public Lcom/evernote/edam/error/EDAMSystemException;
.super Ljava/lang/Exception;
.source "EDAMSystemException.java"

# interfaces
.implements Lcom/evernote/thrift/TBase;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Exception;",
        "Lcom/evernote/thrift/TBase<",
        "Lcom/evernote/edam/error/EDAMSystemException;",
        ">;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final ERROR_CODE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final MESSAGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final RATE_LIMIT_DURATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

.field private static final STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

.field private static final __RATELIMITDURATION_ISSET_ID:I


# instance fields
.field private __isset_vector:[Z

.field private errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

.field private message:Ljava/lang/String;

.field private rateLimitDuration:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 33
    new-instance v0, Lcom/evernote/thrift/protocol/TStruct;

    const-string v1, "EDAMSystemException"

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TStruct;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/edam/error/EDAMSystemException;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    .line 35
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "errorCode"

    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/error/EDAMSystemException;->ERROR_CODE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 36
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "message"

    const/16 v3, 0xb

    const/4 v4, 0x2

    invoke-direct {v0, v1, v3, v4}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/error/EDAMSystemException;->MESSAGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    .line 37
    new-instance v0, Lcom/evernote/thrift/protocol/TField;

    const-string v1, "rateLimitDuration"

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/evernote/thrift/protocol/TField;-><init>(Ljava/lang/String;BS)V

    sput-object v0, Lcom/evernote/edam/error/EDAMSystemException;->RATE_LIMIT_DURATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/4 v0, 0x1

    .line 46
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->__isset_vector:[Z

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/error/EDAMErrorCode;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/evernote/edam/error/EDAMSystemException;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    return-void
.end method

.method public constructor <init>(Lcom/evernote/edam/error/EDAMSystemException;)V
    .locals 4

    .line 61
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/4 v0, 0x1

    .line 46
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->__isset_vector:[Z

    .line 62
    iget-object v0, p1, Lcom/evernote/edam/error/EDAMSystemException;->__isset_vector:[Z

    iget-object v1, p0, Lcom/evernote/edam/error/EDAMSystemException;->__isset_vector:[Z

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    invoke-virtual {p1}, Lcom/evernote/edam/error/EDAMSystemException;->isSetErrorCode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p1, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 66
    :cond_0
    invoke-virtual {p1}, Lcom/evernote/edam/error/EDAMSystemException;->isSetMessage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-object v0, p1, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    .line 69
    :cond_1
    iget p1, p1, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    iput p1, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    .line 77
    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    .line 78
    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    const/4 v0, 0x0

    .line 79
    invoke-virtual {p0, v0}, Lcom/evernote/edam/error/EDAMSystemException;->setRateLimitDurationIsSet(Z)V

    .line 80
    iput v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    return-void
.end method

.method public compareTo(Lcom/evernote/edam/error/EDAMSystemException;)I
    .locals 2

    .line 208
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1

    .line 215
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetErrorCode()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/error/EDAMSystemException;->isSetErrorCode()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_1

    return v0

    .line 219
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetErrorCode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    iget-object v1, p1, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/Comparable;Ljava/lang/Comparable;)I

    move-result v0

    if-eqz v0, :cond_2

    return v0

    .line 224
    :cond_2
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetMessage()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/error/EDAMSystemException;->isSetMessage()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_3

    return v0

    .line 228
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetMessage()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    iget-object v1, p1, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    return v0

    .line 233
    :cond_4
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetRateLimitDuration()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1}, Lcom/evernote/edam/error/EDAMSystemException;->isSetRateLimitDuration()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->compareTo(Ljava/lang/Boolean;)I

    move-result v0

    if-eqz v0, :cond_5

    return v0

    .line 237
    :cond_5
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetRateLimitDuration()Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    iget p1, p1, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    invoke-static {v0, p1}, Lcom/evernote/thrift/TBaseHelper;->compareTo(II)I

    move-result p1

    if-eqz p1, :cond_6

    return p1

    :cond_6
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 32
    check-cast p1, Lcom/evernote/edam/error/EDAMSystemException;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/error/EDAMSystemException;->compareTo(Lcom/evernote/edam/error/EDAMSystemException;)I

    move-result p1

    return p1
.end method

.method public deepCopy()Lcom/evernote/edam/error/EDAMSystemException;
    .locals 1

    .line 73
    new-instance v0, Lcom/evernote/edam/error/EDAMSystemException;

    invoke-direct {v0, p0}, Lcom/evernote/edam/error/EDAMSystemException;-><init>(Lcom/evernote/edam/error/EDAMSystemException;)V

    return-object v0
.end method

.method public bridge synthetic deepCopy()Lcom/evernote/thrift/TBase;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->deepCopy()Lcom/evernote/edam/error/EDAMSystemException;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/evernote/edam/error/EDAMSystemException;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 172
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetErrorCode()Z

    move-result v1

    .line 173
    invoke-virtual {p1}, Lcom/evernote/edam/error/EDAMSystemException;->isSetErrorCode()Z

    move-result v2

    if-nez v1, :cond_1

    if-eqz v2, :cond_3

    :cond_1
    if-eqz v1, :cond_c

    if-nez v2, :cond_2

    goto :goto_2

    .line 177
    :cond_2
    iget-object v1, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    iget-object v2, p1, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    invoke-virtual {v1, v2}, Lcom/evernote/edam/error/EDAMErrorCode;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v0

    .line 181
    :cond_3
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetMessage()Z

    move-result v1

    .line 182
    invoke-virtual {p1}, Lcom/evernote/edam/error/EDAMSystemException;->isSetMessage()Z

    move-result v2

    if-nez v1, :cond_4

    if-eqz v2, :cond_6

    :cond_4
    if-eqz v1, :cond_b

    if-nez v2, :cond_5

    goto :goto_1

    .line 186
    :cond_5
    iget-object v1, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    iget-object v2, p1, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    return v0

    .line 190
    :cond_6
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetRateLimitDuration()Z

    move-result v1

    .line 191
    invoke-virtual {p1}, Lcom/evernote/edam/error/EDAMSystemException;->isSetRateLimitDuration()Z

    move-result v2

    if-nez v1, :cond_7

    if-eqz v2, :cond_9

    :cond_7
    if-eqz v1, :cond_a

    if-nez v2, :cond_8

    goto :goto_0

    .line 195
    :cond_8
    iget v1, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    iget p1, p1, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    if-eq v1, p1, :cond_9

    return v0

    :cond_9
    const/4 p1, 0x1

    return p1

    :cond_a
    :goto_0
    return v0

    :cond_b
    :goto_1
    return v0

    :cond_c
    :goto_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 163
    :cond_0
    instance-of v1, p1, Lcom/evernote/edam/error/EDAMSystemException;

    if-eqz v1, :cond_1

    .line 164
    check-cast p1, Lcom/evernote/edam/error/EDAMSystemException;

    invoke-virtual {p0, p1}, Lcom/evernote/edam/error/EDAMSystemException;->equals(Lcom/evernote/edam/error/EDAMSystemException;)Z

    move-result p1

    return p1

    :cond_1
    return v0
.end method

.method public getErrorCode()Lcom/evernote/edam/error/EDAMErrorCode;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getRateLimitDuration()I
    .locals 1

    .line 138
    iget v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isSetErrorCode()Z
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetMessage()Z
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSetRateLimitDuration()Z
    .locals 2

    .line 152
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->__isset_vector:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    return v0
.end method

.method public read(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 247
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructBegin()Lcom/evernote/thrift/protocol/TStruct;

    .line 250
    :goto_0
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldBegin()Lcom/evernote/thrift/protocol/TField;

    move-result-object v0

    .line 251
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-nez v1, :cond_0

    .line 282
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readStructEnd()V

    .line 283
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->validate()V

    return-void

    .line 254
    :cond_0
    iget-short v1, v0, Lcom/evernote/thrift/protocol/TField;->id:S

    const/16 v2, 0x8

    packed-switch v1, :pswitch_data_0

    .line 278
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 270
    :pswitch_0
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_1

    .line 271
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    iput v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    const/4 v0, 0x1

    .line 272
    invoke-virtual {p0, v0}, Lcom/evernote/edam/error/EDAMSystemException;->setRateLimitDurationIsSet(Z)V

    goto :goto_1

    .line 274
    :cond_1
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 263
    :pswitch_1
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    const/16 v2, 0xb

    if-ne v1, v2, :cond_2

    .line 264
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    goto :goto_1

    .line 266
    :cond_2
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    goto :goto_1

    .line 256
    :pswitch_2
    iget-byte v1, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    if-ne v1, v2, :cond_3

    .line 257
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readI32()I

    move-result v0

    invoke-static {v0}, Lcom/evernote/edam/error/EDAMErrorCode;->findByValue(I)Lcom/evernote/edam/error/EDAMErrorCode;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    goto :goto_1

    .line 259
    :cond_3
    iget-byte v0, v0, Lcom/evernote/thrift/protocol/TField;->type:B

    invoke-static {p1, v0}, Lcom/evernote/thrift/protocol/TProtocolUtil;->skip(Lcom/evernote/thrift/protocol/TProtocol;B)V

    .line 280
    :goto_1
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->readFieldEnd()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setErrorCode(Lcom/evernote/edam/error/EDAMErrorCode;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    return-void
.end method

.method public setErrorCodeIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 110
    iput-object p1, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    :cond_0
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    return-void
.end method

.method public setMessageIsSet(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 133
    iput-object p1, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setRateLimitDuration(I)V
    .locals 0

    .line 142
    iput p1, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    const/4 p1, 0x1

    .line 143
    invoke-virtual {p0, p1}, Lcom/evernote/edam/error/EDAMSystemException;->setRateLimitDurationIsSet(Z)V

    return-void
.end method

.method public setRateLimitDurationIsSet(Z)V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean p1, v0, v1

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "EDAMSystemException("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "errorCode:"

    .line 316
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    iget-object v1, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    if-nez v1, :cond_0

    const-string v1, "null"

    .line 318
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 320
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 323
    :goto_0
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetMessage()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ", "

    .line 324
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "message:"

    .line 325
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 326
    iget-object v1, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "null"

    .line 327
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 329
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 333
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetRateLimitDuration()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, ", "

    .line 334
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "rateLimitDuration:"

    .line 335
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    iget v1, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, ")"

    .line 339
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unsetErrorCode()V
    .locals 1

    const/4 v0, 0x0

    .line 100
    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    return-void
.end method

.method public unsetMessage()V
    .locals 1

    const/4 v0, 0x0

    .line 123
    iput-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    return-void
.end method

.method public unsetRateLimitDuration()V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->__isset_vector:[Z

    const/4 v1, 0x0

    aput-boolean v1, v0, v1

    return-void
.end method

.method public validate()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 345
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetErrorCode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 346
    :cond_0
    new-instance v0, Lcom/evernote/thrift/protocol/TProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required field \'errorCode\' is unset! Struct:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public write(Lcom/evernote/thrift/protocol/TProtocol;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 287
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->validate()V

    .line 289
    sget-object v0, Lcom/evernote/edam/error/EDAMSystemException;->STRUCT_DESC:Lcom/evernote/thrift/protocol/TStruct;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructBegin(Lcom/evernote/thrift/protocol/TStruct;)V

    .line 290
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    if-eqz v0, :cond_0

    .line 291
    sget-object v0, Lcom/evernote/edam/error/EDAMSystemException;->ERROR_CODE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 292
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->errorCode:Lcom/evernote/edam/error/EDAMErrorCode;

    invoke-virtual {v0}, Lcom/evernote/edam/error/EDAMErrorCode;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 293
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 296
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetMessage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    sget-object v0, Lcom/evernote/edam/error/EDAMSystemException;->MESSAGE_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 298
    iget-object v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeString(Ljava/lang/String;)V

    .line 299
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 302
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/edam/error/EDAMSystemException;->isSetRateLimitDuration()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 303
    sget-object v0, Lcom/evernote/edam/error/EDAMSystemException;->RATE_LIMIT_DURATION_FIELD_DESC:Lcom/evernote/thrift/protocol/TField;

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldBegin(Lcom/evernote/thrift/protocol/TField;)V

    .line 304
    iget v0, p0, Lcom/evernote/edam/error/EDAMSystemException;->rateLimitDuration:I

    invoke-virtual {p1, v0}, Lcom/evernote/thrift/protocol/TProtocol;->writeI32(I)V

    .line 305
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldEnd()V

    .line 307
    :cond_2
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeFieldStop()V

    .line 308
    invoke-virtual {p1}, Lcom/evernote/thrift/protocol/TProtocol;->writeStructEnd()V

    return-void
.end method

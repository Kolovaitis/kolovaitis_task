.class Lcom/evernote/client/conn/mobile/TAndroidTransport$1;
.super Lcom/squareup/okhttp/RequestBody;
.source "TAndroidTransport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/conn/mobile/TAndroidTransport;->flush()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/conn/mobile/TAndroidTransport;


# direct methods
.method constructor <init>(Lcom/evernote/client/conn/mobile/TAndroidTransport;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/evernote/client/conn/mobile/TAndroidTransport$1;->this$0:Lcom/evernote/client/conn/mobile/TAndroidTransport;

    invoke-direct {p0}, Lcom/squareup/okhttp/RequestBody;-><init>()V

    return-void
.end method


# virtual methods
.method public contentType()Lcom/squareup/okhttp/MediaType;
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/TAndroidTransport$1;->this$0:Lcom/evernote/client/conn/mobile/TAndroidTransport;

    invoke-static {v0}, Lcom/evernote/client/conn/mobile/TAndroidTransport;->access$000(Lcom/evernote/client/conn/mobile/TAndroidTransport;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/evernote/client/conn/mobile/TAndroidTransport$1;->this$0:Lcom/evernote/client/conn/mobile/TAndroidTransport;

    invoke-static {v0}, Lcom/evernote/client/conn/mobile/TAndroidTransport;->access$000(Lcom/evernote/client/conn/mobile/TAndroidTransport;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "Content-Type"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/TAndroidTransport$1;->this$0:Lcom/evernote/client/conn/mobile/TAndroidTransport;

    invoke-static {v0}, Lcom/evernote/client/conn/mobile/TAndroidTransport;->access$000(Lcom/evernote/client/conn/mobile/TAndroidTransport;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "Content-Type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/okhttp/MediaType;->parse(Ljava/lang/String;)Lcom/squareup/okhttp/MediaType;

    move-result-object v0

    return-object v0

    .line 106
    :cond_0
    invoke-static {}, Lcom/evernote/client/conn/mobile/TAndroidTransport;->access$100()Lcom/squareup/okhttp/MediaType;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lokio/BufferedSink;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/TAndroidTransport$1;->this$0:Lcom/evernote/client/conn/mobile/TAndroidTransport;

    invoke-static {v0}, Lcom/evernote/client/conn/mobile/TAndroidTransport;->access$200(Lcom/evernote/client/conn/mobile/TAndroidTransport;)Lcom/evernote/client/conn/mobile/ByteStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/conn/mobile/ByteStore;->getData()[B

    move-result-object v0

    iget-object v1, p0, Lcom/evernote/client/conn/mobile/TAndroidTransport$1;->this$0:Lcom/evernote/client/conn/mobile/TAndroidTransport;

    invoke-static {v1}, Lcom/evernote/client/conn/mobile/TAndroidTransport;->access$200(Lcom/evernote/client/conn/mobile/TAndroidTransport;)Lcom/evernote/client/conn/mobile/ByteStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/evernote/client/conn/mobile/ByteStore;->getBytesWritten()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v2, v1}, Lokio/BufferedSink;->write([BII)Lokio/BufferedSink;

    return-void
.end method

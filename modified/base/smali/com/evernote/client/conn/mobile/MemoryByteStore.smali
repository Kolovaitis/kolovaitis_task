.class public Lcom/evernote/client/conn/mobile/MemoryByteStore;
.super Lcom/evernote/client/conn/mobile/ByteStore;
.source "MemoryByteStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/conn/mobile/MemoryByteStore$Factory;
    }
.end annotation


# instance fields
.field protected final mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

.field protected mBytesWritten:I

.field protected mClosed:Z

.field protected mData:[B


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 47
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/ByteStore;-><init>()V

    .line 48
    new-instance v0, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-direct {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    return-void
.end method

.method private checkNotClosed()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 68
    iget-boolean v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    if-nez v0, :cond_0

    return-void

    .line 69
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Already closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 75
    iget-boolean v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->reset()V

    const/4 v0, 0x1

    .line 77
    iput-boolean v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    :cond_0
    return-void
.end method

.method public getBytesWritten()I
    .locals 1

    .line 83
    iget v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    return v0
.end method

.method public getData()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    if-eqz v0, :cond_0

    return-object v0

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/MemoryByteStore;->close()V

    .line 94
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    .line 95
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    return-object v0
.end method

.method public reset()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 101
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/MemoryByteStore;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    .line 105
    iput v1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    .line 106
    iput-boolean v1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    return-void

    :catchall_0
    move-exception v2

    .line 104
    iput-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mData:[B

    .line 105
    iput v1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    .line 106
    iput-boolean v1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mClosed:Z

    throw v2
.end method

.method public write(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 61
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/MemoryByteStore;->checkNotClosed()V

    .line 63
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0, p1}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->write(I)V

    .line 64
    iget p1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    return-void
.end method

.method public write([BII)V
    .locals 1
    .param p1    # [B
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/MemoryByteStore;->checkNotClosed()V

    .line 55
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->write([BII)V

    .line 56
    iget p1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/evernote/client/conn/mobile/MemoryByteStore;->mBytesWritten:I

    return-void
.end method

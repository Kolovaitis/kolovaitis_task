.class public Lcom/evernote/client/conn/mobile/DiskBackedByteStore;
.super Lcom/evernote/client/conn/mobile/ByteStore;
.source "DiskBackedByteStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/conn/mobile/DiskBackedByteStore$Factory;
    }
.end annotation


# static fields
.field private static final DEFAULT_MEMORY_BUFFER_SIZE:I = 0x200000


# instance fields
.field protected final mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

.field protected mBytesWritten:I

.field protected final mCacheDir:Ljava/io/File;

.field protected mCacheFile:Ljava/io/File;

.field protected mClosed:Z

.field protected mCurrentOutputStream:Ljava/io/OutputStream;

.field protected mData:[B

.field protected mFileBuffer:[B

.field protected mFileOutputStream:Ljava/io/FileOutputStream;

.field protected final mMaxMemory:I


# direct methods
.method protected constructor <init>(Ljava/io/File;I)V
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/ByteStore;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    .line 72
    iput p2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mMaxMemory:I

    .line 73
    new-instance p1, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-direct {p1}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;-><init>()V

    iput-object p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    .line 74
    iget-object p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    iput-object p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    return-void
.end method

.method private initBuffers()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 96
    iget-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    if-nez v0, :cond_2

    .line 100
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    if-nez v0, :cond_1

    .line 101
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapped()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    :cond_1
    :goto_0
    return-void

    .line 97
    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Already closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private isSwapRequired(I)Z
    .locals 1

    .line 116
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapped()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    add-int/2addr v0, p1

    iget p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mMaxMemory:I

    if-le v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private static readFile(Ljava/io/File;[BI)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 199
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 p0, 0x0

    const/4 v0, 0x0

    :goto_0
    if-lez p2, :cond_0

    if-ltz p0, :cond_0

    .line 205
    :try_start_1
    invoke-virtual {v1, p1, v0, p2}, Ljava/io/InputStream;->read([BII)I

    move-result p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/2addr v0, p0

    sub-int/2addr p2, p0

    goto :goto_0

    :catchall_0
    move-exception p0

    move-object v0, v1

    goto :goto_1

    .line 212
    :cond_0
    invoke-static {v1}, Lcom/squareup/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    return-void

    :catchall_1
    move-exception p0

    :goto_1
    invoke-static {v0}, Lcom/squareup/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    throw p0
.end method

.method private swapIfNecessary(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 110
    invoke-direct {p0, p1}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->isSwapRequired(I)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapToDisk()V

    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 142
    iget-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 144
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->reset()V

    const/4 v0, 0x1

    .line 145
    iput-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    :cond_0
    return-void
.end method

.method public getBytesWritten()I
    .locals 1

    .line 151
    iget v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    return v0
.end method

.method public getData()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    if-eqz v0, :cond_0

    return-object v0

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->close()V

    .line 162
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapped()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileBuffer:[B

    if-eqz v0, :cond_1

    array-length v0, v0

    iget v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    if-ge v0, v1, :cond_2

    .line 164
    :cond_1
    iget v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileBuffer:[B

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileBuffer:[B

    iget v2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    invoke-static {v0, v1, v2}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->readFile(Ljava/io/File;[BI)V

    .line 168
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileBuffer:[B

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    goto :goto_0

    .line 171
    :cond_3
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    .line 174
    :goto_0
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    return-object v0
.end method

.method public reset()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 180
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->close()V

    .line 182
    iget-object v2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 183
    iget-object v2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 184
    :cond_0
    new-instance v2, Ljava/io/IOException;

    const-string v3, "could not delete cache file"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :cond_1
    :goto_0
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 189
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 190
    iput v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 191
    iput-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    .line 192
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    return-void

    :catchall_0
    move-exception v2

    .line 188
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 189
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    .line 190
    iput v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    .line 191
    iput-boolean v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mClosed:Z

    .line 192
    iput-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mData:[B

    throw v2
.end method

.method protected swapToDisk()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 125
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "could not create cache dir"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "byte_store"

    const/4 v1, 0x0

    .line 131
    iget-object v2, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheDir:Ljava/io/File;

    invoke-static {v0, v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    .line 132
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCacheFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    .line 134
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    invoke-virtual {v0, v1}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->writeTo(Ljava/io/OutputStream;)V

    .line 135
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mByteArrayOutputStream:Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;

    invoke-virtual {v0}, Lcom/evernote/client/conn/mobile/LazyByteArrayOutputStream;->reset()V

    .line 137
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mFileOutputStream:Ljava/io/FileOutputStream;

    iput-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    return-void

    .line 128
    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "cache dir is no directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected swapped()Z
    .locals 2

    .line 120
    iget v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    iget v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mMaxMemory:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public write(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 88
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->initBuffers()V

    const/4 v0, 0x1

    .line 89
    invoke-direct {p0, v0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapIfNecessary(I)V

    .line 91
    iget-object v1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write(I)V

    .line 92
    iget p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    return-void
.end method

.method public write([BII)V
    .locals 1
    .param p1    # [B
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 79
    invoke-direct {p0}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->initBuffers()V

    .line 80
    invoke-direct {p0, p3}, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->swapIfNecessary(I)V

    .line 82
    iget-object v0, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mCurrentOutputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 83
    iget p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/evernote/client/conn/mobile/DiskBackedByteStore;->mBytesWritten:I

    return-void
.end method

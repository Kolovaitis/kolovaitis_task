.class public Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;
.super Landroid/support/v4/app/Fragment;
.source "EvernoteOAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/client/android/EvernoteOAuthActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WebViewFragment"
.end annotation


# static fields
.field private static final INTENT_KEY:Ljava/lang/String; = "IntentKey"


# instance fields
.field private mIsWebViewAvailable:Z

.field private mUrl:Ljava/lang/String;

.field private mWebView:Landroid/webkit/WebView;

.field private mWebViewClient:Landroid/webkit/WebViewClient;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 81
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 172
    new-instance v0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment$1;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment$1;-><init>(Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;)V

    iput-object v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebViewClient:Landroid/webkit/WebViewClient;

    return-void
.end method

.method public static createInstance()Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;
    .locals 2

    .line 86
    new-instance v0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;

    invoke-direct {v0}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;-><init>()V

    const/4 v1, 0x1

    .line 87
    invoke-virtual {v0, v1}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->setRetainInstance(Z)V

    return-object v0
.end method

.method private destroyWebView()V
    .locals 2

    .line 161
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_1

    .line 162
    invoke-virtual {v0}, Landroid/webkit/WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 164
    iget-object v1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    const/4 v0, 0x0

    .line 168
    iput-object v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    :cond_1
    return-void
.end method


# virtual methods
.method public getWebView()Landroid/webkit/WebView;
    .locals 1

    .line 157
    iget-boolean v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mIsWebViewAvailable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .line 98
    instance-of v0, p1, Lcom/evernote/client/android/EvernoteOAuthActivity;

    if-eqz v0, :cond_0

    .line 102
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 104
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "authorization_url"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mUrl:Ljava/lang/String;

    return-void

    .line 99
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 0
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .line 110
    invoke-direct {p0}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->destroyWebView()V

    .line 112
    new-instance p1, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p2

    invoke-direct {p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    .line 113
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    iget-object p2, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebViewClient:Landroid/webkit/WebViewClient;

    invoke-virtual {p1, p2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 114
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    if-nez p3, :cond_0

    .line 117
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    iget-object p3, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, p3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_0
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p1, p3}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 122
    :goto_0
    iput-boolean p2, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mIsWebViewAvailable:Z

    .line 123
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    return-object p1
.end method

.method public onDestroy()V
    .locals 0

    .line 146
    invoke-direct {p0}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->destroyWebView()V

    .line 147
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    const/4 v0, 0x0

    .line 140
    iput-boolean v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mIsWebViewAvailable:Z

    .line 141
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 128
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 129
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    .line 135
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .line 152
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 153
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    return-void
.end method

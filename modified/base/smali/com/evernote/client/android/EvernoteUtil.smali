.class public final Lcom/evernote/client/android/EvernoteUtil;
.super Ljava/lang/Object;
.source "EvernoteUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/EvernoteUtil$EvernoteUtilException;,
        Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;
    }
.end annotation


# static fields
.field public static final ACTION_AUTHORIZE:Ljava/lang/String; = "com.evernote.action.AUTHORIZE"

.field public static final ACTION_GET_BOOTSTRAP_PROFILE_NAME:Ljava/lang/String; = "com.evernote.action.GET_BOOTSTRAP_PROFILE_NAME"

.field private static final CAT:Lcom/evernote/client/android/helper/Cat;

.field private static final EDAM_HASH_ALGORITHM:Ljava/lang/String; = "MD5"

.field private static final EVERNOTE_SIGNATURE:Ljava/lang/String; = "XS7HhF3x8-kho4iOnAQIdP7_m4UsbRKgaEAr1HaXwnc="

.field public static final EXTRA_AUTHORIZATION_URL:Ljava/lang/String; = "authorization_url"

.field public static final EXTRA_BOOTSTRAP_PROFILE_NAME:Ljava/lang/String; = "bootstrap_profile_name"

.field public static final EXTRA_OAUTH_CALLBACK_URL:Ljava/lang/String; = "oauth_callback_url"

.field private static final HASH_DIGEST:Ljava/security/MessageDigest;

.field public static final NOTE_PREFIX:Ljava/lang/String; = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE en-note SYSTEM \"http://xml.evernote.com/pub/enml2.dtd\"><en-note>"

.field public static final NOTE_SUFFIX:Ljava/lang/String; = "</en-note>"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.evernote"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 57
    new-instance v0, Lcom/evernote/client/android/helper/Cat;

    const-string v1, "EvernoteUtil"

    invoke-direct {v0, v1}, Lcom/evernote/client/android/helper/Cat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/client/android/EvernoteUtil;->CAT:Lcom/evernote/client/android/helper/Cat;

    :try_start_0
    const-string v0, "MD5"

    .line 113
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    .line 118
    :goto_0
    sput-object v0, Lcom/evernote/client/android/EvernoteUtil;->HASH_DIGEST:Ljava/security/MessageDigest;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bytesToHex([B)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 160
    invoke-static {p0, v0}, Lcom/evernote/client/android/EvernoteUtil;->bytesToHex([BZ)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static bytesToHex([BZ)Ljava/lang/String;
    .locals 5

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-byte v3, p0, v2

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0x10

    if-ge v3, v4, :cond_0

    const/16 v4, 0x30

    .line 175
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 177
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_1

    const/16 v3, 0x20

    .line 179
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 182
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createAuthorizationIntent(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 1

    if-nez p2, :cond_0

    .line 317
    sget-object p2, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->INSTALLED:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    const-string v0, "com.evernote.action.AUTHORIZE"

    invoke-static {p0, v0}, Lcom/evernote/client/android/EvernoteUtil;->getEvernoteInstallStatus(Landroid/content/Context;Ljava/lang/String;)Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 318
    new-instance p0, Landroid/content/Intent;

    const-string p2, "com.evernote.action.AUTHORIZE"

    invoke-direct {p0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p2, "com.evernote"

    .line 319
    invoke-virtual {p0, p2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 321
    :cond_0
    new-instance p2, Landroid/content/Intent;

    const-class v0, Lcom/evernote/client/android/EvernoteOAuthActivity;

    invoke-direct {p2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object p0, p2

    :goto_0
    const-string p2, "authorization_url"

    .line 324
    invoke-virtual {p0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public static createEnMediaTag(Lcom/evernote/edam/type/Resource;)Ljava/lang/String;
    .locals 2

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<en-media hash=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->getData()Lcom/evernote/edam/type/Data;

    move-result-object v1

    invoke-virtual {v1}, Lcom/evernote/edam/type/Data;->getBodyHash()[B

    move-result-object v1

    invoke-static {v1}, Lcom/evernote/client/android/EvernoteUtil;->bytesToHex([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\" type=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/evernote/edam/type/Resource;->getMime()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\"/>"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static createGetBootstrapProfileNameIntent(Landroid/content/Context;Lcom/evernote/client/android/EvernoteSession;)Landroid/content/Intent;
    .locals 1

    .line 339
    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession;->isForceAuthenticationInThirdPartyApp()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    return-object v0

    :cond_0
    const-string p1, "com.evernote.action.GET_BOOTSTRAP_PROFILE_NAME"

    .line 344
    invoke-static {p0, p1}, Lcom/evernote/client/android/EvernoteUtil;->getEvernoteInstallStatus(Landroid/content/Context;Ljava/lang/String;)Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    move-result-object p0

    .line 345
    sget-object p1, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->INSTALLED:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    invoke-virtual {p1, p0}, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_1

    return-object v0

    .line 349
    :cond_1
    new-instance p0, Landroid/content/Intent;

    const-string p1, "com.evernote.action.GET_BOOTSTRAP_PROFILE_NAME"

    invoke-direct {p0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p1, "com.evernote"

    invoke-virtual {p0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    return-object p0
.end method

.method private static encodeBase64([B)Ljava/lang/String;
    .locals 1

    const/16 v0, 0xa

    .line 409
    invoke-static {p0, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static generateUserAgentString(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 362
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 363
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    invoke-virtual {p0, v1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0

    iget v0, p0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p0

    goto :goto_0

    :catch_1
    move-exception p0

    const/4 v1, 0x0

    .line 366
    :goto_0
    sget-object v2, Lcom/evernote/client/android/EvernoteUtil;->CAT:Lcom/evernote/client/android/helper/Cat;

    invoke-virtual {p0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/evernote/client/android/helper/Cat;->e(Ljava/lang/String;)V

    .line 369
    :goto_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " Android/"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 371
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    if-nez v0, :cond_0

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " ("

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, ");"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    .line 375
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, " ("

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "); "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 377
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "Android/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "; "

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object p0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ";"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getEvernoteInstallStatus(Landroid/content/Context;Ljava/lang/String;)Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;
    .locals 1

    .line 257
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p0

    .line 258
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p1, "com.evernote"

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p1

    const/high16 v0, 0x10000

    .line 260
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object p1

    .line 261
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    .line 262
    invoke-static {p0}, Lcom/evernote/client/android/EvernoteUtil;->validateSignature(Landroid/content/pm/PackageManager;)Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    move-result-object p0

    return-object p0

    :cond_0
    :try_start_0
    const-string p1, "com.evernote"

    const/4 v0, 0x1

    .line 267
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 268
    sget-object p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->OLD_VERSION:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 271
    :catch_0
    sget-object p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->NOT_INSTALLED:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    return-object p0
.end method

.method private static getSha256Digest()Ljava/security/MessageDigest;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    :try_start_0
    const-string v0, "SHA-256"

    .line 402
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public static hash(Ljava/io/InputStream;)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 143
    sget-object v0, Lcom/evernote/client/android/EvernoteUtil;->HASH_DIGEST:Ljava/security/MessageDigest;

    if-eqz v0, :cond_1

    const/16 v0, 0x400

    .line 147
    new-array v0, v0, [B

    .line 149
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 150
    sget-object v2, Lcom/evernote/client/android/EvernoteUtil;->HASH_DIGEST:Ljava/security/MessageDigest;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v1}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_0

    .line 152
    :cond_0
    sget-object p0, Lcom/evernote/client/android/EvernoteUtil;->HASH_DIGEST:Ljava/security/MessageDigest;

    invoke-virtual {p0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object p0

    return-object p0

    .line 144
    :cond_1
    new-instance p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteUtilException;

    new-instance v0, Ljava/security/NoSuchAlgorithmException;

    const-string v1, "MD5"

    invoke-direct {v0, v1}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    const-string v1, "MD5 not supported"

    invoke-direct {p0, v1, v0}, Lcom/evernote/client/android/EvernoteUtil$EvernoteUtilException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p0
.end method

.method public static hash([B)[B
    .locals 2

    .line 132
    sget-object v0, Lcom/evernote/client/android/EvernoteUtil;->HASH_DIGEST:Ljava/security/MessageDigest;

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {v0, p0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p0

    return-object p0

    .line 135
    :cond_0
    new-instance p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteUtilException;

    new-instance v0, Ljava/security/NoSuchAlgorithmException;

    const-string v1, "MD5"

    invoke-direct {v0, v1}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    const-string v1, "MD5 not supported"

    invoke-direct {p0, v1, v0}, Lcom/evernote/client/android/EvernoteUtil$EvernoteUtilException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p0
.end method

.method public static hexToBytes(Ljava/lang/String;)[B
    .locals 4

    .line 192
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    new-array v0, v0, [B

    const/4 v1, 0x0

    .line 193
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    mul-int/lit8 v2, v1, 0x2

    add-int/lit8 v3, v2, 0x2

    .line 195
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static removeAllCookies(Landroid/content/Context;)V
    .locals 2

    .line 205
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 206
    invoke-static {}, Lcom/evernote/client/android/EvernoteUtil;->removeAllCookiesV21()V

    goto :goto_0

    .line 208
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, Lcom/evernote/client/android/EvernoteUtil;->removeAllCookiesV14(Landroid/content/Context;)V

    :goto_0
    return-void
.end method

.method private static removeAllCookiesV14(Landroid/content/Context;)V
    .locals 0

    .line 214
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 215
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object p0

    .line 216
    invoke-virtual {p0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    return-void
.end method

.method private static removeAllCookiesV21()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 221
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 223
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-nez v1, :cond_0

    .line 226
    invoke-static {}, Landroid/os/Looper;->prepare()V

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 231
    :goto_0
    new-instance v2, Lcom/evernote/client/android/EvernoteUtil$1;

    invoke-direct {v2, v0}, Lcom/evernote/client/android/EvernoteUtil$1;-><init>(Landroid/webkit/CookieManager;)V

    invoke-virtual {v0, v2}, Landroid/webkit/CookieManager;->removeAllCookies(Landroid/webkit/ValueCallback;)V

    if-eqz v1, :cond_1

    .line 246
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 248
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_1
    return-void
.end method

.method private static validateSignature(Landroid/content/pm/PackageManager;)Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PackageManagerGetSignatures"
        }
    .end annotation

    .line 277
    invoke-static {}, Lcom/evernote/client/android/EvernoteUtil;->getSha256Digest()Ljava/security/MessageDigest;

    move-result-object v0

    if-nez v0, :cond_0

    .line 280
    sget-object p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->NOT_INSTALLED:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    return-object p0

    :cond_0
    :try_start_0
    const-string v1, "com.evernote"

    const/16 v2, 0x40

    .line 285
    invoke-virtual {p0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    if-nez v1, :cond_1

    goto :goto_1

    .line 296
    :cond_1
    iget-object p0, p0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, p0, v2

    .line 297
    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 298
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    invoke-static {v3}, Lcom/evernote/client/android/EvernoteUtil;->encodeBase64([B)Ljava/lang/String;

    move-result-object v3

    const-string v4, "XS7HhF3x8-kho4iOnAQIdP7_m4UsbRKgaEAr1HaXwnc="

    .line 299
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 300
    sget-object p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->INSTALLED:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    return-object p0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 303
    :cond_3
    sget-object p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->NOT_INSTALLED:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    return-object p0

    .line 292
    :cond_4
    :goto_1
    sget-object p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->NOT_INSTALLED:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    return-object p0

    .line 287
    :catch_0
    sget-object p0, Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;->NOT_INSTALLED:Lcom/evernote/client/android/EvernoteUtil$EvernoteInstallStatus;

    return-object p0
.end method

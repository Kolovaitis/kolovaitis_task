.class Lcom/evernote/client/android/BootstrapManager;
.super Ljava/lang/Object;
.source "BootstrapManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/BootstrapManager$ClientUnsupportedException;,
        Lcom/evernote/client/android/BootstrapManager$BootstrapInfoWrapper;
    }
.end annotation


# static fields
.field public static final CHINA_LOCALES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOGTAG:Ljava/lang/String; = "EvernoteSession"


# instance fields
.field private mBootstrapServerUrls:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mBootstrapServerUsed:Ljava/lang/String;

.field private final mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

.field private mLocale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    .line 60
    new-array v0, v0, [Ljava/util/Locale;

    sget-object v1, Ljava/util/Locale;->TRADITIONAL_CHINESE:Ljava/util/Locale;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Ljava/util/Locale;->SIMPLIFIED_CHINESE:Ljava/util/Locale;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/evernote/client/android/BootstrapManager;->CHINA_LOCALES:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lcom/evernote/client/android/EvernoteSession$EvernoteService;Lcom/evernote/client/android/EvernoteSession;)V
    .locals 1

    .line 80
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/evernote/client/android/BootstrapManager;-><init>(Lcom/evernote/client/android/EvernoteSession$EvernoteService;Lcom/evernote/client/android/EvernoteSession;Ljava/util/Locale;)V

    return-void
.end method

.method constructor <init>(Lcom/evernote/client/android/EvernoteSession$EvernoteService;Lcom/evernote/client/android/EvernoteSession;Ljava/util/Locale;)V
    .locals 1

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUrls:Ljava/util/ArrayList;

    .line 91
    iput-object p2, p0, Lcom/evernote/client/android/BootstrapManager;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 92
    iput-object p3, p0, Lcom/evernote/client/android/BootstrapManager;->mLocale:Ljava/util/Locale;

    .line 94
    iget-object p2, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUrls:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 95
    sget-object p2, Lcom/evernote/client/android/BootstrapManager$1;->$SwitchMap$com$evernote$client$android$EvernoteSession$EvernoteService:[I

    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession$EvernoteService;->ordinal()I

    move-result p1

    aget p1, p2, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 104
    :pswitch_0
    iget-object p1, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUrls:Ljava/util/ArrayList;

    const-string p2, "https://sandbox.evernote.com"

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 97
    :pswitch_1
    sget-object p1, Lcom/evernote/client/android/BootstrapManager;->CHINA_LOCALES:Ljava/util/List;

    iget-object p2, p0, Lcom/evernote/client/android/BootstrapManager;->mLocale:Ljava/util/Locale;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 98
    iget-object p1, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUrls:Ljava/util/ArrayList;

    const-string p2, "https://app.yinxiang.com"

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_0
    iget-object p1, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUrls:Ljava/util/ArrayList;

    const-string p2, "https://www.evernote.com"

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>(Lcom/evernote/client/android/EvernoteSession;)V
    .locals 1

    .line 76
    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteService()Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/evernote/client/android/BootstrapManager;-><init>(Lcom/evernote/client/android/EvernoteSession$EvernoteService;Lcom/evernote/client/android/EvernoteSession;)V

    return-void
.end method

.method private getUserStoreUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/edam/user"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private initializeUserStoreAndCheckVersion()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "1.25"

    .line 121
    iget-object v1, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUrls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v4, 0x1

    add-int/2addr v2, v4

    .line 124
    :try_start_0
    iget-object v5, p0, Lcom/evernote/client/android/BootstrapManager;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v5}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object v5

    invoke-direct {p0, v3}, Lcom/evernote/client/android/BootstrapManager;->getUserStoreUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    move-result-object v5

    .line 126
    iget-object v6, p0, Lcom/evernote/client/android/BootstrapManager;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v6}, Lcom/evernote/client/android/EvernoteSession;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/evernote/client/android/EvernoteUtil;->generateUserAgentString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x19

    invoke-virtual {v5, v6, v4, v7}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->checkVersion(Ljava/lang/String;SS)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 132
    iput-object v3, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUsed:Ljava/lang/String;

    return-void

    .line 129
    :cond_0
    new-instance v4, Lcom/evernote/client/android/BootstrapManager$ClientUnsupportedException;

    invoke-direct {v4, v0}, Lcom/evernote/client/android/BootstrapManager$ClientUnsupportedException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Lcom/evernote/client/android/BootstrapManager$ClientUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v4

    .line 139
    iget-object v5, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUrls:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    const-string v5, "EvernoteSession"

    .line 140
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error contacting bootstrap server="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 142
    :cond_1
    throw v4

    :catch_1
    move-exception v0

    const-string v1, "EvernoteSession"

    const-string v2, "Invalid Version"

    .line 136
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 137
    throw v0

    :cond_2
    return-void
.end method


# virtual methods
.method getBootstrapInfo()Lcom/evernote/client/android/BootstrapManager$BootstrapInfoWrapper;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "EvernoteSession"

    const-string v1, "getBootstrapInfo()"

    .line 156
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    .line 159
    :try_start_0
    iget-object v1, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUsed:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 160
    invoke-direct {p0}, Lcom/evernote/client/android/BootstrapManager;->initializeUserStoreAndCheckVersion()V

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/evernote/client/android/BootstrapManager;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUsed:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/evernote/client/android/BootstrapManager;->getUserStoreUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    move-result-object v1

    iget-object v2, p0, Lcom/evernote/client/android/BootstrapManager;->mLocale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->getBootstrapInfo(Ljava/lang/String;)Lcom/evernote/edam/userstore/BootstrapInfo;

    move-result-object v0

    .line 164
    invoke-virtual {p0, v0}, Lcom/evernote/client/android/BootstrapManager;->printBootstrapInfo(Lcom/evernote/edam/userstore/BootstrapInfo;)V
    :try_end_0
    .catch Lcom/evernote/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "EvernoteSession"

    const-string v3, "error getting bootstrap info"

    .line 167
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 170
    :goto_0
    new-instance v1, Lcom/evernote/client/android/BootstrapManager$BootstrapInfoWrapper;

    iget-object v2, p0, Lcom/evernote/client/android/BootstrapManager;->mBootstrapServerUsed:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/evernote/client/android/BootstrapManager$BootstrapInfoWrapper;-><init>(Ljava/lang/String;Lcom/evernote/edam/userstore/BootstrapInfo;)V

    return-object v1
.end method

.method printBootstrapInfo(Lcom/evernote/edam/userstore/BootstrapInfo;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "EvernoteSession"

    const-string v1, "printBootstrapInfo"

    .line 180
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapInfo;->getProfiles()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 183
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/edam/userstore/BootstrapProfile;

    const-string v1, "EvernoteSession"

    .line 184
    invoke-virtual {v0}, Lcom/evernote/edam/userstore/BootstrapProfile;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string p1, "EvernoteSession"

    const-string v0, "Profiles are null"

    .line 187
    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

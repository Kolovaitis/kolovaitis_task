.class Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment$1;
.super Landroid/webkit/WebViewClient;
.source "EvernoteOAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment$1;->this$0:Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2

    .line 176
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "en-oauth"

    .line 177
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment$1;->this$0:Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;

    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/EvernoteOAuthActivity;

    invoke-static {p1, p2}, Lcom/evernote/client/android/EvernoteOAuthActivity;->access$000(Lcom/evernote/client/android/EvernoteOAuthActivity;Ljava/lang/String;)V

    .line 179
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment$1;->this$0:Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;

    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    const/4 p1, 0x1

    return p1

    .line 183
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

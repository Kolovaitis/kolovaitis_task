.class public final Lcom/evernote/client/android/EvernoteSession;
.super Ljava/lang/Object;
.source "EvernoteSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/EvernoteSession$Builder;,
        Lcom/evernote/client/android/EvernoteSession$EvernoteService;
    }
.end annotation


# static fields
.field private static final CAT:Lcom/evernote/client/android/helper/Cat;

.field public static final HOST_CHINA:Ljava/lang/String; = "https://app.yinxiang.com"

.field public static final HOST_PRODUCTION:Ljava/lang/String; = "https://www.evernote.com"

.field public static final HOST_SANDBOX:Ljava/lang/String; = "https://sandbox.evernote.com"

.field public static final REQUEST_CODE_LOGIN:I = 0x3836

.field public static final SCREEN_NAME_INTERNATIONAL:Ljava/lang/String; = "Evernote International"

.field public static final SCREEN_NAME_YXBIJI:Ljava/lang/String; = "\u5370\u8c61\u7b14\u8bb0"

.field private static sInstance:Lcom/evernote/client/android/EvernoteSession;


# instance fields
.field private mApplicationContext:Landroid/content/Context;

.field private mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

.field private mConsumerKey:Ljava/lang/String;

.field private mConsumerSecret:Ljava/lang/String;

.field private mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

.field private mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

.field private mFactoryThreadLocal:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;",
            ">;"
        }
    .end annotation
.end field

.field private mForceAuthenticationInThirdPartyApp:Z

.field private mLocale:Ljava/util/Locale;

.field private mSupportAppLinkedNotebooks:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 90
    new-instance v0, Lcom/evernote/client/android/helper/Cat;

    const-string v1, "EvernoteSession"

    invoke-direct {v0, v1}, Lcom/evernote/client/android/helper/Cat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/client/android/EvernoteSession;->CAT:Lcom/evernote/client/android/helper/Cat;

    const/4 v0, 0x0

    .line 92
    sput-object v0, Lcom/evernote/client/android/EvernoteSession;->sInstance:Lcom/evernote/client/android/EvernoteSession;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/evernote/client/android/EvernoteSession$1;)V
    .locals 0

    .line 74
    invoke-direct {p0}, Lcom/evernote/client/android/EvernoteSession;-><init>()V

    return-void
.end method

.method static synthetic access$102(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerKey:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerSecret:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/AuthenticationResult;)Lcom/evernote/client/android/AuthenticationResult;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    return-object p1
.end method

.method static synthetic access$402(Lcom/evernote/client/android/EvernoteSession;Landroid/content/Context;)Landroid/content/Context;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mApplicationContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$502(Lcom/evernote/client/android/EvernoteSession;Ljava/util/Locale;)Ljava/util/Locale;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mLocale:Ljava/util/Locale;

    return-object p1
.end method

.method static synthetic access$602(Lcom/evernote/client/android/EvernoteSession;Z)Z
    .locals 0

    .line 74
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession;->mSupportAppLinkedNotebooks:Z

    return p1
.end method

.method static synthetic access$702(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/EvernoteSession$EvernoteService;)Lcom/evernote/client/android/EvernoteSession$EvernoteService;
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    return-object p1
.end method

.method static synthetic access$802(Lcom/evernote/client/android/EvernoteSession;Z)Z
    .locals 0

    .line 74
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession;->mForceAuthenticationInThirdPartyApp:Z

    return p1
.end method

.method public static getInstance()Lcom/evernote/client/android/EvernoteSession;
    .locals 1

    .line 95
    sget-object v0, Lcom/evernote/client/android/EvernoteSession;->sInstance:Lcom/evernote/client/android/EvernoteSession;

    return-object v0
.end method


# virtual methods
.method public asSingleton()Lcom/evernote/client/android/EvernoteSession;
    .locals 0

    .line 240
    sput-object p0, Lcom/evernote/client/android/EvernoteSession;->sInstance:Lcom/evernote/client/android/EvernoteSession;

    return-object p0
.end method

.method public authenticate(Landroid/app/Activity;)V
    .locals 4

    .line 230
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerSecret:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/evernote/client/android/EvernoteSession;->mSupportAppLinkedNotebooks:Z

    iget-object v3, p0, Lcom/evernote/client/android/EvernoteSession;->mLocale:Ljava/util/Locale;

    invoke-static {p1, v0, v1, v2, v3}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Locale;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3836

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public authenticate(Landroid/support/v4/app/FragmentActivity;)V
    .locals 4

    .line 207
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/evernote/client/android/EvernoteSession;->mConsumerSecret:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/evernote/client/android/EvernoteSession;->mSupportAppLinkedNotebooks:Z

    iget-object v3, p0, Lcom/evernote/client/android/EvernoteSession;->mLocale:Ljava/util/Locale;

    invoke-static {v0, v1, v2, v3}, Lcom/evernote/client/android/login/EvernoteLoginFragment;->create(Ljava/lang/String;Ljava/lang/String;ZLjava/util/Locale;)Lcom/evernote/client/android/login/EvernoteLoginFragment;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/evernote/client/android/EvernoteSession;->authenticate(Landroid/support/v4/app/FragmentActivity;Lcom/evernote/client/android/login/EvernoteLoginFragment;)V

    return-void
.end method

.method public authenticate(Landroid/support/v4/app/FragmentActivity;Lcom/evernote/client/android/login/EvernoteLoginFragment;)V
    .locals 1

    .line 214
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    const-string v0, "EvernoteDialogFragment"

    invoke-virtual {p2, p1, v0}, Lcom/evernote/client/android/login/EvernoteLoginFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public getApplicationContext()Landroid/content/Context;
    .locals 1

    .line 167
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mApplicationContext:Landroid/content/Context;

    return-object v0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {v0}, Lcom/evernote/client/android/AuthenticationResult;->getAuthToken()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAuthenticationResult()Lcom/evernote/client/android/AuthenticationResult;
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    return-object v0
.end method

.method public declared-synchronized getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;
    .locals 2

    monitor-enter p0

    .line 140
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    if-nez v0, :cond_1

    .line 144
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;-><init>(Lcom/evernote/client/android/EvernoteSession;)V

    iput-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    if-nez v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;->build()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :cond_2
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getEvernoteService()Lcom/evernote/client/android/EvernoteSession$EvernoteService;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    return-object v0
.end method

.method isForceAuthenticationInThirdPartyApp()Z
    .locals 1

    .line 276
    iget-boolean v0, p0, Lcom/evernote/client/android/EvernoteSession;->mForceAuthenticationInThirdPartyApp:Z

    return v0
.end method

.method public declared-synchronized isLoggedIn()Z
    .locals 1

    monitor-enter p0

    .line 253
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized logOut()Z
    .locals 1

    monitor-enter p0

    .line 264
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteSession;->isLoggedIn()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 265
    monitor-exit p0

    return v0

    .line 268
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    invoke-virtual {v0}, Lcom/evernote/client/android/AuthenticationResult;->clear()V

    const/4 v0, 0x0

    .line 269
    iput-object v0, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;

    .line 271
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteSession;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/evernote/client/android/EvernoteUtil;->removeAllCookies(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    .line 272
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized setAuthenticationResult(Lcom/evernote/client/android/AuthenticationResult;)V
    .locals 0

    monitor-enter p0

    .line 245
    :try_start_0
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mAuthenticationResult:Lcom/evernote/client/android/AuthenticationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized setEvernoteClientFactoryBuilder(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;)V
    .locals 0

    monitor-enter p0

    .line 159
    :try_start_0
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mEvernoteClientFactoryBuilder:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;

    const/4 p1, 0x0

    .line 160
    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession;->mFactoryThreadLocal:Ljava/lang/ThreadLocal;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

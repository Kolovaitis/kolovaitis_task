.class public Lcom/evernote/client/android/EvernoteOAuthActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "EvernoteOAuthActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;
    }
.end annotation


# static fields
.field private static final CAT:Lcom/evernote/client/android/helper/Cat;

.field private static final HOST_CHINA:Ljava/lang/String; = "app.yinxiang.com"

.field private static final HOST_EVERNOTE:Ljava/lang/String; = "www.evernote.com"

.field private static final HOST_SANDBOX:Ljava/lang/String; = "sandbox.evernote.com"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 29
    new-instance v0, Lcom/evernote/client/android/helper/Cat;

    const-string v1, "EvernoteOAuthActivity"

    invoke-direct {v0, v1}, Lcom/evernote/client/android/helper/Cat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/client/android/EvernoteOAuthActivity;->CAT:Lcom/evernote/client/android/helper/Cat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/evernote/client/android/EvernoteOAuthActivity;Ljava/lang/String;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/evernote/client/android/EvernoteOAuthActivity;->setResultUri(Ljava/lang/String;)V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .line 36
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/evernote/client/android/EvernoteOAuthActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "authorization_url"

    .line 37
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private setResultUri(Ljava/lang/String;)V
    .locals 2

    .line 76
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "oauth_callback_url"

    .line 77
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/evernote/client/android/EvernoteOAuthActivity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 43
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, v0}, Lcom/evernote/client/android/EvernoteOAuthActivity;->setResultUri(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteOAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authorization_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    sget-object p1, Lcom/evernote/client/android/EvernoteOAuthActivity;->CAT:Lcom/evernote/client/android/helper/Cat;

    const-string v0, "no uri passed, return cancelled"

    invoke-virtual {p1, v0}, Lcom/evernote/client/android/helper/Cat;->w(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteOAuthActivity;->finish()V

    return-void

    .line 54
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "https"

    .line 55
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 56
    sget-object p1, Lcom/evernote/client/android/EvernoteOAuthActivity;->CAT:Lcom/evernote/client/android/helper/Cat;

    const-string v0, "https required, return cancelled"

    invoke-virtual {p1, v0}, Lcom/evernote/client/android/helper/Cat;->w(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteOAuthActivity;->finish()V

    return-void

    .line 61
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "www.evernote.com"

    .line 62
    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "sandbox.evernote.com"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "app.yinxiang.com"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 63
    sget-object p1, Lcom/evernote/client/android/EvernoteOAuthActivity;->CAT:Lcom/evernote/client/android/helper/Cat;

    const-string v0, "unacceptable host, return cancelled"

    invoke-virtual {p1, v0}, Lcom/evernote/client/android/helper/Cat;->w(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteOAuthActivity;->finish()V

    return-void

    :cond_2
    if-nez p1, :cond_3

    .line 69
    invoke-virtual {p0}, Lcom/evernote/client/android/EvernoteOAuthActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    const v0, 0x1020002

    new-instance v1, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;

    invoke-direct {v1}, Lcom/evernote/client/android/EvernoteOAuthActivity$WebViewFragment;-><init>()V

    .line 70
    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_3
    return-void
.end method

.class public Lcom/evernote/client/android/EvernoteSession$Builder;
.super Ljava/lang/Object;
.source "EvernoteSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/client/android/EvernoteSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

.field private mForceAuthenticationInThirdPartyApp:Z

.field private mLocale:Ljava/util/Locale;

.field private mSupportAppLinkedNotebooks:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mContext:Landroid/content/Context;

    const/4 p1, 0x1

    .line 339
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mSupportAppLinkedNotebooks:Z

    .line 340
    sget-object p1, Lcom/evernote/client/android/EvernoteSession$EvernoteService;->SANDBOX:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 342
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mLocale:Ljava/util/Locale;

    return-void
.end method

.method private build(Lcom/evernote/client/android/EvernoteSession;)Lcom/evernote/client/android/EvernoteSession;
    .locals 1

    .line 437
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mContext:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$402(Lcom/evernote/client/android/EvernoteSession;Landroid/content/Context;)Landroid/content/Context;

    .line 438
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mLocale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$502(Lcom/evernote/client/android/EvernoteSession;Ljava/util/Locale;)Ljava/util/Locale;

    .line 439
    iget-boolean v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mSupportAppLinkedNotebooks:Z

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$602(Lcom/evernote/client/android/EvernoteSession;Z)Z

    .line 440
    iget-object v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$702(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/EvernoteSession$EvernoteService;)Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    .line 441
    iget-boolean v0, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mForceAuthenticationInThirdPartyApp:Z

    invoke-static {p1, v0}, Lcom/evernote/client/android/EvernoteSession;->access$802(Lcom/evernote/client/android/EvernoteSession;Z)Z

    return-object p1
.end method


# virtual methods
.method public build(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/EvernoteSession;
    .locals 2

    .line 413
    new-instance v0, Lcom/evernote/client/android/EvernoteSession;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/evernote/client/android/EvernoteSession;-><init>(Lcom/evernote/client/android/EvernoteSession$1;)V

    .line 414
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/evernote/client/android/EvernoteSession;->access$102(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;)Ljava/lang/String;

    .line 415
    invoke-static {p2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/evernote/client/android/EvernoteSession;->access$202(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;)Ljava/lang/String;

    .line 416
    iget-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/evernote/client/android/AuthenticationResult;->fromPreferences(Landroid/content/Context;)Lcom/evernote/client/android/AuthenticationResult;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/evernote/client/android/EvernoteSession;->access$302(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/AuthenticationResult;)Lcom/evernote/client/android/AuthenticationResult;

    .line 418
    invoke-direct {p0, v0}, Lcom/evernote/client/android/EvernoteSession$Builder;->build(Lcom/evernote/client/android/EvernoteSession;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object p1

    return-object p1
.end method

.method public buildForSingleUser(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/EvernoteSession;
    .locals 3

    .line 429
    new-instance v0, Lcom/evernote/client/android/EvernoteSession;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/evernote/client/android/EvernoteSession;-><init>(Lcom/evernote/client/android/EvernoteSession$1;)V

    .line 430
    new-instance v1, Lcom/evernote/client/android/AuthenticationResult;

    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 431
    invoke-static {p2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    iget-boolean v2, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mSupportAppLinkedNotebooks:Z

    invoke-direct {v1, p1, p2, v2}, Lcom/evernote/client/android/AuthenticationResult;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 430
    invoke-static {v0, v1}, Lcom/evernote/client/android/EvernoteSession;->access$302(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/AuthenticationResult;)Lcom/evernote/client/android/AuthenticationResult;

    .line 433
    invoke-direct {p0, v0}, Lcom/evernote/client/android/EvernoteSession$Builder;->build(Lcom/evernote/client/android/EvernoteSession;)Lcom/evernote/client/android/EvernoteSession;

    move-result-object p1

    return-object p1
.end method

.method public setEvernoteService(Lcom/evernote/client/android/EvernoteSession$EvernoteService;)Lcom/evernote/client/android/EvernoteSession$Builder;
    .locals 0

    .line 353
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mEvernoteService:Lcom/evernote/client/android/EvernoteSession$EvernoteService;

    return-object p0
.end method

.method public setForceAuthenticationInThirdPartyApp(Z)Lcom/evernote/client/android/EvernoteSession$Builder;
    .locals 0

    .line 377
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mForceAuthenticationInThirdPartyApp:Z

    return-object p0
.end method

.method public setLocale(Ljava/util/Locale;)Lcom/evernote/client/android/EvernoteSession$Builder;
    .locals 0

    .line 401
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Locale;

    iput-object p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mLocale:Ljava/util/Locale;

    return-object p0
.end method

.method public setSupportAppLinkedNotebooks(Z)Lcom/evernote/client/android/EvernoteSession$Builder;
    .locals 0

    .line 364
    iput-boolean p1, p0, Lcom/evernote/client/android/EvernoteSession$Builder;->mSupportAppLinkedNotebooks:Z

    return-object p0
.end method

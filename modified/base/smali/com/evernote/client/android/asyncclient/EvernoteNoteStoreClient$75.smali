.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findRelatedAsync(Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/notestore/RelatedResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$query:Lcom/evernote/edam/notestore/RelatedQuery;

.field final synthetic val$resultSpec:Lcom/evernote/edam/notestore/RelatedResultSpec;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;)V
    .locals 0

    .line 1064
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;->val$query:Lcom/evernote/edam/notestore/RelatedQuery;

    iput-object p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;->val$resultSpec:Lcom/evernote/edam/notestore/RelatedResultSpec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/notestore/RelatedResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1067
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;->val$query:Lcom/evernote/edam/notestore/RelatedQuery;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;->val$resultSpec:Lcom/evernote/edam/notestore/RelatedResultSpec;

    invoke-virtual {v0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findRelated(Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;)Lcom/evernote/edam/notestore/RelatedResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1064
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;->call()Lcom/evernote/edam/notestore/RelatedResult;

    move-result-object v0

    return-object v0
.end method

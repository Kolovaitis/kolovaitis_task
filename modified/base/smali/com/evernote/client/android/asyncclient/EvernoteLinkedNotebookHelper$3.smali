.class Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$3;
.super Ljava/lang/Object;
.source "EvernoteLinkedNotebookHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->getCorrespondingNotebookAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/type/Notebook;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$3;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/type/Notebook;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$3;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->getCorrespondingNotebook()Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 134
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$3;->call()Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;
.super Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;
.source "EvernoteBusinessNotebookHelper.java"


# instance fields
.field private final mBusinessUserName:Ljava/lang/String;

.field private final mBusinessUserShardId:Ljava/lang/String;

.field private final mClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;


# direct methods
.method public constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 45
    invoke-direct {p0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 46
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    .line 47
    invoke-static {p3}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mBusinessUserName:Ljava/lang/String;

    .line 48
    invoke-static {p4}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mBusinessUserShardId:Ljava/lang/String;

    return-void
.end method

.method public static isBusinessNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Z
    .locals 0

    .line 180
    invoke-virtual {p0}, Lcom/evernote/edam/type/LinkedNotebook;->isSetBusinessId()Z

    move-result p0

    return p0
.end method


# virtual methods
.method public createBusinessNotebook(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/EvernoteSession;)Lcom/evernote/edam/type/LinkedNotebook;
    .locals 0
    .param p1    # Lcom/evernote/edam/type/Notebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/EvernoteSession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 128
    invoke-virtual {p2}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object p2

    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->createBusinessNotebook(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object p1

    return-object p1
.end method

.method public createBusinessNotebook(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)Lcom/evernote/edam/type/LinkedNotebook;
    .locals 2
    .param p1    # Lcom/evernote/edam/type/Notebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-virtual {v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->createNotebook(Lcom/evernote/edam/type/Notebook;)Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    .line 141
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->getSharedNotebooks()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    .line 142
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/edam/type/SharedNotebook;

    .line 144
    new-instance v1, Lcom/evernote/edam/type/LinkedNotebook;

    invoke-direct {v1}, Lcom/evernote/edam/type/LinkedNotebook;-><init>()V

    .line 145
    invoke-virtual {v0}, Lcom/evernote/edam/type/SharedNotebook;->getShareKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/evernote/edam/type/LinkedNotebook;->setShareKey(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p1}, Lcom/evernote/edam/type/Notebook;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/LinkedNotebook;->setShareName(Ljava/lang/String;)V

    .line 147
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mBusinessUserName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/LinkedNotebook;->setUsername(Ljava/lang/String;)V

    .line 148
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mBusinessUserShardId:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/evernote/edam/type/LinkedNotebook;->setShardId(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->createLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object p1

    return-object p1
.end method

.method public createBusinessNotebookAsync(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 0
    .param p1    # Lcom/evernote/edam/type/Notebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/EvernoteSession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Notebook;",
            "Lcom/evernote/client/android/EvernoteSession;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .line 159
    invoke-virtual {p2}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object p2

    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->createBusinessNotebookAsync(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public createBusinessNotebookAsync(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1    # Lcom/evernote/edam/type/Notebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Notebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .line 168
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getBusinessUserName()Ljava/lang/String;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mBusinessUserName:Ljava/lang/String;

    return-object v0
.end method

.method public getBusinessUserShardId()Ljava/lang/String;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mBusinessUserShardId:Ljava/lang/String;

    return-object v0
.end method

.method public getClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->mClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    return-object v0
.end method

.method public listBusinessNotebooks(Lcom/evernote/client/android/EvernoteSession;)Ljava/util/List;
    .locals 0
    .param p1    # Lcom/evernote/client/android/EvernoteSession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/EvernoteSession;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 77
    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->listBusinessNotebooks(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listBusinessNotebooks(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)Ljava/util/List;
    .locals 2
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;
        }
    .end annotation

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->listLinkedNotebooks()Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 89
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 90
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 91
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/edam/type/LinkedNotebook;

    invoke-static {v1}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->isBusinessNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 92
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public listBusinessNotebooksAsync(Lcom/evernote/client/android/EvernoteSession;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 0
    .param p1    # Lcom/evernote/client/android/EvernoteSession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/EvernoteSession;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;>;"
        }
    .end annotation

    .line 103
    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object p1

    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->listBusinessNotebooksAsync(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public listBusinessNotebooksAsync(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;>;"
        }
    .end annotation

    .line 112
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$1;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

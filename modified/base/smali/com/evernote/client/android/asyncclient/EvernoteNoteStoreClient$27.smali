.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findNotesMetadataAsync(Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/notestore/NotesMetadataList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$filter:Lcom/evernote/edam/notestore/NoteFilter;

.field final synthetic val$maxNotes:I

.field final synthetic val$offset:I

.field final synthetic val$resultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)V
    .locals 0

    .line 414
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->val$filter:Lcom/evernote/edam/notestore/NoteFilter;

    iput p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->val$offset:I

    iput p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->val$maxNotes:I

    iput-object p5, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->val$resultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/notestore/NotesMetadataList;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 417
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->val$filter:Lcom/evernote/edam/notestore/NoteFilter;

    iget v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->val$offset:I

    iget v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->val$maxNotes:I

    iget-object v4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->val$resultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findNotesMetadata(Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)Lcom/evernote/edam/notestore/NotesMetadataList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 414
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;->call()Lcom/evernote/edam/notestore/NotesMetadataList;

    move-result-object v0

    return-object v0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$59;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->createSharedNotebookAsync(Lcom/evernote/edam/type/SharedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/type/SharedNotebook;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$sharedNotebook:Lcom/evernote/edam/type/SharedNotebook;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/SharedNotebook;)V
    .locals 0

    .line 846
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$59;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$59;->val$sharedNotebook:Lcom/evernote/edam/type/SharedNotebook;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/type/SharedNotebook;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 849
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$59;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$59;->val$sharedNotebook:Lcom/evernote/edam/type/SharedNotebook;

    invoke-virtual {v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->createSharedNotebook(Lcom/evernote/edam/type/SharedNotebook;)Lcom/evernote/edam/type/SharedNotebook;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 846
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$59;->call()Lcom/evernote/edam/type/SharedNotebook;

    move-result-object v0

    return-object v0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$2;
.super Ljava/lang/Object;
.source "EvernoteLinkedNotebookHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->deleteLinkedNotebookAsync(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

.field final synthetic val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$2;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$2;->val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Integer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$2;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$2;->val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-virtual {v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->deleteLinkedNotebook(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 114
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$2;->call()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

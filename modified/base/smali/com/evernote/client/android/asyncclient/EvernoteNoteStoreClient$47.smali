.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getResourceAsync(Ljava/lang/String;ZZZZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/type/Resource;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$guid:Ljava/lang/String;

.field final synthetic val$withAlternateData:Z

.field final synthetic val$withAttributes:Z

.field final synthetic val$withData:Z

.field final synthetic val$withRecognition:Z


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;ZZZZ)V
    .locals 0

    .line 686
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$guid:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$withData:Z

    iput-boolean p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$withRecognition:Z

    iput-boolean p5, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$withAttributes:Z

    iput-boolean p6, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$withAlternateData:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/type/Resource;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 689
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$guid:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$withData:Z

    iget-boolean v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$withRecognition:Z

    iget-boolean v4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$withAttributes:Z

    iget-boolean v5, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->val$withAlternateData:Z

    invoke-virtual/range {v0 .. v5}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getResource(Ljava/lang/String;ZZZZ)Lcom/evernote/edam/type/Resource;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 686
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;->call()Lcom/evernote/edam/type/Resource;

    move-result-object v0

    return-object v0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$1;
.super Ljava/lang/Object;
.source "EvernoteLinkedNotebookHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->createNoteInLinkedNotebookAsync(Lcom/evernote/edam/type/Note;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/type/Note;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

.field final synthetic val$note:Lcom/evernote/edam/type/Note;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;Lcom/evernote/edam/type/Note;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$1;->val$note:Lcom/evernote/edam/type/Note;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/type/Note;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$1;->val$note:Lcom/evernote/edam/type/Note;

    invoke-virtual {v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->createNoteInLinkedNotebook(Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 71
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper$1;->call()Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getSyncChunkAsync(IIZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/notestore/SyncChunk;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$afterUSN:I

.field final synthetic val$fullSyncOnly:Z

.field final synthetic val$maxEntries:I


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;IIZ)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->val$afterUSN:I

    iput p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->val$maxEntries:I

    iput-boolean p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->val$fullSyncOnly:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/notestore/SyncChunk;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->val$afterUSN:I

    iget v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->val$maxEntries:I

    iget-boolean v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->val$fullSyncOnly:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getSyncChunk(IIZ)Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;->call()Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object v0

    return-object v0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getNoteVersionAsync(Ljava/lang/String;IZZZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/type/Note;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$noteGuid:Ljava/lang/String;

.field final synthetic val$updateSequenceNum:I

.field final synthetic val$withResourcesAlternateData:Z

.field final synthetic val$withResourcesData:Z

.field final synthetic val$withResourcesRecognition:Z


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;IZZZ)V
    .locals 0

    .line 669
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$noteGuid:Ljava/lang/String;

    iput p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$updateSequenceNum:I

    iput-boolean p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$withResourcesData:Z

    iput-boolean p5, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$withResourcesRecognition:Z

    iput-boolean p6, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$withResourcesAlternateData:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/type/Note;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 672
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$noteGuid:Ljava/lang/String;

    iget v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$updateSequenceNum:I

    iget-boolean v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$withResourcesData:Z

    iget-boolean v4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$withResourcesRecognition:Z

    iget-boolean v5, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->val$withResourcesAlternateData:Z

    invoke-virtual/range {v0 .. v5}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getNoteVersion(Ljava/lang/String;IZZZ)Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 669
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;->call()Lcom/evernote/edam/type/Note;

    move-result-object v0

    return-object v0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper$1;
.super Ljava/lang/Object;
.source "EvernoteHtmlHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;->downloadNoteAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/squareup/okhttp/Response;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

.field final synthetic val$noteGuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;Ljava/lang/String;)V
    .locals 0

    .line 76
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper$1;->val$noteGuid:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/squareup/okhttp/Response;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper$1;->val$noteGuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;->downloadNote(Ljava/lang/String;)Lcom/squareup/okhttp/Response;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 76
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper$1;->call()Lcom/squareup/okhttp/Response;

    move-result-object v0

    return-object v0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;
.super Ljava/lang/Object;
.source "EvernoteSearchHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->executeAsync(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

.field final synthetic val$search:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;->val$search:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;->val$search:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;

    invoke-virtual {v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->execute(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 120
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;->call()Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;

    move-result-object v0

    return-object v0
.end method

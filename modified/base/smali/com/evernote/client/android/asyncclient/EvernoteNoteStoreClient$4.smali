.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getFilteredSyncChunkAsync(IILcom/evernote/edam/notestore/SyncChunkFilter;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/notestore/SyncChunk;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$afterUSN:I

.field final synthetic val$filter:Lcom/evernote/edam/notestore/SyncChunkFilter;

.field final synthetic val$maxEntries:I


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;IILcom/evernote/edam/notestore/SyncChunkFilter;)V
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->val$afterUSN:I

    iput p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->val$maxEntries:I

    iput-object p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->val$filter:Lcom/evernote/edam/notestore/SyncChunkFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/notestore/SyncChunk;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->val$afterUSN:I

    iget v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->val$maxEntries:I

    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->val$filter:Lcom/evernote/edam/notestore/SyncChunkFilter;

    invoke-virtual {v0, v1, v2, v3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getFilteredSyncChunk(IILcom/evernote/edam/notestore/SyncChunkFilter;)Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 106
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;->call()Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object v0

    return-object v0
.end method

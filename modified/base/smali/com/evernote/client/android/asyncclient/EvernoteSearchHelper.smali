.class public Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;
.super Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;
.source "EvernoteSearchHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;,
        Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;,
        Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;
    }
.end annotation


# instance fields
.field private final mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

.field private final mPrivateClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field private final mSession:Lcom/evernote/client/android/EvernoteSession;


# direct methods
.method public constructor <init>(Lcom/evernote/client/android/EvernoteSession;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Lcom/evernote/client/android/EvernoteSession;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 60
    invoke-direct {p0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 61
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/EvernoteSession;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mSession:Lcom/evernote/client/android/EvernoteSession;

    .line 62
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {p1}, Lcom/evernote/client/android/EvernoteSession;->getEvernoteClientFactory()Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    .line 63
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p1

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mPrivateClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    return-void
.end method

.method private maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 209
    invoke-virtual {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->isIgnoreExceptions()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 210
    :cond_0
    throw p2
.end method


# virtual methods
.method public execute(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;
    .locals 5
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 73
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    move-result v0

    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 77
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;

    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$200(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/EnumSet;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;-><init>(Ljava/util/Set;Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;)V

    .line 79
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$200(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/EnumSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    .line 80
    sget-object v3, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$2;->$SwitchMap$com$evernote$client$android$asyncclient$EvernoteSearchHelper$Scope:[I

    invoke-virtual {v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v2, 0x1

    .line 101
    invoke-virtual {p0, p1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->getLinkedNotebooks(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Z)Ljava/util/List;

    move-result-object v2

    .line 102
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/evernote/edam/type/LinkedNotebook;

    .line 104
    :try_start_0
    invoke-virtual {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findNotesInBusinessNotebook(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/edam/type/LinkedNotebook;)Ljava/util/List;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;->access$600(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;Lcom/evernote/edam/type/LinkedNotebook;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    .line 106
    invoke-direct {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    goto :goto_1

    :pswitch_1
    const/4 v2, 0x0

    .line 90
    invoke-virtual {p0, p1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->getLinkedNotebooks(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Z)Ljava/util/List;

    move-result-object v2

    .line 91
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/evernote/edam/type/LinkedNotebook;

    .line 93
    :try_start_1
    invoke-virtual {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findNotesInLinkedNotebook(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/edam/type/LinkedNotebook;)Ljava/util/List;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;->access$500(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;Lcom/evernote/edam/type/LinkedNotebook;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v3

    .line 95
    invoke-direct {p0, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    goto :goto_2

    .line 83
    :pswitch_2
    :try_start_2
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findPersonalNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;->access$400(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;Ljava/util/List;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v2

    .line 85
    invoke-direct {p0, p1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_1
    return-object v0

    .line 74
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "offset must be less than max notes"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public executeAsync(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Result;",
            ">;"
        }
    .end annotation

    .line 120
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$1;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method protected findAllNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;",
            "Lcom/evernote/edam/notestore/NoteFilter;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 157
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 159
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    move-result v1

    .line 160
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    move-result v2

    sub-int v3, v1, v2

    :goto_0
    if-lez v3, :cond_0

    .line 166
    :try_start_0
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$800(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    move-result-object v4

    invoke-virtual {p2, p3, v2, v1, v4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findNotesMetadata(Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)Lcom/evernote/edam/notestore/NotesMetadataList;

    move-result-object v4

    .line 167
    invoke-virtual {v4}, Lcom/evernote/edam/notestore/NotesMetadataList;->getTotalNotes()I

    move-result v5

    invoke-virtual {v4}, Lcom/evernote/edam/notestore/NotesMetadataList;->getStartIndex()I

    move-result v6

    invoke-virtual {v4}, Lcom/evernote/edam/notestore/NotesMetadataList;->getNotesSize()I

    move-result v3

    add-int/2addr v6, v3

    sub-int v3, v5, v6

    .line 169
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/evernote/edam/error/EDAMUserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/evernote/edam/error/EDAMSystemException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/evernote/thrift/TException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/evernote/edam/error/EDAMNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v4

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_1

    :catch_2
    move-exception v4

    goto :goto_1

    :catch_3
    move-exception v4

    .line 171
    :goto_1
    invoke-direct {p0, p1, v4}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 172
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$900(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    move-result v4

    sub-int/2addr v3, v4

    .line 175
    :goto_2
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$900(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I

    move-result v4

    add-int/2addr v2, v4

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method protected findNotesInBusinessNotebook(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/edam/type/LinkedNotebook;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    invoke-virtual {v1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    move-result-object p2

    .line 147
    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->getCorrespondingNotebook()Lcom/evernote/edam/type/Notebook;

    move-result-object p2

    .line 150
    new-instance v1, Lcom/evernote/edam/notestore/NoteFilter;

    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$700(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NoteFilter;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/evernote/edam/notestore/NoteFilter;-><init>(Lcom/evernote/edam/notestore/NoteFilter;)V

    .line 151
    invoke-virtual {p2}, Lcom/evernote/edam/type/Notebook;->getGuid()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/evernote/edam/notestore/NoteFilter;->setNotebookGuid(Ljava/lang/String;)V

    .line 153
    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->getClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p2

    invoke-virtual {p0, p1, p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findAllNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected findNotesInLinkedNotebook(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/edam/type/LinkedNotebook;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 133
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    invoke-virtual {v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    move-result-object p2

    .line 134
    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->getCorrespondingNotebook()Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    .line 137
    new-instance v1, Lcom/evernote/edam/notestore/NoteFilter;

    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$700(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NoteFilter;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/evernote/edam/notestore/NoteFilter;-><init>(Lcom/evernote/edam/notestore/NoteFilter;)V

    .line 138
    invoke-virtual {v0}, Lcom/evernote/edam/type/Notebook;->getGuid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/evernote/edam/notestore/NoteFilter;->setNotebookGuid(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;->getClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object p2

    invoke-virtual {p0, p1, p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findAllNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected findPersonalNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 129
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mPrivateClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$700(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NoteFilter;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->findAllNotes(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method protected getLinkedNotebooks(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Z)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    if-eqz p2, :cond_1

    .line 183
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$1000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 185
    :try_start_0
    iget-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mClientFactory:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    move-result-object p2

    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {p2, v0}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->listBusinessNotebooks(Lcom/evernote/client/android/EvernoteSession;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catch Lcom/evernote/edam/error/EDAMUserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/evernote/edam/error/EDAMSystemException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/evernote/edam/error/EDAMNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/evernote/thrift/TException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p2

    goto :goto_0

    :catch_1
    move-exception p2

    goto :goto_0

    :catch_2
    move-exception p2

    goto :goto_0

    :catch_3
    move-exception p2

    .line 187
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 188
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 191
    :cond_0
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$1000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 195
    :cond_1
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$1100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 197
    :try_start_1
    iget-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->mPrivateClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-virtual {p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->listLinkedNotebooks()Ljava/util/List;

    move-result-object p1
    :try_end_1
    .catch Lcom/evernote/edam/error/EDAMUserException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lcom/evernote/edam/error/EDAMNotFoundException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Lcom/evernote/thrift/TException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/evernote/edam/error/EDAMSystemException; {:try_start_1 .. :try_end_1} :catch_4

    return-object p1

    :catch_4
    move-exception p2

    goto :goto_1

    :catch_5
    move-exception p2

    goto :goto_1

    :catch_6
    move-exception p2

    goto :goto_1

    :catch_7
    move-exception p2

    .line 199
    :goto_1
    invoke-direct {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;->maybeRethrow(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;Ljava/lang/Exception;)V

    .line 200
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 203
    :cond_2
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->access$1100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

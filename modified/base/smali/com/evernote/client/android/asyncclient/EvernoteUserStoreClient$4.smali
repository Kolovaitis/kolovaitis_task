.class Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;
.super Ljava/lang/Object;
.source "EvernoteUserStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->authenticateLongSessionAsync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/userstore/AuthenticationResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

.field final synthetic val$consumerKey:Ljava/lang/String;

.field final synthetic val$consumerSecret:Ljava/lang/String;

.field final synthetic val$deviceDescription:Ljava/lang/String;

.field final synthetic val$deviceIdentifier:Ljava/lang/String;

.field final synthetic val$password:Ljava/lang/String;

.field final synthetic val$supportsTwoFactor:Z

.field final synthetic val$username:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$username:Ljava/lang/String;

    iput-object p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$password:Ljava/lang/String;

    iput-object p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$consumerKey:Ljava/lang/String;

    iput-object p5, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$consumerSecret:Ljava/lang/String;

    iput-object p6, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$deviceIdentifier:Ljava/lang/String;

    iput-object p7, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$deviceDescription:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$supportsTwoFactor:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$username:Ljava/lang/String;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$password:Ljava/lang/String;

    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$consumerKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$consumerSecret:Ljava/lang/String;

    iget-object v5, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$deviceIdentifier:Ljava/lang/String;

    iget-object v6, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$deviceDescription:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->val$supportsTwoFactor:Z

    invoke-virtual/range {v0 .. v7}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->authenticateLongSession(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 96
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$4;->call()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    return-object v0
.end method

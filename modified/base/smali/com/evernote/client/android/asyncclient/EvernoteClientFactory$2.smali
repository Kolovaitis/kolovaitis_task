.class Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;
.super Ljava/lang/Object;
.source "EvernoteClientFactory.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getLinkedNotebookHelperAsync(Lcom/evernote/edam/type/LinkedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

.field final synthetic val$linkedNotebook:Lcom/evernote/edam/type/LinkedNotebook;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;Lcom/evernote/edam/type/LinkedNotebook;)V
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;->val$linkedNotebook:Lcom/evernote/edam/type/LinkedNotebook;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 188
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;->val$linkedNotebook:Lcom/evernote/edam/type/LinkedNotebook;

    invoke-virtual {v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 185
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;->call()Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    move-result-object v0

    return-object v0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;
.super Ljava/lang/Object;
.source "EvernoteAsyncClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

.field final synthetic val$callable:Ljava/util/concurrent/Callable;

.field final synthetic val$callback:Lcom/evernote/client/android/asyncclient/EvernoteCallback;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;->val$callable:Ljava/util/concurrent/Callable;

    iput-object p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;->val$callback:Lcom/evernote/client/android/asyncclient/EvernoteCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;->val$callable:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;->val$callback:Lcom/evernote/client/android/asyncclient/EvernoteCallback;

    invoke-static {v1, v0, v2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->access$000(Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;Ljava/lang/Object;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 40
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;->val$callback:Lcom/evernote/client/android/asyncclient/EvernoteCallback;

    invoke-static {v1, v0, v2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->access$100(Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;Ljava/lang/Exception;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V

    const/4 v0, 0x0

    return-object v0
.end method

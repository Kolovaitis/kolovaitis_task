.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->setSharedNotebookRecipientSettingsAsync(JLcom/evernote/edam/type/SharedNotebookRecipientSettings;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

.field final synthetic val$sharedNotebookId:J


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;JLcom/evernote/edam/type/SharedNotebookRecipientSettings;)V
    .locals 0

    .line 876
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput-wide p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;->val$sharedNotebookId:J

    iput-object p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;->val$recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Integer;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 879
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget-wide v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;->val$sharedNotebookId:J

    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;->val$recipientSettings:Lcom/evernote/edam/type/SharedNotebookRecipientSettings;

    invoke-virtual {v0, v1, v2, v3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->setSharedNotebookRecipientSettings(JLcom/evernote/edam/type/SharedNotebookRecipientSettings;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 876
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;->call()Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

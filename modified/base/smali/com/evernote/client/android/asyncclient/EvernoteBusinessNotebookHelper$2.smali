.class Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;
.super Ljava/lang/Object;
.source "EvernoteBusinessNotebookHelper.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->createBusinessNotebookAsync(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/type/LinkedNotebook;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

.field final synthetic val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$notebook:Lcom/evernote/edam/type/Notebook;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->val$notebook:Lcom/evernote/edam/type/Notebook;

    iput-object p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/type/LinkedNotebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->val$notebook:Lcom/evernote/edam/type/Notebook;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->val$defaultClient:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-virtual {v0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->createBusinessNotebook(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 168
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper$2;->call()Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object v0

    return-object v0
.end method

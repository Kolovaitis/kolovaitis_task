.class public Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;
.super Ljava/lang/Object;
.source "EvernoteClientFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$Builder;
    }
.end annotation


# instance fields
.field private mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

.field private mBusinessNotebookHelper:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

.field protected final mByteStore:Lcom/evernote/client/conn/mobile/ByteStore;

.field private final mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

.field private mEvernoteSearchHelper:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

.field protected final mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

.field protected final mExecutorService:Ljava/util/concurrent/ExecutorService;

.field protected final mHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHtmlHelperBusiness:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

.field private mHtmlHelperDefault:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

.field protected final mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

.field private final mLinkedHtmlHelper:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mLinkedNotebookHelpers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mNoteStoreClients:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;",
            ">;"
        }
    .end annotation
.end field

.field private final mUserStoreClients:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/evernote/client/android/EvernoteSession;Lcom/squareup/okhttp/OkHttpClient;Lcom/evernote/client/conn/mobile/ByteStore;Ljava/util/Map;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/EvernoteSession;",
            "Lcom/squareup/okhttp/OkHttpClient;",
            "Lcom/evernote/client/conn/mobile/ByteStore;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/EvernoteSession;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 75
    invoke-static {p2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/okhttp/OkHttpClient;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    .line 76
    invoke-static {p3}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/conn/mobile/ByteStore;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mByteStore:Lcom/evernote/client/conn/mobile/ByteStore;

    .line 77
    iput-object p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHeaders:Ljava/util/Map;

    .line 78
    invoke-static {p5}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 80
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mUserStoreClients:Ljava/util/Map;

    .line 81
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mNoteStoreClients:Ljava/util/Map;

    .line 82
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedNotebookHelpers:Ljava/util/Map;

    .line 83
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedHtmlHelper:Ljava/util/Map;

    .line 85
    new-instance p1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$1;

    iget-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p1, p0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$1;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;Ljava/util/concurrent/ExecutorService;)V

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    return-void
.end method


# virtual methods
.method protected final authenticateToBusiness()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 362
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->isBusinessAuthExpired()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 363
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getUserStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->authenticateToBusiness()Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    :cond_0
    return-void
.end method

.method protected checkLoggedIn()V
    .locals 2

    .line 372
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 373
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "user not logged in"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected createBinaryProtocol(Ljava/lang/String;)Lcom/evernote/thrift/protocol/TBinaryProtocol;
    .locals 5

    .line 338
    new-instance v0, Lcom/evernote/thrift/protocol/TBinaryProtocol;

    new-instance v1, Lcom/evernote/client/conn/mobile/TAndroidTransport;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mByteStore:Lcom/evernote/client/conn/mobile/ByteStore;

    iget-object v4, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHeaders:Ljava/util/Map;

    invoke-direct {v1, v2, v3, p1, v4}, Lcom/evernote/client/conn/mobile/TAndroidTransport;-><init>(Lcom/squareup/okhttp/OkHttpClient;Lcom/evernote/client/conn/mobile/ByteStore;Ljava/lang/String;Ljava/util/Map;)V

    invoke-direct {v0, v1}, Lcom/evernote/thrift/protocol/TBinaryProtocol;-><init>(Lcom/evernote/thrift/transport/TTransport;)V

    return-object v0
.end method

.method protected createBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    .line 233
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->authenticateToBusiness()V

    .line 235
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    invoke-virtual {v0}, Lcom/evernote/edam/userstore/AuthenticationResult;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getUser()Lcom/evernote/edam/type/User;

    move-result-object v1

    .line 238
    new-instance v2, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v1}, Lcom/evernote/edam/type/User;->getUsername()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/evernote/edam/type/User;->getShardId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v3, v4, v1}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method protected declared-synchronized createEvernoteNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
    .locals 2

    monitor-enter p0

    .line 346
    :try_start_0
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createNoteStoreClient(Ljava/lang/String;)Lcom/evernote/edam/notestore/NoteStore$Client;

    move-result-object p1

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, p1, p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;-><init>(Lcom/evernote/edam/notestore/NoteStore$Client;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected createEvernoteSearchHelper()Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;
    .locals 3

    .line 334
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;-><init>(Lcom/evernote/client/android/EvernoteSession;Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method

.method protected createHtmlHelper(Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    .locals 4

    .line 318
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHttpClient:Lcom/squareup/okhttp/OkHttpClient;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v2}, Lcom/evernote/client/android/EvernoteSession;->getAuthenticationResult()Lcom/evernote/client/android/AuthenticationResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/evernote/client/android/AuthenticationResult;->getEvernoteHost()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;-><init>(Lcom/squareup/okhttp/OkHttpClient;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V

    return-object v0
.end method

.method protected final createKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    .line 351
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_1
    :goto_0
    if-nez p1, :cond_2

    return-object p2

    :cond_2
    if-nez p2, :cond_3

    return-object p1

    .line 357
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected createLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;
    .locals 3
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 196
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v1

    .line 199
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getShareKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->authenticateToSharedNotebook(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object v1

    .line 201
    invoke-virtual {v1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v0

    .line 203
    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v1, v0, p1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/LinkedNotebook;Ljava/util/concurrent/ExecutorService;)V

    return-object v1
.end method

.method protected createNoteStoreClient(Ljava/lang/String;)Lcom/evernote/edam/notestore/NoteStore$Client;
    .locals 1

    .line 342
    new-instance v0, Lcom/evernote/edam/notestore/NoteStore$Client;

    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createBinaryProtocol(Ljava/lang/String;)Lcom/evernote/thrift/protocol/TBinaryProtocol;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;-><init>(Lcom/evernote/thrift/protocol/TProtocol;)V

    return-object v0
.end method

.method protected createUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;
    .locals 2

    .line 123
    new-instance v0, Lcom/evernote/edam/userstore/UserStore$Client;

    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createBinaryProtocol(Ljava/lang/String;)Lcom/evernote/thrift/protocol/TBinaryProtocol;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/evernote/edam/userstore/UserStore$Client;-><init>(Lcom/evernote/thrift/protocol/TProtocol;)V

    .line 124
    new-instance p1, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    invoke-direct {p1, v0, p2, v1}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;-><init>(Lcom/evernote/edam/userstore/UserStore$Client;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V

    return-object p1
.end method

.method public declared-synchronized getBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    monitor-enter p0

    .line 214
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessNotebookHelper:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->isBusinessAuthExpired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessNotebookHelper:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessNotebookHelper:Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getBusinessNotebookHelperAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;",
            ">;"
        }
    .end annotation

    .line 224
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$3;

    invoke-direct {v1, p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$3;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;)V

    invoke-virtual {v0, v1, p1}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getEvernoteSearchHelper()Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;
    .locals 1

    .line 325
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->checkLoggedIn()V

    .line 327
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSearchHelper:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    if-nez v0, :cond_0

    .line 328
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createEvernoteSearchHelper()Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSearchHelper:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSearchHelper:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;

    return-object v0
.end method

.method public declared-synchronized getHtmlHelperBusiness()Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/thrift/TException;,
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;
        }
    .end annotation

    monitor-enter p0

    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperBusiness:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    if-nez v0, :cond_0

    .line 299
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->authenticateToBusiness()V

    .line 300
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    invoke-virtual {v0}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createHtmlHelper(Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperBusiness:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperBusiness:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getHtmlHelperBusinessAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;"
        }
    .end annotation

    .line 309
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$5;

    invoke-direct {v1, p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$5;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;)V

    invoke-virtual {v0, v1, p1}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public declared-synchronized getHtmlHelperDefault()Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    .locals 1

    monitor-enter p0

    .line 247
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->checkLoggedIn()V

    .line 249
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperDefault:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    if-nez v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createHtmlHelper(Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperDefault:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mHtmlHelperDefault:Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getLinkedHtmlHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;
    .locals 3
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 263
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getGuid()Ljava/lang/String;

    move-result-object v0

    .line 265
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedHtmlHelper:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    if-nez v1, :cond_0

    .line 267
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object v1

    .line 269
    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v2}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v1

    .line 270
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getShareKey()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->authenticateToSharedNotebook(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    .line 272
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/AuthenticationResult;->getAuthenticationToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createHtmlHelper(Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;

    move-result-object v1

    .line 274
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedHtmlHelper:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v1
.end method

.method public getLinkedHtmlHelperAsync(Lcom/evernote/edam/type/LinkedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteHtmlHelper;",
            ">;"
        }
    .end annotation

    .line 284
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$4;

    invoke-direct {v1, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$4;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;Lcom/evernote/edam/type/LinkedNotebook;)V

    invoke-virtual {v0, v1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public declared-synchronized getLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;
    .locals 2
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    monitor-enter p0

    .line 169
    :try_start_0
    invoke-virtual {p1}, Lcom/evernote/edam/type/LinkedNotebook;->getGuid()Ljava/lang/String;

    move-result-object v0

    .line 170
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedNotebookHelpers:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    if-nez v1, :cond_0

    .line 172
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createLinkedNotebookHelper(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;

    move-result-object v1

    .line 173
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mLinkedNotebookHelpers:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getLinkedNotebookHelperAsync(Lcom/evernote/edam/type/LinkedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Lcom/evernote/edam/type/LinkedNotebook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/client/android/asyncclient/EvernoteLinkedNotebookHelper;",
            ">;"
        }
    .end annotation

    .line 185
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mCreateHelperClient:Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;

    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;

    invoke-direct {v1, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$2;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;Lcom/evernote/edam/type/LinkedNotebook;)V

    invoke-virtual {v0, v1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public declared-synchronized getNoteStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
    .locals 2

    monitor-enter p0

    .line 133
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->checkLoggedIn()V

    .line 135
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getAuthenticationResult()Lcom/evernote/client/android/AuthenticationResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/AuthenticationResult;->getNoteStoreUrl()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    monitor-enter p0

    .line 147
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mNoteStoreClients:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    if-nez v1, :cond_0

    .line 150
    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createEvernoteNoteStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    move-result-object v1

    .line 151
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mNoteStoreClients:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getUserStoreClient()Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;
    .locals 2

    monitor-enter p0

    .line 94
    :try_start_0
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->checkLoggedIn()V

    .line 96
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "https"

    .line 97
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    .line 98
    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getAuthenticationResult()Lcom/evernote/client/android/AuthenticationResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/evernote/client/android/AuthenticationResult;->getEvernoteHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/edam/user"

    .line 99
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mEvernoteSession:Lcom/evernote/client/android/EvernoteSession;

    invoke-virtual {v1}, Lcom/evernote/client/android/EvernoteSession;->getAuthToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    monitor-enter p0

    .line 113
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mUserStoreClients:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    if-nez v1, :cond_0

    .line 116
    invoke-virtual {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->createUserStoreClient(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    move-result-object v1

    .line 117
    iget-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mUserStoreClients:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method protected final isBusinessAuthExpired()Z
    .locals 5

    .line 368
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->mBusinessAuthenticationResult:Lcom/evernote/edam/userstore/AuthenticationResult;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/evernote/edam/userstore/AuthenticationResult;->getExpiration()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

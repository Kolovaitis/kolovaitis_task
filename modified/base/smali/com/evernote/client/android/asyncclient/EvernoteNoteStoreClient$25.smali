.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findNotesAsync(Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/notestore/NoteList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$filter:Lcom/evernote/edam/notestore/NoteFilter;

.field final synthetic val$maxNotes:I

.field final synthetic val$offset:I


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;II)V
    .locals 0

    .line 384
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->val$filter:Lcom/evernote/edam/notestore/NoteFilter;

    iput p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->val$offset:I

    iput p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->val$maxNotes:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/notestore/NoteList;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 387
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->val$filter:Lcom/evernote/edam/notestore/NoteFilter;

    iget v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->val$offset:I

    iget v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->val$maxNotes:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->findNotes(Lcom/evernote/edam/notestore/NoteFilter;II)Lcom/evernote/edam/notestore/NoteList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 384
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;->call()Lcom/evernote/edam/notestore/NoteList;

    move-result-object v0

    return-object v0
.end method

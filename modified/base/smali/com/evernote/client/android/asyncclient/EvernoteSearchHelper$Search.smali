.class public Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
.super Ljava/lang/Object;
.source "EvernoteSearchHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Search"
.end annotation


# instance fields
.field private final mBusinessNotebooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation
.end field

.field private mIgnoreExceptions:Z

.field private final mLinkedNotebooks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation
.end field

.field private mMaxNotes:I

.field private mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

.field private mOffset:I

.field private mPageSize:I

.field private mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

.field private final mScopes:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    const-class v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mLinkedNotebooks:Ljava/util/List;

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mBusinessNotebooks:Ljava/util/List;

    const/4 v0, -0x1

    .line 239
    iput v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mOffset:I

    .line 240
    iput v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mMaxNotes:I

    .line 241
    iput v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mPageSize:I

    return-void
.end method

.method static synthetic access$000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I
    .locals 0

    .line 223
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getOffset()I

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I
    .locals 0

    .line 223
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getMaxNotes()I

    move-result p0

    return p0
.end method

.method static synthetic access$1000(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;
    .locals 0

    .line 223
    iget-object p0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mBusinessNotebooks:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/List;
    .locals 0

    .line 223
    iget-object p0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mLinkedNotebooks:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$200(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Ljava/util/EnumSet;
    .locals 0

    .line 223
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getScopes()Ljava/util/EnumSet;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$700(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NoteFilter;
    .locals 0

    .line 223
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getNoteFilter()Lcom/evernote/edam/notestore/NoteFilter;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$800(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)Lcom/evernote/edam/notestore/NotesMetadataResultSpec;
    .locals 0

    .line 223
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getResultSpec()Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$900(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;)I
    .locals 0

    .line 223
    invoke-direct {p0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->getPageSize()I

    move-result p0

    return p0
.end method

.method private getMaxNotes()I
    .locals 1

    .line 379
    iget v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mMaxNotes:I

    if-gez v0, :cond_0

    const/16 v0, 0xa

    return v0

    :cond_0
    return v0
.end method

.method private getNoteFilter()Lcom/evernote/edam/notestore/NoteFilter;
    .locals 2

    .line 354
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    if-nez v0, :cond_0

    .line 355
    new-instance v0, Lcom/evernote/edam/notestore/NoteFilter;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NoteFilter;-><init>()V

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    .line 356
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    sget-object v1, Lcom/evernote/edam/type/NoteSortOrder;->UPDATED:Lcom/evernote/edam/type/NoteSortOrder;

    invoke-virtual {v1}, Lcom/evernote/edam/type/NoteSortOrder;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteFilter;->setOrder(I)V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    return-object v0
.end method

.method private getOffset()I
    .locals 1

    .line 372
    iget v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mOffset:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    return v0
.end method

.method private getPageSize()I
    .locals 1

    .line 386
    iget v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mPageSize:I

    if-gez v0, :cond_0

    const/16 v0, 0xa

    return v0

    :cond_0
    return v0
.end method

.method private getResultSpec()Lcom/evernote/edam/notestore/NotesMetadataResultSpec;
    .locals 2

    .line 362
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    if-nez v0, :cond_0

    .line 363
    new-instance v0, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    invoke-direct {v0}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;-><init>()V

    iput-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    .line 364
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeTitle(Z)V

    .line 365
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;->setIncludeNotebookGuid(Z)V

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    return-object v0
.end method

.method private getScopes()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;",
            ">;"
        }
    .end annotation

    .line 347
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    sget-object v1, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;->PERSONAL_NOTES:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    return-object v0
.end method


# virtual methods
.method public addLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    .line 275
    invoke-static {p1}, Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;->isBusinessNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    sget-object v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;->BUSINESS:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->addScope(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;

    .line 277
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mBusinessNotebooks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 279
    :cond_0
    sget-object v0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;->LINKED_NOTEBOOKS:Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;

    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->addScope(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;

    .line 280
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mLinkedNotebooks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object p0
.end method

.method public addScope(Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Scope;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    .line 257
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mScopes:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public isIgnoreExceptions()Z
    .locals 1

    .line 393
    iget-boolean v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mIgnoreExceptions:Z

    return v0
.end method

.method public setIgnoreExceptions(Z)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 0

    .line 342
    iput-boolean p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mIgnoreExceptions:Z

    return-object p0
.end method

.method public setMaxNotes(I)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    const-string v0, "maxNotes must be greater or equal 1"

    .line 322
    invoke-static {p1, v0}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkArgumentPositive(ILjava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mMaxNotes:I

    return-object p0
.end method

.method public setNoteFilter(Lcom/evernote/edam/notestore/NoteFilter;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 0

    .line 291
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/edam/notestore/NoteFilter;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mNoteFilter:Lcom/evernote/edam/notestore/NoteFilter;

    return-object p0
.end method

.method public setOffset(I)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    const-string v0, "negative value now allowed"

    .line 311
    invoke-static {p1, v0}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkArgumentNonnegative(ILjava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mOffset:I

    return-object p0
.end method

.method public setPageSize(I)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 1

    const-string v0, "pageSize must be greater or equal 1"

    .line 332
    invoke-static {p1, v0}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkArgumentPositive(ILjava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mPageSize:I

    return-object p0
.end method

.method public setResultSpec(Lcom/evernote/edam/notestore/NotesMetadataResultSpec;)Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;
    .locals 0

    .line 301
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteSearchHelper$Search;->mResultSpec:Lcom/evernote/edam/notestore/NotesMetadataResultSpec;

    return-object p0
.end method

.class Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$3;
.super Ljava/lang/Object;
.source "EvernoteClientFactory.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getBusinessNotebookHelperAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;)V
    .locals 0

    .line 224
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$3;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 227
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$3;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory;->getBusinessNotebookHelper()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 224
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteClientFactory$3;->call()Lcom/evernote/client/android/asyncclient/EvernoteBusinessNotebookHelper;

    move-result-object v0

    return-object v0
.end method

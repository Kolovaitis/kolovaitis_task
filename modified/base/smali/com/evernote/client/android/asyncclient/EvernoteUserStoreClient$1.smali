.class Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;
.super Ljava/lang/Object;
.source "EvernoteUserStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->checkVersionAsync(Ljava/lang/String;SSLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

.field final synthetic val$clientName:Ljava/lang/String;

.field final synthetic val$edamVersionMajor:S

.field final synthetic val$edamVersionMinor:S


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;Ljava/lang/String;SS)V
    .locals 0

    .line 47
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    iput-object p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->val$clientName:Ljava/lang/String;

    iput-short p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->val$edamVersionMajor:S

    iput-short p4, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->val$edamVersionMinor:S

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Boolean;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->val$clientName:Ljava/lang/String;

    iget-short v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->val$edamVersionMajor:S

    iget-short v3, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->val$edamVersionMinor:S

    invoke-virtual {v0, v1, v2, v3}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->checkVersion(Ljava/lang/String;SS)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 47
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$1;->call()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

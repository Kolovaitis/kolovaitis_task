.class Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;
.super Ljava/lang/Object;
.source "EvernoteNoteStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getPublicNotebookAsync(ILjava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Lcom/evernote/edam/type/Notebook;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

.field final synthetic val$publicUri:Ljava/lang/String;

.field final synthetic val$userId:I


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;ILjava/lang/String;)V
    .locals 0

    .line 833
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iput p2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;->val$userId:I

    iput-object p3, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;->val$publicUri:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Lcom/evernote/edam/type/Notebook;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 836
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;

    iget v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;->val$userId:I

    iget-object v2, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;->val$publicUri:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->getPublicNotebook(ILjava/lang/String;)Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 833
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;->call()Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;
.super Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;
.source "EvernoteNoteStoreClient.java"


# instance fields
.field private final mAuthenticationToken:Ljava/lang/String;

.field private final mClient:Lcom/evernote/edam/notestore/NoteStore$Client;


# direct methods
.method constructor <init>(Lcom/evernote/edam/notestore/NoteStore$Client;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Lcom/evernote/edam/notestore/NoteStore$Client;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 57
    invoke-direct {p0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;-><init>(Ljava/util/concurrent/ExecutorService;)V

    .line 58
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/evernote/edam/notestore/NoteStore$Client;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    .line 59
    invoke-static {p2}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotEmpty(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public authenticateToSharedNote(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1047
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, p1, p2, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->authenticateToSharedNote(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public authenticateToSharedNoteAsync(Ljava/lang/String;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/userstore/AuthenticationResult;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/userstore/AuthenticationResult;",
            ">;"
        }
    .end annotation

    .line 1051
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$74;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$74;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public authenticateToSharedNotebook(Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 980
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->authenticateToSharedNotebook(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/userstore/AuthenticationResult;

    move-result-object p1

    return-object p1
.end method

.method public authenticateToSharedNotebookAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/userstore/AuthenticationResult;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/userstore/AuthenticationResult;",
            ">;"
        }
    .end annotation

    .line 984
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$69;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$69;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public copyNote(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/Note;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 635
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->copyNote(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public copyNoteAsync(Ljava/lang/String;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Note;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Note;",
            ">;"
        }
    .end annotation

    .line 639
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$44;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$44;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public createLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/LinkedNotebook;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 928
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->createLinkedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/type/LinkedNotebook;

    move-result-object p1

    return-object p1
.end method

.method public createLinkedNotebookAsync(Lcom/evernote/edam/type/LinkedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .line 932
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$65;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$65;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/LinkedNotebook;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public createNote(Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 557
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->createNote(Ljava/lang/String;Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public createNoteAsync(Lcom/evernote/edam/type/Note;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Note;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Note;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Note;",
            ">;"
        }
    .end annotation

    .line 561
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$38;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$38;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/Note;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public createNotebook(Lcom/evernote/edam/type/Notebook;)Lcom/evernote/edam/type/Notebook;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 184
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->createNotebook(Ljava/lang/String;Lcom/evernote/edam/type/Notebook;)Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    return-object p1
.end method

.method public createNotebookAsync(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Notebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;"
        }
    .end annotation

    .line 188
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$10;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$10;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/Notebook;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public createSearch(Lcom/evernote/edam/type/SavedSearch;)Lcom/evernote/edam/type/SavedSearch;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 341
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->createSearch(Ljava/lang/String;Lcom/evernote/edam/type/SavedSearch;)Lcom/evernote/edam/type/SavedSearch;

    move-result-object p1

    return-object p1
.end method

.method public createSearchAsync(Lcom/evernote/edam/type/SavedSearch;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/SavedSearch;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;"
        }
    .end annotation

    .line 345
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$22;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$22;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/SavedSearch;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public createSharedNotebook(Lcom/evernote/edam/type/SharedNotebook;)Lcom/evernote/edam/type/SharedNotebook;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 842
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->createSharedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/SharedNotebook;)Lcom/evernote/edam/type/SharedNotebook;

    move-result-object p1

    return-object p1
.end method

.method public createSharedNotebookAsync(Lcom/evernote/edam/type/SharedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/SharedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;"
        }
    .end annotation

    .line 846
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$59;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$59;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/SharedNotebook;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public createTag(Lcom/evernote/edam/type/Tag;)Lcom/evernote/edam/type/Tag;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 262
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->createTag(Ljava/lang/String;Lcom/evernote/edam/type/Tag;)Lcom/evernote/edam/type/Tag;

    move-result-object p1

    return-object p1
.end method

.method public createTagAsync(Lcom/evernote/edam/type/Tag;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Tag;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Tag;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Tag;",
            ">;"
        }
    .end annotation

    .line 266
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$16;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$16;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/Tag;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public deleteNote(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 583
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->deleteNote(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public deleteNoteAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 587
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$40;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$40;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public emailNote(Lcom/evernote/edam/notestore/NoteEmailParameters;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1006
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->emailNote(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteEmailParameters;)V

    return-void
.end method

.method public emailNoteAsync(Lcom/evernote/edam/notestore/NoteEmailParameters;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/notestore/NoteEmailParameters;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Void;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 1010
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$71;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$71;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteEmailParameters;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public expungeInactiveNotes()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 622
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->expungeInactiveNotes(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public expungeInactiveNotesAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 626
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$43;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$43;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public expungeLinkedNotebook(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 967
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->expungeLinkedNotebook(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public expungeLinkedNotebookAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 971
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$68;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$68;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public expungeNote(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 596
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->expungeNote(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public expungeNoteAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 600
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$41;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$41;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public expungeNotebook(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 210
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->expungeNotebook(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public expungeNotebookAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 214
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$12;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$12;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public expungeNotes(Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 609
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->expungeNotes(Ljava/lang/String;Ljava/util/List;)I

    move-result p1

    return p1
.end method

.method public expungeNotesAsync(Ljava/util/List;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 613
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$42;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$42;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/util/List;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public expungeSearch(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 367
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->expungeSearch(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public expungeSearchAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 371
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$24;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$24;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public expungeSharedNotebooks(Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 915
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->expungeSharedNotebooks(Ljava/lang/String;Ljava/util/List;)I

    move-result p1

    return p1
.end method

.method public expungeSharedNotebooksAsync(Ljava/util/List;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 919
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$64;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$64;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/util/List;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public expungeTag(Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 302
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->expungeTag(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public expungeTagAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 306
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$19;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$19;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public findNoteCounts(Lcom/evernote/edam/notestore/NoteFilter;Z)Lcom/evernote/edam/notestore/NoteCollectionCounts;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 423
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->findNoteCounts(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;Z)Lcom/evernote/edam/notestore/NoteCollectionCounts;

    move-result-object p1

    return-object p1
.end method

.method public findNoteCountsAsync(Lcom/evernote/edam/notestore/NoteFilter;ZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/notestore/NoteFilter;",
            "Z",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/NoteCollectionCounts;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/NoteCollectionCounts;",
            ">;"
        }
    .end annotation

    .line 427
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$28;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$28;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;Z)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public findNoteOffset(Lcom/evernote/edam/notestore/NoteFilter;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 393
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->findNoteOffset(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public findNoteOffsetAsync(Lcom/evernote/edam/notestore/NoteFilter;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/notestore/NoteFilter;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 397
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$26;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$26;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public findNotes(Lcom/evernote/edam/notestore/NoteFilter;II)Lcom/evernote/edam/notestore/NoteList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 380
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->findNotes(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;II)Lcom/evernote/edam/notestore/NoteList;

    move-result-object p1

    return-object p1
.end method

.method public findNotesAsync(Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/notestore/NoteFilter;",
            "II",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/NoteList;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/NoteList;",
            ">;"
        }
    .end annotation

    .line 384
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$25;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;II)V

    invoke-virtual {p0, v0, p4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public findNotesMetadata(Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)Lcom/evernote/edam/notestore/NotesMetadataList;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 408
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/evernote/edam/notestore/NoteStore$Client;->findNotesMetadata(Ljava/lang/String;Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)Lcom/evernote/edam/notestore/NotesMetadataList;

    move-result-object p1

    return-object p1
.end method

.method public findNotesMetadataAsync(Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/notestore/NoteFilter;",
            "II",
            "Lcom/evernote/edam/notestore/NotesMetadataResultSpec;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/NotesMetadataList;",
            ">;"
        }
    .end annotation

    .line 414
    new-instance v6, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$27;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/NoteFilter;IILcom/evernote/edam/notestore/NotesMetadataResultSpec;)V

    invoke-virtual {p0, v6, p5}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public findRelated(Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;)Lcom/evernote/edam/notestore/RelatedResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1060
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->findRelated(Ljava/lang/String;Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;)Lcom/evernote/edam/notestore/RelatedResult;

    move-result-object p1

    return-object p1
.end method

.method public findRelatedAsync(Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/notestore/RelatedQuery;",
            "Lcom/evernote/edam/notestore/RelatedResultSpec;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/RelatedResult;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/RelatedResult;",
            ">;"
        }
    .end annotation

    .line 1064
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$75;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/RelatedQuery;Lcom/evernote/edam/notestore/RelatedResultSpec;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getDefaultNotebook()Lcom/evernote/edam/type/Notebook;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 171
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getDefaultNotebook(Ljava/lang/String;)Lcom/evernote/edam/type/Notebook;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultNotebookAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;"
        }
    .end annotation

    .line 175
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$9;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$9;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getFilteredSyncChunk(IILcom/evernote/edam/notestore/SyncChunkFilter;)Lcom/evernote/edam/notestore/SyncChunk;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->getFilteredSyncChunk(Ljava/lang/String;IILcom/evernote/edam/notestore/SyncChunkFilter;)Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object p1

    return-object p1
.end method

.method public getFilteredSyncChunkAsync(IILcom/evernote/edam/notestore/SyncChunkFilter;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/evernote/edam/notestore/SyncChunkFilter;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/SyncChunk;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/SyncChunk;",
            ">;"
        }
    .end annotation

    .line 106
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$4;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;IILcom/evernote/edam/notestore/SyncChunkFilter;)V

    invoke-virtual {p0, v0, p4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getLinkedNotebookSyncChunk(Lcom/evernote/edam/type/LinkedNotebook;IIZ)Lcom/evernote/edam/notestore/SyncChunk;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/evernote/edam/notestore/NoteStore$Client;->getLinkedNotebookSyncChunk(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;IIZ)Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object p1

    return-object p1
.end method

.method public getLinkedNotebookSyncChunkAsync(Lcom/evernote/edam/type/LinkedNotebook;IIZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            "IIZ",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/SyncChunk;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/SyncChunk;",
            ">;"
        }
    .end annotation

    .line 136
    new-instance v6, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$6;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$6;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/LinkedNotebook;IIZ)V

    invoke-virtual {p0, v6, p5}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getLinkedNotebookSyncState(Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/notestore/SyncState;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getLinkedNotebookSyncState(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)Lcom/evernote/edam/notestore/SyncState;

    move-result-object p1

    return-object p1
.end method

.method public getLinkedNotebookSyncStateAsync(Lcom/evernote/edam/type/LinkedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/SyncState;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/SyncState;",
            ">;"
        }
    .end annotation

    .line 119
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$5;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$5;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/LinkedNotebook;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getNote(Ljava/lang/String;ZZZZ)Lcom/evernote/edam/type/Note;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 438
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/evernote/edam/notestore/NoteStore$Client;->getNote(Ljava/lang/String;Ljava/lang/String;ZZZZ)Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public getNoteApplicationData(Ljava/lang/String;)Lcom/evernote/edam/type/LazyMap;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 453
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getNoteApplicationData(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/LazyMap;

    move-result-object p1

    return-object p1
.end method

.method public getNoteApplicationDataAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/LazyMap;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/LazyMap;",
            ">;"
        }
    .end annotation

    .line 457
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$30;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$30;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 466
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->getNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNoteApplicationDataEntryAsync(Ljava/lang/String;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 470
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$31;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$31;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getNoteAsync(Ljava/lang/String;ZZZZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZZZ",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Note;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Note;",
            ">;"
        }
    .end annotation

    .line 444
    new-instance v7, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$29;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$29;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;ZZZZ)V

    invoke-virtual {p0, v7, p6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getNoteContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 505
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getNoteContent(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNoteContentAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 509
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$34;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$34;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getNoteSearchText(Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 518
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->getNoteSearchText(Ljava/lang/String;Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNoteSearchTextAsync(Ljava/lang/String;ZZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZ",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 522
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$35;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$35;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;ZZ)V

    invoke-virtual {p0, v0, p4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getNoteTagNames(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 544
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getNoteTagNames(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getNoteTagNamesAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 548
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$37;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$37;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getNoteVersion(Ljava/lang/String;IZZZ)Lcom/evernote/edam/type/Note;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 663
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/evernote/edam/notestore/NoteStore$Client;->getNoteVersion(Ljava/lang/String;Ljava/lang/String;IZZZ)Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public getNoteVersionAsync(Ljava/lang/String;IZZZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZZZ",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Note;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Note;",
            ">;"
        }
    .end annotation

    .line 669
    new-instance v7, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$46;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;IZZZ)V

    invoke-virtual {p0, v7, p6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getNotebook(Ljava/lang/String;)Lcom/evernote/edam/type/Notebook;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 158
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getNotebook(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    return-object p1
.end method

.method public getNotebookAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;"
        }
    .end annotation

    .line 162
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$8;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$8;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getPublicNotebook(ILjava/lang/String;)Lcom/evernote/edam/type/Notebook;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 829
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    invoke-virtual {v0, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->getPublicNotebook(ILjava/lang/String;)Lcom/evernote/edam/type/Notebook;

    move-result-object p1

    return-object p1
.end method

.method public getPublicNotebookAsync(ILjava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;"
        }
    .end annotation

    .line 833
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$58;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;ILjava/lang/String;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResource(Ljava/lang/String;ZZZZ)Lcom/evernote/edam/type/Resource;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 680
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResource(Ljava/lang/String;Ljava/lang/String;ZZZZ)Lcom/evernote/edam/type/Resource;

    move-result-object p1

    return-object p1
.end method

.method public getResourceAlternateData(Ljava/lang/String;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 803
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResourceAlternateData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object p1

    return-object p1
.end method

.method public getResourceAlternateDataAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "[B>;)",
            "Ljava/util/concurrent/Future<",
            "[B>;"
        }
    .end annotation

    .line 807
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$56;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$56;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResourceApplicationData(Ljava/lang/String;)Lcom/evernote/edam/type/LazyMap;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 695
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResourceApplicationData(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/LazyMap;

    move-result-object p1

    return-object p1
.end method

.method public getResourceApplicationDataAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/LazyMap;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/LazyMap;",
            ">;"
        }
    .end annotation

    .line 699
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$48;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$48;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 708
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getResourceApplicationDataEntryAsync(Ljava/lang/String;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 712
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$49;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$49;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResourceAsync(Ljava/lang/String;ZZZZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZZZ",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Resource;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Resource;",
            ">;"
        }
    .end annotation

    .line 686
    new-instance v7, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$47;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;ZZZZ)V

    invoke-virtual {p0, v7, p6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResourceAttributes(Ljava/lang/String;)Lcom/evernote/edam/type/ResourceAttributes;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 816
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResourceAttributes(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/ResourceAttributes;

    move-result-object p1

    return-object p1
.end method

.method public getResourceAttributesAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/ResourceAttributes;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/ResourceAttributes;",
            ">;"
        }
    .end annotation

    .line 820
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$57;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$57;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResourceByHash(Ljava/lang/String;[BZZZ)Lcom/evernote/edam/type/Resource;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 775
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResourceByHash(Ljava/lang/String;Ljava/lang/String;[BZZZ)Lcom/evernote/edam/type/Resource;

    move-result-object p1

    return-object p1
.end method

.method public getResourceByHashAsync(Ljava/lang/String;[BZZZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[BZZZ",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Resource;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Resource;",
            ">;"
        }
    .end annotation

    .line 781
    new-instance v7, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$54;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$54;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;[BZZZ)V

    invoke-virtual {p0, v7, p6}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResourceData(Ljava/lang/String;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 760
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResourceData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object p1

    return-object p1
.end method

.method public getResourceDataAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "[B>;)",
            "Ljava/util/concurrent/Future<",
            "[B>;"
        }
    .end annotation

    .line 764
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$53;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$53;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResourceRecognition(Ljava/lang/String;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 790
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResourceRecognition(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object p1

    return-object p1
.end method

.method public getResourceRecognitionAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "[B>;)",
            "Ljava/util/concurrent/Future<",
            "[B>;"
        }
    .end annotation

    .line 794
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$55;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$55;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getResourceSearchText(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 531
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getResourceSearchText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getResourceSearchTextAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 535
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$36;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$36;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getSearch(Ljava/lang/String;)Lcom/evernote/edam/type/SavedSearch;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 328
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getSearch(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/SavedSearch;

    move-result-object p1

    return-object p1
.end method

.method public getSearchAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;"
        }
    .end annotation

    .line 332
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$21;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$21;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getSharedNotebookByAuth()Lcom/evernote/edam/type/SharedNotebook;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 993
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getSharedNotebookByAuth(Ljava/lang/String;)Lcom/evernote/edam/type/SharedNotebook;

    move-result-object v0

    return-object v0
.end method

.method public getSharedNotebookByAuthAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;"
        }
    .end annotation

    .line 997
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$70;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$70;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getSyncChunk(IIZ)Lcom/evernote/edam/notestore/SyncChunk;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->getSyncChunk(Ljava/lang/String;IIZ)Lcom/evernote/edam/notestore/SyncChunk;

    move-result-object p1

    return-object p1
.end method

.method public getSyncChunkAsync(IIZLcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZ",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/SyncChunk;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/SyncChunk;",
            ">;"
        }
    .end annotation

    .line 93
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$3;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;IIZ)V

    invoke-virtual {p0, v0, p4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getSyncState()Lcom/evernote/edam/notestore/SyncState;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getSyncState(Ljava/lang/String;)Lcom/evernote/edam/notestore/SyncState;

    move-result-object v0

    return-object v0
.end method

.method public getSyncStateAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/SyncState;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/SyncState;",
            ">;"
        }
    .end annotation

    .line 67
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$1;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$1;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getSyncStateWithMetrics(Lcom/evernote/edam/notestore/ClientUsageMetrics;)Lcom/evernote/edam/notestore/SyncState;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getSyncStateWithMetrics(Ljava/lang/String;Lcom/evernote/edam/notestore/ClientUsageMetrics;)Lcom/evernote/edam/notestore/SyncState;

    move-result-object p1

    return-object p1
.end method

.method public getSyncStateWithMetricsAsync(Lcom/evernote/edam/notestore/ClientUsageMetrics;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/notestore/ClientUsageMetrics;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/notestore/SyncState;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/notestore/SyncState;",
            ">;"
        }
    .end annotation

    .line 80
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$2;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$2;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/notestore/ClientUsageMetrics;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public getTag(Ljava/lang/String;)Lcom/evernote/edam/type/Tag;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 249
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->getTag(Ljava/lang/String;Ljava/lang/String;)Lcom/evernote/edam/type/Tag;

    move-result-object p1

    return-object p1
.end method

.method public getTagAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Tag;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Tag;",
            ">;"
        }
    .end annotation

    .line 253
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$15;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$15;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public listLinkedNotebooks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 954
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->listLinkedNotebooks(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public listLinkedNotebooksAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            ">;>;"
        }
    .end annotation

    .line 958
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$67;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$67;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public listNoteVersions(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NoteVersionId;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 648
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->listNoteVersions(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listNoteVersionsAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NoteVersionId;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/notestore/NoteVersionId;",
            ">;>;"
        }
    .end annotation

    .line 652
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$45;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$45;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public listNotebooks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->listNotebooks(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public listNotebooksAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Notebook;",
            ">;>;"
        }
    .end annotation

    .line 149
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$7;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$7;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public listSearches()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 315
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->listSearches(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public listSearchesAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SavedSearch;",
            ">;>;"
        }
    .end annotation

    .line 319
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$20;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$20;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public listSharedNotebooks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 902
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->listSharedNotebooks(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public listSharedNotebooksAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/SharedNotebook;",
            ">;>;"
        }
    .end annotation

    .line 906
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$63;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$63;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public listTags()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 223
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/evernote/edam/notestore/NoteStore$Client;->listTags(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public listTagsAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;>;"
        }
    .end annotation

    .line 227
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$13;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$13;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;)V

    invoke-virtual {p0, v0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public listTagsByNotebook(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 236
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->listTagsByNotebook(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public listTagsByNotebookAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;>;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/util/List<",
            "Lcom/evernote/edam/type/Tag;",
            ">;>;"
        }
    .end annotation

    .line 240
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$14;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$14;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public sendMessageToSharedNotebookMembers(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 887
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->sendMessageToSharedNotebookMembers(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I

    move-result p1

    return p1
.end method

.method public sendMessageToSharedNotebookMembersAsync(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 893
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$62;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$62;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v0, p4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public setNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 479
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->setNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public setNoteApplicationDataEntryAsync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 483
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$32;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$32;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public setResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 721
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->setResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public setResourceApplicationDataEntryAsync(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 725
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$50;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$50;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public setSharedNotebookRecipientSettings(JLcom/evernote/edam/type/SharedNotebookRecipientSettings;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 870
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/evernote/edam/notestore/NoteStore$Client;->setSharedNotebookRecipientSettings(Ljava/lang/String;JLcom/evernote/edam/type/SharedNotebookRecipientSettings;)I

    move-result p1

    return p1
.end method

.method public setSharedNotebookRecipientSettingsAsync(JLcom/evernote/edam/type/SharedNotebookRecipientSettings;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/evernote/edam/type/SharedNotebookRecipientSettings;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 876
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$61;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;JLcom/evernote/edam/type/SharedNotebookRecipientSettings;)V

    invoke-virtual {p0, v0, p4}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public shareNote(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1020
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->shareNote(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public shareNoteAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1024
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$72;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$72;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public stopSharingNote(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 1033
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->stopSharingNote(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public stopSharingNoteAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Void;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 1037
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$73;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$73;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public unsetNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 492
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->unsetNoteApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public unsetNoteApplicationDataEntryAsync(Ljava/lang/String;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 496
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$33;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$33;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public unsetResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 734
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/evernote/edam/notestore/NoteStore$Client;->unsetResourceApplicationDataEntry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public unsetResourceApplicationDataEntryAsync(Ljava/lang/String;Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 738
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$51;

    invoke-direct {v0, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$51;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p3}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public untagAll(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 288
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->untagAll(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public untagAllAsync(Ljava/lang/String;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Void;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .line 292
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$18;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$18;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public updateLinkedNotebook(Lcom/evernote/edam/type/LinkedNotebook;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 941
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->updateLinkedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/LinkedNotebook;)I

    move-result p1

    return p1
.end method

.method public updateLinkedNotebookAsync(Lcom/evernote/edam/type/LinkedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/LinkedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 945
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$66;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$66;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/LinkedNotebook;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public updateNote(Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 570
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->updateNote(Ljava/lang/String;Lcom/evernote/edam/type/Note;)Lcom/evernote/edam/type/Note;

    move-result-object p1

    return-object p1
.end method

.method public updateNoteAsync(Lcom/evernote/edam/type/Note;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Note;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Lcom/evernote/edam/type/Note;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Lcom/evernote/edam/type/Note;",
            ">;"
        }
    .end annotation

    .line 574
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$39;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$39;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/Note;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public updateNotebook(Lcom/evernote/edam/type/Notebook;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->updateNotebook(Ljava/lang/String;Lcom/evernote/edam/type/Notebook;)I

    move-result p1

    return p1
.end method

.method public updateNotebookAsync(Lcom/evernote/edam/type/Notebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Notebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 201
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$11;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$11;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/Notebook;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public updateResource(Lcom/evernote/edam/type/Resource;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 747
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->updateResource(Ljava/lang/String;Lcom/evernote/edam/type/Resource;)I

    move-result p1

    return p1
.end method

.method public updateResourceAsync(Lcom/evernote/edam/type/Resource;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Resource;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 751
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$52;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$52;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/Resource;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public updateSearch(Lcom/evernote/edam/type/SavedSearch;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 354
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->updateSearch(Ljava/lang/String;Lcom/evernote/edam/type/SavedSearch;)I

    move-result p1

    return p1
.end method

.method public updateSearchAsync(Lcom/evernote/edam/type/SavedSearch;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/SavedSearch;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 358
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$23;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$23;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/SavedSearch;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public updateSharedNotebook(Lcom/evernote/edam/type/SharedNotebook;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 855
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->updateSharedNotebook(Ljava/lang/String;Lcom/evernote/edam/type/SharedNotebook;)I

    move-result p1

    return p1
.end method

.method public updateSharedNotebookAsync(Lcom/evernote/edam/type/SharedNotebook;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/SharedNotebook;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 859
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$60;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$60;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/SharedNotebook;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

.method public updateTag(Lcom/evernote/edam/type/Tag;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/evernote/edam/error/EDAMUserException;,
            Lcom/evernote/edam/error/EDAMSystemException;,
            Lcom/evernote/edam/error/EDAMNotFoundException;,
            Lcom/evernote/thrift/TException;
        }
    .end annotation

    .line 275
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mClient:Lcom/evernote/edam/notestore/NoteStore$Client;

    iget-object v1, p0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->mAuthenticationToken:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/evernote/edam/notestore/NoteStore$Client;->updateTag(Ljava/lang/String;Lcom/evernote/edam/type/Tag;)I

    move-result p1

    return p1
.end method

.method public updateTagAsync(Lcom/evernote/edam/type/Tag;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/evernote/edam/type/Tag;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/concurrent/Future<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 279
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$17;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient$17;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;Lcom/evernote/edam/type/Tag;)V

    invoke-virtual {p0, v0, p2}, Lcom/evernote/client/android/asyncclient/EvernoteNoteStoreClient;->submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

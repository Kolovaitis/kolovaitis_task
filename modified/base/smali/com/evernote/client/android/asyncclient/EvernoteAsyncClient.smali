.class public abstract Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;
.super Ljava/lang/Object;
.source "EvernoteAsyncClient.java"


# static fields
.field private static final UI_HANDLER:Landroid/os/Handler;

.field private static final UI_THREAD:Ljava/lang/Thread;


# instance fields
.field private final mExecutorService:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 21
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->UI_HANDLER:Landroid/os/Handler;

    .line 22
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    sput-object v0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->UI_THREAD:Ljava/lang/Thread;

    return-void
.end method

.method protected constructor <init>(Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lcom/evernote/client/android/helper/EvernotePreconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method static synthetic access$000(Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;Ljava/lang/Object;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->onResult(Ljava/lang/Object;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V

    return-void
.end method

.method static synthetic access$100(Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;Ljava/lang/Exception;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->onException(Ljava/lang/Exception;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V

    return-void
.end method

.method private onException(Ljava/lang/Exception;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Exception;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "TT;>;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 60
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$3;

    invoke-direct {v0, p0, p2, p1}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$3;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;Ljava/lang/Exception;)V

    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method private onResult(Ljava/lang/Object;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "TT;>;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 49
    new-instance v0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$2;

    invoke-direct {v0, p0, p2, p1}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$2;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;Lcom/evernote/client/android/asyncclient/EvernoteCallback;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->runOnUiThread(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final runOnUiThread(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 70
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->UI_THREAD:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 71
    sget-object v0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->UI_HANDLER:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 73
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void
.end method

.method protected submitTask(Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
    .locals 2
    .param p1    # Ljava/util/concurrent/Callable;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/evernote/client/android/asyncclient/EvernoteCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable<",
            "TT;>;",
            "Lcom/evernote/client/android/asyncclient/EvernoteCallback<",
            "TT;>;)",
            "Ljava/util/concurrent/Future<",
            "TT;>;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient$1;-><init>(Lcom/evernote/client/android/asyncclient/EvernoteAsyncClient;Ljava/util/concurrent/Callable;Lcom/evernote/client/android/asyncclient/EvernoteCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p1

    return-object p1
.end method

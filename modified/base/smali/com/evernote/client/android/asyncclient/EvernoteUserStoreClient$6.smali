.class Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$6;
.super Ljava/lang/Object;
.source "EvernoteUserStoreClient.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->revokeLongSessionAsync(Lcom/evernote/client/android/asyncclient/EvernoteCallback;)Ljava/util/concurrent/Future;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;)V
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$6;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 127
    invoke-virtual {p0}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$6;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient$6;->this$0:Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;

    invoke-virtual {v0}, Lcom/evernote/client/android/asyncclient/EvernoteUserStoreClient;->revokeLongSession()V

    const/4 v0, 0x0

    return-object v0
.end method

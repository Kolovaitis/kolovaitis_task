.class Lcom/evernote/client/android/login/EvernoteLoginTask;
.super Lnet/vrallev/android/task/Task;
.source "EvernoteLoginTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lnet/vrallev/android/task/Task<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final CAT:Lcom/evernote/client/android/helper/Cat;

.field public static final REQUEST_AUTH:I = 0x35a

.field public static final REQUEST_PROFILE_NAME:I = 0x35b


# instance fields
.field private mBootstrapCountDownLatch:Ljava/util/concurrent/CountDownLatch;

.field private mBootstrapIndex:I

.field private mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

.field private mBootstrapProfiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/evernote/edam/userstore/BootstrapProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mData:Landroid/content/Intent;

.field private final mIsFragment:Z

.field private final mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

.field private mResultCode:I

.field private mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/evernote/client/android/helper/Cat;

    const-string v1, "EvernoteLoginTask"

    invoke-direct {v0, v1}, Lcom/evernote/client/android/helper/Cat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/client/android/login/EvernoteLoginTask;->CAT:Lcom/evernote/client/android/helper/Cat;

    return-void
.end method

.method public constructor <init>(Lcom/evernote/client/android/EvernoteOAuthHelper;Z)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lnet/vrallev/android/task/Task;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    .line 46
    iput-boolean p2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mIsFragment:Z

    return-void
.end method

.method private canContinue()Z
    .locals 1

    .line 164
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private finishAuthorization()Z
    .locals 4

    .line 160
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCode:I

    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mData:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/evernote/client/android/EvernoteOAuthHelper;->finishAuthorization(Landroid/app/Activity;ILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private getBootstrapProfileNameFromMainApp()Ljava/lang/String;
    .locals 5

    .line 233
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 239
    :cond_0
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    move-result-object v2

    if-nez v2, :cond_1

    return-object v1

    .line 244
    :cond_1
    invoke-static {}, Lcom/evernote/client/android/EvernoteSession;->getInstance()Lcom/evernote/client/android/EvernoteSession;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/evernote/client/android/EvernoteUtil;->createGetBootstrapProfileNameIntent(Landroid/content/Context;Lcom/evernote/client/android/EvernoteSession;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    return-object v1

    :cond_2
    const/16 v3, 0x35b

    .line 249
    invoke-interface {v2, v0, v3}, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;->startActivityForResult(Landroid/content/Intent;I)V

    .line 251
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 253
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mData:Landroid/content/Intent;

    if-nez v0, :cond_3

    return-object v1

    :cond_3
    const-string v1, "bootstrap_profile_name"

    .line 262
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    return-object v1
.end method

.method private getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;
    .locals 3

    .line 168
    iget-boolean v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mIsFragment:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 169
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 170
    instance-of v2, v0, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    if-eqz v2, :cond_0

    .line 171
    check-cast v0, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    return-object v0

    :cond_0
    return-object v1

    .line 177
    :cond_1
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 178
    instance-of v2, v0, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    if-eqz v2, :cond_2

    .line 179
    check-cast v0, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    return-object v0

    :cond_2
    return-object v1
.end method

.method private getNextBootstrapProfile()Lcom/evernote/edam/userstore/BootstrapProfile;
    .locals 2

    .line 216
    iget v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    .line 217
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/edam/userstore/BootstrapProfile;

    return-object v0
.end method

.method private getScreenName(Lcom/evernote/edam/userstore/BootstrapProfile;)Ljava/lang/String;
    .locals 2

    const-string v0, "Evernote-China"

    .line 221
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapProfile;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "\u5370\u8c61\u7b14\u8bb0"

    return-object p1

    :cond_0
    const-string v0, "https://www.evernote.com"

    .line 224
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapProfile;->getSettings()Lcom/evernote/edam/userstore/BootstrapSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/evernote/edam/userstore/BootstrapSettings;->getServiceHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "Evernote International"

    return-object p1

    .line 228
    :cond_1
    invoke-virtual {p1}, Lcom/evernote/edam/userstore/BootstrapProfile;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private showBootstrapOption()V
    .locals 4

    .line 187
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 192
    :cond_0
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getNextBootstrapProfile()Lcom/evernote/edam/userstore/BootstrapProfile;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getScreenName(Lcom/evernote/edam/userstore/BootstrapProfile;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;->show(Ljava/lang/String;)V

    .line 196
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v1, 0x3

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->showBootstrapOption()V

    goto :goto_0

    .line 204
    :cond_1
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    .line 206
    invoke-interface {v0, v1}, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;->show(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 211
    sget-object v1, Lcom/evernote/client/android/login/EvernoteLoginTask;->CAT:Lcom/evernote/client/android/helper/Cat;

    invoke-virtual {v1, v0}, Lcom/evernote/client/android/helper/Cat;->e(Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private startAuthorization()Z
    .locals 6

    .line 89
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    .line 94
    :try_start_0
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    invoke-virtual {v2}, Lcom/evernote/client/android/EvernoteOAuthHelper;->fetchBootstrapProfiles()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    .line 95
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/evernote/client/android/EvernoteOAuthHelper;->getDefaultBootstrapProfile(Ljava/util/List;)Lcom/evernote/edam/userstore/BootstrapProfile;

    move-result-object v2

    iput-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 97
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    move-result v2

    if-nez v2, :cond_1

    return v1

    .line 101
    :cond_1
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v0, :cond_7

    .line 102
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getBootstrapProfileNameFromMainApp()Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    move-result v3

    if-nez v3, :cond_2

    return v1

    .line 109
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 110
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 111
    invoke-virtual {v4}, Lcom/evernote/edam/userstore/BootstrapProfile;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 112
    iput-object v4, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    const/4 v2, 0x0

    goto :goto_0

    :cond_4
    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_7

    const/4 v2, 0x0

    .line 120
    :goto_1
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 121
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    iget-object v4, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/evernote/edam/userstore/BootstrapProfile;

    invoke-virtual {v3, v4}, Lcom/evernote/edam/userstore/BootstrapProfile;->equals(Lcom/evernote/edam/userstore/BootstrapProfile;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 122
    iput v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    goto :goto_2

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 128
    :cond_6
    :goto_2
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->showBootstrapOption()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v2

    .line 133
    sget-object v3, Lcom/evernote/client/android/login/EvernoteLoginTask;->CAT:Lcom/evernote/client/android/helper/Cat;

    invoke-virtual {v3, v2}, Lcom/evernote/client/android/helper/Cat;->e(Ljava/lang/Throwable;)V

    .line 136
    :cond_7
    :goto_3
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    if-eqz v2, :cond_8

    .line 137
    iget-object v3, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    invoke-virtual {v3, v2}, Lcom/evernote/client/android/EvernoteOAuthHelper;->setBootstrapProfile(Lcom/evernote/edam/userstore/BootstrapProfile;)V

    .line 140
    :cond_8
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    move-result v2

    if-nez v2, :cond_9

    return v1

    .line 144
    :cond_9
    iget-object v2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mOAuthHelper:Lcom/evernote/client/android/EvernoteOAuthHelper;

    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/evernote/client/android/EvernoteOAuthHelper;->startAuthorization(Landroid/app/Activity;)Landroid/content/Intent;

    move-result-object v2

    .line 146
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    move-result v3

    if-eqz v3, :cond_c

    if-nez v2, :cond_a

    goto :goto_4

    .line 150
    :cond_a
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getLoginTaskCallback()Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;

    move-result-object v3

    if-eqz v3, :cond_b

    const/16 v1, 0x35a

    .line 152
    invoke-interface {v3, v2, v1}, Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;->startActivityForResult(Landroid/content/Intent;I)V

    return v0

    :cond_b
    return v1

    :cond_c
    :goto_4
    return v1
.end method


# virtual methods
.method public execute()Ljava/lang/Boolean;
    .locals 3

    .line 51
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->startAuthorization()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 53
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 56
    :cond_0
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->canContinue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 60
    :cond_1
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    invoke-direct {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->finishAuthorization()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 64
    :catch_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic execute()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->execute()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onActivityResult(ILandroid/content/Intent;)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 84
    :cond_0
    iput p1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mResultCode:I

    .line 85
    iput-object p2, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mData:Landroid/content/Intent;

    return-void
.end method

.method public switchBootstrapProfile()V
    .locals 2

    .line 71
    iget v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    .line 72
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfiles:Ljava/util/List;

    iget v1, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/evernote/edam/userstore/BootstrapProfile;

    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapProfile:Lcom/evernote/edam/userstore/BootstrapProfile;

    .line 74
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginTask;->mBootstrapCountDownLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    return-void
.end method

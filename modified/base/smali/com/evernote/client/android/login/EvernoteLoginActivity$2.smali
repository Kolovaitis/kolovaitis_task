.class Lcom/evernote/client/android/login/EvernoteLoginActivity$2;
.super Ljava/lang/Object;
.source "EvernoteLoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/login/EvernoteLoginActivity;->show(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/login/EvernoteLoginActivity;

.field final synthetic val$bootstrapScreenName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/login/EvernoteLoginActivity;Ljava/lang/String;)V
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$2;->this$0:Lcom/evernote/client/android/login/EvernoteLoginActivity;

    iput-object p2, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$2;->val$bootstrapScreenName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 145
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$2;->this$0:Lcom/evernote/client/android/login/EvernoteLoginActivity;

    invoke-static {v0}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->access$100(Lcom/evernote/client/android/login/EvernoteLoginActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$2;->val$bootstrapScreenName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$2;->this$0:Lcom/evernote/client/android/login/EvernoteLoginActivity;

    sget v2, Lcom/evernote/androidsdk/R$string;->esdk_switch_to:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$2;->val$bootstrapScreenName:Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 150
    new-instance v1, Lcom/evernote/client/android/login/EvernoteLoginActivity$2$1;

    invoke-direct {v1, p0}, Lcom/evernote/client/android/login/EvernoteLoginActivity$2$1;-><init>(Lcom/evernote/client/android/login/EvernoteLoginActivity$2;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_0
    const/16 v1, 0x8

    .line 158
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    const/4 v1, 0x0

    .line 159
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.class Lcom/evernote/client/android/login/EvernoteLoginFragment$1;
.super Ljava/lang/Object;
.source "EvernoteLoginFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/login/EvernoteLoginFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/login/EvernoteLoginFragment;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/login/EvernoteLoginFragment;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/evernote/client/android/login/EvernoteLoginFragment$1;->this$0:Lcom/evernote/client/android/login/EvernoteLoginFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 100
    invoke-static {}, Lnet/vrallev/android/task/TaskExecutor;->getInstance()Lnet/vrallev/android/task/TaskExecutor;

    move-result-object p1

    iget-object p2, p0, Lcom/evernote/client/android/login/EvernoteLoginFragment$1;->this$0:Lcom/evernote/client/android/login/EvernoteLoginFragment;

    invoke-static {p2}, Lcom/evernote/client/android/login/EvernoteLoginFragment;->access$000(Lcom/evernote/client/android/login/EvernoteLoginFragment;)I

    move-result p2

    invoke-virtual {p1, p2}, Lnet/vrallev/android/task/TaskExecutor;->getTask(I)Lnet/vrallev/android/task/Task;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/login/EvernoteLoginTask;

    if-eqz p1, :cond_0

    .line 102
    invoke-virtual {p1}, Lcom/evernote/client/android/login/EvernoteLoginTask;->cancel()V

    .line 104
    :cond_0
    iget-object p2, p0, Lcom/evernote/client/android/login/EvernoteLoginFragment$1;->this$0:Lcom/evernote/client/android/login/EvernoteLoginFragment;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/evernote/client/android/login/EvernoteLoginFragment;->onResult(Ljava/lang/Boolean;Lcom/evernote/client/android/login/EvernoteLoginTask;)V

    return-void
.end method

.class public Lcom/evernote/client/android/login/EvernoteLoginActivity;
.super Landroid/app/Activity;
.source "EvernoteLoginActivity.java"

# interfaces
.implements Lcom/evernote/client/android/login/EvernoteLoginTask$LoginTaskCallback;


# static fields
.field private static final EXTRA_CONSUMER_KEY:Ljava/lang/String; = "EXTRA_CONSUMER_KEY"

.field private static final EXTRA_CONSUMER_SECRET:Ljava/lang/String; = "EXTRA_CONSUMER_SECRET"

.field private static final EXTRA_LOCALE:Ljava/lang/String; = "EXTRA_LOCALE"

.field private static final EXTRA_SUPPORT_APP_LINKED_NOTEBOOKS:Ljava/lang/String; = "EXTRA_SUPPORT_APP_LINKED_NOTEBOOKS"

.field private static final KEY_RESULT_POSTED:Ljava/lang/String; = "KEY_RESULT_POSTED"

.field private static final KEY_TASK:Ljava/lang/String; = "KEY_TASK"


# instance fields
.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mResultPosted:Z

.field private mTaskKey:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/evernote/client/android/login/EvernoteLoginActivity;)I
    .locals 0

    .line 29
    iget p0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mTaskKey:I

    return p0
.end method

.method static synthetic access$100(Lcom/evernote/client/android/login/EvernoteLoginActivity;)Landroid/app/ProgressDialog;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p0
.end method

.method public static createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Locale;)Landroid/content/Intent;
    .locals 2

    .line 40
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/evernote/client/android/login/EvernoteLoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "EXTRA_CONSUMER_KEY"

    .line 41
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "EXTRA_CONSUMER_SECRET"

    .line 42
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "EXTRA_SUPPORT_APP_LINKED_NOTEBOOKS"

    .line 43
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string p0, "EXTRA_LOCALE"

    .line 44
    invoke-virtual {v0, p0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/16 v0, 0x35a

    if-eq p1, v0, :cond_1

    const/16 v0, 0x35b

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 101
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1

    .line 95
    :cond_1
    :goto_0
    invoke-static {}, Lnet/vrallev/android/task/TaskExecutor;->getInstance()Lnet/vrallev/android/task/TaskExecutor;

    move-result-object p1

    iget v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mTaskKey:I

    invoke-virtual {p1, v0}, Lnet/vrallev/android/task/TaskExecutor;->getTask(I)Lnet/vrallev/android/task/Task;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/login/EvernoteLoginTask;

    if-eqz p1, :cond_2

    .line 97
    invoke-virtual {p1, p2, p3}, Lcom/evernote/client/android/login/EvernoteLoginTask;->onActivityResult(ILandroid/content/Intent;)V

    :cond_2
    :goto_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .line 58
    new-instance v7, Lcom/evernote/client/android/EvernoteOAuthHelper;

    invoke-static {}, Lcom/evernote/client/android/EvernoteSession;->getInstance()Lcom/evernote/client/android/EvernoteSession;

    move-result-object v2

    const-string v1, "EXTRA_CONSUMER_KEY"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "EXTRA_CONSUMER_SECRET"

    .line 59
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "EXTRA_SUPPORT_APP_LINKED_NOTEBOOKS"

    const/4 v5, 0x1

    invoke-virtual {p1, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    const-string v1, "EXTRA_LOCALE"

    .line 60
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    move-object v6, p1

    check-cast v6, Ljava/util/Locale;

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/evernote/client/android/EvernoteOAuthHelper;-><init>(Lcom/evernote/client/android/EvernoteSession;Ljava/lang/String;Ljava/lang/String;ZLjava/util/Locale;)V

    .line 62
    invoke-static {}, Lnet/vrallev/android/task/TaskExecutor;->getInstance()Lnet/vrallev/android/task/TaskExecutor;

    move-result-object p1

    new-instance v1, Lcom/evernote/client/android/login/EvernoteLoginTask;

    invoke-direct {v1, v7, v0}, Lcom/evernote/client/android/login/EvernoteLoginTask;-><init>(Lcom/evernote/client/android/EvernoteOAuthHelper;Z)V

    invoke-virtual {p1, v1, p0}, Lnet/vrallev/android/task/TaskExecutor;->execute(Lnet/vrallev/android/task/Task;Landroid/app/Activity;)I

    move-result p1

    iput p1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mTaskKey:I

    goto :goto_0

    :cond_0
    const-string v1, "KEY_TASK"

    const/4 v2, -0x1

    .line 65
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mTaskKey:I

    const-string v1, "KEY_RESULT_POSTED"

    .line 66
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mResultPosted:Z

    :goto_0
    return-void
.end method

.method public final onResult(Ljava/lang/Boolean;Lcom/evernote/client/android/login/EvernoteLoginTask;)V
    .locals 1
    .annotation runtime Lnet/vrallev/android/task/TaskResult;
    .end annotation

    .line 108
    iget-boolean v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mResultPosted:Z

    if-nez v0, :cond_2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/evernote/client/android/login/EvernoteLoginTask;->getKey()I

    move-result p2

    iget v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mTaskKey:I

    if-eq p2, v0, :cond_0

    goto :goto_1

    :cond_0
    const/4 p2, 0x1

    .line 112
    iput-boolean p2, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mResultPosted:Z

    .line 114
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->setResult(I)V

    .line 115
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->finish()V

    return-void

    :cond_2
    :goto_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .line 87
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "KEY_TASK"

    .line 88
    iget v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mTaskKey:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "KEY_RESULT_POSTED"

    .line 89
    iget-boolean v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mResultPosted:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    .line 72
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 73
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 74
    :cond_0
    invoke-virtual {p0}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->showDialog()V

    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    .line 81
    iput-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 82
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public show(Ljava/lang/String;)V
    .locals 1

    .line 142
    new-instance v0, Lcom/evernote/client/android/login/EvernoteLoginActivity$2;

    invoke-direct {v0, p0, p1}, Lcom/evernote/client/android/login/EvernoteLoginActivity$2;-><init>(Lcom/evernote/client/android/login/EvernoteLoginActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected showDialog()V
    .locals 4

    .line 119
    new-instance v0, Lcom/evernote/client/android/login/EvernoteLoginActivity$1;

    invoke-direct {v0, p0}, Lcom/evernote/client/android/login/EvernoteLoginActivity$1;-><init>(Lcom/evernote/client/android/login/EvernoteLoginActivity;)V

    .line 131
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 132
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 133
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    sget v2, Lcom/evernote/androidsdk/R$string;->esdk_loading:I

    invoke-virtual {p0, v2}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/high16 v2, 0x1040000

    invoke-virtual {p0, v2}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, -0x2

    invoke-virtual {v1, v3, v2, v0}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 135
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 137
    iget-object v0, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method protected switchBootstrapProfile()V
    .locals 2

    .line 166
    invoke-static {}, Lnet/vrallev/android/task/TaskExecutor;->getInstance()Lnet/vrallev/android/task/TaskExecutor;

    move-result-object v0

    iget v1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity;->mTaskKey:I

    invoke-virtual {v0, v1}, Lnet/vrallev/android/task/TaskExecutor;->getTask(I)Lnet/vrallev/android/task/Task;

    move-result-object v0

    check-cast v0, Lcom/evernote/client/android/login/EvernoteLoginTask;

    if-eqz v0, :cond_0

    .line 168
    invoke-virtual {v0}, Lcom/evernote/client/android/login/EvernoteLoginTask;->switchBootstrapProfile()V

    :cond_0
    return-void
.end method

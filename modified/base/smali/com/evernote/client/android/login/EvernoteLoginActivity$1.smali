.class Lcom/evernote/client/android/login/EvernoteLoginActivity$1;
.super Ljava/lang/Object;
.source "EvernoteLoginActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/evernote/client/android/login/EvernoteLoginActivity;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/evernote/client/android/login/EvernoteLoginActivity;


# direct methods
.method constructor <init>(Lcom/evernote/client/android/login/EvernoteLoginActivity;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$1;->this$0:Lcom/evernote/client/android/login/EvernoteLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 122
    invoke-static {}, Lnet/vrallev/android/task/TaskExecutor;->getInstance()Lnet/vrallev/android/task/TaskExecutor;

    move-result-object p1

    iget-object p2, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$1;->this$0:Lcom/evernote/client/android/login/EvernoteLoginActivity;

    invoke-static {p2}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->access$000(Lcom/evernote/client/android/login/EvernoteLoginActivity;)I

    move-result p2

    invoke-virtual {p1, p2}, Lnet/vrallev/android/task/TaskExecutor;->getTask(I)Lnet/vrallev/android/task/Task;

    move-result-object p1

    check-cast p1, Lcom/evernote/client/android/login/EvernoteLoginTask;

    if-eqz p1, :cond_0

    .line 124
    invoke-virtual {p1}, Lcom/evernote/client/android/login/EvernoteLoginTask;->cancel()V

    .line 127
    :cond_0
    iget-object p2, p0, Lcom/evernote/client/android/login/EvernoteLoginActivity$1;->this$0:Lcom/evernote/client/android/login/EvernoteLoginActivity;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/evernote/client/android/login/EvernoteLoginActivity;->onResult(Ljava/lang/Boolean;Lcom/evernote/client/android/login/EvernoteLoginTask;)V

    return-void
.end method

.class public Lcom/evernote/client/android/AuthenticationResult;
.super Ljava/lang/Object;
.source "AuthenticationResult.java"


# static fields
.field private static final KEY_AUTH_TOKEN:Ljava/lang/String; = "evernote.mAuthToken"

.field private static final KEY_EVERNOTE_HOST:Ljava/lang/String; = "evernote.mEvernoteHost"

.field private static final KEY_IS_APP_LINKED_NOTEBOOK:Ljava/lang/String; = "evernote.isAppLinkedNotebook"

.field private static final KEY_NOTESTORE_URL:Ljava/lang/String; = "evernote.notestoreUrl"

.field private static final KEY_USER_ID:Ljava/lang/String; = "evernote.userId"

.field private static final KEY_WEB_API_URL_PREFIX:Ljava/lang/String; = "evernote.webApiUrlPrefix"

.field private static final PREFERENCE_NAME:Ljava/lang/String; = "evernote.preferences"


# instance fields
.field private mAuthToken:Ljava/lang/String;

.field private mEvernoteHost:Ljava/lang/String;

.field private mIsAppLinkedNotebook:Z

.field private mNoteStoreUrl:Ljava/lang/String;

.field private mUserId:I

.field private mWebApiUrlPrefix:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Lcom/evernote/client/android/AuthenticationResult;->mAuthToken:Ljava/lang/String;

    .line 82
    iput-object p2, p0, Lcom/evernote/client/android/AuthenticationResult;->mNoteStoreUrl:Ljava/lang/String;

    .line 83
    iput-object p3, p0, Lcom/evernote/client/android/AuthenticationResult;->mWebApiUrlPrefix:Ljava/lang/String;

    .line 84
    iput-object p4, p0, Lcom/evernote/client/android/AuthenticationResult;->mEvernoteHost:Ljava/lang/String;

    .line 85
    iput p5, p0, Lcom/evernote/client/android/AuthenticationResult;->mUserId:I

    .line 86
    iput-boolean p6, p0, Lcom/evernote/client/android/AuthenticationResult;->mIsAppLinkedNotebook:Z

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .line 77
    invoke-static {p2}, Lcom/evernote/client/android/AuthenticationResult;->parseWebApiUrlPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Lcom/evernote/client/android/AuthenticationResult;->parseHost(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/evernote/client/android/AuthenticationResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-void
.end method

.method static fromPreferences(Landroid/content/Context;)Lcom/evernote/client/android/AuthenticationResult;
    .locals 9

    .line 53
    invoke-static {p0}, Lcom/evernote/client/android/AuthenticationResult;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object p0

    const-string v0, "evernote.mAuthToken"

    const/4 v1, 0x0

    .line 54
    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "evernote.notestoreUrl"

    .line 55
    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 57
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 61
    :cond_0
    new-instance v0, Lcom/evernote/client/android/AuthenticationResult;

    const-string v2, "evernote.webApiUrlPrefix"

    .line 62
    invoke-interface {p0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "evernote.mEvernoteHost"

    .line 63
    invoke-interface {p0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v1, "evernote.userId"

    const/4 v2, -0x1

    .line 64
    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    const-string v1, "evernote.isAppLinkedNotebook"

    const/4 v2, 0x0

    .line 65
    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/evernote/client/android/AuthenticationResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    return-object v0

    :cond_1
    :goto_0
    return-object v1
.end method

.method protected static getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    const-string v0, "evernote.preferences"

    const/4 v1, 0x0

    .line 157
    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p0

    return-object p0
.end method

.method private static parseHost(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 170
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static parseWebApiUrlPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "notestore"

    .line 161
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x0

    .line 163
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    return-object p0
.end method


# virtual methods
.method clear()V
    .locals 2

    .line 102
    invoke-static {}, Lcom/evernote/client/android/EvernoteSession;->getInstance()Lcom/evernote/client/android/EvernoteSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/evernote/client/android/AuthenticationResult;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 103
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.mAuthToken"

    .line 104
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.notestoreUrl"

    .line 105
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.webApiUrlPrefix"

    .line 106
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.mEvernoteHost"

    .line 107
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.userId"

    .line 108
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.isAppLinkedNotebook"

    .line 109
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 110
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/evernote/client/android/AuthenticationResult;->mAuthToken:Ljava/lang/String;

    return-object v0
.end method

.method public getEvernoteHost()Ljava/lang/String;
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/evernote/client/android/AuthenticationResult;->mEvernoteHost:Ljava/lang/String;

    return-object v0
.end method

.method public getNoteStoreUrl()Ljava/lang/String;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/evernote/client/android/AuthenticationResult;->mNoteStoreUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .line 145
    iget v0, p0, Lcom/evernote/client/android/AuthenticationResult;->mUserId:I

    return v0
.end method

.method public getWebApiUrlPrefix()Ljava/lang/String;
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/evernote/client/android/AuthenticationResult;->mWebApiUrlPrefix:Ljava/lang/String;

    return-object v0
.end method

.method public isAppLinkedNotebook()Z
    .locals 1

    .line 153
    iget-boolean v0, p0, Lcom/evernote/client/android/AuthenticationResult;->mIsAppLinkedNotebook:Z

    return v0
.end method

.method persist()V
    .locals 3

    .line 90
    invoke-static {}, Lcom/evernote/client/android/EvernoteSession;->getInstance()Lcom/evernote/client/android/EvernoteSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/evernote/client/android/EvernoteSession;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/evernote/client/android/AuthenticationResult;->getPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.mAuthToken"

    iget-object v2, p0, Lcom/evernote/client/android/AuthenticationResult;->mAuthToken:Ljava/lang/String;

    .line 92
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.notestoreUrl"

    iget-object v2, p0, Lcom/evernote/client/android/AuthenticationResult;->mNoteStoreUrl:Ljava/lang/String;

    .line 93
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.webApiUrlPrefix"

    iget-object v2, p0, Lcom/evernote/client/android/AuthenticationResult;->mWebApiUrlPrefix:Ljava/lang/String;

    .line 94
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.mEvernoteHost"

    iget-object v2, p0, Lcom/evernote/client/android/AuthenticationResult;->mEvernoteHost:Ljava/lang/String;

    .line 95
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.userId"

    iget v2, p0, Lcom/evernote/client/android/AuthenticationResult;->mUserId:I

    .line 96
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "evernote.isAppLinkedNotebook"

    iget-boolean v2, p0, Lcom/evernote/client/android/AuthenticationResult;->mIsAppLinkedNotebook:Z

    .line 97
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 98
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

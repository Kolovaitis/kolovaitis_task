.class Lcom/eclipsesource/json/JsonLiteral;
.super Lcom/eclipsesource/json/JsonValue;
.source "JsonLiteral.java"


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonValue;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/eclipsesource/json/JsonLiteral;->value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public asBoolean()Z
    .locals 1

    .line 37
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonLiteral;->isBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonLiteral;->isTrue()Z

    move-result v0

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/eclipsesource/json/JsonValue;->asBoolean()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    .line 73
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_2

    return v0

    .line 76
    :cond_2
    check-cast p1, Lcom/eclipsesource/json/JsonLiteral;

    .line 77
    iget-object v0, p0, Lcom/eclipsesource/json/JsonLiteral;->value:Ljava/lang/String;

    iget-object p1, p1, Lcom/eclipsesource/json/JsonLiteral;->value:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/eclipsesource/json/JsonLiteral;->value:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isBoolean()Z
    .locals 1

    .line 47
    sget-object v0, Lcom/eclipsesource/json/JsonLiteral;->TRUE:Lcom/eclipsesource/json/JsonValue;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/eclipsesource/json/JsonLiteral;->FALSE:Lcom/eclipsesource/json/JsonValue;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isFalse()Z
    .locals 1

    .line 57
    sget-object v0, Lcom/eclipsesource/json/JsonLiteral;->FALSE:Lcom/eclipsesource/json/JsonValue;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNull()Z
    .locals 1

    .line 42
    sget-object v0, Lcom/eclipsesource/json/JsonLiteral;->NULL:Lcom/eclipsesource/json/JsonValue;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isTrue()Z
    .locals 1

    .line 52
    sget-object v0, Lcom/eclipsesource/json/JsonLiteral;->TRUE:Lcom/eclipsesource/json/JsonValue;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/eclipsesource/json/JsonLiteral;->value:Ljava/lang/String;

    return-object v0
.end method

.method protected write(Lcom/eclipsesource/json/JsonWriter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/eclipsesource/json/JsonLiteral;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/eclipsesource/json/JsonWriter;->write(Ljava/lang/String;)V

    return-void
.end method

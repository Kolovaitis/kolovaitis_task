.class public Lcom/eclipsesource/json/ParseException;
.super Ljava/lang/RuntimeException;
.source "ParseException.java"


# instance fields
.field private final column:I

.field private final line:I

.field private final offset:I


# direct methods
.method constructor <init>(Ljava/lang/String;III)V
    .locals 1

    .line 25
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " at "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ":"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 26
    iput p2, p0, Lcom/eclipsesource/json/ParseException;->offset:I

    .line 27
    iput p3, p0, Lcom/eclipsesource/json/ParseException;->line:I

    .line 28
    iput p4, p0, Lcom/eclipsesource/json/ParseException;->column:I

    return-void
.end method


# virtual methods
.method public getColumn()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/eclipsesource/json/ParseException;->column:I

    return v0
.end method

.method public getLine()I
    .locals 1

    .line 47
    iget v0, p0, Lcom/eclipsesource/json/ParseException;->line:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    .line 38
    iget v0, p0, Lcom/eclipsesource/json/ParseException;->offset:I

    return v0
.end method

.class Lcom/eclipsesource/json/JsonWriter;
.super Ljava/lang/Object;
.source "JsonWriter.java"


# static fields
.field private static final BS_CHARS:[C

.field private static final CONTROL_CHARACTERS_END:I = 0x1f

.field private static final CONTROL_CHARACTERS_START:I

.field private static final CR_CHARS:[C

.field private static final HEX_DIGITS:[C

.field private static final LF_CHARS:[C

.field private static final QUOT_CHARS:[C

.field private static final TAB_CHARS:[C

.field private static final UNICODE_2028_CHARS:[C

.field private static final UNICODE_2029_CHARS:[C


# instance fields
.field protected final writer:Ljava/io/Writer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x2

    .line 22
    new-array v1, v0, [C

    fill-array-data v1, :array_0

    sput-object v1, Lcom/eclipsesource/json/JsonWriter;->QUOT_CHARS:[C

    .line 23
    new-array v1, v0, [C

    fill-array-data v1, :array_1

    sput-object v1, Lcom/eclipsesource/json/JsonWriter;->BS_CHARS:[C

    .line 24
    new-array v1, v0, [C

    fill-array-data v1, :array_2

    sput-object v1, Lcom/eclipsesource/json/JsonWriter;->LF_CHARS:[C

    .line 25
    new-array v1, v0, [C

    fill-array-data v1, :array_3

    sput-object v1, Lcom/eclipsesource/json/JsonWriter;->CR_CHARS:[C

    .line 26
    new-array v0, v0, [C

    fill-array-data v0, :array_4

    sput-object v0, Lcom/eclipsesource/json/JsonWriter;->TAB_CHARS:[C

    const/4 v0, 0x6

    .line 29
    new-array v1, v0, [C

    fill-array-data v1, :array_5

    sput-object v1, Lcom/eclipsesource/json/JsonWriter;->UNICODE_2028_CHARS:[C

    .line 30
    new-array v0, v0, [C

    fill-array-data v0, :array_6

    sput-object v0, Lcom/eclipsesource/json/JsonWriter;->UNICODE_2029_CHARS:[C

    const/16 v0, 0x10

    .line 31
    new-array v0, v0, [C

    fill-array-data v0, :array_7

    sput-object v0, Lcom/eclipsesource/json/JsonWriter;->HEX_DIGITS:[C

    return-void

    nop

    :array_0
    .array-data 2
        0x5cs
        0x22s
    .end array-data

    :array_1
    .array-data 2
        0x5cs
        0x5cs
    .end array-data

    :array_2
    .array-data 2
        0x5cs
        0x6es
    .end array-data

    :array_3
    .array-data 2
        0x5cs
        0x72s
    .end array-data

    :array_4
    .array-data 2
        0x5cs
        0x74s
    .end array-data

    :array_5
    .array-data 2
        0x5cs
        0x75s
        0x32s
        0x30s
        0x32s
        0x38s
    .end array-data

    :array_6
    .array-data 2
        0x5cs
        0x75s
        0x32s
        0x30s
        0x32s
        0x39s
    .end array-data

    :array_7
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method constructor <init>(Ljava/io/Writer;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    return-void
.end method

.method private static getReplacementChars(C)[C
    .locals 4

    const/16 v0, 0x22

    if-ne p0, v0, :cond_0

    .line 65
    sget-object p0, Lcom/eclipsesource/json/JsonWriter;->QUOT_CHARS:[C

    goto :goto_0

    :cond_0
    const/16 v0, 0x5c

    if-ne p0, v0, :cond_1

    .line 67
    sget-object p0, Lcom/eclipsesource/json/JsonWriter;->BS_CHARS:[C

    goto :goto_0

    :cond_1
    const/16 v0, 0xa

    if-ne p0, v0, :cond_2

    .line 69
    sget-object p0, Lcom/eclipsesource/json/JsonWriter;->LF_CHARS:[C

    goto :goto_0

    :cond_2
    const/16 v0, 0xd

    if-ne p0, v0, :cond_3

    .line 71
    sget-object p0, Lcom/eclipsesource/json/JsonWriter;->CR_CHARS:[C

    goto :goto_0

    :cond_3
    const/16 v0, 0x9

    if-ne p0, v0, :cond_4

    .line 73
    sget-object p0, Lcom/eclipsesource/json/JsonWriter;->TAB_CHARS:[C

    goto :goto_0

    :cond_4
    const/16 v0, 0x2028

    if-ne p0, v0, :cond_5

    .line 75
    sget-object p0, Lcom/eclipsesource/json/JsonWriter;->UNICODE_2028_CHARS:[C

    goto :goto_0

    :cond_5
    const/16 v0, 0x2029

    if-ne p0, v0, :cond_6

    .line 77
    sget-object p0, Lcom/eclipsesource/json/JsonWriter;->UNICODE_2029_CHARS:[C

    goto :goto_0

    :cond_6
    if-ltz p0, :cond_7

    const/16 v0, 0x1f

    if-gt p0, v0, :cond_7

    const/4 v0, 0x6

    .line 79
    new-array v0, v0, [C

    fill-array-data v0, :array_0

    .line 80
    sget-object v1, Lcom/eclipsesource/json/JsonWriter;->HEX_DIGITS:[C

    shr-int/lit8 v2, p0, 0x4

    and-int/lit8 v2, v2, 0xf

    aget-char v2, v1, v2

    const/4 v3, 0x4

    aput-char v2, v0, v3

    const/4 v2, 0x5

    and-int/lit8 p0, p0, 0xf

    .line 81
    aget-char p0, v1, p0

    aput-char p0, v0, v2

    move-object p0, v0

    goto :goto_0

    :cond_7
    const/4 p0, 0x0

    :goto_0
    return-object p0

    nop

    :array_0
    .array-data 2
        0x5cs
        0x75s
        0x30s
        0x30s
        0x30s
        0x30s
    .end array-data
.end method


# virtual methods
.method write(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method protected writeArray(Lcom/eclipsesource/json/JsonArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 118
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonWriter;->writeBeginArray()V

    .line 120
    invoke-virtual {p1}, Lcom/eclipsesource/json/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/eclipsesource/json/JsonValue;

    if-nez v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonWriter;->writeArrayValueSeparator()V

    .line 124
    :cond_0
    invoke-virtual {v1, p0}, Lcom/eclipsesource/json/JsonValue;->write(Lcom/eclipsesource/json/JsonWriter;)V

    const/4 v0, 0x0

    goto :goto_0

    .line 127
    :cond_1
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonWriter;->writeEndArray()V

    return-void
.end method

.method protected writeArrayValueSeparator()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.method protected writeBeginArray()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 131
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.method protected writeBeginObject()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 102
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.method protected writeEndArray()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.method protected writeEndObject()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 106
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.method protected writeNameValueSeparator()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.method protected writeObject(Lcom/eclipsesource/json/JsonObject;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 87
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonWriter;->writeBeginObject()V

    .line 89
    invoke-virtual {p1}, Lcom/eclipsesource/json/JsonObject;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/eclipsesource/json/JsonObject$Member;

    if-nez v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonWriter;->writeObjectValueSeparator()V

    .line 93
    :cond_0
    invoke-virtual {v1}, Lcom/eclipsesource/json/JsonObject$Member;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/eclipsesource/json/JsonWriter;->writeString(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonWriter;->writeNameValueSeparator()V

    .line 95
    invoke-virtual {v1}, Lcom/eclipsesource/json/JsonObject$Member;->getValue()Lcom/eclipsesource/json/JsonValue;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/eclipsesource/json/JsonValue;->write(Lcom/eclipsesource/json/JsonWriter;)V

    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonWriter;->writeEndObject()V

    return-void
.end method

.method protected writeObjectValueSeparator()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.method writeString(Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(I)V

    .line 46
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 48
    new-array v2, v0, [C

    const/4 v3, 0x0

    .line 49
    invoke-virtual {p1, v3, v0, v2, v3}, Ljava/lang/String;->getChars(II[CI)V

    const/4 p1, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    .line 51
    aget-char v4, v2, v3

    invoke-static {v4}, Lcom/eclipsesource/json/JsonWriter;->getReplacementChars(C)[C

    move-result-object v4

    if-eqz v4, :cond_0

    .line 53
    iget-object v5, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    sub-int v6, v3, p1

    invoke-virtual {v5, v2, p1, v6}, Ljava/io/Writer;->write([CII)V

    .line 54
    iget-object p1, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    invoke-virtual {p1, v4}, Ljava/io/Writer;->write([C)V

    add-int/lit8 p1, v3, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 58
    :cond_1
    iget-object v3, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    sub-int/2addr v0, p1

    invoke-virtual {v3, v2, p1, v0}, Ljava/io/Writer;->write([CII)V

    .line 59
    iget-object p1, p0, Lcom/eclipsesource/json/JsonWriter;->writer:Ljava/io/Writer;

    invoke-virtual {p1, v1}, Ljava/io/Writer;->write(I)V

    return-void
.end method

.class public Lcom/eclipsesource/json/JsonObject;
.super Lcom/eclipsesource/json/JsonValue;
.source "JsonObject.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/eclipsesource/json/JsonObject$HashIndexTable;,
        Lcom/eclipsesource/json/JsonObject$Member;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/eclipsesource/json/JsonValue;",
        "Ljava/lang/Iterable<",
        "Lcom/eclipsesource/json/JsonObject$Member;",
        ">;"
    }
.end annotation


# instance fields
.field private final names:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private transient table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

.field private final values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/eclipsesource/json/JsonValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 73
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonValue;-><init>()V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    .line 76
    new-instance v0, Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonObject$HashIndexTable;-><init>()V

    iput-object v0, p0, Lcom/eclipsesource/json/JsonObject;->table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 1

    const/4 v0, 0x0

    .line 86
    invoke-direct {p0, p1, v0}, Lcom/eclipsesource/json/JsonObject;-><init>(Lcom/eclipsesource/json/JsonObject;Z)V

    return-void
.end method

.method private constructor <init>(Lcom/eclipsesource/json/JsonObject;Z)V
    .locals 1

    .line 89
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonValue;-><init>()V

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 94
    iget-object p2, p1, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    .line 95
    iget-object p1, p1, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    goto :goto_0

    .line 97
    :cond_0
    new-instance p2, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p2, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    .line 98
    new-instance p2, Ljava/util/ArrayList;

    iget-object p1, p1, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-direct {p2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p2, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    .line 100
    :goto_0
    new-instance p1, Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    invoke-direct {p1}, Lcom/eclipsesource/json/JsonObject$HashIndexTable;-><init>()V

    iput-object p1, p0, Lcom/eclipsesource/json/JsonObject;->table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    .line 101
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonObject;->updateHashIndex()V

    return-void

    .line 91
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "object is null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static readFrom(Ljava/io/Reader;)Lcom/eclipsesource/json/JsonObject;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 123
    invoke-static {p0}, Lcom/eclipsesource/json/JsonValue;->readFrom(Ljava/io/Reader;)Lcom/eclipsesource/json/JsonValue;

    move-result-object p0

    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonValue;->asObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object p0

    return-object p0
.end method

.method public static readFrom(Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 138
    invoke-static {p0}, Lcom/eclipsesource/json/JsonValue;->readFrom(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;

    move-result-object p0

    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonValue;->asObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object p0

    return-object p0
.end method

.method private declared-synchronized readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    monitor-enter p0

    .line 638
    :try_start_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 639
    new-instance p1, Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    invoke-direct {p1}, Lcom/eclipsesource/json/JsonObject$HashIndexTable;-><init>()V

    iput-object p1, p0, Lcom/eclipsesource/json/JsonObject;->table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    .line 640
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonObject;->updateHashIndex()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 641
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public static unmodifiableObject(Lcom/eclipsesource/json/JsonObject;)Lcom/eclipsesource/json/JsonObject;
    .locals 2

    .line 155
    new-instance v0, Lcom/eclipsesource/json/JsonObject;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/eclipsesource/json/JsonObject;-><init>(Lcom/eclipsesource/json/JsonObject;Z)V

    return-object v0
.end method

.method private updateHashIndex()V
    .locals 4

    .line 644
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 646
    iget-object v2, p0, Lcom/eclipsesource/json/JsonObject;->table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    iget-object v3, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/eclipsesource/json/JsonObject$HashIndexTable;->add(Ljava/lang/String;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;D)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 246
    invoke-static {p2, p3}, Lcom/eclipsesource/json/JsonObject;->valueOf(D)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public add(Ljava/lang/String;F)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 223
    invoke-static {p2}, Lcom/eclipsesource/json/JsonObject;->valueOf(F)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public add(Ljava/lang/String;I)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 177
    invoke-static {p2}, Lcom/eclipsesource/json/JsonObject;->valueOf(I)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public add(Ljava/lang/String;J)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 200
    invoke-static {p2, p3}, Lcom/eclipsesource/json/JsonObject;->valueOf(J)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;
    .locals 2

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    .line 321
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    iget-object v1, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/eclipsesource/json/JsonObject$HashIndexTable;->add(Ljava/lang/String;I)V

    .line 322
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    iget-object p1, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0

    .line 319
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value is null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 316
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "name is null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 292
    invoke-static {p2}, Lcom/eclipsesource/json/JsonObject;->valueOf(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public add(Ljava/lang/String;Z)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 269
    invoke-static {p2}, Lcom/eclipsesource/json/JsonObject;->valueOf(Z)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public asObject()Lcom/eclipsesource/json/JsonObject;
    .locals 0

    return-object p0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 620
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    return v1

    .line 623
    :cond_2
    check-cast p1, Lcom/eclipsesource/json/JsonObject;

    .line 624
    iget-object v2, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    iget-object v3, p1, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    iget-object p1, p1, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public get(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;
    .locals 1

    if-eqz p1, :cond_1

    .line 528
    invoke-virtual {p0, p1}, Lcom/eclipsesource/json/JsonObject;->indexOf(Ljava/lang/String;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 529
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/eclipsesource/json/JsonValue;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1

    .line 526
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "name is null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public hashCode()I
    .locals 2

    .line 607
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    const/16 v1, 0x1f

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 608
    iget-object v1, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method indexOf(Ljava/lang/String;)I
    .locals 2

    .line 628
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    invoke-virtual {v0, p1}, Lcom/eclipsesource/json/JsonObject$HashIndexTable;->get(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return v0

    .line 632
    :cond_0
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->lastIndexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public isEmpty()Z
    .locals 1

    .line 547
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isObject()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/eclipsesource/json/JsonObject$Member;",
            ">;"
        }
    .end annotation

    .line 568
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 569
    iget-object v1, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 570
    new-instance v2, Lcom/eclipsesource/json/JsonObject$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/eclipsesource/json/JsonObject$1;-><init>(Lcom/eclipsesource/json/JsonObject;Ljava/util/Iterator;Ljava/util/Iterator;)V

    return-object v2
.end method

.method public names()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 558
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;
    .locals 1

    if-eqz p1, :cond_1

    .line 506
    invoke-virtual {p0, p1}, Lcom/eclipsesource/json/JsonObject;->indexOf(Ljava/lang/String;)I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    invoke-virtual {v0, p1}, Lcom/eclipsesource/json/JsonObject$HashIndexTable;->remove(I)V

    .line 509
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 510
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    return-object p0

    .line 504
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "name is null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public set(Ljava/lang/String;D)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 411
    invoke-static {p2, p3}, Lcom/eclipsesource/json/JsonObject;->valueOf(D)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->set(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public set(Ljava/lang/String;F)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 389
    invoke-static {p2}, Lcom/eclipsesource/json/JsonObject;->valueOf(F)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->set(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public set(Ljava/lang/String;I)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 345
    invoke-static {p2}, Lcom/eclipsesource/json/JsonObject;->valueOf(I)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->set(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public set(Ljava/lang/String;J)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 367
    invoke-static {p2, p3}, Lcom/eclipsesource/json/JsonObject;->valueOf(J)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->set(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public set(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;
    .locals 2

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 482
    invoke-virtual {p0, p1}, Lcom/eclipsesource/json/JsonObject;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 484
    iget-object p1, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-interface {p1, v0, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->table:Lcom/eclipsesource/json/JsonObject$HashIndexTable;

    iget-object v1, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/eclipsesource/json/JsonObject$HashIndexTable;->add(Ljava/lang/String;I)V

    .line 487
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 488
    iget-object p1, p0, Lcom/eclipsesource/json/JsonObject;->values:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object p0

    .line 480
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "value is null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 477
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string p2, "name is null"

    invoke-direct {p1, p2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 455
    invoke-static {p2}, Lcom/eclipsesource/json/JsonObject;->valueOf(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->set(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public set(Ljava/lang/String;Z)Lcom/eclipsesource/json/JsonObject;
    .locals 0

    .line 433
    invoke-static {p2}, Lcom/eclipsesource/json/JsonObject;->valueOf(Z)Lcom/eclipsesource/json/JsonValue;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/eclipsesource/json/JsonObject;->set(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    return-object p0
.end method

.method public size()I
    .locals 1

    .line 538
    iget-object v0, p0, Lcom/eclipsesource/json/JsonObject;->names:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected write(Lcom/eclipsesource/json/JsonWriter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 591
    invoke-virtual {p1, p0}, Lcom/eclipsesource/json/JsonWriter;->writeObject(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method

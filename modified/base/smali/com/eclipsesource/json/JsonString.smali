.class Lcom/eclipsesource/json/JsonString;
.super Lcom/eclipsesource/json/JsonValue;
.source "JsonString.java"


# instance fields
.field private final string:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 21
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonValue;-><init>()V

    if-eqz p1, :cond_0

    .line 25
    iput-object p1, p0, Lcom/eclipsesource/json/JsonString;->string:Ljava/lang/String;

    return-void

    .line 23
    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "string is null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/eclipsesource/json/JsonString;->string:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    return v0

    .line 56
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_2

    return v0

    .line 59
    :cond_2
    check-cast p1, Lcom/eclipsesource/json/JsonString;

    .line 60
    iget-object v0, p0, Lcom/eclipsesource/json/JsonString;->string:Ljava/lang/String;

    iget-object p1, p1, Lcom/eclipsesource/json/JsonString;->string:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/eclipsesource/json/JsonString;->string:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public isString()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected write(Lcom/eclipsesource/json/JsonWriter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/eclipsesource/json/JsonString;->string:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/eclipsesource/json/JsonWriter;->writeString(Ljava/lang/String;)V

    return-void
.end method

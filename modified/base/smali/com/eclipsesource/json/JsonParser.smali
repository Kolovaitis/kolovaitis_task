.class Lcom/eclipsesource/json/JsonParser;
.super Ljava/lang/Object;
.source "JsonParser.java"


# static fields
.field private static final DEFAULT_BUFFER_SIZE:I = 0x400

.field private static final MIN_BUFFER_SIZE:I = 0xa


# instance fields
.field private final buffer:[C

.field private bufferOffset:I

.field private captureBuffer:Ljava/lang/StringBuilder;

.field private captureStart:I

.field private current:I

.field private fill:I

.field private index:I

.field private line:I

.field private lineOffset:I

.field private final reader:Ljava/io/Reader;


# direct methods
.method constructor <init>(Ljava/io/Reader;)V
    .locals 1

    const/16 v0, 0x400

    .line 49
    invoke-direct {p0, p1, v0}, Lcom/eclipsesource/json/JsonParser;-><init>(Ljava/io/Reader;I)V

    return-void
.end method

.method constructor <init>(Ljava/io/Reader;I)V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/eclipsesource/json/JsonParser;->reader:Ljava/io/Reader;

    .line 54
    new-array p1, p2, [C

    iput-object p1, p0, Lcom/eclipsesource/json/JsonParser;->buffer:[C

    const/4 p1, 0x1

    .line 55
    iput p1, p0, Lcom/eclipsesource/json/JsonParser;->line:I

    const/4 p1, -0x1

    .line 56
    iput p1, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 44
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/16 v1, 0x400

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    const/16 v1, 0xa

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-direct {p0, v0, p1}, Lcom/eclipsesource/json/JsonParser;-><init>(Ljava/io/Reader;I)V

    return-void
.end method

.method private endCapture()Ljava/lang/String;
    .locals 5

    .line 347
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    add-int/lit8 v0, v0, -0x1

    .line 349
    :goto_0
    iget-object v2, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 350
    iget-object v2, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/eclipsesource/json/JsonParser;->buffer:[C

    iget v4, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    sub-int/2addr v0, v4

    invoke-virtual {v2, v3, v4, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 351
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 352
    iget-object v2, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_1

    .line 354
    :cond_1
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/eclipsesource/json/JsonParser;->buffer:[C

    iget v4, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    sub-int/2addr v0, v4

    invoke-direct {v2, v3, v4, v0}, Ljava/lang/String;-><init>([CII)V

    move-object v0, v2

    .line 356
    :goto_1
    iput v1, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    return-object v0
.end method

.method private error(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;
    .locals 4

    .line 368
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->bufferOffset:I

    iget v1, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    add-int/2addr v0, v1

    .line 369
    iget v1, p0, Lcom/eclipsesource/json/JsonParser;->lineOffset:I

    sub-int v1, v0, v1

    .line 370
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->isEndOfText()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 371
    :goto_0
    new-instance v2, Lcom/eclipsesource/json/ParseException;

    iget v3, p0, Lcom/eclipsesource/json/JsonParser;->line:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v2, p1, v0, v3, v1}, Lcom/eclipsesource/json/ParseException;-><init>(Ljava/lang/String;III)V

    return-object v2
.end method

.method private expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;
    .locals 2

    .line 361
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->isEndOfText()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "Unexpected end of input"

    .line 362
    invoke-direct {p0, p1}, Lcom/eclipsesource/json/JsonParser;->error(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object p1

    return-object p1

    .line 364
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/eclipsesource/json/JsonParser;->error(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object p1

    return-object p1
.end method

.method private isDigit()Z
    .locals 2

    .line 379
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x30

    if-lt v0, v1, :cond_0

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isEndOfText()Z
    .locals 2

    .line 389
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isHexDigit()Z
    .locals 2

    .line 383
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x30

    if-lt v0, v1, :cond_0

    const/16 v1, 0x39

    if-le v0, v1, :cond_2

    :cond_0
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x61

    if-lt v0, v1, :cond_1

    const/16 v1, 0x66

    if-le v0, v1, :cond_2

    :cond_1
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x41

    if-lt v0, v1, :cond_3

    const/16 v1, 0x46

    if-gt v0, v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isWhiteSpace()Z
    .locals 2

    .line 375
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x20

    if-eq v0, v1, :cond_1

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private pauseCapture()V
    .locals 5

    .line 341
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    add-int/lit8 v0, v0, -0x1

    .line 342
    :goto_0
    iget-object v2, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/eclipsesource/json/JsonParser;->buffer:[C

    iget v4, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    sub-int/2addr v0, v4

    invoke-virtual {v2, v3, v4, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 343
    iput v1, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    return-void
.end method

.method private read()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 310
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->isEndOfText()Z

    move-result v0

    if-nez v0, :cond_3

    .line 313
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    iget v1, p0, Lcom/eclipsesource/json/JsonParser;->fill:I

    if-ne v0, v1, :cond_1

    .line 314
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 315
    iget-object v4, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/eclipsesource/json/JsonParser;->buffer:[C

    sub-int/2addr v1, v0

    invoke-virtual {v4, v5, v0, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 316
    iput v2, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    .line 318
    :cond_0
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->bufferOffset:I

    iget v1, p0, Lcom/eclipsesource/json/JsonParser;->fill:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/eclipsesource/json/JsonParser;->bufferOffset:I

    .line 319
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->reader:Ljava/io/Reader;

    iget-object v1, p0, Lcom/eclipsesource/json/JsonParser;->buffer:[C

    array-length v4, v1

    invoke-virtual {v0, v1, v2, v4}, Ljava/io/Reader;->read([CII)I

    move-result v0

    iput v0, p0, Lcom/eclipsesource/json/JsonParser;->fill:I

    .line 320
    iput v2, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    .line 321
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->fill:I

    if-ne v0, v3, :cond_1

    .line 322
    iput v3, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    return-void

    .line 326
    :cond_1
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    .line 327
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->line:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/eclipsesource/json/JsonParser;->line:I

    .line 328
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->bufferOffset:I

    iget v1, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/eclipsesource/json/JsonParser;->lineOffset:I

    .line 330
    :cond_2
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->buffer:[C

    iget v1, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    aget-char v0, v0, v1

    iput v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    return-void

    :cond_3
    const-string v0, "Unexpected end of input"

    .line 311
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->error(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0
.end method

.method private readArray()Lcom/eclipsesource/json/JsonArray;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 102
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    .line 103
    new-instance v0, Lcom/eclipsesource/json/JsonArray;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonArray;-><init>()V

    .line 104
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    const/16 v1, 0x5d

    .line 105
    invoke-direct {p0, v1}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    .line 109
    :cond_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    .line 110
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readValue()Lcom/eclipsesource/json/JsonValue;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/eclipsesource/json/JsonArray;->add(Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonArray;

    .line 111
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    const/16 v2, 0x2c

    .line 112
    invoke-direct {p0, v2}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    invoke-direct {p0, v1}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v1

    if-eqz v1, :cond_1

    return-object v0

    :cond_1
    const-string v0, "\',\' or \']\'"

    .line 114
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0
.end method

.method private readChar(C)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 288
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    if-eq v0, p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 291
    :cond_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    const/4 p1, 0x1

    return p1
.end method

.method private readDigit()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 296
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->isDigit()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 299
    :cond_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    const/4 v0, 0x1

    return v0
.end method

.method private readEscape()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    .line 206
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x22

    if-eq v0, v1, :cond_6

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_6

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_6

    const/16 v1, 0x62

    if-eq v0, v1, :cond_5

    const/16 v1, 0x66

    if-eq v0, v1, :cond_4

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_3

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    packed-switch v0, :pswitch_data_0

    const-string v0, "valid escape sequence"

    .line 239
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0

    :pswitch_0
    const/4 v0, 0x4

    .line 228
    new-array v1, v0, [C

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 230
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    .line 231
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->isHexDigit()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 234
    iget v3, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    int-to-char v3, v3

    aput-char v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "hexadecimal digit"

    .line 232
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 225
    :pswitch_1
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 222
    :cond_2
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 219
    :cond_3
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 216
    :cond_4
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 213
    :cond_5
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 210
    :cond_6
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 241
    :goto_1
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    return-void

    :pswitch_data_0
    .packed-switch 0x74
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private readExponent()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x65

    .line 273
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x45

    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/16 v0, 0x2b

    .line 276
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x2d

    .line 277
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    .line 279
    :cond_1
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readDigit()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 282
    :goto_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readDigit()Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    return v0

    :cond_3
    const-string v0, "digit"

    .line 280
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0
.end method

.method private readFalse()Lcom/eclipsesource/json/JsonValue;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 167
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    const/16 v0, 0x61

    .line 168
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    const/16 v0, 0x6c

    .line 169
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    const/16 v0, 0x73

    .line 170
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    const/16 v0, 0x65

    .line 171
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    .line 172
    sget-object v0, Lcom/eclipsesource/json/JsonValue;->FALSE:Lcom/eclipsesource/json/JsonValue;

    return-object v0
.end method

.method private readFraction()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x2e

    .line 261
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 264
    :cond_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readDigit()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    :goto_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readDigit()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    return v0

    :cond_2
    const-string v0, "digit"

    .line 265
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0
.end method

.method private readName()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 144
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x22

    if-ne v0, v1, :cond_0

    .line 147
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readStringInternal()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "name"

    .line 145
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0
.end method

.method private readNull()Lcom/eclipsesource/json/JsonValue;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 151
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    const/16 v0, 0x75

    .line 152
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    const/16 v0, 0x6c

    .line 153
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    .line 154
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    .line 155
    sget-object v0, Lcom/eclipsesource/json/JsonValue;->NULL:Lcom/eclipsesource/json/JsonValue;

    return-object v0
.end method

.method private readNumber()Lcom/eclipsesource/json/JsonValue;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 245
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->startCapture()V

    const/16 v0, 0x2d

    .line 246
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    .line 247
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    .line 248
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readDigit()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x30

    if-eq v0, v1, :cond_0

    .line 252
    :goto_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readDigit()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 255
    :cond_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readFraction()Z

    .line 256
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readExponent()Z

    .line 257
    new-instance v0, Lcom/eclipsesource/json/JsonNumber;

    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->endCapture()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/eclipsesource/json/JsonNumber;-><init>(Ljava/lang/String;)V

    return-object v0

    :cond_1
    const-string v0, "digit"

    .line 249
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0
.end method

.method private readObject()Lcom/eclipsesource/json/JsonObject;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 120
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    .line 121
    new-instance v0, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    .line 122
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    const/16 v1, 0x7d

    .line 123
    invoke-direct {p0, v1}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v0

    .line 127
    :cond_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    .line 128
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readName()Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    const/16 v3, 0x3a

    .line 130
    invoke-direct {p0, v3}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 133
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    .line 134
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readValue()Lcom/eclipsesource/json/JsonValue;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    .line 135
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    const/16 v2, 0x2c

    .line 136
    invoke-direct {p0, v2}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v2

    if-nez v2, :cond_0

    .line 137
    invoke-direct {p0, v1}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v1

    if-eqz v1, :cond_1

    return-object v0

    :cond_1
    const-string v0, "\',\' or \'}\'"

    .line 138
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0

    :cond_2
    const-string v0, "\':\'"

    .line 131
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0
.end method

.method private readRequiredChar(C)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 176
    invoke-direct {p0, p1}, Lcom/eclipsesource/json/JsonParser;->readChar(C)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 177
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object p1

    throw p1
.end method

.method private readString()Lcom/eclipsesource/json/JsonValue;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 182
    new-instance v0, Lcom/eclipsesource/json/JsonString;

    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readStringInternal()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/eclipsesource/json/JsonString;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private readStringInternal()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 186
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    .line 187
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->startCapture()V

    .line 188
    :goto_0
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x22

    if-eq v0, v1, :cond_2

    const/16 v1, 0x5c

    if-ne v0, v1, :cond_0

    .line 190
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->pauseCapture()V

    .line 191
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readEscape()V

    .line 192
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->startCapture()V

    goto :goto_0

    :cond_0
    const/16 v1, 0x20

    if-lt v0, v1, :cond_1

    .line 196
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    goto :goto_0

    :cond_1
    const-string v0, "valid string character"

    .line 194
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0

    .line 199
    :cond_2
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->endCapture()Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    return-object v0
.end method

.method private readTrue()Lcom/eclipsesource/json/JsonValue;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 159
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    const/16 v0, 0x72

    .line 160
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    const/16 v0, 0x75

    .line 161
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    const/16 v0, 0x65

    .line 162
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->readRequiredChar(C)V

    .line 163
    sget-object v0, Lcom/eclipsesource/json/JsonValue;->TRUE:Lcom/eclipsesource/json/JsonValue;

    return-object v0
.end method

.method private readValue()Lcom/eclipsesource/json/JsonValue;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 71
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->current:I

    const/16 v1, 0x22

    if-eq v0, v1, :cond_6

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_5

    const/16 v1, 0x5b

    if-eq v0, v1, :cond_4

    const/16 v1, 0x66

    if-eq v0, v1, :cond_3

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_1

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    const-string v0, "value"

    .line 97
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->expected(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0

    .line 83
    :cond_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    return-object v0

    .line 75
    :cond_1
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readTrue()Lcom/eclipsesource/json/JsonValue;

    move-result-object v0

    return-object v0

    .line 73
    :cond_2
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readNull()Lcom/eclipsesource/json/JsonValue;

    move-result-object v0

    return-object v0

    .line 77
    :cond_3
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readFalse()Lcom/eclipsesource/json/JsonValue;

    move-result-object v0

    return-object v0

    .line 81
    :cond_4
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readArray()Lcom/eclipsesource/json/JsonArray;

    move-result-object v0

    return-object v0

    .line 95
    :cond_5
    :pswitch_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readNumber()Lcom/eclipsesource/json/JsonValue;

    move-result-object v0

    return-object v0

    .line 79
    :cond_6
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readString()Lcom/eclipsesource/json/JsonValue;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x30
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private skipWhiteSpace()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 304
    :goto_0
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->isWhiteSpace()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private startCapture()V
    .locals 1

    .line 334
    iget-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/eclipsesource/json/JsonParser;->captureBuffer:Ljava/lang/StringBuilder;

    .line 337
    :cond_0
    iget v0, p0, Lcom/eclipsesource/json/JsonParser;->index:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/eclipsesource/json/JsonParser;->captureStart:I

    return-void
.end method


# virtual methods
.method parse()Lcom/eclipsesource/json/JsonValue;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->read()V

    .line 61
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    .line 62
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->readValue()Lcom/eclipsesource/json/JsonValue;

    move-result-object v0

    .line 63
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->skipWhiteSpace()V

    .line 64
    invoke-direct {p0}, Lcom/eclipsesource/json/JsonParser;->isEndOfText()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    const-string v0, "Unexpected character"

    .line 65
    invoke-direct {p0, v0}, Lcom/eclipsesource/json/JsonParser;->error(Ljava/lang/String;)Lcom/eclipsesource/json/ParseException;

    move-result-object v0

    throw v0
.end method

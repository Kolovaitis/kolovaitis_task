.class public Lcom/adobe/mps/ARAESCryptor;
.super Ljava/lang/Object;
.source "ARAESCryptor.java"


# static fields
.field private static final AES_BLOCK_SIZE:I = 0x10

.field private static final CIPHER_ALGORITHM:Ljava/lang/String; = "AES"

.field private static final DECRYPTOR_TRANSFORMATION:Ljava/lang/String; = "AES/ECB/NoPadding"

.field private static final ENCRYPTOR_TRANSFORMATION:Ljava/lang/String; = "AES/CBC/NoPadding"

.field private static final ENCRYPTOR_TRANSFORMATION_PADDING:Ljava/lang/String; = "AES/CBC/PKCS5Padding"


# instance fields
.field private mDecryptor:Ljavax/crypto/Cipher;

.field private mEncryptor:Ljavax/crypto/Cipher;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public Decrypt([BI[B)V
    .locals 2

    const/4 v0, 0x0

    .line 79
    :try_start_0
    iget-object v1, p0, Lcom/adobe/mps/ARAESCryptor;->mDecryptor:Ljavax/crypto/Cipher;

    invoke-virtual {v1, p1, v0, p2, p3}, Ljavax/crypto/Cipher;->update([BII[B)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/16 p1, 0x10

    return-void
.end method

.method public Encrypt([BI[BI)I
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/adobe/mps/ARAESCryptor;->mEncryptor:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v0

    const/4 v1, -0x1

    if-ge p4, v0, :cond_0

    return v1

    .line 101
    :cond_0
    :try_start_0
    iget-object p4, p0, Lcom/adobe/mps/ARAESCryptor;->mEncryptor:Ljavax/crypto/Cipher;

    const/4 v0, 0x0

    invoke-virtual {p4, p1, v0, p2, p3}, Ljavax/crypto/Cipher;->doFinal([BII[B)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    return v1
.end method

.method public Init_Decryptor([B)Z
    .locals 3

    const/4 v0, 0x0

    .line 36
    :try_start_0
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    const-string p1, "AES/ECB/NoPadding"

    .line 37
    invoke-static {p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object p1

    iput-object p1, p0, Lcom/adobe/mps/ARAESCryptor;->mDecryptor:Ljavax/crypto/Cipher;

    .line 38
    iget-object p1, p0, Lcom/adobe/mps/ARAESCryptor;->mDecryptor:Ljavax/crypto/Cipher;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    iget-object p1, p0, Lcom/adobe/mps/ARAESCryptor;->mDecryptor:Ljavax/crypto/Cipher;

    invoke-virtual {p1}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result p1

    const/16 v1, 0x10

    if-eq v1, p1, :cond_0

    return v0

    :cond_0
    const/4 p1, 0x1

    return p1

    :catch_0
    return v0
.end method

.method public Init_Encryptor([B[BZ)Z
    .locals 3

    const/4 v0, 0x0

    .line 55
    :try_start_0
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, p1, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    if-eqz p3, :cond_0

    const-string p1, "AES/CBC/PKCS5Padding"

    .line 57
    invoke-static {p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object p1

    iput-object p1, p0, Lcom/adobe/mps/ARAESCryptor;->mEncryptor:Ljavax/crypto/Cipher;

    goto :goto_0

    :cond_0
    const-string p1, "AES/CBC/NoPadding"

    .line 59
    invoke-static {p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object p1

    iput-object p1, p0, Lcom/adobe/mps/ARAESCryptor;->mEncryptor:Ljavax/crypto/Cipher;

    .line 60
    :goto_0
    new-instance p1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {p1, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 61
    iget-object p2, p0, Lcom/adobe/mps/ARAESCryptor;->mEncryptor:Ljavax/crypto/Cipher;

    const/4 p3, 0x1

    invoke-virtual {p2, p3, v1, p1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    iget-object p1, p0, Lcom/adobe/mps/ARAESCryptor;->mEncryptor:Ljavax/crypto/Cipher;

    invoke-virtual {p1}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result p1

    const/16 p2, 0x10

    if-eq p2, p1, :cond_1

    return v0

    :cond_1
    return p3

    :catch_0
    return v0
.end method

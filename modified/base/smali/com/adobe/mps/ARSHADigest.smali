.class public Lcom/adobe/mps/ARSHADigest;
.super Ljava/lang/Object;
.source "ARSHADigest.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public Digest([BII)[B
    .locals 1

    const/16 v0, 0x14

    if-eq p3, v0, :cond_2

    const/16 v0, 0x30

    if-eq p3, v0, :cond_1

    const/16 v0, 0x40

    if-eq p3, v0, :cond_0

    const-string p3, "SHA-256"

    goto :goto_0

    :cond_0
    const-string p3, "SHA-512"

    goto :goto_0

    :cond_1
    const-string p3, "SHA-384"

    goto :goto_0

    :cond_2
    const-string p3, "SHA-1"

    .line 47
    :goto_0
    :try_start_0
    invoke-static {p3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object p3

    const/4 v0, 0x0

    .line 48
    invoke-virtual {p3, p1, v0, p2}, Ljava/security/MessageDigest;->update([BII)V

    .line 49
    invoke-virtual {p3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    const/4 p1, 0x0

    return-object p1
.end method

.class public Lcom/adobe/mps/MPS;
.super Ljava/lang/Object;
.source "MPS.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "APC"

    .line 40
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const-string v0, "MPS"

    .line 41
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native ImagetoPDF([Ljava/lang/Object;Ljava/lang/String;)I
.end method

.method public native MPSInit(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native MPSTerm()I
.end method

.method public native PDFDocInit(Ljava/lang/String;)I
.end method

.method public native PDFDocInit(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native PDFGetPageAttributes(I)[I
.end method

.method public native PDFHasPrintPermission(Ljava/lang/String;)I
.end method

.method public native PDFPageRender(IIIILjava/nio/ByteBuffer;I[I)I
.end method

.method public native PDFPagetoImage(ILjava/lang/String;III)I
.end method

.method public native PDFtoImages(Ljava/lang/String;Ljava/lang/String;IIIZ)I
.end method

.class public Lcom/box/androidsdk/content/auth/OAuthWebView;
.super Landroid/webkit/WebView;
.source "OAuthWebView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;,
        Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;,
        Lcom/box/androidsdk/content/auth/OAuthWebView$InvalidUrlException;,
        Lcom/box/androidsdk/content/auth/OAuthWebView$OnPageFinishedListener;,
        Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;
    }
.end annotation


# static fields
.field private static final STATE:Ljava/lang/String; = "state"

.field private static final URL_QUERY_LOGIN:Ljava/lang/String; = "box_login"


# instance fields
.field private mBoxAccountEmail:Ljava/lang/String;

.field private state:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public authenticate(Landroid/net/Uri$Builder;)V
    .locals 2

    .line 90
    invoke-static {}, Lcom/box/androidsdk/content/utils/SdkUtils;->generateStateToken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView;->state:Ljava/lang/String;

    const-string v0, "state"

    .line 91
    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView;->state:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 92
    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/auth/OAuthWebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public authenticate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView;->buildUrl(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/auth/OAuthWebView;->authenticate(Landroid/net/Uri$Builder;)V

    return-void
.end method

.method protected buildUrl(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 3

    .line 96
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "https"

    .line 97
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "account.box.com"

    .line 98
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "api"

    .line 99
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "oauth2"

    .line 100
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "authorize"

    .line 101
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "response_type"

    const-string v2, "code"

    .line 102
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "client_id"

    .line 103
    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string p1, "redirect_uri"

    .line 104
    invoke-virtual {v0, p1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 105
    iget-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView;->mBoxAccountEmail:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string p2, "box_login"

    .line 106
    invoke-virtual {v0, p2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    return-object v0
.end method

.method public getStateString()Ljava/lang/String;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView;->state:Ljava/lang/String;

    return-object v0
.end method

.method public setBoxAccountEmail(Ljava/lang/String;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView;->mBoxAccountEmail:Ljava/lang/String;

    return-void
.end method

.class public Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "OAuthWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/auth/OAuthWebView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OAuthWebViewClient"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;,
        Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;
    }
.end annotation


# static fields
.field private static final WEB_VIEW_TIMEOUT:I = 0x7530


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mOnPageFinishedListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OnPageFinishedListener;

.field private mRedirectUrl:Ljava/lang/String;

.field private mTimeOutRunnable:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;

.field private mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

.field private sslErrorDialogContinueButtonClicked:Z


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;Ljava/lang/String;)V
    .locals 2

    .line 137
    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 126
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mHandler:Landroid/os/Handler;

    .line 138
    iput-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

    .line 139
    iput-object p2, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mRedirectUrl:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;)Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;
    .locals 0

    .line 116
    iget-object p0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

    return-object p0
.end method

.method static synthetic access$200(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;)Z
    .locals 0

    .line 116
    iget-boolean p0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->sslErrorDialogContinueButtonClicked:Z

    return p0
.end method

.method static synthetic access$202(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;Z)Z
    .locals 0

    .line 116
    iput-boolean p1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->sslErrorDialogContinueButtonClicked:Z

    return p1
.end method

.method private formatCertificateDate(Landroid/content/Context;Ljava/util/Date;)Ljava/lang/String;
    .locals 0

    if-nez p2, :cond_0

    const-string p1, ""

    return-object p1

    .line 386
    :cond_0
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object p1

    invoke-virtual {p1, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getCertErrorView(Landroid/content/Context;Landroid/net/http/SslCertificate;)Landroid/view/View;
    .locals 4

    .line 340
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 342
    sget v1, Lcom/box/sdk/android/R$layout;->ssl_certificate:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 346
    invoke-virtual {p2}, Landroid/net/http/SslCertificate;->getIssuedTo()Landroid/net/http/SslCertificate$DName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 348
    sget v2, Lcom/box/sdk/android/R$id;->to_common:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 349
    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 350
    sget v2, Lcom/box/sdk/android/R$id;->to_org:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 351
    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getOName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 352
    sget v2, Lcom/box/sdk/android/R$id;->to_org_unit:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 353
    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getUName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 358
    :cond_0
    invoke-virtual {p2}, Landroid/net/http/SslCertificate;->getIssuedBy()Landroid/net/http/SslCertificate$DName;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 360
    sget v2, Lcom/box/sdk/android/R$id;->by_common:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 361
    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getCName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    sget v2, Lcom/box/sdk/android/R$id;->by_org:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 363
    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getOName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 364
    sget v2, Lcom/box/sdk/android/R$id;->by_org_unit:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 365
    invoke-virtual {v1}, Landroid/net/http/SslCertificate$DName;->getUName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    :cond_1
    invoke-virtual {p2}, Landroid/net/http/SslCertificate;->getValidNotBeforeDate()Ljava/util/Date;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->formatCertificateDate(Landroid/content/Context;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 370
    sget v2, Lcom/box/sdk/android/R$id;->issued_on:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 371
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    invoke-virtual {p2}, Landroid/net/http/SslCertificate;->getValidNotAfterDate()Ljava/util/Date;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->formatCertificateDate(Landroid/content/Context;Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    .line 375
    sget p2, Lcom/box/sdk/android/R$id;->expires_on:I

    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 376
    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method private getURIfromURL(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .line 397
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 399
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mRedirectUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 400
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mRedirectUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 401
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    return-object p1

    :cond_1
    return-object p1
.end method

.method private getValueFromURI(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/auth/OAuthWebView$InvalidUrlException;
        }
    .end annotation

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 424
    :cond_0
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    const/4 v0, 0x0

    .line 393
    iput-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mTimeOutRunnable:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;

    if-eqz v0, :cond_0

    .line 183
    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 185
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mOnPageFinishedListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OnPageFinishedListener;

    if-eqz v0, :cond_1

    .line 187
    invoke-interface {v0, p1, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OnPageFinishedListener;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4

    const/4 p3, 0x0

    .line 145
    :try_start_0
    invoke-direct {p0, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->getURIfromURL(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "code"

    .line 146
    invoke-direct {p0, v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->getValueFromURI(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-static {v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    instance-of v2, p1, Lcom/box/androidsdk/content/auth/OAuthWebView;

    if-eqz v2, :cond_1

    .line 149
    move-object v2, p1

    check-cast v2, Lcom/box/androidsdk/content/auth/OAuthWebView;

    invoke-virtual {v2}, Lcom/box/androidsdk/content/auth/OAuthWebView;->getStateString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "state"

    .line 150
    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    move-object v3, p1

    check-cast v3, Lcom/box/androidsdk/content/auth/OAuthWebView;

    invoke-virtual {v3}, Lcom/box/androidsdk/content/auth/OAuthWebView;->getStateString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 152
    :cond_0
    new-instance v0, Lcom/box/androidsdk/content/auth/OAuthWebView$InvalidUrlException;

    invoke-direct {v0, p3}, Lcom/box/androidsdk/content/auth/OAuthWebView$InvalidUrlException;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$1;)V

    throw v0

    :cond_1
    :goto_0
    const-string v2, "error"

    .line 157
    invoke-direct {p0, v0, v2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->getValueFromURI(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 159
    invoke-static {v2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 160
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

    new-instance v1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p3}, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;->onAuthFailure(Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;)Z

    goto :goto_1

    .line 161
    :cond_2
    invoke-static {v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "base_domain"

    .line 162
    invoke-direct {p0, v0, v2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->getValueFromURI(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 164
    iget-object v2, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

    invoke-interface {v2, v1, v0}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;->onReceivedAuthCode(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 167
    :cond_3
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

    invoke-interface {v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;->onReceivedAuthCode(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/box/androidsdk/content/auth/OAuthWebView$InvalidUrlException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 171
    :catch_0
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

    new-instance v1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p3}, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;-><init>(ILjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;->onAuthFailure(Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;)Z

    .line 173
    :cond_4
    :goto_1
    iget-object p3, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mTimeOutRunnable:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;

    if-eqz p3, :cond_5

    .line 174
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 176
    :cond_5
    new-instance p3, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;

    invoke-direct {p3, p0, p1, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;Landroid/webkit/WebView;Ljava/lang/String;)V

    iput-object p3, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mTimeOutRunnable:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;

    .line 177
    iget-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mHandler:Landroid/os/Handler;

    iget-object p2, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mTimeOutRunnable:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;

    const-wide/16 v0, 0x7530

    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 16

    move-object/from16 v0, p0

    move/from16 v1, p2

    .line 193
    iget-object v2, v0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mTimeOutRunnable:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;

    if-eqz v2, :cond_0

    .line 194
    iget-object v3, v0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 196
    :cond_0
    iget-object v2, v0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mWebEventListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;

    new-instance v3, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;

    new-instance v4, Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-direct {v4, v1, v5, v6}, Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4}, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;)V

    invoke-interface {v2, v3}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;->onAuthFailure(Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    :cond_1
    const/4 v2, -0x8

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x3

    if-eq v1, v2, :cond_3

    const/4 v2, -0x6

    if-eq v1, v2, :cond_2

    const/4 v2, -0x2

    if-eq v1, v2, :cond_2

    goto/16 :goto_0

    .line 202
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isInternetAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 203
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v9, "offline.html"

    invoke-static {v2, v9}, Lcom/box/androidsdk/content/utils/SdkUtils;->getAssetFile(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 204
    new-instance v9, Ljava/util/Formatter;

    invoke-direct {v9}, Ljava/util/Formatter;-><init>()V

    .line 205
    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v10

    sget v11, Lcom/box/sdk/android/R$string;->boxsdk_no_offline_access:I

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v7

    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v10, Lcom/box/sdk/android/R$string;->boxsdk_no_offline_access_detail:I

    invoke-virtual {v7, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v4

    .line 206
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v7, Lcom/box/sdk/android/R$string;->boxsdk_no_offline_access_todo:I

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v3

    .line 205
    invoke-virtual {v9, v2, v8}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    const/4 v11, 0x0

    .line 212
    invoke-virtual {v9}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "text/html"

    const-string v14, "UTF-8"

    const/4 v15, 0x0

    move-object/from16 v10, p1

    invoke-virtual/range {v10 .. v15}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    invoke-virtual {v9}, Ljava/util/Formatter;->close()V

    goto :goto_0

    .line 217
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v9, "offline.html"

    invoke-static {v2, v9}, Lcom/box/androidsdk/content/utils/SdkUtils;->getAssetFile(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 218
    new-instance v9, Ljava/util/Formatter;

    invoke-direct {v9}, Ljava/util/Formatter;-><init>()V

    .line 219
    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v10

    sget v11, Lcom/box/sdk/android/R$string;->boxsdk_unable_to_connect:I

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v7

    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v10, Lcom/box/sdk/android/R$string;->boxsdk_unable_to_connect_detail:I

    invoke-virtual {v7, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v4

    .line 220
    invoke-virtual/range {p1 .. p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget v7, Lcom/box/sdk/android/R$string;->boxsdk_unable_to_connect_todo:I

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v3

    .line 219
    invoke-virtual {v9, v2, v8}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    const/4 v11, 0x0

    .line 226
    invoke-virtual {v9}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "text/html"

    const-string v14, "UTF-8"

    const/4 v15, 0x0

    move-object/from16 v10, p1

    invoke-virtual/range {v10 .. v15}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-virtual {v9}, Ljava/util/Formatter;->close()V

    .line 231
    :goto_0
    invoke-super/range {p0 .. p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedHttpAuthRequest(Landroid/webkit/WebView;Landroid/webkit/HttpAuthHandler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 239
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p3

    .line 240
    sget p4, Lcom/box/sdk/android/R$layout;->boxsdk_alert_dialog_text_entry:I

    const/4 v0, 0x0

    invoke-virtual {p3, p4, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 242
    new-instance p4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p4, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/box/sdk/android/R$string;->boxsdk_alert_dialog_text_entry:I

    invoke-virtual {p4, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    invoke-virtual {p1, p3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget p4, Lcom/box/sdk/android/R$string;->boxsdk_alert_dialog_ok:I

    new-instance v0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$2;

    invoke-direct {v0, p0, p3, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$2;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;Landroid/view/View;Landroid/webkit/HttpAuthHandler;)V

    .line 243
    invoke-virtual {p1, p4, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget p3, Lcom/box/sdk/android/R$string;->boxsdk_alert_dialog_cancel:I

    new-instance p4, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$1;

    invoke-direct {p4, p0, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$1;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;Landroid/webkit/HttpAuthHandler;)V

    .line 251
    invoke-virtual {p1, p3, p4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 258
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 259
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 4

    .line 264
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mTimeOutRunnable:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebViewTimeOutRunnable;

    if-eqz v0, :cond_0

    .line 265
    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 267
    :cond_0
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 268
    new-instance v1, Ljava/lang/StringBuilder;

    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_There_are_problems_with_the_security_certificate_for_this_site:I

    .line 269
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    .line 270
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    invoke-virtual {p3}, Landroid/net/http/SslError;->getPrimaryError()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 292
    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_ssl_error_warning_INVALID:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 289
    :pswitch_0
    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_ssl_error_warning_INVALID:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 274
    :pswitch_1
    invoke-virtual {p1}, Landroid/webkit/WebView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/box/sdk/android/R$string;->boxsdk_ssl_error_warning_DATE_INVALID:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 286
    :pswitch_2
    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_ssl_error_warning_UNTRUSTED:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 280
    :pswitch_3
    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_ssl_error_warning_ID_MISMATCH:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 277
    :pswitch_4
    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_ssl_error_warning_EXPIRED:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 283
    :pswitch_5
    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_ssl_error_warning_NOT_YET_VALID:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 295
    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    .line 296
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_ssl_should_not_proceed:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    .line 299
    iput-boolean v0, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->sslErrorDialogContinueButtonClicked:Z

    .line 300
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/box/sdk/android/R$string;->boxsdk_Security_Warning:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 301
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/box/sdk/android/R$drawable;->boxsdk_dialog_warning:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_Go_back:I

    new-instance v2, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$3;

    invoke-direct {v2, p0, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$3;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;Landroid/webkit/SslErrorHandler;)V

    .line 302
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p2

    .line 312
    sget v0, Lcom/box/sdk/android/R$string;->boxsdk_ssl_error_details:I

    new-instance v1, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$4;

    invoke-direct {v1, p0, p1, p3}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$4;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;Landroid/webkit/WebView;Landroid/net/http/SslError;)V

    invoke-virtual {p2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 319
    invoke-virtual {p2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 320
    new-instance p2, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$5;

    invoke-direct {p2, p0}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$5;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;)V

    invoke-virtual {p1, p2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 329
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setOnPageFinishedListener(Lcom/box/androidsdk/content/auth/OAuthWebView$OnPageFinishedListener;)V
    .locals 0

    .line 433
    iput-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->mOnPageFinishedListener:Lcom/box/androidsdk/content/auth/OAuthWebView$OnPageFinishedListener;

    return-void
.end method

.method protected showCertDialog(Landroid/content/Context;Landroid/net/http/SslError;)V
    .locals 2

    .line 334
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_Security_Warning:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 335
    invoke-virtual {p2}, Landroid/net/http/SslError;->getCertificate()Landroid/net/http/SslCertificate;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->getCertErrorView(Landroid/content/Context;Landroid/net/http/SslCertificate;)Landroid/view/View;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 336
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

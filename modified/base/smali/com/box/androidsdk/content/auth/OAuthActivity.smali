.class public Lcom/box/androidsdk/content/auth/OAuthActivity;
.super Landroid/app/Activity;
.source "OAuthActivity.java"

# interfaces
.implements Lcom/box/androidsdk/content/auth/ChooseAuthenticationFragment$OnAuthenticationChosen;
.implements Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;
.implements Lcom/box/androidsdk/content/auth/OAuthWebView$OnPageFinishedListener;


# static fields
.field public static final AUTH_CODE:Ljava/lang/String; = "authcode"

.field public static final AUTH_INFO:Ljava/lang/String; = "authinfo"

.field public static final AUTH_TYPE_APP:I = 0x1

.field public static final AUTH_TYPE_WEBVIEW:I = 0x0

.field private static final CHOOSE_AUTH_TAG:Ljava/lang/String; = "choose_auth"

.field public static final EXTRA_DISABLE_ACCOUNT_CHOOSING:Ljava/lang/String; = "disableAccountChoosing"

.field public static final EXTRA_SESSION:Ljava/lang/String; = "session"

.field public static final EXTRA_USER_ID_RESTRICTION:Ljava/lang/String; = "restrictToUserId"

.field protected static final IS_LOGGING_IN_VIA_BOX_APP:Ljava/lang/String; = "loggingInViaBoxApp"

.field protected static final LOGIN_VIA_BOX_APP:Ljava/lang/String; = "loginviaboxapp"

.field public static final REQUEST_BOX_APP_FOR_AUTH_CODE:I = 0x1

.field public static final USER_ID:Ljava/lang/String; = "userId"

.field private static dialog:Landroid/app/Dialog;


# instance fields
.field private apiCallStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private authType:I

.field private mAuthWasSuccessful:Z

.field private mClientId:Ljava/lang/String;

.field private mClientSecret:Ljava/lang/String;

.field private mConnectedReceiver:Landroid/content/BroadcastReceiver;

.field private mDeviceId:Ljava/lang/String;

.field private mDeviceName:Ljava/lang/String;

.field private mIsLoggingInViaBoxApp:Z

.field private mRedirectUrl:Ljava/lang/String;

.field private mSession:Lcom/box/androidsdk/content/models/BoxSession;

.field protected oauthClient:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;

.field protected oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    .line 88
    iput-boolean v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mAuthWasSuccessful:Z

    .line 89
    iput v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->authType:I

    .line 92
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->apiCallStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 93
    new-instance v0, Lcom/box/androidsdk/content/auth/OAuthActivity$1;

    invoke-direct {v0, p0}, Lcom/box/androidsdk/content/auth/OAuthActivity$1;-><init>(Lcom/box/androidsdk/content/auth/OAuthActivity;)V

    iput-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mConnectedReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/box/androidsdk/content/auth/OAuthActivity;)Lcom/box/androidsdk/content/models/BoxSession;
    .locals 0

    .line 53
    iget-object p0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    return-object p0
.end method

.method static synthetic access$102(Lcom/box/androidsdk/content/auth/OAuthActivity;Z)Z
    .locals 0

    .line 53
    iput-boolean p1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mAuthWasSuccessful:Z

    return p1
.end method

.method private clearCachedAuthenticationData()V
    .locals 2

    .line 579
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 580
    invoke-virtual {v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView;->clearCache(Z)V

    .line 581
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView;->clearFormData()V

    .line 582
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView;->clearHistory()V

    .line 585
    :cond_0
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 586
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    .line 587
    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    const-string v0, "webview.db"

    .line 589
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->deleteDatabase(Ljava/lang/String;)Z

    const-string v0, "webviewCache.db"

    .line 590
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->deleteDatabase(Ljava/lang/String;)Z

    .line 591
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 592
    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->deleteFolderRecursive(Ljava/io/File;)Z

    .line 593
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    return-void
.end method

.method public static createOAuthActivityIntent(Landroid/content/Context;Lcom/box/androidsdk/content/models/BoxSession;Z)Landroid/content/Intent;
    .locals 3

    .line 540
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxSession;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxSession;->getClientSecret()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxSession;->getRedirectUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v1, v2, p2}, Lcom/box/androidsdk/content/auth/OAuthActivity;->createOAuthActivityIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object p0

    const-string p2, "session"

    .line 541
    invoke-virtual {p0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 542
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxSession;->getUserId()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result p2

    if-nez p2, :cond_0

    const-string p2, "restrictToUserId"

    .line 543
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxSession;->getUserId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object p0
.end method

.method public static createOAuthActivityIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 2

    .line 520
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/box/androidsdk/content/auth/OAuthActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p0, "client_id"

    .line 521
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string p0, "client_secret"

    .line 522
    invoke-virtual {v0, p0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 523
    invoke-static {p3}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "redirect_uri"

    .line 524
    invoke-virtual {v0, p0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string p0, "loginviaboxapp"

    .line 526
    invoke-virtual {v0, p0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    return-object v0
.end method

.method private getAuthFailure(Ljava/lang/Exception;)Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;
    .locals 4

    .line 555
    sget v0, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail:I

    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_4

    .line 558
    instance-of v1, p1, Ljava/util/concurrent/ExecutionException;

    if-eqz v1, :cond_0

    check-cast p1, Ljava/util/concurrent/ExecutionException;

    .line 559
    invoke-virtual {p1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object p1

    .line 561
    :cond_0
    instance-of v1, p1, Lcom/box/androidsdk/content/BoxException;

    if-eqz v1, :cond_3

    .line 562
    move-object v1, p1

    check-cast v1, Lcom/box/androidsdk/content/BoxException;

    invoke-virtual {v1}, Lcom/box/androidsdk/content/BoxException;->getAsBoxError()Lcom/box/androidsdk/content/models/BoxError;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 564
    invoke-virtual {v1}, Lcom/box/androidsdk/content/BoxException;->getResponseCode()I

    move-result p1

    const/16 v3, 0x193

    if-eq p1, v3, :cond_2

    invoke-virtual {v1}, Lcom/box/androidsdk/content/BoxException;->getResponseCode()I

    move-result p1

    const/16 v1, 0x191

    if-eq p1, v1, :cond_2

    invoke-virtual {v2}, Lcom/box/androidsdk/content/models/BoxError;->getError()Ljava/lang/String;

    move-result-object p1

    const-string v1, "unauthorized_device"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_0

    .line 567
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 565
    :cond_2
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail_forbidden:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 569
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/box/androidsdk/content/models/BoxError;->getErrorDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 570
    new-instance v0, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p1}, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;-><init>(ILjava/lang/String;)V

    return-object v0

    .line 573
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ":"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 575
    :cond_4
    new-instance p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;

    const/4 v1, -0x1

    invoke-direct {p1, v1, v0}, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;-><init>(ILjava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method protected createOAuthView()Lcom/box/androidsdk/content/auth/OAuthWebView;
    .locals 4

    .line 437
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getOAuthWebViewRId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/auth/OAuthWebView;

    const/4 v1, 0x0

    .line 438
    invoke-virtual {v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView;->setVisibility(I)V

    .line 439
    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 440
    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 441
    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    return-object v0
.end method

.method protected createOAuthWebViewClient()Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;
    .locals 2

    .line 446
    new-instance v0, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;

    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mRedirectUrl:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;-><init>(Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient$WebEventListener;Ljava/lang/String;)V

    return-object v0
.end method

.method protected declared-synchronized dismissSpinner()V
    .locals 2

    monitor-enter p0

    .line 481
    :try_start_0
    sget-object v0, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 483
    :try_start_1
    sget-object v0, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487
    :catch_0
    :try_start_2
    sput-object v1, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;

    goto :goto_0

    .line 488
    :cond_0
    sget-object v0, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    .line 489
    sput-object v1, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 491
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected dismissSpinnerAndFailAuthenticate(Ljava/lang/Exception;)V
    .locals 1

    .line 424
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getAuthFailure(Ljava/lang/Exception;)Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;

    move-result-object p1

    .line 425
    new-instance v0, Lcom/box/androidsdk/content/auth/OAuthActivity$5;

    invoke-direct {v0, p0, p1}, Lcom/box/androidsdk/content/auth/OAuthActivity$5;-><init>(Lcom/box/androidsdk/content/auth/OAuthActivity;Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;)V

    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected dismissSpinnerAndFinishAuthenticate(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;)V
    .locals 1

    .line 407
    new-instance v0, Lcom/box/androidsdk/content/auth/OAuthActivity$4;

    invoke-direct {v0, p0, p1}, Lcom/box/androidsdk/content/auth/OAuthActivity$4;-><init>(Lcom/box/androidsdk/content/auth/OAuthActivity;Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;)V

    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public finish()V
    .locals 2

    .line 181
    invoke-direct {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->clearCachedAuthenticationData()V

    .line 182
    iget-boolean v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mAuthWasSuccessful:Z

    if-nez v0, :cond_0

    .line 183
    invoke-static {}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getInstance()Lcom/box/androidsdk/content/auth/BoxAuthentication;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->onAuthenticationFailure(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;Ljava/lang/Exception;)V

    .line 185
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method protected getBoxAuthApp()Landroid/content/Intent;
    .locals 8

    .line 284
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.box.android.action.AUTHENTICATE_VIA_BOX_APP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 285
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const v2, 0x10040

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_6

    .line 287
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_0

    goto/16 :goto_1

    .line 290
    :cond_0
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/box/sdk/android/R$string;->boxsdk_box_app_signature:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 291
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :catch_0
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 293
    :try_start_0
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const/16 v7, 0x40

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v6, 0x0

    .line 294
    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/content/pm/Signature;->toCharsString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 295
    iget-object v4, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    invoke-static {}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getInstance()Lcom/box/androidsdk/content/auth/BoxAuthentication;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getStoredAuthInfo(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 297
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 298
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 299
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 300
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;

    invoke-virtual {v7}, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;->getUser()Lcom/box/androidsdk/content/models/BoxUser;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 301
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;

    invoke-virtual {v6}, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;->getUser()Lcom/box/androidsdk/content/models/BoxUser;

    move-result-object v6

    invoke-virtual {v6}, Lcom/box/androidsdk/content/models/BoxUser;->toJson()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_4

    const-string v4, "boxusers"

    .line 305
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    return-object v0

    :cond_5
    return-object v2

    :cond_6
    :goto_1
    return-object v2
.end method

.method protected getContentView()I
    .locals 1

    .line 240
    sget v0, Lcom/box/sdk/android/R$layout;->boxsdk_activity_oauth:I

    return v0
.end method

.method protected getOAuthWebViewRId()I
    .locals 1

    .line 450
    sget v0, Lcom/box/sdk/android/R$id;->oauthview:I

    return v0
.end method

.method isAuthErrored()Z
    .locals 3

    .line 156
    iget-boolean v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mIsLoggingInViaBoxApp:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    const-string v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    if-ne v0, p2, :cond_2

    const/4 v0, 0x1

    if-ne v0, p1, :cond_2

    const-string p1, "userId"

    .line 348
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "authcode"

    .line 349
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 350
    invoke-static {p2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isBlank(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->isBlank(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_1

    .line 351
    invoke-static {}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getInstance()Lcom/box/androidsdk/content/auth/BoxAuthentication;

    move-result-object p2

    invoke-virtual {p2, p0}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getStoredAuthInfo(Landroid/content/Context;)Ljava/util/Map;

    move-result-object p2

    .line 352
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;

    if-eqz p1, :cond_0

    .line 354
    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/auth/OAuthActivity;->onAuthenticationChosen(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;)V

    goto :goto_0

    .line 356
    :cond_0
    new-instance p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;

    const/4 p2, 0x0

    const-string p3, ""

    invoke-direct {p1, p2, p3}, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/auth/OAuthActivity;->onAuthFailure(Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;)Z

    goto :goto_0

    .line 358
    :cond_1
    invoke-static {p2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isBlank(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    const/4 p1, 0x0

    .line 359
    invoke-virtual {p0, p2, p1}, Lcom/box/androidsdk/content/auth/OAuthActivity;->startMakingOAuthAPICall(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-nez p2, :cond_3

    .line 362
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->finish()V

    :cond_3
    :goto_0
    return-void
.end method

.method public onAuthFailure(Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;)Z
    .locals 7

    .line 197
    iget v0, p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;->type:I

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-ne v0, v3, :cond_2

    .line 198
    iget-object v0, p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;->mWebException:Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;->getErrorCode()I

    move-result v0

    const/4 v5, -0x6

    if-eq v0, v5, :cond_1

    iget-object v0, p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;->mWebException:Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;->getErrorCode()I

    move-result v0

    const/4 v5, -0x2

    if-eq v0, v5, :cond_1

    iget-object v0, p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;->mWebException:Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;->getErrorCode()I

    move-result v0

    const/4 v5, -0x8

    if-ne v0, v5, :cond_0

    goto :goto_0

    .line 201
    :cond_0
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v5, "%s\n%s: %s"

    .line 202
    new-array v2, v2, [Ljava/lang/Object;

    sget v6, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail:I

    .line 204
    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_details:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;->mWebException:Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;

    .line 205
    invoke-virtual {v1}, Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;->getErrorCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;->mWebException:Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;

    invoke-virtual {p1}, Lcom/box/androidsdk/content/auth/OAuthWebView$WebViewException;->getDescription()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    .line 204
    invoke-static {v5, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 202
    invoke-static {p0, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 205
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_1
    :goto_0
    return v1

    .line 207
    :cond_2
    iget-object v0, p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;->message:Ljava/lang/String;

    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 208
    sget p1, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail:I

    invoke-static {p0, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 210
    :cond_3
    iget p1, p1, Lcom/box/androidsdk/content/auth/OAuthWebView$AuthFailure;->type:I

    if-eq p1, v4, :cond_5

    if-eq p1, v2, :cond_4

    .line 232
    sget p1, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail:I

    invoke-static {p0, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 219
    :cond_4
    new-instance p1, Landroid/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail:I

    .line 220
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail_forbidden:I

    .line 221
    invoke-virtual {p1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/box/sdk/android/R$string;->boxsdk_button_ok:I

    new-instance v1, Lcom/box/androidsdk/content/auth/OAuthActivity$2;

    invoke-direct {v1, p0}, Lcom/box/androidsdk/content/auth/OAuthActivity$2;-><init>(Lcom/box/androidsdk/content/auth/OAuthActivity;)V

    .line 222
    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 229
    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    return v4

    .line 212
    :cond_5
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string v0, "%s\n%s: %s"

    .line 213
    new-array v2, v2, [Ljava/lang/Object;

    sget v5, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail:I

    .line 215
    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_details:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_Authentication_fail_url_mismatch:I

    .line 216
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    .line 215
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 213
    invoke-static {p0, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p1

    .line 216
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 235
    :goto_1
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->finish()V

    return v4
.end method

.method public onAuthenticationChosen(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 331
    invoke-static {}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getInstance()Lcom/box/androidsdk/content/auth/BoxAuthentication;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->onAuthenticated(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;Landroid/content/Context;)V

    .line 332
    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/auth/OAuthActivity;->dismissSpinnerAndFinishAuthenticate(Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .line 321
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "choose_auth"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->finish()V

    return-void

    .line 325
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .line 108
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 109
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 110
    sget-boolean v1, Lcom/box/androidsdk/content/BoxConfig;->IS_FLAG_SECURE:Z

    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x2000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getContentView()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/box/androidsdk/content/auth/OAuthActivity;->setContentView(I)V

    .line 115
    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mConnectedReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v2}, Lcom/box/androidsdk/content/auth/OAuthActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v1, "client_id"

    .line 118
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mClientId:Ljava/lang/String;

    const-string v1, "client_secret"

    .line 119
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mClientSecret:Ljava/lang/String;

    const-string v1, "box_device_id"

    .line 120
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mDeviceId:Ljava/lang/String;

    const-string v1, "box_device_name"

    .line 121
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mDeviceName:Ljava/lang/String;

    const-string v1, "redirect_uri"

    .line 122
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mRedirectUrl:Ljava/lang/String;

    const-string v1, "loginviaboxapp"

    const/4 v2, 0x0

    .line 123
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 124
    iput v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->authType:I

    .line 125
    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->apiCallStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    const-string v1, "session"

    .line 126
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSession;

    iput-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    if-eqz p1, :cond_1

    const-string v0, "loggingInViaBoxApp"

    .line 129
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mIsLoggingInViaBoxApp:Z

    .line 132
    :cond_1
    iget-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    if-eqz p1, :cond_2

    .line 133
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/box/androidsdk/content/models/BoxSession;->setApplicationContext(Landroid/content/Context;)V

    goto :goto_0

    .line 135
    :cond_2
    new-instance p1, Lcom/box/androidsdk/content/models/BoxSession;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mClientId:Ljava/lang/String;

    iget-object v5, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mClientSecret:Ljava/lang/String;

    iget-object v6, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mRedirectUrl:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/box/androidsdk/content/models/BoxSession;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    .line 136
    iget-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/box/androidsdk/content/models/BoxSession;->setDeviceId(Ljava/lang/String;)V

    .line 137
    iget-object p1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mDeviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/box/androidsdk/content/models/BoxSession;->setDeviceName(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .line 495
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mConnectedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 496
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->apiCallStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 497
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->dismissSpinner()V

    .line 498
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onDifferentAuthenticationChosen()V
    .locals 2

    .line 339
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "choose_auth"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 341
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    :cond_0
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0

    .line 190
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->dismissSpinner()V

    return-void
.end method

.method public onReceivedAuthCode(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 166
    invoke-virtual {p0, p1, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->onReceivedAuthCode(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onReceivedAuthCode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 173
    iget v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->authType:I

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView;->setVisibility(I)V

    .line 176
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/auth/OAuthActivity;->startMakingOAuthAPICall(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    .line 143
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 144
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->isAuthErrored()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->startOAuth()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "loggingInViaBoxApp"

    .line 151
    iget-boolean v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mIsLoggingInViaBoxApp:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 152
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected showDialogWhileWaitingForAuthenticationAPICall()Landroid/app/Dialog;
    .locals 2

    .line 459
    sget v0, Lcom/box/sdk/android/R$string;->boxsdk_Authenticating:I

    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_Please_wait:I

    invoke-virtual {p0, v1}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p0, v0, v1}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    return-object v0
.end method

.method protected declared-synchronized showSpinner()V
    .locals 1

    monitor-enter p0

    .line 464
    :try_start_0
    sget-object v0, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 465
    sget-object v0, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 467
    monitor-exit p0

    return-void

    .line 470
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->showDialogWhileWaitingForAuthenticationAPICall()Landroid/app/Dialog;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 478
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    .line 475
    :try_start_2
    sput-object v0, Lcom/box/androidsdk/content/auth/OAuthActivity;->dialog:Landroid/app/Dialog;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 476
    monitor-exit p0

    return-void

    :goto_0
    monitor-exit p0

    throw v0
.end method

.method protected startMakingOAuthAPICall(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 377
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->apiCallStarted:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 380
    :cond_0
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->showSpinner()V

    if-eqz p2, :cond_1

    .line 382
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->getAuthInfo()Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/box/androidsdk/content/auth/BoxAuthentication$BoxAuthenticationInfo;->setBaseDomain(Ljava/lang/String;)V

    const-string v0, "setting Base Domain"

    .line 383
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "base domain being used"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p2, v1}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->nonFatalE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 385
    :cond_1
    new-instance p2, Lcom/box/androidsdk/content/auth/OAuthActivity$3;

    invoke-direct {p2, p0, p1}, Lcom/box/androidsdk/content/auth/OAuthActivity$3;-><init>(Lcom/box/androidsdk/content/auth/OAuthActivity;Ljava/lang/String;)V

    .line 403
    invoke-virtual {p2}, Lcom/box/androidsdk/content/auth/OAuthActivity$3;->start()V

    return-void
.end method

.method protected startOAuth()V
    .locals 5

    .line 245
    iget v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->authType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "disableAccountChoosing"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "choose_auth"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 246
    invoke-static {}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getInstance()Lcom/box/androidsdk/content/auth/BoxAuthentication;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/auth/BoxAuthentication;->getStoredAuthInfo(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v0

    .line 247
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "restrictToUserId"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 248
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 249
    sget v2, Lcom/box/sdk/android/R$id;->oauth_container:I

    invoke-static {p0}, Lcom/box/androidsdk/content/auth/ChooseAuthenticationFragment;->createAuthenticationActivity(Landroid/content/Context;)Lcom/box/androidsdk/content/auth/ChooseAuthenticationFragment;

    move-result-object v3

    const-string v4, "choose_auth"

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    const-string v2, "choose_auth"

    .line 250
    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 251
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 254
    :cond_0
    iget v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->authType:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 256
    :pswitch_0
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getBoxAuthApp()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v2, "client_id"

    .line 258
    iget-object v3, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mClientId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "redirect_uri"

    .line 259
    iget-object v3, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mRedirectUrl:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "restrictToUserId"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "restrictToUserId"

    .line 261
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "restrictToUserId"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    :cond_1
    iput-boolean v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mIsLoggingInViaBoxApp:Z

    .line 264
    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/auth/OAuthActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 268
    :cond_2
    :pswitch_1
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->showSpinner()V

    .line 269
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->createOAuthView()Lcom/box/androidsdk/content/auth/OAuthWebView;

    move-result-object v0

    iput-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    .line 270
    invoke-virtual {p0}, Lcom/box/androidsdk/content/auth/OAuthActivity;->createOAuthWebViewClient()Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;

    move-result-object v0

    iput-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthClient:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;

    .line 271
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthClient:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;

    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;->setOnPageFinishedListener(Lcom/box/androidsdk/content/auth/OAuthWebView$OnPageFinishedListener;)V

    .line 272
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthClient:Lcom/box/androidsdk/content/auth/OAuthWebView$OAuthWebViewClient;

    invoke-virtual {v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 273
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->getBoxAccountEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 274
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxSession;->getBoxAccountEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/box/androidsdk/content/auth/OAuthWebView;->setBoxAccountEmail(Ljava/lang/String;)V

    .line 276
    :cond_3
    iget-object v0, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->oauthView:Lcom/box/androidsdk/content/auth/OAuthWebView;

    iget-object v1, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mClientId:Ljava/lang/String;

    iget-object v2, p0, Lcom/box/androidsdk/content/auth/OAuthActivity;->mRedirectUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/box/androidsdk/content/auth/OAuthWebView;->authenticate(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

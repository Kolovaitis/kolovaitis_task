.class public abstract Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;
.super Lcom/box/androidsdk/content/requests/BoxRequestItemUpdate;
.source "BoxRequestUpdateSharedItem.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/box/androidsdk/content/models/BoxItem;",
        "R:",
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "TE;TR;>;>",
        "Lcom/box/androidsdk/content/requests/BoxRequestItemUpdate<",
        "TE;TR;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/box/androidsdk/content/requests/BoxRequestItemUpdate;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestItemUpdate;-><init>(Lcom/box/androidsdk/content/requests/BoxRequestItemUpdate;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TE;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/box/androidsdk/content/models/BoxSession;",
            ")V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/box/androidsdk/content/requests/BoxRequestItemUpdate;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 25
    sget-object p1, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->PUT:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    return-void
.end method

.method private getPermissionsJsonObject()Lcom/eclipsesource/json/JsonObject;
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "permissions"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "permissions"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;

    .line 174
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;->toJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    return-object v0

    .line 177
    :cond_0
    new-instance v0, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    return-object v0
.end method

.method private getSharedLinkJsonObject()Lcom/eclipsesource/json/JsonObject;
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSharedLink;

    .line 165
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;->toJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    return-object v0

    .line 168
    :cond_0
    new-instance v0, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getAccess()Lcom/box/androidsdk/content/models/BoxSharedLink$Access;
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    .line 39
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;->getAccess()Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method protected getCanDownload()Ljava/lang/Boolean;
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    .line 132
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;->getPermissions()Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;->getCanDownload()Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    .line 107
    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;->getPassword()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getUnsharedAt()Ljava/util/Date;
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;->getUnsharedDate()Ljava/util/Date;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public setAccess(Lcom/box/androidsdk/content/models/BoxSharedLink$Access;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/box/androidsdk/content/models/BoxSharedLink$Access;",
            ")TR;"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->getSharedLinkJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    const-string v1, "access"

    .line 51
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->getAsStringSafely(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 52
    new-instance p1, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-direct {p1, v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    .line 53
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method protected setCanDownload(Z)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TR;"
        }
    .end annotation

    .line 143
    invoke-direct {p0}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->getPermissionsJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    const-string v1, "can_download"

    .line 144
    invoke-virtual {v0, v1, p1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Z)Lcom/eclipsesource/json/JsonObject;

    .line 145
    new-instance p1, Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;

    invoke-direct {p1, v0}, Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    .line 147
    invoke-direct {p0}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->getSharedLinkJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    const-string v1, "permissions"

    .line 148
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;->toJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    .line 149
    new-instance p1, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-direct {p1, v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    .line 150
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setPassword(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TR;"
        }
    .end annotation

    .line 118
    invoke-direct {p0}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->getSharedLinkJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    const-string v1, "password"

    .line 119
    invoke-virtual {v0, v1, p1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 120
    new-instance p1, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-direct {p1, v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    .line 121
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setRemoveUnsharedAtDate()Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 97
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->setUnsharedAt(Ljava/util/Date;)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object v0

    return-object v0
.end method

.method public setUnsharedAt(Ljava/util/Date;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            ")TR;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 79
    invoke-direct {p0}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->getSharedLinkJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    if-nez p1, :cond_0

    const-string p1, "unshared_at"

    .line 81
    sget-object v1, Lcom/eclipsesource/json/JsonValue;->NULL:Lcom/eclipsesource/json/JsonValue;

    invoke-virtual {v0, p1, v1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonObject;

    goto :goto_0

    :cond_0
    const-string v1, "unshared_at"

    .line 83
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/BoxDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 85
    :goto_0
    new-instance p1, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-direct {p1, v0}, Lcom/box/androidsdk/content/models/BoxSharedLink;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    .line 86
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "shared_link"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public updateSharedLink()Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;
    .locals 0

    return-object p0
.end method

.class public Lcom/box/androidsdk/content/requests/BoxRequestBatch;
.super Lcom/box/androidsdk/content/requests/BoxRequest;
.source "BoxRequestBatch.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "Lcom/box/androidsdk/content/requests/BoxResponseBatch;",
        "Lcom/box/androidsdk/content/requests/BoxRequestBatch;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x70be1f2741234cacL


# instance fields
.field private mExecutor:Ljava/util/concurrent/ExecutorService;

.field protected mRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/box/androidsdk/content/requests/BoxRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 28
    const-class v0, Lcom/box/androidsdk/content/requests/BoxResponseBatch;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Ljava/lang/Class;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->mRequests:Ljava/util/ArrayList;

    .line 29
    iput-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->mExecutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method


# virtual methods
.method public addRequest(Lcom/box/androidsdk/content/requests/BoxRequest;)Lcom/box/androidsdk/content/requests/BoxRequestBatch;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->mRequests:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic onSend()Lcom/box/androidsdk/content/models/BoxObject;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 18
    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->onSend()Lcom/box/androidsdk/content/requests/BoxResponseBatch;

    move-result-object v0

    return-object v0
.end method

.method public onSend()Lcom/box/androidsdk/content/requests/BoxResponseBatch;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 55
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxResponseBatch;

    invoke-direct {v0}, Lcom/box/androidsdk/content/requests/BoxResponseBatch;-><init>()V

    .line 57
    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->mExecutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v1, :cond_1

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 59
    iget-object v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->mRequests:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/box/androidsdk/content/requests/BoxRequest;

    .line 60
    invoke-virtual {v3}, Lcom/box/androidsdk/content/requests/BoxRequest;->toTask()Lcom/box/androidsdk/content/BoxFutureTask;

    move-result-object v3

    .line 61
    iget-object v4, p0, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v4, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 62
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 65
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/box/androidsdk/content/BoxFutureTask;

    .line 67
    :try_start_0
    invoke-virtual {v2}, Lcom/box/androidsdk/content/BoxFutureTask;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/box/androidsdk/content/requests/BoxResponse;

    .line 68
    invoke-virtual {v0, v2}, Lcom/box/androidsdk/content/requests/BoxResponseBatch;->addResponse(Lcom/box/androidsdk/content/requests/BoxResponse;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 72
    new-instance v1, Lcom/box/androidsdk/content/BoxException;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    .line 70
    new-instance v1, Lcom/box/androidsdk/content/BoxException;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 77
    :cond_1
    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->mRequests:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/box/androidsdk/content/requests/BoxRequest;

    const/4 v3, 0x0

    .line 81
    :try_start_1
    invoke-virtual {v2}, Lcom/box/androidsdk/content/requests/BoxRequest;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-object v6, v4

    move-object v4, v3

    move-object v3, v6

    goto :goto_3

    :catch_2
    move-exception v4

    .line 86
    :goto_3
    new-instance v5, Lcom/box/androidsdk/content/requests/BoxResponse;

    invoke-direct {v5, v3, v4, v2}, Lcom/box/androidsdk/content/requests/BoxResponse;-><init>(Lcom/box/androidsdk/content/models/BoxObject;Ljava/lang/Exception;Lcom/box/androidsdk/content/requests/BoxRequest;)V

    .line 87
    invoke-virtual {v0, v5}, Lcom/box/androidsdk/content/requests/BoxResponseBatch;->addResponse(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    goto :goto_2

    :cond_2
    return-object v0
.end method

.method public setExecutor(Ljava/util/concurrent/ExecutorService;)Lcom/box/androidsdk/content/requests/BoxRequestBatch;
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->mExecutor:Ljava/util/concurrent/ExecutorService;

    return-object p0
.end method

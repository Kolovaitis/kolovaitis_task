.class public Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;
.super Lcom/box/androidsdk/content/requests/BoxRequest;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FilePreviewed"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "Lcom/box/androidsdk/content/models/BoxVoid;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;",
        ">;"
    }
.end annotation


# static fields
.field private static final TYPE_FILE:Ljava/lang/String; = "file"

.field private static final TYPE_ITEM_PREVIEW:Ljava/lang/String; = "PREVIEW"


# instance fields
.field private mFileId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 1

    .line 1173
    const-class v0, Lcom/box/androidsdk/content/models/BoxVoid;

    invoke-direct {p0, v0, p2, p3}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Ljava/lang/Class;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 1174
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;->mFileId:Ljava/lang/String;

    .line 1175
    sget-object p2, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->POST:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 1176
    iget-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p3, "type"

    const-string v0, "event"

    invoke-virtual {p2, p3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1177
    iget-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p3, "event_type"

    const-string v0, "PREVIEW"

    invoke-virtual {p2, p3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178
    new-instance p2, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {p2}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    const-string p3, "type"

    const-string v0, "file"

    .line 1179
    invoke-virtual {p2, p3, v0}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    const-string p3, "id"

    .line 1180
    invoke-virtual {p2, p3, p1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 1181
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p3, "source"

    invoke-static {p2}, Lcom/box/androidsdk/content/models/BoxEntity;->createEntityFromJson(Lcom/eclipsesource/json/JsonObject;)Lcom/box/androidsdk/content/models/BoxEntity;

    move-result-object p2

    invoke-virtual {p1, p3, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getFileId()Ljava/lang/String;
    .locals 1

    .line 1189
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;->mFileId:Ljava/lang/String;

    return-object v0
.end method

.method protected onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/box/androidsdk/content/requests/BoxResponse<",
            "Lcom/box/androidsdk/content/models/BoxVoid;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 1194
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequest;->onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    .line 1195
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequest;->handleUpdateCache(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    return-void
.end method

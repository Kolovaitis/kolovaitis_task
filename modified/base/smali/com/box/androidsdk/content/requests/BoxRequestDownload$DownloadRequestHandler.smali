.class public Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;
.super Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;
.source "BoxRequestDownload.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestDownload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadRequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler<",
        "Lcom/box/androidsdk/content/requests/BoxRequestDownload;",
        ">;"
    }
.end annotation


# static fields
.field protected static final DEFAULT_MAX_WAIT_MILLIS:I = 0x15f90

.field protected static final DEFAULT_NUM_RETRIES:I = 0x2


# instance fields
.field protected mNumAcceptedRetries:I

.field protected mRetryAfterMillis:I


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/requests/BoxRequestDownload;)V
    .locals 0

    .line 316
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;-><init>(Lcom/box/androidsdk/content/requests/BoxRequest;)V

    const/4 p1, 0x0

    .line 306
    iput p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mNumAcceptedRetries:I

    const/16 p1, 0x3e8

    .line 307
    iput p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRetryAfterMillis:I

    return-void
.end method


# virtual methods
.method protected getOutputStream(Lcom/box/androidsdk/content/models/BoxDownload;)Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 321
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    iget-object v0, v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->mFileOutputStream:Ljava/io/OutputStream;

    if-nez v0, :cond_1

    .line 322
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxDownload;->getOutputFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxDownload;->getOutputFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 325
    :cond_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxDownload;->getOutputFile()Ljava/io/File;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    return-object v0

    .line 327
    :cond_1
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    iget-object p1, p1, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->mFileOutputStream:Ljava/io/OutputStream;

    return-object p1
.end method

.method public onResponse(Ljava/lang/Class;Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Lcom/box/androidsdk/content/models/BoxDownload;
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;,
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    move-object/from16 v10, p0

    move-object/from16 v11, p2

    .line 332
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getContentType()Ljava/lang/String;

    move-result-object v6

    .line 333
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getHttpURLConnection()Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v12

    .line 336
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {v10, v11}, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->disconnectForInterrupt(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)V

    .line 340
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result v0

    const/16 v1, 0x1ad

    if-ne v0, v1, :cond_1

    .line 341
    invoke-virtual {v10, v11}, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->retryRateLimited(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxDownload;

    return-object v0

    .line 342
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result v0

    const/16 v1, 0xca

    if-ne v0, v1, :cond_4

    .line 345
    :try_start_0
    iget v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mNumAcceptedRetries:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_2

    .line 346
    iget v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mNumAcceptedRetries:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mNumAcceptedRetries:I

    .line 347
    invoke-static {v11, v1}, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->getRetryAfterFromResponse(Lcom/box/androidsdk/content/requests/BoxHttpResponse;I)I

    move-result v0

    iput v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRetryAfterMillis:I

    goto :goto_0

    .line 348
    :cond_2
    iget v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRetryAfterMillis:I

    const v1, 0x15f90

    if-ge v0, v1, :cond_3

    .line 350
    iget v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRetryAfterMillis:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    add-double/2addr v4, v2

    mul-double v0, v0, v4

    double-to-int v0, v0

    iput v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRetryAfterMillis:I

    .line 355
    :goto_0
    iget v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRetryAfterMillis:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    .line 356
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxDownload;

    return-object v0

    .line 353
    :cond_3
    new-instance v0, Lcom/box/androidsdk/content/BoxException$MaxAttemptsExceeded;

    const-string v1, "Max wait time exceeded."

    iget v2, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mNumAcceptedRetries:I

    invoke-direct {v0, v1, v2}, Lcom/box/androidsdk/content/BoxException$MaxAttemptsExceeded;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 358
    new-instance v1, Lcom/box/androidsdk/content/BoxException;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, v11}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Lcom/box/androidsdk/content/requests/BoxHttpResponse;)V

    throw v1

    .line 360
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result v0

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_6

    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result v0

    const/16 v1, 0xce

    if-ne v0, v1, :cond_5

    goto :goto_1

    .line 450
    :cond_5
    new-instance v0, Lcom/box/androidsdk/content/models/BoxDownload;

    const/4 v14, 0x0

    const-wide/16 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object v13, v0

    invoke-direct/range {v13 .. v20}, Lcom/box/androidsdk/content/models/BoxDownload;-><init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 362
    :cond_6
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getHttpURLConnection()Ljava/net/HttpURLConnection;

    move-result-object v0

    const-string v1, "Content-Length"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getHttpURLConnection()Ljava/net/HttpURLConnection;

    move-result-object v1

    const-string v2, "Content-Disposition"

    invoke-virtual {v1, v2}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 366
    :try_start_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-wide v13, v0

    goto :goto_2

    :catch_1
    const-wide/16 v0, -0x1

    move-wide v13, v0

    .line 370
    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getHttpURLConnection()Ljava/net/HttpURLConnection;

    move-result-object v0

    const-string v1, "Content-Range"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 371
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getHttpURLConnection()Ljava/net/HttpURLConnection;

    move-result-object v0

    const-string v1, "Date"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 372
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getHttpURLConnection()Ljava/net/HttpURLConnection;

    move-result-object v0

    const-string v1, "Expiration"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 373
    new-instance v15, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler$1;

    move-object v1, v15

    move-object/from16 v2, p0

    move-wide v4, v13

    invoke-direct/range {v1 .. v9}, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler$1;-><init>(Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    iget-object v0, v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->mDownloadStartListener:Lcom/box/androidsdk/content/listeners/DownloadStartListener;

    if-eqz v0, :cond_7

    .line 390
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    iget-object v0, v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->mDownloadStartListener:Lcom/box/androidsdk/content/listeners/DownloadStartListener;

    invoke-interface {v0, v15}, Lcom/box/androidsdk/content/listeners/DownloadStartListener;->onStart(Lcom/box/androidsdk/content/models/BoxDownload;)V

    :cond_7
    const/4 v1, 0x0

    .line 397
    :try_start_2
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    iget-object v0, v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->mListener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    if-eqz v0, :cond_8

    .line 398
    new-instance v2, Lcom/box/androidsdk/content/utils/ProgressOutputStream;

    invoke-virtual {v10, v15}, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->getOutputStream(Lcom/box/androidsdk/content/models/BoxDownload;)Ljava/io/OutputStream;

    move-result-object v0

    iget-object v3, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v3, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    iget-object v3, v3, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->mListener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    invoke-direct {v2, v0, v3, v13, v14}, Lcom/box/androidsdk/content/utils/ProgressOutputStream;-><init>(Ljava/io/OutputStream;Lcom/box/androidsdk/content/listeners/ProgressListener;J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 399
    :try_start_3
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    iget-object v0, v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->mListener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    const-wide/16 v3, 0x0

    invoke-interface {v0, v3, v4, v13, v14}, Lcom/box/androidsdk/content/listeners/ProgressListener;->onProgressChanged(JJ)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v1, v2

    goto :goto_3

    :catch_2
    move-exception v0

    goto/16 :goto_8

    .line 401
    :cond_8
    :try_start_4
    invoke-virtual {v10, v15}, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->getOutputStream(Lcom/box/androidsdk/content/models/BoxDownload;)Ljava/io/OutputStream;

    move-result-object v0

    move-object v1, v0

    .line 403
    :goto_3
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    invoke-static {v0}, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->access$000(Lcom/box/androidsdk/content/requests/BoxRequestDownload;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 404
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getBody()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    goto :goto_4

    .line 406
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getBody()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->copyStreamAndComputeSha1(Ljava/io/InputStream;Ljava/io/OutputStream;)Ljava/lang/String;

    move-result-object v0

    .line 407
    iget-object v2, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v2, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    invoke-static {v2}, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->access$000(Lcom/box/androidsdk/content/requests/BoxRequestDownload;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v2, :cond_b

    .line 433
    :goto_4
    :try_start_5
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getBody()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_5

    :catch_3
    move-exception v0

    const-string v2, "error closing inputstream"

    .line 435
    invoke-static {v2, v0}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 437
    :goto_5
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->getTargetStream()Ljava/io/OutputStream;

    move-result-object v0

    if-nez v0, :cond_a

    .line 440
    :try_start_6
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_6

    :catch_4
    move-exception v0

    move-object v1, v0

    const-string v0, "error closing outputstream"

    .line 442
    invoke-static {v0, v1}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_a
    :goto_6
    return-object v15

    .line 408
    :cond_b
    :try_start_7
    new-instance v2, Lcom/box/androidsdk/content/BoxException$CorruptedContentException;

    const-string v3, "Sha1 checks failed"

    iget-object v4, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v4, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    invoke-static {v4}, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->access$000(Lcom/box/androidsdk/content/requests/BoxRequestDownload;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/box/androidsdk/content/BoxException$CorruptedContentException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_7
    move-object v1, v0

    goto :goto_a

    :catch_5
    move-exception v0

    move-object v2, v1

    :goto_8
    move-object v1, v0

    .line 414
    :try_start_8
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->getSocket()Ljava/net/Socket;

    move-result-object v0

    if-eqz v0, :cond_c

    if-eqz v12, :cond_c

    const-string v3, "gzip"

    .line 415
    invoke-virtual {v12, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz v3, :cond_c

    .line 417
    :try_start_9
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_9

    :catch_6
    move-exception v0

    move-object v3, v0

    :try_start_a
    const-string v0, "error closing socket"

    .line 419
    invoke-static {v0, v3}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 422
    :cond_c
    :goto_9
    instance-of v0, v1, Lcom/box/androidsdk/content/BoxException;

    if-nez v0, :cond_e

    .line 425
    instance-of v0, v1, Ljavax/net/ssl/SSLException;

    if-eqz v0, :cond_d

    .line 426
    new-instance v0, Lcom/box/androidsdk/content/BoxException$DownloadSSLException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    check-cast v1, Ljavax/net/ssl/SSLException;

    invoke-direct {v0, v3, v1}, Lcom/box/androidsdk/content/BoxException$DownloadSSLException;-><init>(Ljava/lang/String;Ljavax/net/ssl/SSLException;)V

    throw v0

    .line 428
    :cond_d
    new-instance v0, Lcom/box/androidsdk/content/BoxException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3, v1}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 423
    :cond_e
    check-cast v1, Lcom/box/androidsdk/content/BoxException;

    throw v1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catchall_1
    move-exception v0

    goto :goto_7

    .line 433
    :goto_a
    :try_start_b
    invoke-virtual/range {p2 .. p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getBody()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_7

    goto :goto_b

    :catch_7
    move-exception v0

    const-string v3, "error closing inputstream"

    .line 435
    invoke-static {v3, v0}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 437
    :goto_b
    iget-object v0, v10, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxRequestDownload;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/requests/BoxRequestDownload;->getTargetStream()Ljava/io/OutputStream;

    move-result-object v0

    if-nez v0, :cond_f

    .line 440
    :try_start_c
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_8

    goto :goto_c

    :catch_8
    move-exception v0

    move-object v2, v0

    const-string v0, "error closing outputstream"

    .line 442
    invoke-static {v0, v2}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 443
    :cond_f
    :goto_c
    throw v1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Class;Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Lcom/box/androidsdk/content/models/BoxObject;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;,
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 301
    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/requests/BoxRequestDownload$DownloadRequestHandler;->onResponse(Ljava/lang/Class;Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Lcom/box/androidsdk/content/models/BoxDownload;

    move-result-object p1

    return-object p1
.end method

.class public Lcom/box/androidsdk/content/requests/BoxRequestsBookmark;
.super Ljava/lang/Object;
.source "BoxRequestsBookmark.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteBookmarkFromCollection;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$AddBookmarkToCollection;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$AddCommentToBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetBookmarkComments;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$RestoreTrashedBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteTrashedBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetTrashedBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CopyBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateSharedBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;,
        Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetBookmarkInfo;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

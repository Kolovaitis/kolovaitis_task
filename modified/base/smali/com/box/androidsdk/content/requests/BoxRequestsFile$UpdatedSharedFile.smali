.class public Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;
.super Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdatedSharedFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem<",
        "Lcom/box/androidsdk/content/models/BoxFile;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x70be1f2741234cc0L


# direct methods
.method protected constructor <init>(Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;)V
    .locals 0

    .line 211
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;-><init>(Lcom/box/androidsdk/content/requests/BoxRequestItemUpdate;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 1

    .line 207
    const-class v0, Lcom/box/androidsdk/content/models/BoxFile;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-void
.end method


# virtual methods
.method public getCanDownload()Ljava/lang/Boolean;
    .locals 1

    .line 232
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->getCanDownload()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setCanDownload(Z)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 0

    .line 195
    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;->setCanDownload(Z)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;

    move-result-object p1

    return-object p1
.end method

.method public setCanDownload(Z)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;
    .locals 0

    .line 222
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestUpdateSharedItem;->setCanDownload(Z)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;

    return-object p1
.end method

.class public final enum Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;
.super Ljava/lang/Enum;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Format"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

.field public static final enum JPG:Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

.field public static final enum PNG:Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;


# instance fields
.field private final mExt:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 870
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    const-string v1, "JPG"

    const-string v2, ".jpg"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->JPG:Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    .line 871
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    const-string v1, "PNG"

    const-string v2, ".png"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->PNG:Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    const/4 v0, 0x2

    .line 869
    new-array v0, v0, [Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->JPG:Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    aput-object v1, v0, v3

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->PNG:Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    aput-object v1, v0, v4

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->$VALUES:[Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 875
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 876
    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->mExt:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;
    .locals 1

    .line 869
    const-class v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;
    .locals 1

    .line 869
    sget-object v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->$VALUES:[Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 881
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail$Format;->mExt:Ljava/lang/String;

    return-object v0
.end method

.class Lcom/box/androidsdk/content/requests/BoxRequestMultipart;
.super Lcom/box/androidsdk/content/requests/BoxHttpRequest;
.source "BoxRequestMultipart.java"


# static fields
.field private static final BOUNDARY:Ljava/lang/String; = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

.field private static final BUFFER_SIZE:I = 0x2000

.field private static final LOGGER:Ljava/util/logging/Logger;


# instance fields
.field private fields:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private fileSize:J

.field private filename:Ljava/lang/String;

.field private firstBoundary:Z

.field private inputStream:Ljava/io/InputStream;

.field private final loggedRequest:Ljava/lang/StringBuilder;

.field private outputStream:Ljava/io/OutputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    const-class v0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->LOGGER:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Lcom/box/androidsdk/content/requests/BoxRequest$Methods;Lcom/box/androidsdk/content/listeners/ProgressListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/box/androidsdk/content/requests/BoxHttpRequest;-><init>(Ljava/net/URL;Lcom/box/androidsdk/content/requests/BoxRequest$Methods;Lcom/box/androidsdk/content/listeners/ProgressListener;)V

    .line 35
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->loggedRequest:Ljava/lang/StringBuilder;

    .line 55
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->fields:Ljava/util/Map;

    const/4 p1, 0x1

    .line 56
    iput-boolean p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->firstBoundary:Z

    const-string p1, "Content-Type"

    const-string p2, "multipart/form-data; boundary=da39a3ee5e6b4b0d3255bfef95601890afd80709"

    .line 58
    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxHttpRequest;

    return-void
.end method

.method private writeBoundary()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 180
    iget-boolean v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->firstBoundary:Z

    if-nez v0, :cond_0

    const-string v0, "\r\n"

    .line 181
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    .line 184
    iput-boolean v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->firstBoundary:Z

    const-string v0, "--"

    .line 185
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    const-string v0, "da39a3ee5e6b4b0d3255bfef95601890afd80709"

    .line 186
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    return-void
.end method

.method private writeOutput(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 221
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->outputStream:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method private writeOutput(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 214
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->outputStream:Ljava/io/OutputStream;

    const-string v1, "UTF-8"

    invoke-static {v1}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 215
    sget-object v0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->LOGGER:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->loggedRequest:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private writePartHeader([[Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 190
    invoke-direct {p0, p1, v0}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writePartHeader([[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private writePartHeader([[Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 194
    invoke-direct {p0}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeBoundary()V

    const-string v0, "\r\n"

    .line 195
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    const-string v0, "Content-Disposition: form-data"

    .line 196
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 197
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    const-string v2, "; "

    .line 198
    invoke-direct {p0, v2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    .line 199
    aget-object v2, p1, v1

    aget-object v2, v2, v0

    invoke-direct {p0, v2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    const-string v2, "=\""

    .line 200
    invoke-direct {p0, v2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    .line 201
    aget-object v2, p1, v1

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-direct {p0, v2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    const-string v2, "\""

    .line 202
    invoke-direct {p0, v2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    const-string p1, "\r\nContent-Type: "

    .line 206
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    .line 207
    invoke-direct {p0, p2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    :cond_1
    const-string p1, "\r\n\r\n"

    .line 210
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bodyToString()Ljava/lang/String;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->loggedRequest:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public putField(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->fields:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public putField(Ljava/lang/String;Ljava/util/Date;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->fields:Ljava/util/Map;

    invoke-static {p2}, Lcom/box/androidsdk/content/utils/BoxDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected resetBody()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    .line 170
    iput-boolean v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->firstBoundary:Z

    .line 171
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V

    .line 172
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->loggedRequest:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    return-void
.end method

.method public setBody(Ljava/io/InputStream;)Lcom/box/androidsdk/content/requests/BoxHttpRequest;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public setBody(Ljava/lang/String;)V
    .locals 0

    .line 118
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public setFile(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 0

    .line 85
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->inputStream:Ljava/io/InputStream;

    .line 86
    iput-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->filename:Ljava/lang/String;

    return-void
.end method

.method public setFile(Ljava/io/InputStream;Ljava/lang/String;J)V
    .locals 0

    .line 96
    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->setFile(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 97
    iput-wide p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->fileSize:J

    return-void
.end method

.method protected writeBody(Ljava/net/HttpURLConnection;Lcom/box/androidsdk/content/listeners/ProgressListener;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 125
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    const/4 v1, 0x1

    .line 126
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 127
    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 129
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->outputStream:Ljava/io/OutputStream;

    .line 130
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->fields:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 131
    new-array v4, v1, [[Ljava/lang/String;

    new-array v3, v3, [Ljava/lang/String;

    const-string v5, "name"

    aput-object v5, v3, v0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    aput-object v5, v3, v1

    aput-object v3, v4, v0

    invoke-direct {p0, v4}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writePartHeader([[Ljava/lang/String;)V

    .line 132
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_0
    new-array p1, v3, [[Ljava/lang/String;

    const-string v2, "name"

    const-string v4, "filename"

    filled-new-array {v2, v4}, [Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v0

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "filename"

    aput-object v3, v2, v0

    iget-object v3, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->filename:Ljava/lang/String;

    aput-object v3, v2, v1

    aput-object v2, p1, v1

    const-string v1, "application/octet-stream"

    invoke-direct {p0, p1, v1}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writePartHeader([[Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->outputStream:Ljava/io/OutputStream;

    if-eqz p2, :cond_1

    .line 140
    new-instance p1, Lcom/box/androidsdk/content/utils/ProgressOutputStream;

    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->outputStream:Ljava/io/OutputStream;

    iget-wide v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->fileSize:J

    invoke-direct {p1, v1, p2, v2, v3}, Lcom/box/androidsdk/content/utils/ProgressOutputStream;-><init>(Ljava/io/OutputStream;Lcom/box/androidsdk/content/listeners/ProgressListener;J)V

    :cond_1
    const/16 p2, 0x2000

    .line 142
    new-array p2, p2, [B

    .line 143
    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v1, p2}, Ljava/io/InputStream;->read([B)I

    move-result v1

    :goto_1
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 145
    invoke-virtual {p1, p2, v0, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 146
    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->inputStream:Ljava/io/InputStream;

    invoke-virtual {v1, p2}, Ljava/io/InputStream;->read([B)I

    move-result v1

    goto :goto_1

    .line 149
    :cond_2
    sget-object p1, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->LOGGER:Ljava/util/logging/Logger;

    sget-object p2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {p1, p2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 150
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->loggedRequest:Ljava/lang/StringBuilder;

    const-string p2, "<File Contents Omitted>"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_3
    invoke-direct {p0}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeBoundary()V

    const-string p1, "--"

    .line 154
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->writeOutput(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->outputStream:Ljava/io/OutputStream;

    if-eqz p1, :cond_4

    .line 161
    :try_start_1
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_4
    return-void

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_1
    move-exception p1

    .line 156
    :try_start_2
    new-instance p2, Lcom/box/androidsdk/content/BoxException;

    const-string v0, "Couldn\'t connect to the Box API due to a network error."

    invoke-direct {p2, v0, p1}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 159
    :goto_2
    iget-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestMultipart;->outputStream:Ljava/io/OutputStream;

    if-eqz p2, :cond_5

    .line 161
    :try_start_3
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 164
    :catch_2
    :cond_5
    throw p1
.end method

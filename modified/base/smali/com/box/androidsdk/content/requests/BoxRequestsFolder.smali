.class public Lcom/box/androidsdk/content/requests/BoxRequestsFolder;
.super Ljava/lang/Object;
.source "BoxRequestsFolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetTrashedItems;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$DeleteFolderFromCollection;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$AddFolderToCollection;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$RestoreTrashedFolder;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$DeleteTrashedFolder;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetTrashedFolder;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$DeleteFolder;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$CreateFolder;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$CopyFolder;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$UpdateSharedFolder;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$UpdateFolder;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetCollaborations;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderInfo;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

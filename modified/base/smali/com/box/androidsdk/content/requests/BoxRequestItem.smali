.class public abstract Lcom/box/androidsdk/content/requests/BoxRequestItem;
.super Lcom/box/androidsdk/content/requests/BoxRequest;
.source "BoxRequestItem.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/box/androidsdk/content/models/BoxJsonObject;",
        "R:",
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "TE;TR;>;>",
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "TE;TR;>;"
    }
.end annotation


# static fields
.field protected static QUERY_FIELDS:Ljava/lang/String; = "fields"


# instance fields
.field protected mHintHeader:Ljava/lang/StringBuffer;

.field protected mId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method protected constructor <init>(Lcom/box/androidsdk/content/requests/BoxRequestItem;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Lcom/box/androidsdk/content/requests/BoxRequest;)V

    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mId:Ljava/lang/String;

    .line 24
    new-instance p1, Ljava/lang/StringBuffer;

    invoke-direct {p1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mHintHeader:Ljava/lang/StringBuffer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TE;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/box/androidsdk/content/models/BoxSession;",
            ")V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p3, p4}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Ljava/lang/Class;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mId:Ljava/lang/String;

    .line 24
    new-instance p1, Ljava/lang/StringBuffer;

    invoke-direct {p1}, Ljava/lang/StringBuffer;-><init>()V

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mHintHeader:Ljava/lang/StringBuffer;

    .line 36
    sget-object p1, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->JSON:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mContentType:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    .line 37
    iput-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public varargs addRepresentationHintGroup([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")TR;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 75
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mHintHeader:Ljava/lang/StringBuffer;

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 76
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mHintHeader:Ljava/lang/StringBuffer;

    const-string v1, ","

    invoke-static {v1, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mHintHeader:Ljava/lang/StringBuffer;

    const-string v0, "]"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    return-object p0
.end method

.method protected createHeaderMap()V
    .locals 3

    .line 84
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequest;->createHeaderMap()V

    .line 85
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mHintHeader:Ljava/lang/StringBuffer;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mHeaderMap:Ljava/util/LinkedHashMap;

    const-string v1, "x-rep-hints"

    iget-object v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mHintHeader:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mId:Ljava/lang/String;

    return-object v0
.end method

.method protected onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/box/androidsdk/content/requests/BoxResponse<",
            "TE;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 101
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequest;->onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    .line 102
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequest;->handleUpdateCache(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    return-void
.end method

.method public varargs setFields([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")TR;"
        }
    .end annotation

    .line 51
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    aget-object v0, p1, v1

    if-nez v0, :cond_0

    .line 52
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mQueryMap:Ljava/util/HashMap;

    sget-object v0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->QUERY_FIELDS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0

    .line 55
    :cond_0
    array-length v0, p1

    if-lez v0, :cond_2

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    aget-object v3, p1, v1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    .line 58
    :goto_0
    array-length v4, p1

    if-ge v3, v4, :cond_1

    .line 59
    sget-object v4, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v5, ",%s"

    new-array v6, v2, [Ljava/lang/Object;

    aget-object v7, p1, v3

    aput-object v7, v6, v1

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 61
    :cond_1
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestItem;->mQueryMap:Ljava/util/HashMap;

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequestItem;->QUERY_FIELDS:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-object p0
.end method

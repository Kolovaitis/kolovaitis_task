.class public Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;
.super Lcom/box/androidsdk/content/requests/BoxRequest;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CreateUploadSession"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "Lcom/box/androidsdk/content/models/BoxUploadSession;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x710b404887362caeL


# instance fields
.field private mDestinationFolderId:Ljava/lang/String;

.field private mFileInputStream:Ljava/io/InputStream;

.field private mFileName:Ljava/lang/String;

.field private mFileSize:J


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 1263
    const-class v0, Lcom/box/androidsdk/content/models/BoxUploadSession;

    invoke-direct {p0, v0, p3, p4}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Ljava/lang/Class;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 1264
    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mRequestUrlString:Ljava/lang/String;

    .line 1265
    sget-object p3, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->POST:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 1266
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileName:Ljava/lang/String;

    .line 1267
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide p3

    iput-wide p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileSize:J

    .line 1268
    new-instance p3, Ljava/io/FileInputStream;

    invoke-direct {p3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileInputStream:Ljava/io/InputStream;

    .line 1269
    iput-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mDestinationFolderId:Ljava/lang/String;

    .line 1270
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p3, "folder_id"

    invoke-virtual {p1, p3, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1271
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p2, "file_size"

    iget-wide p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileSize:J

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1272
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p2, "file_name"

    iget-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileName:Ljava/lang/String;

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 1

    .line 1285
    const-class v0, Lcom/box/androidsdk/content/models/BoxUploadSession;

    invoke-direct {p0, v0, p6, p7}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Ljava/lang/Class;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 1286
    iput-object p6, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mRequestUrlString:Ljava/lang/String;

    .line 1287
    sget-object p6, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->POST:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p6, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 1288
    iput-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileName:Ljava/lang/String;

    .line 1289
    iput-wide p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileSize:J

    .line 1290
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileInputStream:Ljava/io/InputStream;

    .line 1291
    iput-object p5, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mDestinationFolderId:Ljava/lang/String;

    .line 1292
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p2, "folder_id"

    invoke-virtual {p1, p2, p5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1293
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p2, "file_size"

    iget-wide p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileSize:J

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1294
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string p2, "file_name"

    iget-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileName:Ljava/lang/String;

    invoke-virtual {p1, p2, p3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static computeSha1s(Ljava/io/InputStream;Lcom/box/androidsdk/content/models/BoxUploadSession;J)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 1318
    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getTotalParts()I

    move-result v0

    .line 1319
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const-string v2, "SHA-1"

    .line 1324
    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    const-string v3, "SHA-1"

    .line 1325
    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v0, :cond_2

    .line 1327
    invoke-static {p1, v5, p2, p3}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getChunkSize(Lcom/box/androidsdk/content/models/BoxUploadSession;IJ)I

    move-result v6

    const/16 v7, 0x2000

    .line 1328
    new-array v8, v7, [B

    :goto_1
    if-lez v6, :cond_1

    if-ge v6, v7, :cond_0

    .line 1331
    new-array v8, v6, [B

    .line 1333
    :cond_0
    invoke-virtual {p0, v8}, Ljava/io/InputStream;->read([B)I

    add-int/lit16 v6, v6, -0x2000

    .line 1335
    invoke-virtual {v3, v8}, Ljava/security/MessageDigest;->update([B)V

    .line 1336
    invoke-virtual {v2, v8}, Ljava/security/MessageDigest;->update([B)V

    goto :goto_1

    .line 1338
    :cond_1
    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    invoke-static {v6, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1339
    invoke-virtual {v3}, Ljava/security/MessageDigest;->reset()V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1341
    :cond_2
    invoke-virtual {p1, v1}, Lcom/box/androidsdk/content/models/BoxUploadSession;->setPartsSha1(Ljava/util/List;)V

    .line 1342
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object p0

    invoke-static {p0, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/box/androidsdk/content/models/BoxUploadSession;->setSha1(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getDestinationFolderId()Ljava/lang/String;
    .locals 1

    .line 1377
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mDestinationFolderId:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .line 1360
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFileSize()J
    .locals 2

    .line 1368
    iget-wide v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileSize:J

    return-wide v0
.end method

.method protected onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/box/androidsdk/content/requests/BoxResponse<",
            "Lcom/box/androidsdk/content/models/BoxUploadSession;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 1299
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1300
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->getResult()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUploadSession;

    .line 1303
    :try_start_0
    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileInputStream:Ljava/io/InputStream;

    iget-wide v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileSize:J

    invoke-static {v1, v0, v2, v3}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->computeSha1s(Ljava/io/InputStream;Lcom/box/androidsdk/content/models/BoxUploadSession;J)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 1307
    new-instance v0, Lcom/box/androidsdk/content/BoxException;

    const-string v1, "Can\'t compute sha1 for file"

    invoke-direct {v0, v1, p1}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception p1

    .line 1305
    new-instance v0, Lcom/box/androidsdk/content/BoxException;

    const-string v1, "Can\'t compute sha1 for file"

    invoke-direct {v0, v1, p1}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 1310
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequest;->onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 2

    .line 1350
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mFileName:Ljava/lang/String;

    .line 1351
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "file_name"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.class public final enum Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;
.super Ljava/lang/Enum;
.source "BoxRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

.field public static final enum APPLICATION_OCTET_STREAM:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

.field public static final enum JSON:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

.field public static final enum JSON_PATCH:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

.field public static final enum URL_ENCODED:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;


# instance fields
.field private mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 870
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    const-string v1, "JSON"

    const-string v2, "application/json"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->JSON:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    const-string v1, "URL_ENCODED"

    const-string v2, "application/x-www-form-urlencoded"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->URL_ENCODED:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    .line 871
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    const-string v1, "JSON_PATCH"

    const-string v2, "application/json-patch+json"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->JSON_PATCH:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    const-string v1, "APPLICATION_OCTET_STREAM"

    const-string v2, "application/octet-stream"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->APPLICATION_OCTET_STREAM:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    const/4 v0, 0x4

    .line 869
    new-array v0, v0, [Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->JSON:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    aput-object v1, v0, v3

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->URL_ENCODED:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    aput-object v1, v0, v4

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->JSON_PATCH:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    aput-object v1, v0, v5

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->APPLICATION_OCTET_STREAM:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    aput-object v1, v0, v6

    sput-object v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->$VALUES:[Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 875
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 876
    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->mName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;
    .locals 1

    .line 869
    const-class v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;
    .locals 1

    .line 869
    sget-object v0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->$VALUES:[Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 881
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->mName:Ljava/lang/String;

    return-object v0
.end method

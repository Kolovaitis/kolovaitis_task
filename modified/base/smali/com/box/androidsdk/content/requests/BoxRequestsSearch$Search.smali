.class public Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
.super Lcom/box/androidsdk/content/requests/BoxRequestItem;
.source "BoxRequestsSearch.java"

# interfaces
.implements Lcom/box/androidsdk/content/requests/BoxCacheableRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Search"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search$Scope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequestItem<",
        "Lcom/box/androidsdk/content/models/BoxIteratorItems;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;",
        ">;",
        "Lcom/box/androidsdk/content/requests/BoxCacheableRequest<",
        "Lcom/box/androidsdk/content/models/BoxIteratorItems;",
        ">;"
    }
.end annotation


# static fields
.field public static CONTENT_TYPE_COMMENTS:Ljava/lang/String; = "comments"

.field public static CONTENT_TYPE_DESCRIPTION:Ljava/lang/String; = "description"

.field public static CONTENT_TYPE_FILE_CONTENTS:Ljava/lang/String; = "file_content"

.field public static CONTENT_TYPE_NAME:Ljava/lang/String; = "name"

.field public static CONTENT_TYPE_TAGS:Ljava/lang/String; = "tags"

.field protected static final FIELD_ANCESTOR_FOLDER_IDS:Ljava/lang/String; = "ancestor_folder_ids"

.field protected static final FIELD_CONTENT_TYPES:Ljava/lang/String; = "content_types"

.field protected static final FIELD_CREATED_AT_RANGE:Ljava/lang/String; = "created_at_range"

.field protected static final FIELD_FILE_EXTENSIONS:Ljava/lang/String; = "file_extensions"

.field protected static final FIELD_LIMIT:Ljava/lang/String; = "limit"

.field protected static final FIELD_OFFSET:Ljava/lang/String; = "offset"

.field protected static final FIELD_OWNER_USER_IDS:Ljava/lang/String; = "owner_user_ids"

.field protected static final FIELD_QUERY:Ljava/lang/String; = "query"

.field protected static final FIELD_SCOPE:Ljava/lang/String; = "scope"

.field protected static final FIELD_SIZE_RANGE:Ljava/lang/String; = "size_range"

.field protected static final FIELD_TYPE:Ljava/lang/String; = "type"

.field protected static final FIELD_UPDATED_AT_RANGE:Ljava/lang/String; = "updated_at_range"

.field private static final serialVersionUID:J = 0x70be1f2741234d00L


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 2

    .line 122
    const-class v0, Lcom/box/androidsdk/content/models/BoxIteratorItems;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p2, p3}, Lcom/box/androidsdk/content/requests/BoxRequestItem;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const-string p2, "query"

    .line 123
    invoke-virtual {p0, p2, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    .line 124
    sget-object p1, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->GET:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    return-void
.end method

.method private addTimeRange(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V
    .locals 0

    .line 416
    invoke-static {p2, p3}, Lcom/box/androidsdk/content/utils/BoxDateFormat;->getTimeRangeString(Ljava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    .line 417
    invoke-static {p2}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_0

    .line 418
    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    :cond_0
    return-void
.end method

.method private getStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 392
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const-string v0, ","

    .line 395
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private returnFromDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 401
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 402
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/BoxDateFormat;->getTimeRangeDates(Ljava/lang/String;)[Ljava/util/Date;

    move-result-object p1

    const/4 v0, 0x0

    aget-object p1, p1, v0

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private returnToDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    .line 408
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 409
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/BoxDateFormat;->getTimeRangeDates(Ljava/lang/String;)[Ljava/util/Date;

    move-result-object p1

    const/4 v0, 0x1

    aget-object p1, p1, v0

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public getAncestorFolderIds()[Ljava/lang/String;
    .locals 1

    const-string v0, "ancestor_folder_ids"

    .line 320
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentTypes()[Ljava/lang/String;
    .locals 1

    const-string v0, "content_types"

    .line 327
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCreatedAtDateRangeFrom()Ljava/util/Date;
    .locals 1

    const-string v0, "created_at_range"

    .line 275
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->returnFromDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getCreatedAtDateRangeTo()Ljava/util/Date;
    .locals 1

    const-string v0, "created_at_range"

    .line 282
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->returnToDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getFileExtensions()[Ljava/lang/String;
    .locals 1

    const-string v0, "file_extensions"

    .line 387
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastUpdatedAtDateRangeFrom()Ljava/util/Date;
    .locals 1

    const-string v0, "updated_at_range"

    .line 261
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->returnFromDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getLastUpdatedAtDateRangeTo()Ljava/util/Date;
    .locals 1

    const-string v0, "updated_at_range"

    .line 268
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->returnToDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getLimit()Ljava/lang/Integer;
    .locals 2

    .line 341
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    const-string v1, "limit"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 344
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    return-object v1

    :cond_0
    return-object v1
.end method

.method public getOffset()Ljava/lang/Integer;
    .locals 2

    .line 357
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    const-string v1, "offset"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 360
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    return-object v1

    :cond_0
    return-object v1
.end method

.method public getOwnerUserIds()[Ljava/lang/String;
    .locals 1

    const-string v0, "owner_user_ids"

    .line 313
    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 2

    .line 373
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    const-string v1, "query"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getScope()Ljava/lang/String;
    .locals 2

    .line 380
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    const-string v1, "scope"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getSizeRangeFrom()Ljava/lang/Long;
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    const-string v1, "size_range"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 290
    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const-string v1, ","

    .line 293
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 294
    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getSizeRangeTo()Ljava/lang/Long;
    .locals 2

    .line 301
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    const-string v1, "size_range"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 302
    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    const-string v1, ","

    .line 305
    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    .line 306
    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    .line 334
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public limitAncestorFolderIds([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 2

    const-string v0, "ancestor_folder_ids"

    const-string v1, ","

    .line 210
    invoke-static {p1, v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->concatStringWithDelimiter([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public limitContentTypes([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 2

    const-string v0, "content_types"

    const-string v1, ","

    .line 221
    invoke-static {p1, v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->concatStringWithDelimiter([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public limitCreationTime(Ljava/util/Date;Ljava/util/Date;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 1

    const-string v0, "created_at_range"

    .line 167
    invoke-direct {p0, v0, p1, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->addTimeRange(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V

    return-object p0
.end method

.method public limitFileExtensions([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 2

    const-string v0, "file_extensions"

    const-string v1, ","

    .line 155
    invoke-static {p1, v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->concatStringWithDelimiter([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public limitLastUpdateTime(Ljava/util/Date;Ljava/util/Date;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 1

    const-string v0, "updated_at_range"

    .line 179
    invoke-direct {p0, v0, p1, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->addTimeRange(Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V

    return-object p0
.end method

.method public limitOwnerUserIds([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 2

    const-string v0, "owner_user_ids"

    const-string v1, ","

    .line 200
    invoke-static {p1, v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->concatStringWithDelimiter([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public limitSearchScope(Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search$Scope;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 2

    const-string v0, "scope"

    .line 145
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search$Scope;->name()Ljava/lang/String;

    move-result-object p1

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public limitSizeRange(JJ)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 3

    const-string v0, "size_range"

    const-string v1, "%d,%d"

    const/4 v2, 0x2

    .line 190
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v2, p2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    const/4 p2, 0x1

    aput-object p1, v2, p2

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public limitType(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 1

    const-string v0, "type"

    .line 231
    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->mQueryMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public sendForCachedResult()Lcom/box/androidsdk/content/models/BoxIteratorItems;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 97
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->handleSendForCachedResult()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxIteratorItems;

    return-object v0
.end method

.method public bridge synthetic sendForCachedResult()Lcom/box/androidsdk/content/models/BoxObject;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 21
    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->sendForCachedResult()Lcom/box/androidsdk/content/models/BoxIteratorItems;

    move-result-object v0

    return-object v0
.end method

.method public setLimit(I)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 1

    const-string v0, "limit"

    .line 242
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public setOffset(I)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;
    .locals 1

    const-string v0, "offset"

    .line 253
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;->limitValueForKey(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsSearch$Search;

    return-object p0
.end method

.method public toTaskForCachedResult()Lcom/box/androidsdk/content/BoxFutureTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/box/androidsdk/content/BoxFutureTask<",
            "Lcom/box/androidsdk/content/models/BoxIteratorItems;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 102
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->handleToTaskForCachedResult()Lcom/box/androidsdk/content/BoxFutureTask;

    move-result-object v0

    return-object v0
.end method

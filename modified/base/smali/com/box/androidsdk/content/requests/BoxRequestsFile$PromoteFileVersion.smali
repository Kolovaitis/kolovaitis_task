.class public Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;
.super Lcom/box/androidsdk/content/requests/BoxRequestItem;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PromoteFileVersion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequestItem<",
        "Lcom/box/androidsdk/content/models/BoxFileVersion;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x70be1f2741234cc7L


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 1

    .line 521
    const-class v0, Lcom/box/androidsdk/content/models/BoxFileVersion;

    invoke-direct {p0, v0, p1, p3, p4}, Lcom/box/androidsdk/content/requests/BoxRequestItem;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 522
    sget-object p1, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->POST:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 523
    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;->setVersionId(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;

    return-void
.end method


# virtual methods
.method protected onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/box/androidsdk/content/requests/BoxResponse<",
            "Lcom/box/androidsdk/content/models/BoxFileVersion;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 540
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    .line 541
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->handleUpdateCache(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    return-void
.end method

.method public setVersionId(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;
    .locals 3

    .line 533
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "type"

    const-string v2, "file_version"

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

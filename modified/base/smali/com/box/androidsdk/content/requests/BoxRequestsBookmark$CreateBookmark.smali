.class public Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;
.super Lcom/box/androidsdk/content/requests/BoxRequestItem;
.source "BoxRequestsBookmark.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsBookmark;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CreateBookmark"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequestItem<",
        "Lcom/box/androidsdk/content/models/BoxBookmark;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x70be1f2741234cc6L


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 2

    .line 85
    const-class v0, Lcom/box/androidsdk/content/models/BoxBookmark;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/box/androidsdk/content/requests/BoxRequestItem;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 86
    sget-object p3, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->POST:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 87
    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->setParentId(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;

    .line 88
    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->setUrl(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;

    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "description"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 138
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getParentId()Ljava/lang/String;
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "parent"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    .line 118
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "url"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;
    .locals 2

    .line 168
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "description"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setParentId(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;
    .locals 2

    .line 107
    invoke-static {p1}, Lcom/box/androidsdk/content/models/BoxFolder;->createFromId(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFolder;

    move-result-object p1

    .line 108
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "parent"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

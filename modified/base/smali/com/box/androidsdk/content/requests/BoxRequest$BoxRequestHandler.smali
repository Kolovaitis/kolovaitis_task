.class public Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;
.super Ljava/lang/Object;
.source "BoxRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BoxRequestHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Lcom/box/androidsdk/content/requests/BoxRequest;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DEFAULT_AUTH_REFRESH_RETRY:I = 0x4

.field protected static final DEFAULT_NUM_RETRIES:I = 0x1

.field protected static final DEFAULT_RATE_LIMIT_WAIT:I = 0x14

.field public static final OAUTH_ERROR_HEADER:Ljava/lang/String; = "error"

.field public static final OAUTH_INVALID_TOKEN:Ljava/lang/String; = "invalid_token"

.field public static final WWW_AUTHENTICATE:Ljava/lang/String; = "WWW-Authenticate"


# instance fields
.field protected mNumRateLimitRetries:I

.field private mRefreshRetries:I

.field protected mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/requests/BoxRequest;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .line 577
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 574
    iput v0, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mNumRateLimitRetries:I

    .line 575
    iput v0, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mRefreshRetries:I

    .line 578
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    return-void
.end method

.method private authFailed(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Z
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    .line 748
    :cond_0
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result p1

    const/16 v1, 0x191

    if-ne p1, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected static getRetryAfterFromResponse(Lcom/box/androidsdk/content/requests/BoxHttpResponse;I)I
    .locals 1

    .line 731
    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getHttpURLConnection()Ljava/net/HttpURLConnection;

    move-result-object p0

    const-string v0, "Retry-After"

    invoke-virtual {p0, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 732
    invoke-static {p0}, Lcom/box/androidsdk/content/utils/SdkUtils;->isBlank(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 734
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    if-lez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x1

    const/4 p1, 0x1

    :cond_1
    :goto_0
    mul-int/lit16 p1, p1, 0x3e8

    return p1
.end method

.method private isInvalidTokenError(Ljava/lang/String;)Z
    .locals 5

    const-string v0, "="

    .line 771
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 772
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    aget-object v0, p1, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    aget-object v2, p1, v0

    if-eqz v2, :cond_0

    const-string v2, "error"

    .line 773
    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "invalid_token"

    aget-object p1, p1, v0

    const-string v3, "\""

    const-string v4, ""

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    return v0

    :cond_0
    return v1
.end method

.method private oauthExpired(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Z
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    const/16 v1, 0x191

    .line 755
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result v2

    if-eq v1, v2, :cond_1

    return v0

    .line 758
    :cond_1
    iget-object p1, p1, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->mConnection:Ljava/net/HttpURLConnection;

    const-string v1, "WWW-Authenticate"

    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 759
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ","

    .line 760
    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 761
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_3

    aget-object v3, p1, v2

    .line 762
    invoke-direct {p0, v3}, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->isInvalidTokenError(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 p1, 0x1

    return p1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return v0
.end method


# virtual methods
.method protected disconnectForInterrupt(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 638
    :try_start_0
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getHttpURLConnection()Ljava/net/HttpURLConnection;

    move-result-object p1

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "Interrupt disconnect"

    .line 640
    invoke-static {v0, p1}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 642
    :goto_0
    new-instance p1, Lcom/box/androidsdk/content/BoxException;

    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    const-string v1, "Thread interrupted request cancelled "

    invoke-direct {p1, v1, v0}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method public isResponseSuccess(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Z
    .locals 1

    .line 588
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result p1

    const/16 v0, 0xc8

    if-lt p1, v0, :cond_0

    const/16 v0, 0x12c

    if-lt p1, v0, :cond_1

    :cond_0
    const/16 v0, 0x1ad

    if-ne p1, v0, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public onException(Lcom/box/androidsdk/content/requests/BoxRequest;Lcom/box/androidsdk/content/requests/BoxHttpResponse;Lcom/box/androidsdk/content/BoxException;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException$RefreshFailure;
        }
    .end annotation

    .line 654
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxRequest;->getSession()Lcom/box/androidsdk/content/models/BoxSession;

    move-result-object v0

    .line 655
    invoke-direct {p0, p2}, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->oauthExpired(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 657
    :try_start_0
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->refresh()Lcom/box/androidsdk/content/BoxFutureTask;

    move-result-object p1

    invoke-virtual {p1}, Lcom/box/androidsdk/content/BoxFutureTask;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxResponse;

    .line 658
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->isSuccess()Z

    move-result p2

    if-eqz p2, :cond_0

    return v2

    .line 660
    :cond_0
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->getException()Ljava/lang/Exception;

    move-result-object p2

    if-eqz p2, :cond_c

    .line 661
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->getException()Ljava/lang/Exception;

    move-result-object p2

    instance-of p2, p2, Lcom/box/androidsdk/content/BoxException$RefreshFailure;

    if-nez p2, :cond_1

    return v3

    .line 662
    :cond_1
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->getException()Ljava/lang/Exception;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/BoxException$RefreshFailure;

    throw p1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p1

    const-string p2, "oauthRefresh"

    const-string p3, "Interrupted Exception"

    .line 670
    invoke-static {p2, p3, p1}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :catch_1
    move-exception p1

    const-string p2, "oauthRefresh"

    const-string p3, "Interrupted Exception"

    .line 668
    invoke-static {p2, p3, p1}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 672
    :cond_2
    invoke-direct {p0, p2}, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->authFailed(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Z

    move-result v1

    const/high16 v4, 0x10000000

    if-eqz v1, :cond_a

    .line 673
    invoke-virtual {p3}, Lcom/box/androidsdk/content/BoxException;->getErrorType()Lcom/box/androidsdk/content/BoxException$ErrorType;

    move-result-object v1

    .line 674
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->suppressesAuthErrorUIAfterLogin()Z

    move-result v5

    if-nez v5, :cond_c

    .line 675
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 676
    sget-object v6, Lcom/box/androidsdk/content/BoxException$ErrorType;->IP_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    if-eq v1, v6, :cond_9

    sget-object v6, Lcom/box/androidsdk/content/BoxException$ErrorType;->LOCATION_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    if-ne v1, v6, :cond_3

    goto/16 :goto_0

    .line 681
    :cond_3
    sget-object v4, Lcom/box/androidsdk/content/BoxException$ErrorType;->TERMS_OF_SERVICE_REQUIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    if-ne v1, v4, :cond_4

    .line 682
    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_error_terms_of_service:I

    invoke-static {v5, v1, v2}, Lcom/box/androidsdk/content/utils/SdkUtils;->toastSafely(Landroid/content/Context;II)V

    .line 687
    :cond_4
    :try_start_1
    iget v1, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mRefreshRetries:I

    const/4 v4, 0x4

    if-le v1, v4, :cond_6

    .line 688
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Exceeded max refresh retries for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 689
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " response code"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/box/androidsdk/content/BoxException;->getResponseCode()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " response "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 690
    invoke-virtual {p3}, Lcom/box/androidsdk/content/BoxException;->getAsBoxError()Lcom/box/androidsdk/content/models/BoxError;

    move-result-object p2

    if-eqz p2, :cond_5

    .line 691
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/box/androidsdk/content/BoxException;->getAsBoxError()Lcom/box/androidsdk/content/models/BoxError;

    move-result-object p1

    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxError;->toJson()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_5
    const-string p2, "authFailed"

    .line 693
    invoke-static {p2, p1, p3}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->nonFatalE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return v3

    .line 698
    :cond_6
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->refresh()Lcom/box/androidsdk/content/BoxFutureTask;

    move-result-object p1

    invoke-virtual {p1}, Lcom/box/androidsdk/content/BoxFutureTask;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxResponse;

    .line 699
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->isSuccess()Z

    move-result p2

    if-eqz p2, :cond_7

    .line 700
    iget p1, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mRefreshRetries:I

    add-int/2addr p1, v2

    iput p1, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mRefreshRetries:I

    return v2

    .line 702
    :cond_7
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->getException()Ljava/lang/Exception;

    move-result-object p2

    if-eqz p2, :cond_c

    .line 703
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->getException()Ljava/lang/Exception;

    move-result-object p2

    instance-of p2, p2, Lcom/box/androidsdk/content/BoxException$RefreshFailure;

    if-nez p2, :cond_8

    return v3

    .line 704
    :cond_8
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxResponse;->getException()Ljava/lang/Exception;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/BoxException$RefreshFailure;

    throw p1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2

    :catch_2
    move-exception p1

    const-string p2, "oauthRefresh"

    const-string p3, "Interrupted Exception"

    .line 712
    invoke-static {p2, p3, p1}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :catch_3
    move-exception p1

    const-string p2, "oauthRefresh"

    const-string p3, "Interrupted Exception"

    .line 710
    invoke-static {p2, p3, p1}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 677
    :cond_9
    :goto_0
    new-instance p1, Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    const-class p3, Lcom/box/androidsdk/content/auth/BlockedIPErrorActivity;

    invoke-direct {p1, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 678
    invoke-virtual {p1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 679
    invoke-virtual {v5, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return v3

    :cond_a
    if-eqz p2, :cond_c

    .line 716
    invoke-virtual {p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result p1

    const/16 p2, 0x193

    if-ne p1, p2, :cond_c

    .line 717
    invoke-virtual {p3}, Lcom/box/androidsdk/content/BoxException;->getErrorType()Lcom/box/androidsdk/content/BoxException$ErrorType;

    move-result-object p1

    .line 718
    sget-object p2, Lcom/box/androidsdk/content/BoxException$ErrorType;->IP_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    if-eq p1, p2, :cond_b

    sget-object p2, Lcom/box/androidsdk/content/BoxException$ErrorType;->LOCATION_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    if-ne p1, p2, :cond_c

    .line 719
    :cond_b
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    .line 720
    new-instance p2, Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxSession;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    const-class v0, Lcom/box/androidsdk/content/auth/BlockedIPErrorActivity;

    invoke-direct {p2, p3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 721
    invoke-virtual {p2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 722
    invoke-virtual {p1, p2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return v3

    :cond_c
    :goto_1
    return v3
.end method

.method public onResponse(Ljava/lang/Class;Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Lcom/box/androidsdk/content/models/BoxObject;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/box/androidsdk/content/models/BoxObject;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;",
            "Lcom/box/androidsdk/content/requests/BoxHttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/InstantiationException;,
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 605
    invoke-virtual {p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getResponseCode()I

    move-result v0

    const/16 v1, 0x1ad

    if-ne v0, v1, :cond_0

    .line 606
    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->retryRateLimited(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object p1

    return-object p1

    .line 608
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 609
    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->disconnectForInterrupt(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)V

    .line 612
    :cond_1
    invoke-virtual {p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 613
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/models/BoxObject;

    .line 614
    instance-of v1, p1, Lcom/box/androidsdk/content/models/BoxJsonObject;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->JSON:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    invoke-virtual {v1}, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 615
    invoke-virtual {p2}, Lcom/box/androidsdk/content/requests/BoxHttpResponse;->getStringBody()Ljava/lang/String;

    move-result-object p2

    .line 616
    move-object v0, p1

    check-cast v0, Lcom/box/androidsdk/content/models/BoxJsonObject;

    invoke-virtual {v0, p2}, Lcom/box/androidsdk/content/models/BoxJsonObject;->createFromJson(Ljava/lang/String;)V

    :cond_2
    return-object p1
.end method

.method protected retryRateLimited(Lcom/box/androidsdk/content/requests/BoxHttpResponse;)Lcom/box/androidsdk/content/models/BoxObject;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/box/androidsdk/content/models/BoxObject;",
            ">(",
            "Lcom/box/androidsdk/content/requests/BoxHttpResponse;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 622
    iget v0, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mNumRateLimitRetries:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    add-int/2addr v0, v1

    .line 623
    iput v0, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mNumRateLimitRetries:I

    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    .line 624
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    mul-double v2, v2, v0

    double-to-int v0, v2

    add-int/lit8 v0, v0, 0x14

    .line 625
    invoke-static {p1, v0}, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->getRetryAfterFromResponse(Lcom/box/androidsdk/content/requests/BoxHttpResponse;I)I

    move-result p1

    int-to-long v0, p1

    .line 627
    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 631
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;->mRequest:Lcom/box/androidsdk/content/requests/BoxRequest;

    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxRequest;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    .line 629
    new-instance v0, Lcom/box/androidsdk/content/BoxException;

    invoke-virtual {p1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/box/androidsdk/content/BoxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 633
    :cond_0
    new-instance v1, Lcom/box/androidsdk/content/BoxException$RateLimitAttemptsExceeded;

    const-string v2, "Max attempts exceeded"

    invoke-direct {v1, v2, v0, p1}, Lcom/box/androidsdk/content/BoxException$RateLimitAttemptsExceeded;-><init>(Ljava/lang/String;ILcom/box/androidsdk/content/requests/BoxHttpResponse;)V

    throw v1
.end method

.class public Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;
.super Lcom/box/androidsdk/content/requests/BoxRequest;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommitUploadSession"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "Lcom/box/androidsdk/content/models/BoxFile;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;",
        ">;"
    }
.end annotation


# static fields
.field private static final IF_MATCH:Ljava/lang/String; = "If-Match"

.field private static final IF_NONE_MATCH:Ljava/lang/String; = "If-Non-Match"

.field private static final serialVersionUID:J = 0x726e85c0e4c030a7L


# instance fields
.field private final mIfMatch:Ljava/lang/String;

.field private final mIfNoneMatch:Ljava/lang/String;

.field private final mSha1:Ljava/lang/String;

.field private final mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxUploadSession;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/box/androidsdk/content/models/BoxUploadSessionPart;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/box/androidsdk/content/models/BoxUploadSession;",
            "Lcom/box/androidsdk/content/models/BoxSession;",
            ")V"
        }
    .end annotation

    .line 1533
    const-class v0, Lcom/box/androidsdk/content/models/BoxFile;

    invoke-virtual {p5}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getEndpoints()Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;

    move-result-object v1

    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;->getCommitEndpoint()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p6}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Ljava/lang/Class;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 1534
    sget-object p6, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->POST:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p6, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 1535
    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mIfMatch:Ljava/lang/String;

    .line 1536
    iput-object p4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mIfNoneMatch:Ljava/lang/String;

    .line 1537
    iput-object p5, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;

    .line 1538
    invoke-virtual {p5}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getSha1()Ljava/lang/String;

    move-result-object p3

    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mSha1:Ljava/lang/String;

    .line 1539
    sget-object p3, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->JSON:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mContentType:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    .line 1540
    invoke-direct {p0, p1, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->setCommitBody(Ljava/util/List;Ljava/util/Map;)V

    .line 1541
    new-instance p1, Lcom/box/androidsdk/content/requests/MultiputResponseHandler;

    invoke-direct {p1, p0}, Lcom/box/androidsdk/content/requests/MultiputResponseHandler;-><init>(Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;)V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->setRequestHandler(Lcom/box/androidsdk/content/requests/BoxRequest$BoxRequestHandler;)Lcom/box/androidsdk/content/requests/BoxRequest;

    return-void
.end method

.method private setCommitBody(Ljava/util/List;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/box/androidsdk/content/models/BoxUploadSessionPart;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1561
    new-instance v0, Lcom/eclipsesource/json/JsonArray;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonArray;-><init>()V

    .line 1562
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/box/androidsdk/content/models/BoxUploadSessionPart;

    .line 1563
    new-instance v2, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {v2}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    const-string v3, "part_id"

    .line 1564
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxUploadSessionPart;->getPartId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    const-string v3, "offset"

    .line 1565
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxUploadSessionPart;->getOffset()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;J)Lcom/eclipsesource/json/JsonObject;

    const-string v3, "size"

    .line 1566
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxUploadSessionPart;->getSize()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;J)Lcom/eclipsesource/json/JsonObject;

    .line 1567
    invoke-virtual {v0, v2}, Lcom/eclipsesource/json/JsonArray;->add(Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonArray;

    goto :goto_0

    .line 1569
    :cond_0
    iget-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "parts"

    invoke-virtual {p1, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_2

    .line 1572
    new-instance p1, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {p1}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    .line 1573
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1574
    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    goto :goto_1

    .line 1576
    :cond_1
    iget-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v0, "attributes"

    invoke-virtual {p2, v0, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    return-void
.end method


# virtual methods
.method protected createHeaderMap()V
    .locals 4

    .line 1546
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequest;->createHeaderMap()V

    .line 1547
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mHeaderMap:Ljava/util/LinkedHashMap;

    const-string v1, "digest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sha="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mSha1:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1548
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mIfMatch:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1549
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mHeaderMap:Ljava/util/LinkedHashMap;

    const-string v1, "If-Match"

    iget-object v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mIfMatch:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v0, "If-Non-Match"

    .line 1551
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1552
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mHeaderMap:Ljava/util/LinkedHashMap;

    const-string v1, "If-Non-Match"

    iget-object v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mIfNoneMatch:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public getUploadSession()Lcom/box/androidsdk/content/models/BoxUploadSession;
    .locals 1

    .line 1582
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;->mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;

    return-object v0
.end method

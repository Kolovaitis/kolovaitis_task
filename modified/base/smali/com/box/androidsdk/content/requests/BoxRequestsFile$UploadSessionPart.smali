.class public Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;
.super Lcom/box/androidsdk/content/requests/BoxRequest;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UploadSessionPart"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "Lcom/box/androidsdk/content/models/BoxUploadSessionPart;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;",
        ">;"
    }
.end annotation


# static fields
.field private static final CONTENT_RANGE_HEADER:Ljava/lang/String; = "content-range"

.field static final DIGEST_HEADER:Ljava/lang/String; = "digest"

.field static final DIGEST_HEADER_PREFIX_SHA:Ljava/lang/String; = "sha="

.field private static final serialVersionUID:J = 0x726e85c0e4c02caeL


# instance fields
.field private mCurrentChunkSize:I

.field private mFileSize:J

.field private mInputStream:Ljava/io/InputStream;

.field private final mPartNumber:I

.field private final mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;


# direct methods
.method public constructor <init>(Ljava/io/File;Lcom/box/androidsdk/content/models/BoxUploadSession;ILcom/box/androidsdk/content/models/BoxSession;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1405
    const-class v0, Lcom/box/androidsdk/content/models/BoxUploadSessionPart;

    invoke-virtual {p2}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getEndpoints()Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;

    move-result-object v1

    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;->getUploadPartEndpoint()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p4}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Ljava/lang/Class;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 1406
    invoke-virtual {p2}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getEndpoints()Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;

    move-result-object p4

    invoke-virtual {p4}, Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;->getUploadPartEndpoint()Ljava/lang/String;

    move-result-object p4

    iput-object p4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mRequestUrlString:Ljava/lang/String;

    .line 1407
    sget-object p4, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->PUT:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 1408
    iput p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mPartNumber:I

    .line 1409
    iput-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;

    .line 1410
    new-instance p4, Ljava/io/FileInputStream;

    invoke-direct {p4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object p4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mInputStream:Ljava/io/InputStream;

    .line 1411
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {p2, p3, v0, v1}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getChunkSize(Lcom/box/androidsdk/content/models/BoxUploadSession;IJ)I

    move-result p2

    iput p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mCurrentChunkSize:I

    .line 1412
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mFileSize:J

    .line 1413
    sget-object p1, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->APPLICATION_OCTET_STREAM:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mContentType:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;JLcom/box/androidsdk/content/models/BoxUploadSession;ILcom/box/androidsdk/content/models/BoxSession;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1426
    const-class v0, Lcom/box/androidsdk/content/models/BoxUploadSessionPart;

    invoke-virtual {p4}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getEndpoints()Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;

    move-result-object v1

    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;->getUploadPartEndpoint()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p6}, Lcom/box/androidsdk/content/requests/BoxRequest;-><init>(Ljava/lang/Class;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 1427
    invoke-virtual {p4}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getEndpoints()Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;

    move-result-object p6

    invoke-virtual {p6}, Lcom/box/androidsdk/content/models/BoxUploadSessionEndpoints;->getUploadPartEndpoint()Ljava/lang/String;

    move-result-object p6

    iput-object p6, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mRequestUrlString:Ljava/lang/String;

    .line 1428
    sget-object p6, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->PUT:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p6, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 1429
    iput p5, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mPartNumber:I

    .line 1430
    iput-object p4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;

    .line 1431
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mInputStream:Ljava/io/InputStream;

    .line 1432
    invoke-static {p4, p5, p2, p3}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getChunkSize(Lcom/box/androidsdk/content/models/BoxUploadSession;IJ)I

    move-result p1

    iput p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mCurrentChunkSize:I

    .line 1433
    iput-wide p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mFileSize:J

    .line 1434
    sget-object p1, Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;->APPLICATION_OCTET_STREAM:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mContentType:Lcom/box/androidsdk/content/requests/BoxRequest$ContentTypes;

    return-void
.end method


# virtual methods
.method protected createHeaderMap()V
    .locals 8

    .line 1439
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequest;->createHeaderMap()V

    .line 1440
    iget v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mPartNumber:I

    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;

    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getPartSize()I

    move-result v1

    mul-int v0, v0, v1

    int-to-long v0, v0

    .line 1443
    iget v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mCurrentChunkSize:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    .line 1444
    iget-object v4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mHeaderMap:Ljava/util/LinkedHashMap;

    const-string v5, "content-range"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bytes "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "-"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mFileSize:J

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1446
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mHeaderMap:Ljava/util/LinkedHashMap;

    const-string v1, "digest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sha="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;

    invoke-virtual {v3}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getFieldPartsSha1()Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mPartNumber:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getPartSize()J
    .locals 2

    .line 1506
    iget v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mCurrentChunkSize:I

    int-to-long v0, v0

    return-wide v0
.end method

.method protected setBody(Lcom/box/androidsdk/content/requests/BoxHttpRequest;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1452
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mInputStream:Ljava/io/InputStream;

    iget v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mPartNumber:I

    iget-object v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mUploadSession:Lcom/box/androidsdk/content/models/BoxUploadSession;

    invoke-virtual {v2}, Lcom/box/androidsdk/content/models/BoxUploadSession;->getPartSize()I

    move-result v2

    mul-int v1, v1, v2

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/io/InputStream;->skip(J)J

    .line 1455
    invoke-virtual {p1}, Lcom/box/androidsdk/content/requests/BoxHttpRequest;->getUrlConnection()Ljava/net/HttpURLConnection;

    move-result-object p1

    const/4 v0, 0x1

    .line 1456
    invoke-virtual {p1, v0}, Ljava/net/URLConnection;->setDoOutput(Z)V

    .line 1457
    invoke-virtual {p1}, Ljava/net/URLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object p1

    .line 1458
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mListener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    if-eqz v0, :cond_0

    .line 1459
    new-instance v0, Lcom/box/androidsdk/content/utils/ProgressOutputStream;

    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mListener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->getPartSize()J

    move-result-wide v2

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/box/androidsdk/content/utils/ProgressOutputStream;-><init>(Ljava/io/OutputStream;Lcom/box/androidsdk/content/listeners/ProgressListener;J)V

    move-object p1, v0

    :cond_0
    const/16 v0, 0x2000

    .line 1465
    :try_start_0
    iget-object v1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v1

    if-lez v1, :cond_3

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/16 v2, 0x2000

    .line 1467
    :goto_0
    iget v3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mCurrentChunkSize:I

    if-ge v0, v3, :cond_3

    iget-object v3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v3

    if-lez v3, :cond_3

    .line 1468
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_2

    add-int v3, v0, v2

    .line 1473
    iget v4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mCurrentChunkSize:I

    if-le v3, v4, :cond_1

    .line 1474
    iget v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mCurrentChunkSize:I

    sub-int/2addr v2, v0

    .line 1476
    :cond_1
    new-array v3, v2, [B

    .line 1477
    iget-object v4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mInputStream:Ljava/io/InputStream;

    invoke-virtual {v4, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .line 1478
    invoke-virtual {p1, v3, v1, v4}, Ljava/io/OutputStream;->write([BII)V

    add-int/2addr v0, v4

    goto :goto_0

    .line 1469
    :cond_2
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    .line 1470
    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1485
    :cond_3
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    return-void

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1483
    :try_start_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1485
    :goto_1
    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    throw v0
.end method

.method public setProgressListener(Lcom/box/androidsdk/content/listeners/ProgressListener;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;
    .locals 0

    .line 1497
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;->mListener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    return-object p0
.end method

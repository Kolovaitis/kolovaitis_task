.class Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$BoxMetadataUpdateTask;
.super Lcom/box/androidsdk/content/models/BoxJsonObject;
.source "BoxRequestsMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BoxMetadataUpdateTask"
.end annotation


# static fields
.field public static final OPERATION:Ljava/lang/String; = "op"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final VALUE:Ljava/lang/String; = "value"


# instance fields
.field final synthetic this$0:Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata;


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata;Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$Operations;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 198
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$BoxMetadataUpdateTask;->this$0:Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata;

    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxJsonObject;-><init>()V

    const-string p1, "op"

    .line 199
    invoke-virtual {p2}, Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$Operations;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$BoxMetadataUpdateTask;->set(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "path"

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p1, p3}, Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$BoxMetadataUpdateTask;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    sget-object p1, Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$Operations;->REMOVE:Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$Operations;

    if-eq p2, p1, :cond_0

    const-string p1, "value"

    .line 202
    invoke-virtual {p0, p1, p4}, Lcom/box/androidsdk/content/requests/BoxRequestsMetadata$UpdateItemMetadata$BoxMetadataUpdateTask;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

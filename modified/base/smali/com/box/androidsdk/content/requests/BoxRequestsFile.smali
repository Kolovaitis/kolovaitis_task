.class public Lcom/box/androidsdk/content/requests/BoxRequestsFile;
.super Ljava/lang/Object;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateNewVersionUploadSession;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetUploadSession;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$AbortUploadSession;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$ListUploadSessionParts;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFileFromCollection;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddFileToCollection;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadAvatar;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetCollaborations;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFileVersion;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileVersions;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddTaggedCommentToFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddCommentToFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileComments;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$RestoreTrashedFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteTrashedFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetTrashedFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$CopyFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetEmbedLinkFileInfo;,
        Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileInfo;
    }
.end annotation


# static fields
.field private static final ATTRIBUTES:Ljava/lang/String; = "attributes"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;
.super Lcom/box/androidsdk/content/requests/BoxRequestDownload;
.source "BoxRequestsFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadRepresentation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequestDownload<",
        "Lcom/box/androidsdk/content/models/BoxDownload;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;",
        ">;"
    }
.end annotation


# instance fields
.field protected mRepresentation:Lcom/box/androidsdk/content/models/BoxRepresentation;

.field protected mRequestPage:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/File;Lcom/box/androidsdk/content/models/BoxRepresentation;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 6

    .line 1210
    const-class v2, Lcom/box/androidsdk/content/models/BoxDownload;

    invoke-virtual {p3}, Lcom/box/androidsdk/content/models/BoxRepresentation;->getContent()Lcom/box/androidsdk/content/models/BoxRepresentation$BoxRepContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxRepresentation$BoxRepContent;->getUrl()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/box/androidsdk/content/requests/BoxRequestDownload;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/io/File;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 p1, 0x1

    .line 1207
    iput p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;->mRequestPage:I

    .line 1211
    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;->mRepresentation:Lcom/box/androidsdk/content/models/BoxRepresentation;

    return-void
.end method


# virtual methods
.method protected buildUrl()Ljava/net/URL;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    const-string v0, ""

    .line 1217
    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;->isPaged()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1219
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%d.%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;->mRequestPage:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;->mRepresentation:Lcom/box/androidsdk/content/models/BoxRepresentation;

    invoke-virtual {v4}, Lcom/box/androidsdk/content/models/BoxRepresentation;->getRepresentationType()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1221
    :cond_0
    new-instance v1, Ljava/net/URL;

    iget-object v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;->mRequestUrlString:Ljava/lang/String;

    const-string v3, "{+asset_path}"

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public isPaged()Z
    .locals 1

    .line 1230
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;->mRepresentation:Lcom/box/androidsdk/content/models/BoxRepresentation;

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxRepresentation;->getProperties()Lcom/box/androidsdk/content/models/BoxRepresentation$BoxRepPropertiesMap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxRepresentation$BoxRepPropertiesMap;->isPaged()Z

    move-result v0

    return v0
.end method

.method public setRequestedPage(I)V
    .locals 0

    .line 1239
    iput p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;->mRequestPage:I

    return-void
.end method

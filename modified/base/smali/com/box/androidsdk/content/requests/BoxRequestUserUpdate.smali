.class abstract Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;
.super Lcom/box/androidsdk/content/requests/BoxRequestItem;
.source "BoxRequestUserUpdate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/box/androidsdk/content/models/BoxUser;",
        "R:",
        "Lcom/box/androidsdk/content/requests/BoxRequest<",
        "TE;TR;>;>",
        "Lcom/box/androidsdk/content/requests/BoxRequestItem<",
        "TE;TR;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "TE;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/box/androidsdk/content/models/BoxSession;",
            ")V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/box/androidsdk/content/requests/BoxRequestItem;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-void
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "address"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getCanSeeManagedUsers()Z
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "can_see_managed_users"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getIsExemptFromDeviceLimits()Z
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "is_exempt_from_device_limits"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getIsExemptFromLoginVerification()Z
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "is_exempt_from_login_verification"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getIsSyncEnabled()Z
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "is_sync_enabled"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getJobTitle()Ljava/lang/String;
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "job_title"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRole()Lcom/box/androidsdk/content/models/BoxUser$Role;
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "role"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUser$Role;

    return-object v0
.end method

.method public getSpaceAmount()D
    .locals 2

    .line 144
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "space_amount"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getStatus()Lcom/box/androidsdk/content/models/BoxUser$Status;
    .locals 2

    .line 184
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUser$Status;

    return-object v0
.end method

.method public getTimezone()Ljava/lang/String;
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "timezone"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TR;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "address"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setCanSeeManagedUsers(Z)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TR;"
        }
    .end annotation

    .line 174
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "can_see_managed_users"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setIsExemptFromDeviceLimits(Z)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TR;"
        }
    .end annotation

    .line 234
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "is_exempt_from_device_limits"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setIsExemptFromLoginVerification(Z)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TR;"
        }
    .end annotation

    .line 254
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "is_exempt_from_login_verification"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setIsSyncEnabled(Z)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)TR;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "is_sync_enabled"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setJobTitle(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TR;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "job_title"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setName(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TR;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setPhone(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TR;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "phone"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setRole(Lcom/box/androidsdk/content/models/BoxUser$Role;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/box/androidsdk/content/models/BoxUser$Role;",
            ")TR;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "role"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setSpaceAmount(D)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(D)TR;"
        }
    .end annotation

    .line 154
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "space_amount"

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setStatus(Lcom/box/androidsdk/content/models/BoxUser$Status;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/box/androidsdk/content/models/BoxUser$Status;",
            ")TR;"
        }
    .end annotation

    .line 194
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setTimezone(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TR;"
        }
    .end annotation

    .line 214
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestUserUpdate;->mBodyMap:Ljava/util/LinkedHashMap;

    const-string v1, "timezone"

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

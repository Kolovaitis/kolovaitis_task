.class public Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;
.super Lcom/box/androidsdk/content/requests/BoxRequestItem;
.source "BoxRequestsFolder.java"

# interfaces
.implements Lcom/box/androidsdk/content/requests/BoxCacheableRequest;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/requests/BoxRequestsFolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GetFolderWithAllItems"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/box/androidsdk/content/requests/BoxRequestItem<",
        "Lcom/box/androidsdk/content/models/BoxFolder;",
        "Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;",
        ">;",
        "Lcom/box/androidsdk/content/requests/BoxCacheableRequest<",
        "Lcom/box/androidsdk/content/models/BoxFolder;",
        ">;"
    }
.end annotation


# static fields
.field public static DEFAULT_MAX_LIMIT:I = 0xfa0

.field private static LIMIT:I = 0x64

.field private static final serialVersionUID:J = -0x20a3b3625bcc50cL


# instance fields
.field private mFolderId:Ljava/lang/String;

.field private mItemsUrl:Ljava/lang/String;

.field private mMaxLimit:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 1

    .line 660
    const-class v0, Lcom/box/androidsdk/content/models/BoxFolder;

    invoke-direct {p0, v0, p1, p2, p4}, Lcom/box/androidsdk/content/requests/BoxRequestItem;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 p2, -0x1

    .line 656
    iput p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mMaxLimit:I

    .line 661
    sget-object p2, Lcom/box/androidsdk/content/requests/BoxRequest$Methods;->GET:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    iput-object p2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mRequestMethod:Lcom/box/androidsdk/content/requests/BoxRequest$Methods;

    .line 662
    iput-object p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mFolderId:Ljava/lang/String;

    .line 663
    iput-object p3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mItemsUrl:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getIfNoneMatchEtag()Ljava/lang/String;
    .locals 1

    .line 729
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->getIfNoneMatchEtag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onSend()Lcom/box/androidsdk/content/models/BoxFolder;
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 668
    iget-object v0, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mQueryMap:Ljava/util/HashMap;

    sget-object v1, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->QUERY_FIELDS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 669
    new-instance v1, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems$1;

    iget-object v2, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mFolderId:Ljava/lang/String;

    iget-object v3, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mRequestUrlString:Ljava/lang/String;

    iget-object v4, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems$1;-><init>(Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 674
    invoke-virtual {v1, v3}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems$1;->setFields([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object v1

    check-cast v1, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderInfo;

    sget v3, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->LIMIT:I

    invoke-virtual {v1, v3}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderInfo;->setLimit(I)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderInfo;

    move-result-object v1

    .line 675
    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->getIfNoneMatchEtag()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/box/androidsdk/content/utils/SdkUtils;->isBlank(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 676
    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->getIfNoneMatchEtag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderInfo;->setIfNoneMatchEtag(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderInfo;

    .line 678
    :cond_0
    invoke-virtual {v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderInfo;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v1

    check-cast v1, Lcom/box/androidsdk/content/models/BoxFolder;

    .line 679
    new-instance v3, Lcom/box/androidsdk/content/requests/BoxRequestBatch;

    invoke-direct {v3}, Lcom/box/androidsdk/content/requests/BoxRequestBatch;-><init>()V

    const-wide/16 v5, 0xe10

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const/16 v8, 0xa

    invoke-static {v8, v8, v5, v6, v7}, Lcom/box/androidsdk/content/utils/SdkUtils;->createDefaultThreadPoolExecutor(IIJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ThreadPoolExecutor;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->setExecutor(Ljava/util/concurrent/ExecutorService;)Lcom/box/androidsdk/content/requests/BoxRequestBatch;

    move-result-object v3

    .line 680
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxFolder;->getItemCollection()Lcom/box/androidsdk/content/models/BoxIteratorItems;

    move-result-object v5

    .line 681
    invoke-virtual {v5}, Lcom/box/androidsdk/content/models/BoxIteratorItems;->offset()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->intValue()I

    move-result v6

    .line 682
    invoke-virtual {v5}, Lcom/box/androidsdk/content/models/BoxIteratorItems;->limit()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->intValue()I

    move-result v7

    .line 683
    iget v8, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mMaxLimit:I

    if-lez v8, :cond_1

    int-to-long v8, v8

    invoke-virtual {v5}, Lcom/box/androidsdk/content/models/BoxIteratorItems;->fullSize()Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v12, v8, v10

    if-gez v12, :cond_1

    iget v5, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mMaxLimit:I

    int-to-long v8, v5

    goto :goto_0

    :cond_1
    invoke-virtual {v5}, Lcom/box/androidsdk/content/models/BoxIteratorItems;->fullSize()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    :goto_0
    add-int/2addr v6, v7

    int-to-long v10, v6

    cmp-long v5, v10, v8

    if-gez v5, :cond_2

    .line 686
    sget v7, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->LIMIT:I

    .line 688
    new-instance v5, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems$2;

    iget-object v10, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mFolderId:Ljava/lang/String;

    iget-object v11, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mItemsUrl:Ljava/lang/String;

    iget-object v12, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v5, p0, v10, v11, v12}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems$2;-><init>(Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    new-array v10, v2, [Ljava/lang/String;

    aput-object v0, v10, v4

    .line 693
    invoke-virtual {v5, v10}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems$2;->setFields([Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object v5

    check-cast v5, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;

    .line 694
    invoke-virtual {v5, v6}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;->setOffset(I)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;

    move-result-object v5

    .line 695
    invoke-virtual {v5, v7}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;->setLimit(I)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderItems;

    move-result-object v5

    .line 696
    invoke-virtual {v3, v5}, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->addRequest(Lcom/box/androidsdk/content/requests/BoxRequest;)Lcom/box/androidsdk/content/requests/BoxRequestBatch;

    goto :goto_0

    .line 699
    :cond_2
    invoke-virtual {v3}, Lcom/box/androidsdk/content/requests/BoxRequestBatch;->send()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/requests/BoxResponseBatch;

    .line 700
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxFolder;->toJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v1

    const-string v2, "item_collection"

    .line 701
    invoke-virtual {v1, v2}, Lcom/eclipsesource/json/JsonObject;->get(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/eclipsesource/json/JsonValue;->asObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v2

    const-string v3, "entries"

    .line 702
    invoke-virtual {v2, v3}, Lcom/eclipsesource/json/JsonObject;->get(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/eclipsesource/json/JsonValue;->asArray()Lcom/eclipsesource/json/JsonArray;

    move-result-object v2

    .line 703
    invoke-virtual {v0}, Lcom/box/androidsdk/content/requests/BoxResponseBatch;->getResponses()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/box/androidsdk/content/requests/BoxResponse;

    .line 704
    invoke-virtual {v3}, Lcom/box/androidsdk/content/requests/BoxResponse;->isSuccess()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 705
    invoke-virtual {v3}, Lcom/box/androidsdk/content/requests/BoxResponse;->getResult()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v3

    check-cast v3, Lcom/box/androidsdk/content/models/BoxIteratorItems;

    invoke-virtual {v3}, Lcom/box/androidsdk/content/models/BoxIteratorItems;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/box/androidsdk/content/models/BoxItem;

    .line 706
    invoke-virtual {v4}, Lcom/box/androidsdk/content/models/BoxItem;->toJsonObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/eclipsesource/json/JsonArray;->add(Lcom/eclipsesource/json/JsonValue;)Lcom/eclipsesource/json/JsonArray;

    goto :goto_1

    .line 709
    :cond_4
    invoke-virtual {v3}, Lcom/box/androidsdk/content/requests/BoxResponse;->getException()Ljava/lang/Exception;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/BoxException;

    throw v0

    .line 713
    :cond_5
    new-instance v0, Lcom/box/androidsdk/content/models/BoxFolder;

    invoke-direct {v0, v1}, Lcom/box/androidsdk/content/models/BoxFolder;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-object v0
.end method

.method public bridge synthetic onSend()Lcom/box/androidsdk/content/models/BoxObject;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 651
    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->onSend()Lcom/box/androidsdk/content/models/BoxFolder;

    move-result-object v0

    return-object v0
.end method

.method protected onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/box/androidsdk/content/requests/BoxResponse<",
            "Lcom/box/androidsdk/content/models/BoxFolder;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 744
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->onSendCompleted(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    .line 745
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->handleUpdateCache(Lcom/box/androidsdk/content/requests/BoxResponse;)V

    return-void
.end method

.method public sendForCachedResult()Lcom/box/androidsdk/content/models/BoxFolder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 734
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->handleSendForCachedResult()Lcom/box/androidsdk/content/models/BoxObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxFolder;

    return-object v0
.end method

.method public bridge synthetic sendForCachedResult()Lcom/box/androidsdk/content/models/BoxObject;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 651
    invoke-virtual {p0}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->sendForCachedResult()Lcom/box/androidsdk/content/models/BoxFolder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic setIfNoneMatchEtag(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;
    .locals 0

    .line 651
    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->setIfNoneMatchEtag(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;

    move-result-object p1

    return-object p1
.end method

.method public setIfNoneMatchEtag(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;
    .locals 0

    .line 724
    invoke-super {p0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->setIfNoneMatchEtag(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;

    return-object p1
.end method

.method public setMaximumLimit(I)Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;
    .locals 0

    .line 718
    iput p1, p0, Lcom/box/androidsdk/content/requests/BoxRequestsFolder$GetFolderWithAllItems;->mMaxLimit:I

    return-object p0
.end method

.method public toTaskForCachedResult()Lcom/box/androidsdk/content/BoxFutureTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/box/androidsdk/content/BoxFutureTask<",
            "Lcom/box/androidsdk/content/models/BoxFolder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/box/androidsdk/content/BoxException;
        }
    .end annotation

    .line 739
    invoke-super {p0}, Lcom/box/androidsdk/content/requests/BoxRequestItem;->handleToTaskForCachedResult()Lcom/box/androidsdk/content/BoxFutureTask;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/box/androidsdk/content/BoxApiBookmark;
.super Lcom/box/androidsdk/content/BoxApi;
.source "BoxApiBookmark.java"


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/BoxApi;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    return-void
.end method


# virtual methods
.method public getAddCommentRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$AddCommentToBookmark;
    .locals 3

    .line 182
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$AddCommentToBookmark;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiBookmark;->getCommentUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$AddCommentToBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getAddToCollectionRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$AddBookmarkToCollection;
    .locals 3

    .line 238
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$AddBookmarkToCollection;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$AddBookmarkToCollection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getBookmarkCommentsUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/comments"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getBookmarkCopyUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/copy"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "%s/%s"

    const/4 v1, 0x2

    .line 34
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarksUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getBookmarksUrl()Ljava/lang/String;
    .locals 4

    const-string v0, "%s/web_links"

    const/4 v1, 0x1

    .line 26
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBaseUri()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getCommentUrl()Ljava/lang/String;
    .locals 2

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBaseUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/comments"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCommentsRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetBookmarkComments;
    .locals 3

    .line 226
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetBookmarkComments;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkCommentsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetBookmarkComments;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getCopyRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CopyBookmark;
    .locals 3

    .line 109
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CopyBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkCopyUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CopyBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getCreateRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;
    .locals 3

    .line 86
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarksUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$CreateBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getCreateSharedLinkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateSharedBookmark;
    .locals 3

    .line 157
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateSharedBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateSharedBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 p1, 0x0

    .line 158
    invoke-virtual {v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateSharedBookmark;->setAccess(Lcom/box/androidsdk/content/models/BoxSharedLink$Access;)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateSharedBookmark;

    return-object p1
.end method

.method public getDeleteFromCollectionRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteBookmarkFromCollection;
    .locals 3

    .line 249
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteBookmarkFromCollection;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteBookmarkFromCollection;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDeleteRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteBookmark;
    .locals 3

    .line 146
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDeleteTrashedBookmarkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteTrashedBookmark;
    .locals 3

    .line 204
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteTrashedBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getTrashedBookmarkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$DeleteTrashedBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDisableSharedLinkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;
    .locals 3

    .line 169
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 p1, 0x0

    .line 170
    invoke-virtual {v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;->setSharedLink(Lcom/box/androidsdk/content/models/BoxSharedLink;)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;

    return-object p1
.end method

.method public getInfoRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetBookmarkInfo;
    .locals 3

    .line 74
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetBookmarkInfo;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetBookmarkInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getMoveRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;
    .locals 3

    .line 134
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 135
    invoke-virtual {v0, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;->setParentId(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;

    return-object v0
.end method

.method public getRenameRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;
    .locals 3

    .line 121
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 122
    invoke-virtual {v0, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;->setName(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;

    return-object v0
.end method

.method public getRestoreTrashedBookmarkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$RestoreTrashedBookmark;
    .locals 3

    .line 215
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$RestoreTrashedBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$RestoreTrashedBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getTrashedBookmarkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetTrashedBookmark;
    .locals 3

    .line 193
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetTrashedBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getTrashedBookmarkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$GetTrashedBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getTrashedBookmarkUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/trash"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUpdateRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;
    .locals 3

    .line 97
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiBookmark;->getBookmarkInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiBookmark;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsBookmark$UpdateBookmark;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

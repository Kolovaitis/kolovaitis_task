.class public Lcom/box/androidsdk/content/views/BoxAvatarView;
.super Landroid/widget/LinearLayout;
.source "BoxAvatarView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;
    }
.end annotation


# static fields
.field private static final DEFAULT_NAME:Ljava/lang/String; = ""

.field private static final EXTRA_AVATAR_CONTROLLER:Ljava/lang/String; = "extraAvatarController"

.field private static final EXTRA_PARENT:Ljava/lang/String; = "extraParent"

.field private static final EXTRA_USER:Ljava/lang/String; = "extraUser"


# instance fields
.field private mAvatar:Landroid/widget/ImageView;

.field private mAvatarController:Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

.field private mAvatarDownloadTaskRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/box/androidsdk/content/BoxFutureTask<",
            "Lcom/box/androidsdk/content/models/BoxDownload;",
            ">;>;"
        }
    .end annotation
.end field

.field private mInitials:Landroid/widget/TextView;

.field private mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, v0}, Lcom/box/androidsdk/content/views/BoxAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0, p1, p2, v0}, Lcom/box/androidsdk/content/views/BoxAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 61
    sget p2, Lcom/box/sdk/android/R$layout;->boxsdk_avatar_item:I

    const/4 p3, 0x1

    invoke-virtual {p1, p2, p0, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 62
    sget p2, Lcom/box/sdk/android/R$id;->box_avatar_initials:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mInitials:Landroid/widget/TextView;

    .line 63
    sget p2, Lcom/box/sdk/android/R$id;->box_avatar_image:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatar:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public loadUser(Lcom/box/androidsdk/content/models/BoxCollaborator;Ljava/io/Serializable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ":",
            "Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;",
            ">(",
            "Lcom/box/androidsdk/content/models/BoxCollaborator;",
            "TT;)V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 70
    check-cast p2, Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

    iput-object p2, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarController:Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

    .line 72
    :cond_0
    iget-object p2, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p2}, Lcom/box/androidsdk/content/models/BoxCollaborator;->getId()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Lcom/box/androidsdk/content/models/BoxCollaborator;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_1

    return-void

    .line 76
    :cond_1
    iput-object p1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    .line 77
    iget-object p1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarDownloadTaskRef:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 79
    :try_start_0
    iget-object p1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarDownloadTaskRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/BoxFutureTask;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/box/androidsdk/content/BoxFutureTask;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :catch_0
    :cond_2
    invoke-virtual {p0}, Lcom/box/androidsdk/content/views/BoxAvatarView;->updateAvatar()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 146
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 147
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "extraAvatarController"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

    iput-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarController:Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

    const-string v0, "extraUser"

    .line 148
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUser;

    iput-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    const-string v0, "extraParent"

    .line 149
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 150
    iget-object p1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    if-eqz p1, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/box/androidsdk/content/views/BoxAvatarView;->updateAvatar()V

    :cond_0
    return-void

    .line 155
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 137
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "extraAvatarController"

    .line 138
    iget-object v2, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarController:Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "extraUser"

    .line 139
    iget-object v2, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string v1, "extraParent"

    .line 140
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method protected updateAvatar()V
    .locals 5

    .line 90
    iget-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarController:Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

    if-nez v0, :cond_0

    goto/16 :goto_4

    .line 93
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 94
    new-instance v0, Lcom/box/androidsdk/content/views/BoxAvatarView$1;

    invoke-direct {v0, p0}, Lcom/box/androidsdk/content/views/BoxAvatarView$1;-><init>(Lcom/box/androidsdk/content/views/BoxAvatarView;)V

    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/views/BoxAvatarView;->post(Ljava/lang/Runnable;)Z

    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarController:Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

    iget-object v1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxCollaborator;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;->getAvatarFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    if-eqz v1, :cond_2

    .line 107
    iget-object v1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    iget-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatar:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mInitials:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_2
    const-string v0, ""

    .line 113
    iget-object v1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    instance-of v4, v1, Lcom/box/androidsdk/content/models/BoxCollaborator;

    if-eqz v4, :cond_3

    .line 114
    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxCollaborator;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 115
    :cond_3
    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->isBlank(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    instance-of v4, v1, Lcom/box/androidsdk/content/models/BoxUser;

    if-eqz v4, :cond_4

    .line 116
    check-cast v1, Lcom/box/androidsdk/content/models/BoxUser;

    invoke-virtual {v1}, Lcom/box/androidsdk/content/models/BoxUser;->getLogin()Ljava/lang/String;

    move-result-object v0

    .line 120
    :cond_4
    :goto_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_5

    .line 125
    invoke-virtual {p0}, Lcom/box/androidsdk/content/views/BoxAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mInitials:Landroid/widget/TextView;

    invoke-static {v1, v4, v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->setInitialsThumb(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_2

    .line 127
    :cond_5
    invoke-virtual {p0}, Lcom/box/androidsdk/content/views/BoxAvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v4, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mInitials:Landroid/widget/TextView;

    invoke-static {v0, v4, v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->setCollabNumberThumb(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 129
    :goto_2
    iget-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatar:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mInitials:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarController:Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;

    iget-object v2, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mUser:Lcom/box/androidsdk/content/models/BoxCollaborator;

    invoke-virtual {v2}, Lcom/box/androidsdk/content/models/BoxCollaborator;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p0}, Lcom/box/androidsdk/content/views/BoxAvatarView$AvatarController;->executeAvatarDownloadRequest(Ljava/lang/String;Lcom/box/androidsdk/content/views/BoxAvatarView;)Lcom/box/androidsdk/content/BoxFutureTask;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/box/androidsdk/content/views/BoxAvatarView;->mAvatarDownloadTaskRef:Ljava/lang/ref/WeakReference;

    :goto_3
    return-void

    :cond_6
    :goto_4
    return-void
.end method

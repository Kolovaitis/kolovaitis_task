.class public Lcom/box/androidsdk/content/BoxConstants;
.super Ljava/lang/Object;
.source "BoxConstants.java"


# static fields
.field public static final BASE_UPLOAD_URI:Ljava/lang/String; = "https://upload.box.com/api/2.0"

.field public static final BASE_UPLOAD_URI_TEMPLATE:Ljava/lang/String; = "https://upload.%s/api/2.0"

.field public static final BASE_URI:Ljava/lang/String; = "https://api.box.com/2.0"

.field public static final BASE_URI_TEMPLATE:Ljava/lang/String; = "https://api.%s/2.0"

.field public static final FIELD_COMMENT_COUNT:Ljava/lang/String; = "comment_count"

.field public static final FIELD_CONTENT_CREATED_AT:Ljava/lang/String; = "content_created_at"

.field public static final FIELD_CONTENT_MODIFIED_AT:Ljava/lang/String; = "content_modified_at"

.field public static final FIELD_SIZE:Ljava/lang/String; = "size"

.field public static final HTTP_STATUS_TOO_MANY_REQUESTS:I = 0x1ad

.field public static final KEY_BOX_DEVICE_ID:Ljava/lang/String; = "box_device_id"

.field public static final KEY_BOX_DEVICE_NAME:Ljava/lang/String; = "box_device_name"

.field public static final KEY_BOX_REFRESH_TOKEN_EXPIRES_AT:Ljava/lang/String; = "box_refresh_token_expires_at"

.field public static final KEY_BOX_USERS:Ljava/lang/String; = "boxusers"

.field public static final KEY_CLIENT_ID:Ljava/lang/String; = "client_id"

.field public static final KEY_CLIENT_SECRET:Ljava/lang/String; = "client_secret"

.field public static final KEY_REDIRECT_URL:Ljava/lang/String; = "redirect_uri"

.field public static final KEY_TOKEN:Ljava/lang/String; = "token"

.field public static final OAUTH_BASE_URI:Ljava/lang/String; = "https://api.box.com"

.field public static final OAUTH_BASE_URI_TEMPLATE:Ljava/lang/String; = "https://api.%s"

.field public static final REQUEST_BOX_APP_FOR_AUTH_INTENT_ACTION:Ljava/lang/String; = "com.box.android.action.AUTHENTICATE_VIA_BOX_APP"

.field public static final ROOT_FOLDER_ID:Ljava/lang/String; = "0"

.field public static final TAG:Ljava/lang/String; = "BoxContentSdk"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

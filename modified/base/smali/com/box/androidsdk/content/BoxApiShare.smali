.class public Lcom/box/androidsdk/content/BoxApiShare;
.super Lcom/box/androidsdk/content/BoxApi;
.source "BoxApiShare.java"


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/BoxApi;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    return-void
.end method


# virtual methods
.method protected getSharedItemsUrl()Ljava/lang/String;
    .locals 4

    const-string v0, "%s/shared_items"

    const/4 v1, 0x1

    .line 15
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiShare;->getBaseUri()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSharedLinkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsShare$GetSharedLink;
    .locals 1

    const/4 v0, 0x0

    .line 33
    invoke-virtual {p0, p1, v0}, Lcom/box/androidsdk/content/BoxApiShare;->getSharedLinkRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsShare$GetSharedLink;

    move-result-object p1

    return-object p1
.end method

.method public getSharedLinkRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsShare$GetSharedLink;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/box/androidsdk/content/BoxApiShare;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    instance-of v0, v0, Lcom/box/androidsdk/content/models/BoxSharedLinkSession;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/box/androidsdk/content/BoxApiShare;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSharedLinkSession;

    goto :goto_0

    .line 48
    :cond_0
    new-instance v0, Lcom/box/androidsdk/content/models/BoxSharedLinkSession;

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiShare;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, v1}, Lcom/box/androidsdk/content/models/BoxSharedLinkSession;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 51
    :goto_0
    invoke-virtual {v0, p1}, Lcom/box/androidsdk/content/models/BoxSharedLinkSession;->setSharedLink(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxSharedLinkSession;

    .line 52
    invoke-virtual {v0, p2}, Lcom/box/androidsdk/content/models/BoxSharedLinkSession;->setPassword(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxSharedLinkSession;

    .line 53
    new-instance p1, Lcom/box/androidsdk/content/requests/BoxRequestsShare$GetSharedLink;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiShare;->getSharedItemsUrl()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsShare$GetSharedLink;-><init>(Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSharedLinkSession;)V

    return-object p1
.end method

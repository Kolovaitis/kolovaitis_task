.class public Lcom/box/androidsdk/content/utils/SdkUtils;
.super Ljava/lang/Object;
.source "SdkUtils.java"


# static fields
.field public static final BUFFER_SIZE:I = 0x2000

.field public static final COLLAB_NUMBER_THUMB_COLOR:I = -0xe66d22

.field private static final HEX_CHARS:[C

.field private static LAST_TOAST_TIME:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static SIZE_BYTES:Ljava/lang/String; = null

.field private static SIZE_GIGABYTES:Ljava/lang/String; = null

.field private static SIZE_KILOBYTES:Ljava/lang/String; = null

.field private static SIZE_LANGUAGE:Ljava/lang/String; = null

.field private static SIZE_MEGABYTES:Ljava/lang/String; = null

.field private static SIZE_TERABYTES:Ljava/lang/String; = null

.field protected static final THUMB_COLORS:[I

.field public static TOAST_MIN_REPEAT_DELAY:J = 0x0L

.field private static final constGB:D = 1.073741824E9

.field private static final constKB:I = 0x400

.field private static final constMB:I = 0x100000

.field private static final constTB:D = 1.099511627776E12

.field private static final floatGB:D = 1.073741824E9

.field private static final floatKB:D = 1024.0

.field private static final floatMB:D = 1048576.0

.field private static final floatTB:D = 1.099511627776E12


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v0, 0xf

    .line 47
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->THUMB_COLORS:[I

    const-string v0, "0123456789abcdef"

    .line 231
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->HEX_CHARS:[C

    .line 451
    new-instance v0, Lcom/box/androidsdk/content/utils/SdkUtils$3;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Lcom/box/androidsdk/content/utils/SdkUtils$3;-><init>(I)V

    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->LAST_TOAST_TIME:Ljava/util/HashMap;

    const-wide/16 v0, 0xbb8

    .line 473
    sput-wide v0, Lcom/box/androidsdk/content/utils/SdkUtils;->TOAST_MIN_REPEAT_DELAY:J

    const-string v0, "%4.0f B"

    .line 672
    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_BYTES:Ljava/lang/String;

    const-string v0, "%4.1f KB"

    .line 673
    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_KILOBYTES:Ljava/lang/String;

    const-string v0, "%4.1f MB"

    .line 674
    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_MEGABYTES:Ljava/lang/String;

    const-string v0, "%4.1f GB"

    .line 675
    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_GIGABYTES:Ljava/lang/String;

    const-string v0, "%4.1f TB"

    .line 676
    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_TERABYTES:Ljava/lang/String;

    const-string v0, ""

    .line 678
    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_LANGUAGE:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        -0x3de7a5
        -0x12c8a9
        -0x19464
        -0xa616c
        -0x86a00
        -0xa4ce5
        -0x4839e1
        -0xd93d7f
        -0xea5d55
        -0xab3b11
        -0xee5b01
        -0x907801
        -0xc0ae2d
        -0x98c549
        -0x54b844
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .locals 1

    .line 44
    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->LAST_TOAST_TIME:Ljava/util/HashMap;

    return-object v0
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 3

    .line 651
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 652
    iget p0, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/4 v1, 0x1

    if-gt v0, p2, :cond_0

    if-le p0, p1, :cond_1

    .line 656
    :cond_0
    div-int/lit8 v0, v0, 0x2

    .line 657
    div-int/lit8 p0, p0, 0x2

    .line 661
    :goto_0
    div-int v2, v0, v1

    if-lt v2, p2, :cond_1

    div-int v2, p0, v1

    if-lt v2, p1, :cond_1

    mul-int/lit8 v1, v1, 0x2

    goto :goto_0

    :cond_1
    return v1
.end method

.method public static cloneSerializable(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)TT;"
        }
    .end annotation

    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    .line 322
    :try_start_0
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 323
    :try_start_1
    new-instance v7, Ljava/io/ObjectOutputStream;

    invoke-direct {v7, v6}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 324
    :try_start_2
    invoke-virtual {v7, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 326
    new-instance p0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-direct {p0, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 327
    :try_start_3
    new-instance v8, Ljava/io/ObjectInputStream;

    invoke-direct {v8, p0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 328
    :try_start_4
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 334
    new-array v4, v4, [Ljava/io/Closeable;

    aput-object v6, v4, v3

    aput-object v7, v4, v2

    aput-object p0, v4, v1

    aput-object v8, v4, v0

    :goto_0
    invoke-static {v4}, Lcom/box/androidsdk/content/utils/SdkUtils;->closeQuietly([Ljava/io/Closeable;)V

    return-object v5

    :catchall_0
    move-exception v5

    move-object v9, v5

    move-object v5, p0

    move-object p0, v9

    goto :goto_2

    :catchall_1
    move-exception v8

    move-object v9, v5

    move-object v5, p0

    move-object p0, v8

    move-object v8, v9

    goto :goto_2

    :catch_0
    move-object v8, v5

    goto :goto_4

    :catch_1
    move-object v8, v5

    goto :goto_6

    :catchall_2
    move-exception p0

    move-object v8, v5

    goto :goto_2

    :catch_2
    move-object p0, v5

    move-object v8, p0

    goto :goto_4

    :catch_3
    move-object p0, v5

    move-object v8, p0

    goto :goto_6

    :catchall_3
    move-exception p0

    move-object v7, v5

    goto :goto_1

    :catch_4
    move-object p0, v5

    move-object v7, p0

    goto :goto_3

    :catch_5
    move-object p0, v5

    move-object v7, p0

    goto :goto_5

    :catchall_4
    move-exception p0

    move-object v6, v5

    move-object v7, v6

    :goto_1
    move-object v8, v7

    :goto_2
    new-array v4, v4, [Ljava/io/Closeable;

    aput-object v6, v4, v3

    aput-object v7, v4, v2

    aput-object v5, v4, v1

    aput-object v8, v4, v0

    invoke-static {v4}, Lcom/box/androidsdk/content/utils/SdkUtils;->closeQuietly([Ljava/io/Closeable;)V

    throw p0

    :catch_6
    move-object p0, v5

    move-object v6, p0

    move-object v7, v6

    :goto_3
    move-object v8, v7

    :catch_7
    :goto_4
    new-array v4, v4, [Ljava/io/Closeable;

    aput-object v6, v4, v3

    aput-object v7, v4, v2

    aput-object p0, v4, v1

    aput-object v8, v4, v0

    goto :goto_0

    :catch_8
    move-object p0, v5

    move-object v6, p0

    move-object v7, v6

    :goto_5
    move-object v8, v7

    :catch_9
    :goto_6
    new-array v4, v4, [Ljava/io/Closeable;

    aput-object v6, v4, v3

    aput-object v7, v4, v2

    aput-object p0, v4, v1

    aput-object v8, v4, v0

    goto :goto_0
.end method

.method public static varargs closeQuietly([Ljava/io/Closeable;)V
    .locals 3

    .line 365
    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p0, v1

    .line 367
    :try_start_0
    invoke-interface {v2}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static concatStringWithDelimiter([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 279
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 280
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v3, v1, -0x1

    if-ge v2, v3, :cond_0

    .line 282
    aget-object v3, p0, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 284
    :cond_0
    aget-object p0, p0, v3

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 285
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static convertSerializableToString(Ljava/io/Serializable;)Ljava/lang/String;
    .locals 7

    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 347
    :try_start_0
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 348
    :try_start_1
    new-instance v5, Ljava/io/ObjectOutputStream;

    invoke-direct {v5, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 349
    :try_start_2
    invoke-virtual {v5, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 351
    new-instance p0, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {p0, v6}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 355
    new-array v0, v0, [Ljava/io/Closeable;

    aput-object v4, v0, v3

    aput-object v5, v0, v2

    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->closeQuietly([Ljava/io/Closeable;)V

    .line 356
    new-array v0, v2, [Ljava/io/Closeable;

    aput-object v5, v0, v3

    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->closeQuietly([Ljava/io/Closeable;)V

    return-object p0

    :catchall_0
    move-exception p0

    move-object v1, v5

    goto :goto_0

    :catchall_1
    move-exception p0

    goto :goto_0

    :catch_0
    move-object v5, v1

    goto :goto_1

    :catchall_2
    move-exception p0

    move-object v4, v1

    .line 355
    :goto_0
    new-array v0, v0, [Ljava/io/Closeable;

    aput-object v4, v0, v3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->closeQuietly([Ljava/io/Closeable;)V

    .line 356
    new-array v0, v2, [Ljava/io/Closeable;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->closeQuietly([Ljava/io/Closeable;)V

    throw p0

    :catch_1
    move-object v4, v1

    move-object v5, v4

    .line 355
    :catch_2
    :goto_1
    new-array p0, v0, [Ljava/io/Closeable;

    aput-object v4, p0, v3

    aput-object v5, p0, v2

    invoke-static {p0}, Lcom/box/androidsdk/content/utils/SdkUtils;->closeQuietly([Ljava/io/Closeable;)V

    .line 356
    new-array p0, v2, [Ljava/io/Closeable;

    aput-object v5, p0, v3

    invoke-static {p0}, Lcom/box/androidsdk/content/utils/SdkUtils;->closeQuietly([Ljava/io/Closeable;)V

    return-object v1
.end method

.method public static copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/4 v0, 0x0

    .line 73
    invoke-static {p0, p1, v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    return-void
.end method

.method private static copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/security/MessageDigest;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const/16 v0, 0x2000

    .line 103
    new-array v0, v0, [B

    :cond_0
    :goto_0
    const/4 v1, 0x0

    .line 107
    :try_start_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_2

    .line 108
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x0

    .line 112
    invoke-virtual {p1, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    if-eqz p2, :cond_0

    .line 114
    invoke-virtual {p2, v0, v3, v2}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_0

    .line 109
    :cond_1
    new-instance p0, Ljava/lang/InterruptedException;

    invoke-direct {p0}, Ljava/lang/InterruptedException;-><init>()V

    .line 110
    throw p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_2
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    goto :goto_1

    :catchall_0
    move-exception p0

    goto :goto_2

    :catch_0
    move-exception p0

    move-object v1, p0

    .line 120
    :try_start_1
    nop

    instance-of p0, v1, Ljava/io/IOException;

    if-nez p0, :cond_4

    .line 123
    instance-of p0, v1, Ljava/lang/InterruptedException;

    if-nez p0, :cond_3

    :goto_1
    return-void

    .line 124
    :cond_3
    move-object p0, v1

    check-cast p0, Ljava/lang/InterruptedException;

    throw p0

    .line 121
    :cond_4
    move-object p0, v1

    check-cast p0, Ljava/io/IOException;

    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    if-nez v1, :cond_5

    .line 129
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    :cond_5
    throw p0
.end method

.method public static copyStreamAndComputeSha1(Ljava/io/InputStream;Ljava/io/OutputStream;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    const-string v0, "SHA-1"

    .line 86
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 87
    invoke-static {p0, p1, v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    .line 88
    new-instance p0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object p1

    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->encodeHex([B)[C

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/String;-><init>([C)V

    return-object p0
.end method

.method public static createArrayOutputStream([Ljava/io/OutputStream;)Ljava/io/OutputStream;
    .locals 1

    .line 139
    new-instance v0, Lcom/box/androidsdk/content/utils/SdkUtils$1;

    invoke-direct {v0, p0}, Lcom/box/androidsdk/content/utils/SdkUtils$1;-><init>([Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public static createDefaultThreadPoolExecutor(IIJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ThreadPoolExecutor;
    .locals 9

    .line 300
    new-instance v8, Lcom/box/androidsdk/content/utils/StringMappedThreadPoolExecutor;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v7, Lcom/box/androidsdk/content/utils/SdkUtils$2;

    invoke-direct {v7}, Lcom/box/androidsdk/content/utils/SdkUtils$2;-><init>()V

    move-object v0, v8

    move v1, p0

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/box/androidsdk/content/utils/StringMappedThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    return-object v8
.end method

.method public static decodeSampledBitmapFromFile(Landroid/content/res/Resources;III)Landroid/graphics/Bitmap;
    .locals 2

    .line 604
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 605
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 606
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 609
    invoke-static {v0, p2, p3}, Lcom/box/androidsdk/content/utils/SdkUtils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result p2

    iput p2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 p2, 0x0

    .line 612
    iput-boolean p2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 613
    invoke-static {p0, p1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static decodeSampledBitmapFromFile(Ljava/io/File;II)Landroid/graphics/Bitmap;
    .locals 2

    .line 628
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v1, 0x1

    .line 629
    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 630
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 633
    invoke-static {v0, p1, p2}, Lcom/box/androidsdk/content/utils/SdkUtils;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result p1

    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 p1, 0x0

    .line 636
    iput-boolean p1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 637
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

.method public static deleteFolderRecursive(Ljava/io/File;)Z
    .locals 4

    .line 382
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 387
    :cond_0
    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 388
    invoke-static {v3}, Lcom/box/androidsdk/content/utils/SdkUtils;->deleteFolderRecursive(Ljava/io/File;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 391
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result p0

    return p0
.end method

.method private static encodeHex([B)[C
    .locals 7

    .line 233
    array-length v0, p0

    shl-int/lit8 v1, v0, 0x1

    .line 234
    new-array v1, v1, [C

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    add-int/lit8 v4, v3, 0x1

    .line 237
    sget-object v5, Lcom/box/androidsdk/content/utils/SdkUtils;->HEX_CHARS:[C

    aget-byte v6, p0, v2

    and-int/lit16 v6, v6, 0xf0

    ushr-int/lit8 v6, v6, 0x4

    aget-char v6, v5, v6

    aput-char v6, v1, v3

    add-int/lit8 v3, v4, 0x1

    .line 238
    aget-byte v6, p0, v2

    and-int/lit8 v6, v6, 0xf

    aget-char v5, v5, v6

    aput-char v5, v1, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static generateStateToken()Ljava/lang/String;
    .locals 1

    .line 59
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAsStringSafely(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 191
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static getAssetFile(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 416
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    const/4 v0, 0x0

    .line 419
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 420
    invoke-virtual {p0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p0

    .line 421
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    invoke-direct {v3, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p0, 0x1

    .line 425
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz p0, :cond_0

    const/4 p0, 0x0

    goto :goto_1

    :cond_0
    const/16 v4, 0xa

    .line 429
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 430
    :goto_1
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 432
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 438
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "getAssetFile"

    .line 441
    invoke-static {v1, p1, v0}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    return-object p0

    :catch_1
    move-exception p0

    goto :goto_3

    :catchall_0
    move-exception p0

    goto :goto_5

    :catch_2
    move-exception p0

    move-object v2, v0

    :goto_3
    :try_start_3
    const-string v1, "getAssetFile"

    .line 434
    invoke-static {v1, p1, p0}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_2

    .line 438
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_4

    :catch_3
    move-exception p0

    const-string v1, "getAssetFile"

    .line 441
    invoke-static {v1, p1, p0}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    :goto_4
    return-object v0

    :catchall_1
    move-exception p0

    move-object v0, v2

    :goto_5
    if-eqz v0, :cond_3

    .line 438
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_6

    :catch_4
    move-exception v0

    const-string v1, "getAssetFile"

    .line 441
    invoke-static {v1, p1, v0}, Lcom/box/androidsdk/content/utils/BoxLogUtils;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 442
    :cond_3
    :goto_6
    throw p0
.end method

.method public static getLocalizedFileSize(Landroid/content/Context;D)Ljava/lang/String;
    .locals 7

    .line 702
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 703
    sget-object v1, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 704
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    .line 705
    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_bytes:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_BYTES:Ljava/lang/String;

    .line 706
    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_kilobytes:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_KILOBYTES:Ljava/lang/String;

    .line 707
    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_megabytes:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_MEGABYTES:Ljava/lang/String;

    .line 708
    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_gigabytes:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_GIGABYTES:Ljava/lang/String;

    .line 709
    sget v1, Lcom/box/sdk/android/R$string;->boxsdk_terabytes:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    sput-object p0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_TERABYTES:Ljava/lang/String;

    .line 710
    sput-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_LANGUAGE:Ljava/lang/String;

    :cond_0
    const/4 p0, 0x0

    const-wide/high16 v0, 0x4090000000000000L    # 1024.0

    const/4 v2, 0x0

    const/4 v3, 0x1

    cmpg-double v4, p1, v0

    if-gez v4, :cond_1

    .line 717
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_BYTES:Ljava/lang/String;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    :cond_1
    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    cmpl-double v6, p1, v0

    if-ltz v6, :cond_2

    cmpg-double v6, p1, v4

    if-gez v6, :cond_2

    div-double/2addr p1, v0

    .line 720
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_KILOBYTES:Ljava/lang/String;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_2
    const-wide/high16 v0, 0x41d0000000000000L    # 1.073741824E9

    cmpl-double v6, p1, v4

    if-ltz v6, :cond_3

    cmpg-double v6, p1, v0

    if-gez v6, :cond_3

    div-double/2addr p1, v4

    .line 723
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_MEGABYTES:Ljava/lang/String;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_3
    const-wide/high16 v4, 0x4270000000000000L    # 1.099511627776E12

    cmpl-double v6, p1, v0

    if-ltz v6, :cond_4

    cmpg-double v6, p1, v4

    if-gez v6, :cond_4

    div-double/2addr p1, v0

    .line 726
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_GIGABYTES:Ljava/lang/String;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_4
    cmpl-double v0, p1, v4

    if-ltz v0, :cond_5

    div-double/2addr p1, v4

    .line 729
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p0

    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->SIZE_TERABYTES:Ljava/lang/String;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    :cond_5
    :goto_0
    return-object p0
.end method

.method public static isBlank(Ljava/lang/String;)Z
    .locals 0

    if-eqz p0, :cond_1

    .line 209
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static isEmptyString(Ljava/lang/String;)Z
    .locals 0

    if-eqz p0, :cond_1

    .line 200
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static isInternetAvailable(Landroid/content/Context;)Z
    .locals 3

    .line 400
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/net/ConnectivityManager;

    const/4 v0, 0x1

    .line 401
    invoke-virtual {p0, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    const/4 v2, 0x0

    .line 402
    invoke-virtual {p0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object p0

    .line 403
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0
.end method

.method public static parseJsonValueToInteger(Lcom/eclipsesource/json/JsonValue;)J
    .locals 2

    .line 265
    :try_start_0
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonValue;->asInt()I

    move-result p0
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    int-to-long v0, p0

    return-wide v0

    .line 267
    :catch_0
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonValue;->asString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "\""

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 268
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    int-to-long v0, p0

    return-wide v0
.end method

.method public static parseJsonValueToLong(Lcom/eclipsesource/json/JsonValue;)J
    .locals 2

    .line 251
    :try_start_0
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonValue;->asLong()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide v0

    .line 253
    :catch_0
    invoke-virtual {p0}, Lcom/eclipsesource/json/JsonValue;->asString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "\""

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 254
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static setCollabNumberThumb(Landroid/content/Context;Landroid/widget/TextView;I)V
    .locals 1

    const/16 p0, 0x64

    if-lt p2, p0, :cond_0

    const-string p0, "+99"

    goto :goto_0

    .line 536
    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "+"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 537
    :goto_0
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->setColorForCollabNumberThumb(Landroid/widget/TextView;)V

    const p2, -0xe66d22

    .line 538
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 539
    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static setColorForCollabNumberThumb(Landroid/widget/TextView;)V
    .locals 2

    const/4 v0, -0x1

    const v1, -0xe66d22

    .line 588
    invoke-static {p0, v0, v1}, Lcom/box/androidsdk/content/utils/SdkUtils;->setColorsThumb(Landroid/widget/TextView;II)V

    return-void
.end method

.method public static setColorForInitialsThumb(Landroid/widget/TextView;I)V
    .locals 2

    .line 578
    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->THUMB_COLORS:[I

    array-length v1, v0

    rem-int/2addr p1, v1

    aget p1, v0, p1

    const/4 v0, -0x1

    .line 579
    invoke-static {p0, p1, v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->setColorsThumb(Landroid/widget/TextView;II)V

    return-void
.end method

.method public static setColorsThumb(Landroid/widget/TextView;I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 551
    invoke-static {p0, p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->setColorForInitialsThumb(Landroid/widget/TextView;I)V

    return-void
.end method

.method public static setColorsThumb(Landroid/widget/TextView;II)V
    .locals 3

    .line 560
    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/box/sdk/android/R$drawable;->boxsdk_thumb_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 561
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/GradientDrawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 562
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x3

    const/16 v2, 0xf

    if-le p1, v2, :cond_0

    .line 563
    invoke-virtual {v0, v1, p2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 564
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 566
    :cond_0
    invoke-virtual {v0, v1, p2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 567
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method public static setInitialsThumb(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 3

    const/4 p0, 0x0

    if-eqz p2, :cond_1

    const-string v0, " "

    .line 514
    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    .line 515
    aget-object v0, p2, p0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 516
    aget-object v0, p2, p0

    invoke-virtual {v0, p0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 518
    :goto_0
    array-length v1, p2

    const/4 v2, 0x1

    if-le v1, v2, :cond_2

    .line 519
    array-length v1, p2

    sub-int/2addr v1, v2

    aget-object p2, p2, v1

    invoke-virtual {p2, p0}, Ljava/lang/String;->charAt(I)C

    move-result p0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_1
    add-int p2, v0, p0

    .line 522
    invoke-static {p1, p2}, Lcom/box/androidsdk/content/utils/SdkUtils;->setColorForInitialsThumb(Landroid/widget/TextView;I)V

    .line 523
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 p0, -0x1

    .line 524
    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public static sha1(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    const-string v0, "SHA-1"

    .line 220
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    const/16 v1, 0x2000

    .line 221
    new-array v1, v1, [B

    .line 223
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v3, 0x0

    .line 224
    invoke-virtual {v0, v1, v3, v2}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_0

    .line 227
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 228
    new-instance p0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, Lcom/box/androidsdk/content/utils/SdkUtils;->encodeHex([B)[C

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/String;-><init>([C)V

    return-object p0
.end method

.method public static toastSafely(Landroid/content/Context;II)V
    .locals 5

    .line 483
    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->LAST_TOAST_TIME:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 484
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-wide v2, Lcom/box/androidsdk/content/utils/SdkUtils;->TOAST_MIN_REPEAT_DELAY:J

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    return-void

    .line 487
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    .line 488
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 489
    sget-object v0, Lcom/box/androidsdk/content/utils/SdkUtils;->LAST_TOAST_TIME:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    invoke-static {p0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 492
    :cond_1
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 493
    new-instance v0, Lcom/box/androidsdk/content/utils/SdkUtils$4;

    invoke-direct {v0, p1, p0, p2}, Lcom/box/androidsdk/content/utils/SdkUtils$4;-><init>(ILandroid/content/Context;I)V

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method

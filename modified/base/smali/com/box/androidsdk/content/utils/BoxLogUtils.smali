.class public Lcom/box/androidsdk/content/utils/BoxLogUtils;
.super Ljava/lang/Object;
.source "BoxLogUtils.java"


# static fields
.field private static sLogger:Lcom/box/androidsdk/content/utils/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/box/androidsdk/content/utils/BoxLogger;

    invoke-direct {v0}, Lcom/box/androidsdk/content/utils/BoxLogger;-><init>()V

    sput-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 30
    sget-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    invoke-interface {v0, p0, p1}, Lcom/box/androidsdk/content/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 34
    sget-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    invoke-interface {v0, p0, p1}, Lcom/box/androidsdk/content/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 42
    sget-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    invoke-interface {v0, p0, p1, p2}, Lcom/box/androidsdk/content/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 38
    sget-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    invoke-interface {v0, p0, p1}, Lcom/box/androidsdk/content/utils/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static getIsLoggingEnabled()Z
    .locals 1

    .line 18
    sget-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    invoke-interface {v0}, Lcom/box/androidsdk/content/utils/Logger;->getIsLoggingEnabled()Z

    move-result v0

    return v0
.end method

.method public static getLogger(Lcom/box/androidsdk/content/utils/Logger;)Lcom/box/androidsdk/content/utils/Logger;
    .locals 0

    .line 14
    sget-object p0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    return-object p0
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 22
    sget-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    invoke-interface {v0, p0, p1}, Lcom/box/androidsdk/content/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 26
    sget-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    invoke-interface {v0, p0, p1, p2}, Lcom/box/androidsdk/content/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

.method public static nonFatalE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 46
    sget-object v0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    invoke-interface {v0, p0, p1, p2}, Lcom/box/androidsdk/content/utils/Logger;->nonFatalE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public static setLogger(Lcom/box/androidsdk/content/utils/Logger;)V
    .locals 0

    .line 10
    sput-object p0, Lcom/box/androidsdk/content/utils/BoxLogUtils;->sLogger:Lcom/box/androidsdk/content/utils/Logger;

    return-void
.end method

.class Lcom/box/androidsdk/content/utils/FastDateFormat$TwentyFourHourField;
.super Ljava/lang/Object;
.source "FastDateFormat.java"

# interfaces
.implements Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/utils/FastDateFormat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TwentyFourHourField"
.end annotation


# instance fields
.field private final mRule:Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;


# direct methods
.method constructor <init>(Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;)V
    .locals 0

    .line 1492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1493
    iput-object p1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat$TwentyFourHourField;->mRule:Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    return-void
.end method


# virtual methods
.method public appendTo(Ljava/lang/StringBuffer;I)V
    .locals 1

    .line 1518
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat$TwentyFourHourField;->mRule:Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    invoke-interface {v0, p1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;->appendTo(Ljava/lang/StringBuffer;I)V

    return-void
.end method

.method public appendTo(Ljava/lang/StringBuffer;Ljava/util/Calendar;)V
    .locals 2

    const/16 v0, 0xb

    .line 1507
    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1509
    invoke-virtual {p2, v0}, Ljava/util/Calendar;->getMaximum(I)I

    move-result p2

    add-int/lit8 v1, p2, 0x1

    .line 1511
    :cond_0
    iget-object p2, p0, Lcom/box/androidsdk/content/utils/FastDateFormat$TwentyFourHourField;->mRule:Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    invoke-interface {p2, p1, v1}, Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;->appendTo(Ljava/lang/StringBuffer;I)V

    return-void
.end method

.method public estimateLength()I
    .locals 1

    .line 1500
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat$TwentyFourHourField;->mRule:Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    invoke-interface {v0}, Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;->estimateLength()I

    move-result v0

    return v0
.end method

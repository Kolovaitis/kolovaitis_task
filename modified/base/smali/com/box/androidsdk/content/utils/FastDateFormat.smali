.class public Lcom/box/androidsdk/content/utils/FastDateFormat;
.super Ljava/text/Format;
.source "FastDateFormat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneDisplayKey;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNumberRule;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNameRule;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TwentyFourHourField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TwelveHourField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitMonthField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitYearField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitNumberField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$PaddedNumberField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$UnpaddedMonthField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$UnpaddedNumberField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$StringLiteral;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$CharacterLiteral;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;,
        Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;
    }
.end annotation


# static fields
.field public static final FULL:I = 0x0

.field public static final LONG:I = 0x1

.field public static final MEDIUM:I = 0x2

.field public static final SHORT:I = 0x3

.field private static final cDateInstanceCache:Ljava/util/Map;

.field private static final cDateTimeInstanceCache:Ljava/util/Map;

.field private static cDefaultPattern:Ljava/lang/String; = null

.field private static final cInstanceCache:Ljava/util/Map;

.field private static final cTimeInstanceCache:Ljava/util/Map;

.field private static final cTimeZoneDisplayCache:Ljava/util/Map;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final mLocale:Ljava/util/Locale;

.field private final mLocaleForced:Z

.field private transient mMaxLengthEstimate:I

.field private final mPattern:Ljava/lang/String;

.field private transient mRules:[Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;

.field private final mTimeZone:Ljava/util/TimeZone;

.field private final mTimeZoneForced:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 112
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cInstanceCache:Ljava/util/Map;

    .line 113
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDateInstanceCache:Ljava/util/Map;

    .line 114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cTimeInstanceCache:Ljava/util/Map;

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDateTimeInstanceCache:Ljava/util/Map;

    .line 116
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cTimeZoneDisplayCache:Ljava/util/Map;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)V
    .locals 2

    .line 536
    invoke-direct {p0}, Ljava/text/Format;-><init>()V

    if-eqz p1, :cond_4

    .line 540
    iput-object p1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mPattern:Ljava/lang/String;

    const/4 p1, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 542
    :goto_0
    iput-boolean v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZoneForced:Z

    if-nez p2, :cond_1

    .line 544
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object p2

    .line 546
    :cond_1
    iput-object p2, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    if-eqz p3, :cond_2

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    .line 548
    :goto_1
    iput-boolean p1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocaleForced:Z

    if-nez p3, :cond_3

    .line 550
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    .line 552
    :cond_3
    iput-object p3, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocale:Ljava/util/Locale;

    return-void

    .line 538
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The pattern must not be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static getDateInstance(I)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 236
    invoke-static {p0, v0, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getDateInstance(ILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getDateInstance(ILjava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 251
    invoke-static {p0, v0, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getDateInstance(ILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getDateInstance(ILjava/util/TimeZone;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 267
    invoke-static {p0, p1, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getDateInstance(ILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static declared-synchronized getDateInstance(ILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 3

    const-class v0, Lcom/box/androidsdk/content/utils/FastDateFormat;

    monitor-enter v0

    .line 282
    :try_start_0
    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    if-eqz p1, :cond_0

    .line 284
    new-instance v2, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;

    invoke-direct {v2, v1, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v1, v2

    :cond_0
    if-nez p2, :cond_1

    .line 288
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2

    .line 291
    :cond_1
    new-instance v2, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;

    invoke-direct {v2, v1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 293
    sget-object v1, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDateInstanceCache:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/box/androidsdk/content/utils/FastDateFormat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    .line 296
    :try_start_1
    invoke-static {p0, p2}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object p0

    check-cast p0, Ljava/text/SimpleDateFormat;

    .line 297
    invoke-virtual {p0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object p0

    .line 298
    invoke-static {p0, p1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getInstance(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object v1

    .line 299
    sget-object p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDateInstanceCache:Ljava/util/Map;

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 302
    :catch_0
    :try_start_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No date pattern for locale: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 305
    :cond_2
    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static getDateTimeInstance(II)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 408
    invoke-static {p0, p1, v0, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getDateTimeInstance(IILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getDateTimeInstance(IILjava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 425
    invoke-static {p0, p1, v0, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getDateTimeInstance(IILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getDateTimeInstance(IILjava/util/TimeZone;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 443
    invoke-static {p0, p1, p2, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getDateTimeInstance(IILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static declared-synchronized getDateTimeInstance(IILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 4

    const-class v0, Lcom/box/androidsdk/content/utils/FastDateFormat;

    monitor-enter v0

    .line 461
    :try_start_0
    new-instance v1, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-direct {v1, v2, v3}, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz p2, :cond_0

    .line 463
    new-instance v2, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;

    invoke-direct {v2, v1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v1, v2

    :cond_0
    if-nez p3, :cond_1

    .line 466
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    .line 468
    :cond_1
    new-instance v2, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;

    invoke-direct {v2, v1, p3}, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 470
    sget-object v1, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDateTimeInstanceCache:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/box/androidsdk/content/utils/FastDateFormat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    .line 473
    :try_start_1
    invoke-static {p0, p1, p3}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object p0

    check-cast p0, Ljava/text/SimpleDateFormat;

    .line 475
    invoke-virtual {p0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object p0

    .line 476
    invoke-static {p0, p2, p3}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getInstance(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object v1

    .line 477
    sget-object p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDateTimeInstanceCache:Ljava/util/Map;

    invoke-interface {p0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 480
    :catch_0
    :try_start_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "No date time pattern for locale: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 483
    :cond_2
    :goto_0
    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private static declared-synchronized getDefaultPattern()Ljava/lang/String;
    .locals 2

    const-class v0, Lcom/box/androidsdk/content/utils/FastDateFormat;

    monitor-enter v0

    .line 514
    :try_start_0
    sget-object v1, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDefaultPattern:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 515
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1}, Ljava/text/SimpleDateFormat;-><init>()V

    invoke-virtual {v1}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDefaultPattern:Ljava/lang/String;

    .line 517
    :cond_0
    sget-object v1, Lcom/box/androidsdk/content/utils/FastDateFormat;->cDefaultPattern:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getInstance()Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 2

    .line 155
    invoke-static {}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getDefaultPattern()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1, v1}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getInstance(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 168
    invoke-static {p0, v0, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getInstance(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Ljava/lang/String;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 197
    invoke-static {p0, v0, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getInstance(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getInstance(Ljava/lang/String;Ljava/util/TimeZone;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 183
    invoke-static {p0, p1, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getInstance(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static declared-synchronized getInstance(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 2

    const-class v0, Lcom/box/androidsdk/content/utils/FastDateFormat;

    monitor-enter v0

    .line 214
    :try_start_0
    new-instance v1, Lcom/box/androidsdk/content/utils/FastDateFormat;

    invoke-direct {v1, p0, p1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;-><init>(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)V

    .line 215
    sget-object p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cInstanceCache:Ljava/util/Map;

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/utils/FastDateFormat;

    if-nez p0, :cond_0

    .line 218
    invoke-virtual {v1}, Lcom/box/androidsdk/content/utils/FastDateFormat;->init()V

    .line 219
    sget-object p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cInstanceCache:Ljava/util/Map;

    invoke-interface {p0, v1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object p0, v1

    .line 221
    :cond_0
    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static getTimeInstance(I)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 320
    invoke-static {p0, v0, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getTimeInstance(ILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getTimeInstance(ILjava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 335
    invoke-static {p0, v0, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getTimeInstance(ILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static getTimeInstance(ILjava/util/TimeZone;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 1

    const/4 v0, 0x0

    .line 351
    invoke-static {p0, p1, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getTimeInstance(ILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static declared-synchronized getTimeInstance(ILjava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;
    .locals 3

    const-class v0, Lcom/box/androidsdk/content/utils/FastDateFormat;

    monitor-enter v0

    .line 367
    :try_start_0
    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    if-eqz p1, :cond_0

    .line 369
    new-instance v2, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;

    invoke-direct {v2, v1, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v1, v2

    :cond_0
    if-eqz p2, :cond_1

    .line 372
    new-instance v2, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;

    invoke-direct {v2, v1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat$Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v1, v2

    .line 375
    :cond_1
    sget-object v2, Lcom/box/androidsdk/content/utils/FastDateFormat;->cTimeInstanceCache:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/box/androidsdk/content/utils/FastDateFormat;

    if-nez v2, :cond_3

    if-nez p2, :cond_2

    .line 378
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    :cond_2
    :try_start_1
    invoke-static {p0, p2}, Ljava/text/DateFormat;->getTimeInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object p0

    check-cast p0, Ljava/text/SimpleDateFormat;

    .line 383
    invoke-virtual {p0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object p0

    .line 384
    invoke-static {p0, p1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;->getInstance(Ljava/lang/String;Ljava/util/TimeZone;Ljava/util/Locale;)Lcom/box/androidsdk/content/utils/FastDateFormat;

    move-result-object v2

    .line 385
    sget-object p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cTimeInstanceCache:Ljava/util/Map;

    invoke-interface {p0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 388
    :catch_0
    :try_start_2
    new-instance p0, Ljava/lang/IllegalArgumentException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No date pattern for locale: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 391
    :cond_3
    :goto_0
    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method static declared-synchronized getTimeZoneDisplay(Ljava/util/TimeZone;ZILjava/util/Locale;)Ljava/lang/String;
    .locals 3

    const-class v0, Lcom/box/androidsdk/content/utils/FastDateFormat;

    monitor-enter v0

    .line 498
    :try_start_0
    new-instance v1, Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneDisplayKey;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneDisplayKey;-><init>(Ljava/util/TimeZone;ZILjava/util/Locale;)V

    .line 499
    sget-object v2, Lcom/box/androidsdk/content/utils/FastDateFormat;->cTimeZoneDisplayCache:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_0

    .line 502
    invoke-virtual {p0, p1, p2, p3}, Ljava/util/TimeZone;->getDisplayName(ZILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 503
    sget-object p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->cTimeZoneDisplayCache:Ljava/util/Map;

    invoke-interface {p0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    :cond_0
    monitor-exit v0

    return-object v2

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 1029
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 1030
    invoke-virtual {p0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->init()V

    return-void
.end method


# virtual methods
.method protected applyRules(Ljava/util/Calendar;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 4

    .line 888
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mRules:[Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;

    .line 889
    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    .line 891
    aget-object v3, v0, v2

    invoke-interface {v3, p2, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;->appendTo(Ljava/lang/StringBuffer;Ljava/util/Calendar;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object p2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 978
    instance-of v0, p1, Lcom/box/androidsdk/content/utils/FastDateFormat;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 981
    :cond_0
    check-cast p1, Lcom/box/androidsdk/content/utils/FastDateFormat;

    .line 982
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mPattern:Ljava/lang/String;

    iget-object v2, p1, Lcom/box/androidsdk/content/utils/FastDateFormat;->mPattern:Ljava/lang/String;

    if-eq v0, v2, :cond_1

    .line 983
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    iget-object v2, p1, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    if-eq v0, v2, :cond_2

    .line 984
    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocale:Ljava/util/Locale;

    iget-object v2, p1, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocale:Ljava/util/Locale;

    if-eq v0, v2, :cond_3

    .line 985
    invoke-virtual {v0, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZoneForced:Z

    iget-boolean v2, p1, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZoneForced:Z

    if-ne v0, v2, :cond_4

    iget-boolean v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocaleForced:Z

    iget-boolean p1, p1, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocaleForced:Z

    if-ne v0, p1, :cond_4

    const/4 p1, 0x1

    return p1

    :cond_4
    return v1
.end method

.method public format(J)Ljava/lang/String;
    .locals 1

    .line 810
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public format(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 2

    .line 832
    new-instance v0, Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mMaxLengthEstimate:I

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    invoke-virtual {p0, p1, v0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->format(Ljava/util/Calendar;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public format(Ljava/util/Date;)Ljava/lang/String;
    .locals 2

    .line 820
    new-instance v0, Ljava/util/GregorianCalendar;

    iget-object v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 821
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 822
    new-instance p1, Ljava/lang/StringBuffer;

    iget v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mMaxLengthEstimate:I

    invoke-direct {p1, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    invoke-virtual {p0, v0, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat;->applyRules(Ljava/util/Calendar;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public format(JLjava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 1

    .line 845
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p0, v0, p3}, Lcom/box/androidsdk/content/utils/FastDateFormat;->format(Ljava/util/Date;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p1

    return-object p1
.end method

.method public format(Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/text/FieldPosition;)Ljava/lang/StringBuffer;
    .locals 2

    .line 790
    instance-of p3, p1, Ljava/util/Date;

    if-eqz p3, :cond_0

    .line 791
    check-cast p1, Ljava/util/Date;

    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;->format(Ljava/util/Date;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p1

    return-object p1

    .line 792
    :cond_0
    instance-of p3, p1, Ljava/util/Calendar;

    if-eqz p3, :cond_1

    .line 793
    check-cast p1, Ljava/util/Calendar;

    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;->format(Ljava/util/Calendar;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p1

    return-object p1

    .line 794
    :cond_1
    instance-of p3, p1, Ljava/lang/Long;

    if-eqz p3, :cond_2

    .line 795
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;->format(JLjava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p1

    return-object p1

    .line 797
    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown class: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p1, :cond_3

    const-string p1, "<null>"

    goto :goto_0

    .line 798
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public format(Ljava/util/Calendar;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 1

    .line 871
    iget-boolean v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZoneForced:Z

    if-eqz v0, :cond_0

    .line 872
    invoke-virtual {p1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    .line 873
    invoke-virtual {p1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Calendar;

    .line 874
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 876
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;->applyRules(Ljava/util/Calendar;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p1

    return-object p1
.end method

.method public format(Ljava/util/Date;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;
    .locals 2

    .line 857
    new-instance v0, Ljava/util/GregorianCalendar;

    iget-object v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 858
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 859
    invoke-virtual {p0, v0, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat;->applyRules(Ljava/util/Calendar;Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object p1

    return-object p1
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .line 953
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public getMaxLengthEstimate()I
    .locals 1

    .line 966
    iget v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mMaxLengthEstimate:I

    return v0
.end method

.method public getPattern()Ljava/lang/String;
    .locals 1

    .line 919
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mPattern:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 1

    .line 933
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method public getTimeZoneOverridesCalendar()Z
    .locals 1

    .line 944
    iget-boolean v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZoneForced:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 1001
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mPattern:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1002
    iget-object v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1003
    iget-boolean v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZoneForced:Z

    add-int/2addr v0, v1

    .line 1004
    iget-object v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1005
    iget-boolean v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocaleForced:Z

    add-int/2addr v0, v1

    return v0
.end method

.method protected init()V
    .locals 3

    .line 559
    invoke-virtual {p0}, Lcom/box/androidsdk/content/utils/FastDateFormat;->parsePattern()Ljava/util/List;

    move-result-object v0

    .line 560
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;

    iput-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mRules:[Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;

    .line 563
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mRules:[Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;

    array-length v0, v0

    const/4 v1, 0x0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 564
    iget-object v2, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mRules:[Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/box/androidsdk/content/utils/FastDateFormat$Rule;->estimateLength()I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    .line 567
    :cond_0
    iput v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mMaxLengthEstimate:I

    return-void
.end method

.method public parseObject(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    .line 906
    invoke-virtual {p2, p1}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 907
    invoke-virtual {p2, p1}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method protected parsePattern()Ljava/util/List;
    .locals 16

    move-object/from16 v0, p0

    .line 579
    new-instance v1, Ljava/text/DateFormatSymbols;

    iget-object v2, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocale:Ljava/util/Locale;

    invoke-direct {v1, v2}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 580
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 582
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getEras()[Ljava/lang/String;

    move-result-object v3

    .line 583
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v4

    .line 584
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v5

    .line 585
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v6

    .line 586
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getShortWeekdays()[Ljava/lang/String;

    move-result-object v7

    .line 587
    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v1

    .line 589
    iget-object v8, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mPattern:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x1

    .line 590
    new-array v10, v9, [I

    const/4 v11, 0x0

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v8, :cond_9

    aput v12, v10, v11

    .line 594
    iget-object v12, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mPattern:Ljava/lang/String;

    invoke-virtual {v0, v12, v10}, Lcom/box/androidsdk/content/utils/FastDateFormat;->parseToken(Ljava/lang/String;[I)Ljava/lang/String;

    move-result-object v12

    .line 595
    aget v13, v10, v11

    .line 597
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v14

    if-nez v14, :cond_0

    goto/16 :goto_3

    .line 603
    :cond_0
    invoke-virtual {v12, v11}, Ljava/lang/String;->charAt(I)C

    move-result v15

    const/4 v11, 0x4

    sparse-switch v15, :sswitch_data_0

    .line 692
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Illegal pattern component: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    if-lt v14, v11, :cond_1

    .line 671
    new-instance v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNameRule;

    iget-object v12, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    iget-boolean v14, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZoneForced:Z

    iget-object v15, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocale:Ljava/util/Locale;

    invoke-direct {v11, v12, v14, v15, v9}, Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNameRule;-><init>(Ljava/util/TimeZone;ZLjava/util/Locale;I)V

    const/4 v14, 0x0

    goto/16 :goto_2

    .line 673
    :cond_1
    new-instance v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNameRule;

    iget-object v12, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZone:Ljava/util/TimeZone;

    iget-boolean v14, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mTimeZoneForced:Z

    iget-object v15, v0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mLocale:Ljava/util/Locale;

    const/4 v9, 0x0

    invoke-direct {v11, v12, v14, v15, v9}, Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNameRule;-><init>(Ljava/util/TimeZone;ZLjava/util/Locale;I)V

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :sswitch_1
    if-lt v14, v11, :cond_2

    const/4 v9, 0x1

    .line 611
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v14, 0x0

    goto/16 :goto_2

    .line 613
    :cond_2
    sget-object v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitYearField;->INSTANCE:Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitYearField;

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :sswitch_2
    const/4 v9, 0x3

    .line 655
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :sswitch_3
    const/16 v9, 0xd

    .line 640
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :sswitch_4
    const/16 v9, 0xc

    .line 637
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    .line 664
    :sswitch_5
    new-instance v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TwentyFourHourField;

    const/16 v9, 0xb

    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v9

    invoke-direct {v11, v9}, Lcom/box/androidsdk/content/utils/FastDateFormat$TwentyFourHourField;-><init>(Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;)V

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    .line 631
    :sswitch_6
    new-instance v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TwelveHourField;

    const/16 v9, 0xa

    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v9

    invoke-direct {v11, v9}, Lcom/box/androidsdk/content/utils/FastDateFormat$TwelveHourField;-><init>(Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;)V

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :sswitch_7
    const/4 v9, 0x5

    .line 628
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    .line 661
    :sswitch_8
    new-instance v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;

    const/16 v9, 0x9

    invoke-direct {v11, v9, v1}, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;-><init>(I[Ljava/lang/String;)V

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :sswitch_9
    if-ne v14, v9, :cond_3

    .line 678
    sget-object v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNumberRule;->INSTANCE_NO_COLON:Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNumberRule;

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    .line 680
    :cond_3
    sget-object v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNumberRule;->INSTANCE_COLON:Lcom/box/androidsdk/content/utils/FastDateFormat$TimeZoneNumberRule;

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    .line 658
    :sswitch_a
    invoke-virtual {v0, v11, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :sswitch_b
    const/16 v9, 0xe

    .line 643
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :sswitch_c
    const/4 v9, 0x2

    if-lt v14, v11, :cond_4

    .line 618
    new-instance v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;

    invoke-direct {v11, v9, v4}, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;-><init>(I[Ljava/lang/String;)V

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_4
    const/4 v11, 0x3

    if-ne v14, v11, :cond_5

    .line 620
    new-instance v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;

    invoke-direct {v11, v9, v5}, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;-><init>(I[Ljava/lang/String;)V

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_5
    if-ne v14, v9, :cond_6

    .line 622
    sget-object v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitMonthField;->INSTANCE:Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitMonthField;

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto :goto_2

    .line 624
    :cond_6
    sget-object v11, Lcom/box/androidsdk/content/utils/FastDateFormat$UnpaddedMonthField;->INSTANCE:Lcom/box/androidsdk/content/utils/FastDateFormat$UnpaddedMonthField;

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto :goto_2

    :sswitch_d
    const/16 v9, 0xa

    .line 667
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto :goto_2

    :sswitch_e
    const/16 v9, 0xb

    .line 634
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto :goto_2

    .line 607
    :sswitch_f
    new-instance v11, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;

    const/4 v9, 0x0

    invoke-direct {v11, v9, v3}, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;-><init>(I[Ljava/lang/String;)V

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto :goto_2

    :sswitch_10
    const/16 v9, 0x8

    .line 652
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto :goto_2

    .line 646
    :sswitch_11
    new-instance v9, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;

    const/4 v12, 0x7

    if-ge v14, v11, :cond_7

    move-object v11, v7

    goto :goto_1

    :cond_7
    move-object v11, v6

    :goto_1
    invoke-direct {v9, v12, v11}, Lcom/box/androidsdk/content/utils/FastDateFormat$TextField;-><init>(I[Ljava/lang/String;)V

    move-object v11, v9

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto :goto_2

    :sswitch_12
    const/4 v9, 0x6

    .line 649
    invoke-virtual {v0, v9, v14}, Lcom/box/androidsdk/content/utils/FastDateFormat;->selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;

    move-result-object v11

    const/4 v9, 0x1

    const/4 v14, 0x0

    goto :goto_2

    .line 684
    :sswitch_13
    invoke-virtual {v12, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 685
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    if-ne v12, v9, :cond_8

    .line 686
    new-instance v12, Lcom/box/androidsdk/content/utils/FastDateFormat$CharacterLiteral;

    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-direct {v12, v11}, Lcom/box/androidsdk/content/utils/FastDateFormat$CharacterLiteral;-><init>(C)V

    move-object v11, v12

    goto :goto_2

    :cond_8
    const/4 v14, 0x0

    .line 688
    new-instance v12, Lcom/box/androidsdk/content/utils/FastDateFormat$StringLiteral;

    invoke-direct {v12, v11}, Lcom/box/androidsdk/content/utils/FastDateFormat$StringLiteral;-><init>(Ljava/lang/String;)V

    move-object v11, v12

    .line 695
    :goto_2
    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v13, 0x1

    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_9
    :goto_3
    return-object v2

    :sswitch_data_0
    .sparse-switch
        0x27 -> :sswitch_13
        0x44 -> :sswitch_12
        0x45 -> :sswitch_11
        0x46 -> :sswitch_10
        0x47 -> :sswitch_f
        0x48 -> :sswitch_e
        0x4b -> :sswitch_d
        0x4d -> :sswitch_c
        0x53 -> :sswitch_b
        0x57 -> :sswitch_a
        0x5a -> :sswitch_9
        0x61 -> :sswitch_8
        0x64 -> :sswitch_7
        0x68 -> :sswitch_6
        0x6b -> :sswitch_5
        0x6d -> :sswitch_4
        0x73 -> :sswitch_3
        0x77 -> :sswitch_2
        0x79 -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method

.method protected parseToken(Ljava/lang/String;[I)Ljava/lang/String;
    .locals 13

    .line 709
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    .line 711
    aget v2, p2, v1

    .line 712
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 714
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5a

    const/16 v6, 0x41

    if-lt v4, v6, :cond_0

    if-le v4, v5, :cond_1

    :cond_0
    const/16 v7, 0x7a

    const/16 v8, 0x61

    if-lt v4, v8, :cond_2

    if-gt v4, v7, :cond_2

    .line 718
    :cond_1
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_0
    add-int/lit8 v5, v2, 0x1

    if-ge v5, v3, :cond_8

    .line 721
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-ne v6, v4, :cond_8

    .line 723
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v5

    goto :goto_0

    :cond_2
    const/16 v4, 0x27

    .line 731
    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v9, 0x0

    :goto_1
    if-ge v2, v3, :cond_8

    .line 736
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v10, v4, :cond_4

    add-int/lit8 v11, v2, 0x1

    if-ge v11, v3, :cond_3

    .line 739
    invoke-virtual {p1, v11}, Ljava/lang/String;->charAt(I)C

    move-result v12

    if-ne v12, v4, :cond_3

    .line 742
    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v2, v11

    goto :goto_2

    :cond_3
    xor-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_4
    if-nez v9, :cond_7

    if-lt v10, v6, :cond_5

    if-le v10, v5, :cond_6

    :cond_5
    if-lt v10, v8, :cond_7

    if-gt v10, v7, :cond_7

    :cond_6
    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    .line 751
    :cond_7
    invoke-virtual {v0, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 756
    :cond_8
    :goto_3
    aput v2, p2, v1

    .line 757
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected selectNumberRule(II)Lcom/box/androidsdk/content/utils/FastDateFormat$NumberRule;
    .locals 1

    packed-switch p2, :pswitch_data_0

    .line 774
    new-instance v0, Lcom/box/androidsdk/content/utils/FastDateFormat$PaddedNumberField;

    invoke-direct {v0, p1, p2}, Lcom/box/androidsdk/content/utils/FastDateFormat$PaddedNumberField;-><init>(II)V

    return-object v0

    .line 772
    :pswitch_0
    new-instance p2, Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitNumberField;

    invoke-direct {p2, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat$TwoDigitNumberField;-><init>(I)V

    return-object p2

    .line 770
    :pswitch_1
    new-instance p2, Lcom/box/androidsdk/content/utils/FastDateFormat$UnpaddedNumberField;

    invoke-direct {p2, p1}, Lcom/box/androidsdk/content/utils/FastDateFormat$UnpaddedNumberField;-><init>(I)V

    return-object p2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1015
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FastDateFormat["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/box/androidsdk/content/utils/FastDateFormat;->mPattern:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

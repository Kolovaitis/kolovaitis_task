.class public Lcom/box/androidsdk/content/utils/ProgressInputStream;
.super Ljava/io/InputStream;
.source "ProgressInputStream.java"


# instance fields
.field private final listener:Lcom/box/androidsdk/content/listeners/ProgressListener;

.field private progress:I

.field private final stream:Ljava/io/InputStream;

.field private total:J

.field private totalRead:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/box/androidsdk/content/listeners/ProgressListener;J)V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->stream:Ljava/io/InputStream;

    .line 28
    iput-object p2, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->listener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    .line 29
    iput-wide p3, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->total:J

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public getTotal()J
    .locals 2

    .line 37
    iget-wide v0, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->total:J

    return-wide v0
.end method

.method public read()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 56
    iget-wide v1, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->totalRead:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->totalRead:J

    .line 57
    iget-object v1, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->listener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    iget-wide v2, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->totalRead:J

    iget-wide v4, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->total:J

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/box/androidsdk/content/listeners/ProgressListener;->onProgressChanged(JJ)V

    return v0
.end method

.method public read([BII)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->stream:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1

    .line 65
    iget-wide p2, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->totalRead:J

    int-to-long v0, p1

    add-long/2addr p2, v0

    iput-wide p2, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->totalRead:J

    .line 66
    iget-object p2, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->listener:Lcom/box/androidsdk/content/listeners/ProgressListener;

    iget-wide v0, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->totalRead:J

    iget-wide v2, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->total:J

    invoke-interface {p2, v0, v1, v2, v3}, Lcom/box/androidsdk/content/listeners/ProgressListener;->onProgressChanged(JJ)V

    return p1
.end method

.method public setTotal(J)V
    .locals 0

    .line 45
    iput-wide p1, p0, Lcom/box/androidsdk/content/utils/ProgressInputStream;->total:J

    return-void
.end method

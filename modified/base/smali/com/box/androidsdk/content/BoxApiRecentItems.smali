.class public Lcom/box/androidsdk/content/BoxApiRecentItems;
.super Lcom/box/androidsdk/content/BoxApi;
.source "BoxApiRecentItems.java"


# static fields
.field private static final ENDPOINT_NAME:Ljava/lang/String; = "recent_items"


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/BoxApi;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    return-void
.end method


# virtual methods
.method public getRecentItemsRequest()Lcom/box/androidsdk/content/requests/BoxRequestRecentItems$GetRecentItems;
    .locals 3

    .line 36
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestRecentItems$GetRecentItems;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiRecentItems;->getRecentItemsUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiRecentItems;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestRecentItems$GetRecentItems;-><init>(Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getRecentItemsUrl()Ljava/lang/String;
    .locals 4

    const-string v0, "%s/recent_items"

    const/4 v1, 0x1

    .line 27
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiRecentItems;->getBaseUri()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

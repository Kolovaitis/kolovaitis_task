.class public Lcom/box/androidsdk/content/BoxApiCollection;
.super Lcom/box/androidsdk/content/BoxApi;
.source "BoxApiCollection.java"


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/BoxApi;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    return-void
.end method


# virtual methods
.method protected getCollectionItemsUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const-string v0, "%s/%s/items"

    const/4 v1, 0x2

    .line 36
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiCollection;->getCollectionsUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getCollectionsRequest()Lcom/box/androidsdk/content/requests/BoxRequestsCollections$GetCollections;
    .locals 3

    .line 45
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsCollections$GetCollections;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiCollection;->getCollectionsUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiCollection;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsCollections$GetCollections;-><init>(Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getCollectionsUrl()Ljava/lang/String;
    .locals 4

    const-string v0, "%s/collections"

    const/4 v1, 0x1

    .line 27
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiCollection;->getBaseUri()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemsRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsCollections$GetCollectionItems;
    .locals 3

    .line 56
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsCollections$GetCollectionItems;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiCollection;->getCollectionItemsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiCollection;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsCollections$GetCollectionItems;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

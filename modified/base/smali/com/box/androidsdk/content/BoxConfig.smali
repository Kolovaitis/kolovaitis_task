.class public Lcom/box/androidsdk/content/BoxConfig;
.super Ljava/lang/Object;
.source "BoxConfig.java"


# static fields
.field public static APPLICATION_CONTEXT:Landroid/content/Context; = null

.field public static CLIENT_ID:Ljava/lang/String; = null

.field public static CLIENT_SECRET:Ljava/lang/String; = null

.field public static DEVICE_ID:Ljava/lang/String; = null

.field public static DEVICE_NAME:Ljava/lang/String; = null

.field public static ENABLE_BOX_APP_AUTHENTICATION:Z = false

.field public static ENABLE_TLS_FOR_PRE_20:Z = false

.field public static IS_DEBUG:Z = false

.field public static IS_FLAG_SECURE:Z = false

.field public static IS_LOG_ENABLED:Z = false

.field public static REDIRECT_URL:Ljava/lang/String; = "https://app.box.com/static/sync_redirect.html"

.field public static SDK_VERSION:Ljava/lang/String; = "4.2.0"

.field private static mCache:Lcom/box/androidsdk/content/BoxCache;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 89
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/box/androidsdk/content/BoxConfig;->ENABLE_TLS_FOR_PRE_20:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCache()Lcom/box/androidsdk/content/BoxCache;
    .locals 1

    .line 72
    sget-object v0, Lcom/box/androidsdk/content/BoxConfig;->mCache:Lcom/box/androidsdk/content/BoxCache;

    return-object v0
.end method

.method public static setCache(Lcom/box/androidsdk/content/BoxCache;)V
    .locals 0

    .line 63
    sput-object p0, Lcom/box/androidsdk/content/BoxConfig;->mCache:Lcom/box/androidsdk/content/BoxCache;

    return-void
.end method

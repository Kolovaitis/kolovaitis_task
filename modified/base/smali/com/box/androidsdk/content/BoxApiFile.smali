.class public Lcom/box/androidsdk/content/BoxApiFile;
.super Lcom/box/androidsdk/content/BoxApi;
.source "BoxApiFile.java"


# direct methods
.method public constructor <init>(Lcom/box/androidsdk/content/models/BoxSession;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/BoxApi;-><init>(Lcom/box/androidsdk/content/models/BoxSession;)V

    return-void
.end method


# virtual methods
.method public getAbortUploadSessionRequest(Lcom/box/androidsdk/content/models/BoxUploadSession;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$AbortUploadSession;
    .locals 2

    .line 708
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$AbortUploadSession;

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$AbortUploadSession;-><init>(Lcom/box/androidsdk/content/models/BoxUploadSession;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getAddCommentRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddCommentToFile;
    .locals 3

    .line 283
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddCommentToFile;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getCommentUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddCommentToFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getAddTaggedCommentRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddTaggedCommentToFile;
    .locals 3

    .line 300
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddTaggedCommentToFile;

    .line 301
    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getCommentUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddTaggedCommentToFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getAddToCollectionRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddFileToCollection;
    .locals 3

    .line 546
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddFileToCollection;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$AddFileToCollection;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getCollaborationsRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetCollaborations;
    .locals 3

    .line 497
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetCollaborations;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileCollaborationsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetCollaborations;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getCommentUrl()Ljava/lang/String;
    .locals 2

    .line 139
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getBaseUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/comments"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCommentsRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileComments;
    .locals 3

    .line 486
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileComments;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileCommentsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileComments;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getCommitSessionRequest(Ljava/util/List;Lcom/box/androidsdk/content/models/BoxUploadSession;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/box/androidsdk/content/models/BoxUploadSessionPart;",
            ">;",
            "Lcom/box/androidsdk/content/models/BoxUploadSession;",
            ")",
            "Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;"
        }
    .end annotation

    .line 671
    new-instance v7, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;

    iget-object v6, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, v7

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;-><init>(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxUploadSession;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v7
.end method

.method public getCommitSessionRequest(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxUploadSession;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/box/androidsdk/content/models/BoxUploadSessionPart;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/box/androidsdk/content/models/BoxUploadSession;",
            ")",
            "Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;"
        }
    .end annotation

    .line 661
    new-instance v7, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;

    iget-object v6, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CommitUploadSession;-><init>(Ljava/util/List;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxUploadSession;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v7
.end method

.method public getCopyRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$CopyFile;
    .locals 3

    .line 210
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CopyFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileCopyUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CopyFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getCreateSharedLinkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;
    .locals 3

    .line 258
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 p1, 0x0

    .line 259
    invoke-virtual {v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;->setAccess(Lcom/box/androidsdk/content/models/BoxSharedLink$Access;)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdatedSharedFile;

    return-object p1
.end method

.method public getCreateUploadSessionRequest(Ljava/io/File;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 581
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadSessionForNewFileUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getCreateUploadSessionRequest(Ljava/io/InputStream;Ljava/lang/String;JLjava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;
    .locals 9

    .line 593
    new-instance v8, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadSessionForNewFileUrl()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    move-object v0, v8

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateUploadSession;-><init>(Ljava/io/InputStream;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v8
.end method

.method public getCreateUploadVersionSessionRequest(Ljava/io/File;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateNewVersionUploadSession;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 605
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateNewVersionUploadSession;

    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadSessionForNewFileVersionUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateNewVersionUploadSession;-><init>(Ljava/io/File;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getCreateUploadVersionSessionRequest(Ljava/io/InputStream;Ljava/lang/String;JLjava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateNewVersionUploadSession;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    .line 619
    new-instance v7, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateNewVersionUploadSession;

    invoke-virtual {p0, p5}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadSessionForNewFileVersionUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$CreateNewVersionUploadSession;-><init>(Ljava/io/InputStream;Ljava/lang/String;JLjava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v7
.end method

.method protected getDeleteFileVersionUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 116
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s/%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileVersionsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    aput-object p2, v2, p1

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getDeleteFromCollectionRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFileFromCollection;
    .locals 3

    .line 557
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFileFromCollection;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFileFromCollection;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDeleteRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFile;
    .locals 3

    .line 247
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDeleteTrashedFileRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteTrashedFile;
    .locals 3

    .line 464
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteTrashedFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getTrashedFileUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteTrashedFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDeleteVersionRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFileVersion;
    .locals 2

    .line 534
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFileVersion;

    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getDeleteFileVersionUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p2, p1, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DeleteFileVersion;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDisableSharedLinkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;
    .locals 3

    .line 270
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    const/4 p1, 0x0

    .line 271
    invoke-virtual {v0, p1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;->setSharedLink(Lcom/box/androidsdk/content/models/BoxSharedLink;)Lcom/box/androidsdk/content/requests/BoxRequest;

    move-result-object p1

    check-cast p1, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;

    return-object p1
.end method

.method public getDownloadRepresentationRequest(Ljava/lang/String;Ljava/io/File;Lcom/box/androidsdk/content/models/BoxRepresentation;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;
    .locals 2

    .line 443
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadRepresentation;-><init>(Ljava/lang/String;Ljava/io/File;Lcom/box/androidsdk/content/models/BoxRepresentation;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDownloadRequest(Ljava/io/File;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 368
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 371
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;

    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getFileDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p2, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;-><init>(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0

    .line 369
    :cond_0
    new-instance p1, Ljava/io/FileNotFoundException;

    invoke-direct {p1}, Ljava/io/FileNotFoundException;-><init>()V

    throw p1
.end method

.method public getDownloadRequest(Ljava/io/OutputStream;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;
    .locals 3

    .line 399
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;

    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getFileDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p2, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;-><init>(Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDownloadThumbnailRequest(Ljava/io/File;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 412
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 418
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail;

    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getThumbnailFileDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p2, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail;-><init>(Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0

    .line 416
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "This endpoint only supports files and does not support directories"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 413
    :cond_1
    new-instance p1, Ljava/io/FileNotFoundException;

    invoke-direct {p1}, Ljava/io/FileNotFoundException;-><init>()V

    throw p1
.end method

.method public getDownloadThumbnailRequest(Ljava/io/OutputStream;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail;
    .locals 3

    .line 430
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail;

    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getThumbnailFileDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p2, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadThumbnail;-><init>(Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getDownloadUrlRequest(Ljava/io/File;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 384
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$DownloadFile;-><init>(Ljava/io/File;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0

    .line 385
    :cond_0
    new-instance p1, Ljava/io/FileNotFoundException;

    invoke-direct {p1}, Ljava/io/FileNotFoundException;-><init>()V

    throw p1
.end method

.method public getEmbedLinkRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetEmbedLinkFileInfo;
    .locals 3

    .line 187
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetEmbedLinkFileInfo;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetEmbedLinkFileInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getFileCollaborationsUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/collaborations"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getFileCommentsUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/comments"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getFileCopyUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 56
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/copy"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, p1, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getFileDownloadUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/content"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 48
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s/%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getFilesUrl()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getFilePreviewedRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;
    .locals 3

    .line 569
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getPreviewFileUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$FilePreviewed;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getFileUploadNewVersionUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 71
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s/files/%s/content"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getBaseUploadUri()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getFileUploadUrl()Ljava/lang/String;
    .locals 5

    .line 63
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s/files/content"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getBaseUploadUri()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFileVersionsUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/versions"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getFilesUrl()Ljava/lang/String;
    .locals 5

    .line 40
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s/files"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getBaseUri()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInfoRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileInfo;
    .locals 3

    .line 175
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileInfo;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getListUploadSessionRequest(Lcom/box/androidsdk/content/models/BoxUploadSession;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$ListUploadSessionParts;
    .locals 2

    .line 699
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$ListUploadSessionParts;

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$ListUploadSessionParts;-><init>(Lcom/box/androidsdk/content/models/BoxUploadSession;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getMoveRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;
    .locals 3

    .line 235
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 236
    invoke-virtual {v0, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;->setParentId(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;

    return-object v0
.end method

.method protected getPreviewFileUrl()Ljava/lang/String;
    .locals 2

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getBaseUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/events"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getPromoteFileVersionUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileVersionsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/current"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getPromoteVersionRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;
    .locals 3

    .line 522
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getPromoteFileVersionUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$PromoteFileVersion;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getRenameRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;
    .locals 3

    .line 222
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    .line 223
    invoke-virtual {v0, p2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;->setName(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequest;

    return-object v0
.end method

.method public getRestoreTrashedFileRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$RestoreTrashedFile;
    .locals 3

    .line 475
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$RestoreTrashedFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$RestoreTrashedFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getThumbnailFileDownloadUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 132
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/thumbnail"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getTrashedFileRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetTrashedFile;
    .locals 3

    .line 453
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetTrashedFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getTrashedFileUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetTrashedFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getTrashedFileUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/trash"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUpdateRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;
    .locals 3

    .line 198
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UpdateFile;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getUploadNewVersionRequest(Ljava/io/File;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;
    .locals 3

    .line 350
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadNewVersionRequest(Ljava/io/InputStream;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;

    move-result-object p2

    .line 351
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;->setUploadSize(J)Lcom/box/androidsdk/content/requests/BoxRequest;

    .line 352
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {p2, v0}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;->setModifiedDate(Ljava/util/Date;)Lcom/box/androidsdk/content/requests/BoxRequest;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p2

    :catch_0
    move-exception p1

    .line 355
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public getUploadNewVersionRequest(Ljava/io/InputStream;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;
    .locals 2

    .line 337
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;

    invoke-virtual {p0, p2}, Lcom/box/androidsdk/content/BoxApiFile;->getFileUploadNewVersionUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadNewVersion;-><init>(Ljava/io/InputStream;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getUploadRequest(Ljava/io/File;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;
    .locals 3

    .line 325
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getFileUploadUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getUploadRequest(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;
    .locals 7

    .line 314
    new-instance v6, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getFileUploadUrl()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadFile;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v6
.end method

.method public getUploadSession(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetUploadSession;
    .locals 3

    .line 689
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetUploadSession;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadSessionInfoUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetUploadSession;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method protected getUploadSessionForNewFileUrl()Ljava/lang/String;
    .locals 2

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getBaseUploadUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/files/upload_sessions"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getUploadSessionForNewFileVersionUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 163
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s/files/%s/upload_sessions"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getBaseUploadUri()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getUploadSessionInfoUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 681
    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v1, "%s/%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/box/androidsdk/content/BoxApiFile;->getUploadSessionForNewFileUrl()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getUploadSessionPartRequest(Ljava/io/File;Lcom/box/androidsdk/content/models/BoxUploadSession;I)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 631
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;

    iget-object v1, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;-><init>(Ljava/io/File;Lcom/box/androidsdk/content/models/BoxUploadSession;ILcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

.method public getUploadSessionPartRequest(Ljava/io/InputStream;JLcom/box/androidsdk/content/models/BoxUploadSession;I)Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 645
    new-instance v7, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;

    iget-object v6, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    move-object v0, v7

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$UploadSessionPart;-><init>(Ljava/io/InputStream;JLcom/box/androidsdk/content/models/BoxUploadSession;ILcom/box/androidsdk/content/models/BoxSession;)V

    return-object v7
.end method

.method public getVersionsRequest(Ljava/lang/String;)Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileVersions;
    .locals 3

    .line 508
    new-instance v0, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileVersions;

    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/BoxApiFile;->getFileVersionsUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/box/androidsdk/content/BoxApiFile;->mSession:Lcom/box/androidsdk/content/models/BoxSession;

    invoke-direct {v0, p1, v1, v2}, Lcom/box/androidsdk/content/requests/BoxRequestsFile$GetFileVersions;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/box/androidsdk/content/models/BoxSession;)V

    return-object v0
.end method

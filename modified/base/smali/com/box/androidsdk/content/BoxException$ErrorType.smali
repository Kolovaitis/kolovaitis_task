.class public final enum Lcom/box/androidsdk/content/BoxException$ErrorType;
.super Ljava/lang/Enum;
.source "BoxException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/BoxException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/BoxException$ErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum ACCESS_DENIED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum ACCOUNT_DEACTIVATED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum CORRUPTED_FILE_TRANSFER:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum GRACE_PERIOD_EXPIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum INTERNAL_ERROR:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum INVALID_CLIENT:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum INVALID_GRANT_INVALID_TOKEN:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum INVALID_GRANT_TOKEN_EXPIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum INVALID_REQUEST:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum IP_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum LOCATION_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum NETWORK_ERROR:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum NEW_OWNER_NOT_COLLABORATOR:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum NO_CREDIT_CARD_TRIAL_ENDED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum OTHER:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum PASSWORD_RESET_REQUIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum SERVICE_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum TEMPORARILY_UNAVAILABLE:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum TERMS_OF_SERVICE_REQUIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum UNAUTHORIZED:Lcom/box/androidsdk/content/BoxException$ErrorType;

.field public static final enum UNAUTHORIZED_DEVICE:Lcom/box/androidsdk/content/BoxException$ErrorType;


# instance fields
.field private final mStatusCode:I

.field private final mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 142
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "INVALID_GRANT_TOKEN_EXPIRED"

    const-string v2, "invalid_grant"

    const/4 v3, 0x0

    const/16 v4, 0x190

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->INVALID_GRANT_TOKEN_EXPIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 146
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "INVALID_GRANT_INVALID_TOKEN"

    const-string v2, "invalid_grant"

    const/4 v5, 0x1

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->INVALID_GRANT_INVALID_TOKEN:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 150
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "ACCOUNT_DEACTIVATED"

    const-string v2, "account_deactivated"

    const/4 v6, 0x2

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->ACCOUNT_DEACTIVATED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 154
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "ACCESS_DENIED"

    const-string v2, "access_denied"

    const/4 v7, 0x3

    const/16 v8, 0x193

    invoke-direct {v0, v1, v7, v2, v8}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->ACCESS_DENIED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 158
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "INVALID_REQUEST"

    const-string v2, "invalid_request"

    const/4 v9, 0x4

    invoke-direct {v0, v1, v9, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->INVALID_REQUEST:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 162
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "INVALID_CLIENT"

    const-string v2, "invalid_client"

    const/4 v10, 0x5

    invoke-direct {v0, v1, v10, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->INVALID_CLIENT:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 166
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "PASSWORD_RESET_REQUIRED"

    const-string v2, "password_reset_required"

    const/4 v11, 0x6

    invoke-direct {v0, v1, v11, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->PASSWORD_RESET_REQUIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 170
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "TERMS_OF_SERVICE_REQUIRED"

    const-string v2, "terms_of_service_required"

    const/4 v12, 0x7

    invoke-direct {v0, v1, v12, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->TERMS_OF_SERVICE_REQUIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 174
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "NO_CREDIT_CARD_TRIAL_ENDED"

    const-string v2, "no_credit_card_trial_ended"

    const/16 v13, 0x8

    invoke-direct {v0, v1, v13, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->NO_CREDIT_CARD_TRIAL_ENDED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 178
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "TEMPORARILY_UNAVAILABLE"

    const-string v2, "temporarily_unavailable"

    const/16 v14, 0x9

    const/16 v15, 0x1ad

    invoke-direct {v0, v1, v14, v2, v15}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->TEMPORARILY_UNAVAILABLE:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 182
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "SERVICE_BLOCKED"

    const-string v2, "service_blocked"

    const/16 v15, 0xa

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->SERVICE_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 186
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "UNAUTHORIZED_DEVICE"

    const-string v2, "unauthorized_device"

    const/16 v15, 0xb

    invoke-direct {v0, v1, v15, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->UNAUTHORIZED_DEVICE:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 190
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "GRACE_PERIOD_EXPIRED"

    const-string v2, "grace_period_expired"

    const/16 v15, 0xc

    invoke-direct {v0, v1, v15, v2, v8}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->GRACE_PERIOD_EXPIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 194
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "NETWORK_ERROR"

    const-string v2, "bad_connection_network_error"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15, v2, v3}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->NETWORK_ERROR:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 198
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "LOCATION_BLOCKED"

    const-string v2, "access_from_location_blocked"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15, v2, v8}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->LOCATION_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 202
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "IP_BLOCKED"

    const-string v2, "error_access_from_ip_not_allowed"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15, v2, v8}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->IP_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 206
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "UNAUTHORIZED"

    const-string v2, "unauthorized"

    const/16 v8, 0x10

    const/16 v15, 0x191

    invoke-direct {v0, v1, v8, v2, v15}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->UNAUTHORIZED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 210
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "NEW_OWNER_NOT_COLLABORATOR"

    const-string v2, "new_owner_not_collaborator"

    const/16 v8, 0x11

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->NEW_OWNER_NOT_COLLABORATOR:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 212
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "INTERNAL_ERROR"

    const-string v2, "internal_server_error"

    const/16 v4, 0x12

    const/16 v8, 0x1f4

    invoke-direct {v0, v1, v4, v2, v8}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->INTERNAL_ERROR:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 217
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "CORRUPTED_FILE_TRANSFER"

    const-string v2, "file corrupted"

    const/16 v4, 0x13

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->CORRUPTED_FILE_TRANSFER:Lcom/box/androidsdk/content/BoxException$ErrorType;

    .line 223
    new-instance v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    const-string v1, "OTHER"

    const-string v2, ""

    const/16 v4, 0x14

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/box/androidsdk/content/BoxException$ErrorType;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->OTHER:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v0, 0x15

    .line 138
    new-array v0, v0, [Lcom/box/androidsdk/content/BoxException$ErrorType;

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->INVALID_GRANT_TOKEN_EXPIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->INVALID_GRANT_INVALID_TOKEN:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->ACCOUNT_DEACTIVATED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->ACCESS_DENIED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->INVALID_REQUEST:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->INVALID_CLIENT:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->PASSWORD_RESET_REQUIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->TERMS_OF_SERVICE_REQUIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->NO_CREDIT_CARD_TRIAL_ENDED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->TEMPORARILY_UNAVAILABLE:Lcom/box/androidsdk/content/BoxException$ErrorType;

    aput-object v1, v0, v14

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->SERVICE_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->UNAUTHORIZED_DEVICE:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->GRACE_PERIOD_EXPIRED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->NETWORK_ERROR:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->LOCATION_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->IP_BLOCKED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->UNAUTHORIZED:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->NEW_OWNER_NOT_COLLABORATOR:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->INTERNAL_ERROR:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->CORRUPTED_FILE_TRANSFER:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/BoxException$ErrorType;->OTHER:Lcom/box/androidsdk/content/BoxException$ErrorType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sput-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->$VALUES:[Lcom/box/androidsdk/content/BoxException$ErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 228
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 229
    iput-object p3, p0, Lcom/box/androidsdk/content/BoxException$ErrorType;->mValue:Ljava/lang/String;

    .line 230
    iput p4, p0, Lcom/box/androidsdk/content/BoxException$ErrorType;->mStatusCode:I

    return-void
.end method

.method public static fromErrorInfo(Ljava/lang/String;I)Lcom/box/androidsdk/content/BoxException$ErrorType;
    .locals 5

    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_0

    .line 235
    sget-object p0, Lcom/box/androidsdk/content/BoxException$ErrorType;->INTERNAL_ERROR:Lcom/box/androidsdk/content/BoxException$ErrorType;

    return-object p0

    .line 237
    :cond_0
    invoke-static {}, Lcom/box/androidsdk/content/BoxException$ErrorType;->values()[Lcom/box/androidsdk/content/BoxException$ErrorType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 238
    iget v4, v3, Lcom/box/androidsdk/content/BoxException$ErrorType;->mStatusCode:I

    if-ne v4, p1, :cond_1

    iget-object v4, v3, Lcom/box/androidsdk/content/BoxException$ErrorType;->mValue:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    return-object v3

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 242
    :cond_2
    sget-object p0, Lcom/box/androidsdk/content/BoxException$ErrorType;->OTHER:Lcom/box/androidsdk/content/BoxException$ErrorType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/BoxException$ErrorType;
    .locals 1

    .line 138
    const-class v0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/BoxException$ErrorType;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/BoxException$ErrorType;
    .locals 1

    .line 138
    sget-object v0, Lcom/box/androidsdk/content/BoxException$ErrorType;->$VALUES:[Lcom/box/androidsdk/content/BoxException$ErrorType;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/BoxException$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/BoxException$ErrorType;

    return-object v0
.end method

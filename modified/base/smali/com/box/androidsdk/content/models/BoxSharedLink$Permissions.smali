.class public Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;
.super Lcom/box/androidsdk/content/models/BoxJsonObject;
.source "BoxSharedLink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/models/BoxSharedLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Permissions"
.end annotation


# static fields
.field public static final FIELD_CAN_DOWNLOAD:Ljava/lang/String; = "can_download"

.field private static final FIELD_CAN_PREVIEW:Ljava/lang/String; = "can_preview"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 161
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxJsonObject;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 171
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxJsonObject;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method


# virtual methods
.method public getCanDownload()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "can_download"

    .line 180
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxSharedLink$Permissions;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/box/androidsdk/content/models/BoxFileVersion;
.super Lcom/box/androidsdk/content/models/BoxEntity;
.source "BoxFileVersion.java"


# static fields
.field public static final ALL_FIELDS:[Ljava/lang/String;

.field public static final FIELD_CREATED_AT:Ljava/lang/String; = "created_at"

.field public static final FIELD_DELETED_AT:Ljava/lang/String; = "deleted_at"

.field public static final FIELD_MODIFIED_AT:Ljava/lang/String; = "modified_at"

.field public static final FIELD_MODIFIED_BY:Ljava/lang/String; = "modified_by"

.field public static final FIELD_NAME:Ljava/lang/String; = "name"

.field public static final FIELD_SHA1:Ljava/lang/String; = "sha1"

.field public static final FIELD_SIZE:Ljava/lang/String; = "size"

.field public static final TYPE:Ljava/lang/String; = "file_version"

.field private static final serialVersionUID:J = -0xe11960d65cb510cL


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const-string v0, "name"

    const-string v1, "size"

    const-string v2, "sha1"

    const-string v3, "modified_by"

    const-string v4, "created_at"

    const-string v5, "modified_at"

    const-string v6, "deleted_at"

    .line 28
    filled-new-array/range {v0 .. v6}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/models/BoxFileVersion;->ALL_FIELDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>()V

    return-void
.end method

.method private parseUserInfo(Lcom/eclipsesource/json/JsonObject;)Lcom/box/androidsdk/content/models/BoxUser;
    .locals 1

    .line 109
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxUser;-><init>()V

    .line 110
    invoke-virtual {v0, p1}, Lcom/box/androidsdk/content/models/BoxUser;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    return-object v0
.end method


# virtual methods
.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "created_at"

    .line 60
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFileVersion;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getDeletedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "deleted_at"

    .line 87
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFileVersion;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getModifiedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "modified_at"

    .line 69
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFileVersion;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getModifiedBy()Lcom/box/androidsdk/content/models/BoxUser;
    .locals 2

    .line 105
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator()Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "modified_by"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxFileVersion;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUser;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    .line 51
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFileVersion;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSha1()Ljava/lang/String;
    .locals 1

    const-string v0, "sha1"

    .line 78
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFileVersion;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize()Ljava/lang/Long;
    .locals 1

    const-string v0, "size"

    .line 96
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFileVersion;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.class public final enum Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;
.super Ljava/lang/Enum;
.source "BoxEnterpriseEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/models/BoxEnterpriseEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum ADD_DEVICE_ASSOCIATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum ADMIN_LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum COLLABORATION_ACCEPT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum COLLABORATION_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum COLLABORATION_INVITE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum COLLABORATION_REMOVE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum COLLABORATION_ROLE_CHANGE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum COPY:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum DELETE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum DELETE_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum DOWNLOAD:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum EDIT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum EDIT_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum FAILED_LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum GROUP_ADD_FOLDER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum GROUP_ADD_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum GROUP_CREATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum GROUP_DELETION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum GROUP_EDITED:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum GROUP_REMOVE_FOLDER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum GROUP_REMOVE_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum ITEM_SHARED_UPDATE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum ITEM_SYNC:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum ITEM_UNSYNC:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum LOCK:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum MOVE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum NEW_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum PREVIEW:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum REMOVE_DEVICE_ASSOCIATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum RENAME:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum SHARE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum SHARE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum STORAGE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum TERMS_OF_SERVICE_AGREE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum TERMS_OF_SERVICE_REJECT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum UNDELETE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum UNLOCK:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum UNSHARE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum UPDATE_COLLABORATION_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum UPDATE_SHARE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum UPLOAD:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

.field public static final enum USER_AUTHENTICATE_OAUTH2_TOKEN_REFRESH:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 55
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "GROUP_ADD_USER"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_ADD_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 59
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "NEW_USER"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->NEW_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 63
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "GROUP_CREATION"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_CREATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 67
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "GROUP_DELETION"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_DELETION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 71
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "DELETE_USER"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->DELETE_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 75
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "GROUP_EDITED"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_EDITED:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 79
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "EDIT_USER"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->EDIT_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 83
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "GROUP_ADD_FOLDER"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_ADD_FOLDER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 87
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "GROUP_REMOVE_USER"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_REMOVE_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 91
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "GROUP_REMOVE_FOLDER"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_REMOVE_FOLDER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 95
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "ADMIN_LOGIN"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ADMIN_LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 99
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "ADD_DEVICE_ASSOCIATION"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ADD_DEVICE_ASSOCIATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 103
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "FAILED_LOGIN"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->FAILED_LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 107
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "LOGIN"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 111
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "USER_AUTHENTICATE_OAUTH2_TOKEN_REFRESH"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->USER_AUTHENTICATE_OAUTH2_TOKEN_REFRESH:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 115
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "REMOVE_DEVICE_ASSOCIATION"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->REMOVE_DEVICE_ASSOCIATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 119
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "TERMS_OF_SERVICE_AGREE"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->TERMS_OF_SERVICE_AGREE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 123
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "TERMS_OF_SERVICE_REJECT"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->TERMS_OF_SERVICE_REJECT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 127
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "COPY"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COPY:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 131
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "DELETE"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->DELETE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 135
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "DOWNLOAD"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->DOWNLOAD:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 139
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "EDIT"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->EDIT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 143
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "LOCK"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->LOCK:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 147
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "MOVE"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->MOVE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 151
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "PREVIEW"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->PREVIEW:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 155
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "RENAME"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->RENAME:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 159
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "STORAGE_EXPIRATION"

    const/16 v15, 0x1a

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->STORAGE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 163
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "UNDELETE"

    const/16 v15, 0x1b

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UNDELETE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 167
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "UNLOCK"

    const/16 v15, 0x1c

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UNLOCK:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 171
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "UPLOAD"

    const/16 v15, 0x1d

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UPLOAD:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 175
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "SHARE"

    const/16 v15, 0x1e

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->SHARE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 179
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "ITEM_SHARED_UPDATE"

    const/16 v15, 0x1f

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ITEM_SHARED_UPDATE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 183
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "UPDATE_SHARE_EXPIRATION"

    const/16 v15, 0x20

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UPDATE_SHARE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 187
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "SHARE_EXPIRATION"

    const/16 v15, 0x21

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->SHARE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 191
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "UNSHARE"

    const/16 v15, 0x22

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UNSHARE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 195
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "COLLABORATION_ACCEPT"

    const/16 v15, 0x23

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_ACCEPT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 199
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "COLLABORATION_ROLE_CHANGE"

    const/16 v15, 0x24

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_ROLE_CHANGE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 203
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "UPDATE_COLLABORATION_EXPIRATION"

    const/16 v15, 0x25

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UPDATE_COLLABORATION_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 207
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "COLLABORATION_REMOVE"

    const/16 v15, 0x26

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_REMOVE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 211
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "COLLABORATION_INVITE"

    const/16 v15, 0x27

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_INVITE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 215
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "COLLABORATION_EXPIRATION"

    const/16 v15, 0x28

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 219
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "ITEM_SYNC"

    const/16 v15, 0x29

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ITEM_SYNC:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    .line 223
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const-string v1, "ITEM_UNSYNC"

    const/16 v15, 0x2a

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ITEM_UNSYNC:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v0, 0x2b

    .line 51
    new-array v0, v0, [Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_ADD_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->NEW_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_CREATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_DELETION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->DELETE_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_EDITED:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->EDIT_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_ADD_FOLDER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v9

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_REMOVE_USER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v10

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->GROUP_REMOVE_FOLDER:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v11

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ADMIN_LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v12

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ADD_DEVICE_ASSOCIATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v13

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->FAILED_LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    aput-object v1, v0, v14

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->LOGIN:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->USER_AUTHENTICATE_OAUTH2_TOKEN_REFRESH:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->REMOVE_DEVICE_ASSOCIATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->TERMS_OF_SERVICE_AGREE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->TERMS_OF_SERVICE_REJECT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COPY:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->DELETE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->DOWNLOAD:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->EDIT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->LOCK:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->MOVE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->PREVIEW:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->RENAME:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->STORAGE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UNDELETE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UNLOCK:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UPLOAD:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->SHARE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ITEM_SHARED_UPDATE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UPDATE_SHARE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->SHARE_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UNSHARE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_ACCEPT:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_ROLE_CHANGE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->UPDATE_COLLABORATION_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_REMOVE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_INVITE:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->COLLABORATION_EXPIRATION:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ITEM_SYNC:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->ITEM_UNSYNC:Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->$VALUES:[Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;
    .locals 1

    .line 51
    const-class v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;
    .locals 1

    .line 51
    sget-object v0, Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->$VALUES:[Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/models/BoxEnterpriseEvent$Type;

    return-object v0
.end method

.class public abstract Lcom/box/androidsdk/content/models/BoxCollaborationItem;
.super Lcom/box/androidsdk/content/models/BoxItem;
.source "BoxCollaborationItem.java"


# static fields
.field public static final FIELD_ALLOWED_INVITEE_ROLES:Ljava/lang/String; = "allowed_invitee_roles"

.field public static final FIELD_CAN_NON_OWNERS_INVITE:Ljava/lang/String; = "can_non_owners_invite"

.field public static final FIELD_HAS_COLLABORATIONS:Ljava/lang/String; = "has_collaborations"

.field public static final FIELD_IS_EXTERNALLY_OWNED:Ljava/lang/String; = "is_externally_owned"

.field private static final serialVersionUID:J = 0x43abae8f509b5d16L


# instance fields
.field private transient mCachedAllowedInviteeRoles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/box/androidsdk/content/models/BoxCollaboration$Role;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxItem;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxItem;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method


# virtual methods
.method public getAllowedInviteeRoles()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/box/androidsdk/content/models/BoxCollaboration$Role;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->mCachedAllowedInviteeRoles:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "allowed_invitee_roles"

    .line 67
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getPropertyAsStringArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    .line 71
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->mCachedAllowedInviteeRoles:Ljava/util/ArrayList;

    .line 72
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 73
    iget-object v2, p0, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->mCachedAllowedInviteeRoles:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/box/androidsdk/content/models/BoxCollaboration$Role;->fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxCollaboration$Role;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    :cond_2
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->mCachedAllowedInviteeRoles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCanNonOwnersInvite()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "can_non_owners_invite"

    .line 96
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getHasCollaborations()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "has_collaborations"

    .line 51
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getIsExternallyOwned()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "is_externally_owned"

    .line 86
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.class public final enum Lcom/box/androidsdk/content/models/BoxEvent$Type;
.super Ljava/lang/Enum;
.source "BoxEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/models/BoxEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/models/BoxEvent$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ADD_LOGIN_ACTIVITY_DEVICE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum CHANGE_ADMIN_ROLE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum COLLAB_ADD_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum COLLAB_INVITE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum COLLAB_REMOVE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum COLLAB_ROLE_CHANGE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum COMMENT_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_COPY:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_DOWNLOAD:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_MOVE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_PREVIEW:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_RENAME:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_SHARED:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_SHARED_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_SHARED_UNSHARE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_SYNC:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_TRASH:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_UNDELETE_VIA_TRASH:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_UNSYNC:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum ITEM_UPLOAD:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum LOCK_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum LOCK_DESTROY:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum REMOVE_LOGIN_ACTIVITY_DEVICE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum TAG_ITEM_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

.field public static final enum TASK_ASSIGNMENT_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 148
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_CREATE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 153
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_UPLOAD"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_UPLOAD:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 158
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "COMMENT_CREATE"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v4}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COMMENT_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 163
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_DOWNLOAD"

    const/4 v5, 0x3

    invoke-direct {v0, v1, v5}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_DOWNLOAD:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 168
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_PREVIEW"

    const/4 v6, 0x4

    invoke-direct {v0, v1, v6}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_PREVIEW:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 173
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_MOVE"

    const/4 v7, 0x5

    invoke-direct {v0, v1, v7}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_MOVE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 178
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_COPY"

    const/4 v8, 0x6

    invoke-direct {v0, v1, v8}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_COPY:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 183
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "TASK_ASSIGNMENT_CREATE"

    const/4 v9, 0x7

    invoke-direct {v0, v1, v9}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->TASK_ASSIGNMENT_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 188
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "LOCK_CREATE"

    const/16 v10, 0x8

    invoke-direct {v0, v1, v10}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->LOCK_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 193
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "LOCK_DESTROY"

    const/16 v11, 0x9

    invoke-direct {v0, v1, v11}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->LOCK_DESTROY:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 198
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_TRASH"

    const/16 v12, 0xa

    invoke-direct {v0, v1, v12}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_TRASH:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 203
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_UNDELETE_VIA_TRASH"

    const/16 v13, 0xb

    invoke-direct {v0, v1, v13}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_UNDELETE_VIA_TRASH:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 208
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "COLLAB_ADD_COLLABORATOR"

    const/16 v14, 0xc

    invoke-direct {v0, v1, v14}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COLLAB_ADD_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 213
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "COLLAB_REMOVE_COLLABORATOR"

    const/16 v15, 0xd

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COLLAB_REMOVE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 218
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "COLLAB_INVITE_COLLABORATOR"

    const/16 v15, 0xe

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COLLAB_INVITE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 223
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "COLLAB_ROLE_CHANGE"

    const/16 v15, 0xf

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COLLAB_ROLE_CHANGE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 228
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_SYNC"

    const/16 v15, 0x10

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_SYNC:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 233
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_UNSYNC"

    const/16 v15, 0x11

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_UNSYNC:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 238
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_RENAME"

    const/16 v15, 0x12

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_RENAME:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 243
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_SHARED_CREATE"

    const/16 v15, 0x13

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_SHARED_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 248
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_SHARED_UNSHARE"

    const/16 v15, 0x14

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_SHARED_UNSHARE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 253
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ITEM_SHARED"

    const/16 v15, 0x15

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_SHARED:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 258
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "TAG_ITEM_CREATE"

    const/16 v15, 0x16

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->TAG_ITEM_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 263
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "ADD_LOGIN_ACTIVITY_DEVICE"

    const/16 v15, 0x17

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ADD_LOGIN_ACTIVITY_DEVICE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 268
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "REMOVE_LOGIN_ACTIVITY_DEVICE"

    const/16 v15, 0x18

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->REMOVE_LOGIN_ACTIVITY_DEVICE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    .line 273
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const-string v1, "CHANGE_ADMIN_ROLE"

    const/16 v15, 0x19

    invoke-direct {v0, v1, v15}, Lcom/box/androidsdk/content/models/BoxEvent$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->CHANGE_ADMIN_ROLE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v0, 0x1a

    .line 143
    new-array v0, v0, [Lcom/box/androidsdk/content/models/BoxEvent$Type;

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_UPLOAD:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COMMENT_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_DOWNLOAD:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_PREVIEW:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_MOVE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_COPY:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->TASK_ASSIGNMENT_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v9

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->LOCK_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v10

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->LOCK_DESTROY:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v11

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_TRASH:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v12

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_UNDELETE_VIA_TRASH:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v13

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COLLAB_ADD_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    aput-object v1, v0, v14

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COLLAB_REMOVE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COLLAB_INVITE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->COLLAB_ROLE_CHANGE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_SYNC:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_UNSYNC:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_RENAME:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_SHARED_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_SHARED_UNSHARE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ITEM_SHARED:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->TAG_ITEM_CREATE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->ADD_LOGIN_ACTIVITY_DEVICE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->REMOVE_LOGIN_ACTIVITY_DEVICE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxEvent$Type;->CHANGE_ADMIN_ROLE:Lcom/box/androidsdk/content/models/BoxEvent$Type;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sput-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->$VALUES:[Lcom/box/androidsdk/content/models/BoxEvent$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 143
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxEvent$Type;
    .locals 1

    .line 143
    const-class v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/models/BoxEvent$Type;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/models/BoxEvent$Type;
    .locals 1

    .line 143
    sget-object v0, Lcom/box/androidsdk/content/models/BoxEvent$Type;->$VALUES:[Lcom/box/androidsdk/content/models/BoxEvent$Type;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/models/BoxEvent$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/models/BoxEvent$Type;

    return-object v0
.end method

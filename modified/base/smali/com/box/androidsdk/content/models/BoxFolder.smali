.class public Lcom/box/androidsdk/content/models/BoxFolder;
.super Lcom/box/androidsdk/content/models/BoxCollaborationItem;
.source "BoxFolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/models/BoxFolder$SyncState;
    }
.end annotation


# static fields
.field public static final ALL_FIELDS:[Ljava/lang/String;

.field public static final FIELD_CONTENT_CREATED_AT:Ljava/lang/String; = "content_created_at"

.field public static final FIELD_CONTENT_MODIFIED_AT:Ljava/lang/String; = "content_modified_at"

.field public static final FIELD_FOLDER_UPLOAD_EMAIL:Ljava/lang/String; = "folder_upload_email"

.field public static final FIELD_ITEM_COLLECTION:Ljava/lang/String; = "item_collection"

.field public static final FIELD_SHA1:Ljava/lang/String; = "sha1"

.field public static final FIELD_SIZE:Ljava/lang/String; = "size"

.field public static final FIELD_SYNC_STATE:Ljava/lang/String; = "sync_state"

.field public static final TYPE:Ljava/lang/String; = "folder"

.field private static final serialVersionUID:J = 0x6f4d06761d67ca4eL


# instance fields
.field private transient mCachedAccessLevels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/box/androidsdk/content/models/BoxSharedLink$Access;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 30

    const-string v0, "type"

    const-string v1, "sha1"

    const-string v2, "id"

    const-string v3, "sequence_id"

    const-string v4, "etag"

    const-string v5, "name"

    const-string v6, "created_at"

    const-string v7, "modified_at"

    const-string v8, "description"

    const-string v9, "size"

    const-string v10, "path_collection"

    const-string v11, "created_by"

    const-string v12, "modified_by"

    const-string v13, "trashed_at"

    const-string v14, "purged_at"

    const-string v15, "content_created_at"

    const-string v16, "content_modified_at"

    const-string v17, "owned_by"

    const-string v18, "shared_link"

    const-string v19, "folder_upload_email"

    const-string v20, "parent"

    const-string v21, "item_status"

    const-string v22, "item_collection"

    const-string v23, "sync_state"

    const-string v24, "has_collaborations"

    const-string v25, "permissions"

    const-string v26, "can_non_owners_invite"

    const-string v27, "is_externally_owned"

    const-string v28, "allowed_invitee_roles"

    const-string v29, "collections"

    .line 38
    filled-new-array/range {v0 .. v29}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/models/BoxFolder;->ALL_FIELDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 76
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 85
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method

.method public static createFromId(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFolder;
    .locals 1

    const/4 v0, 0x0

    .line 96
    invoke-static {p0, v0}, Lcom/box/androidsdk/content/models/BoxFolder;->createFromIdAndName(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFolder;

    move-result-object p0

    return-object p0
.end method

.method public static createFromIdAndName(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFolder;
    .locals 2

    .line 108
    new-instance v0, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    const-string v1, "id"

    .line 109
    invoke-virtual {v0, v1, p0}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    const-string p0, "type"

    const-string v1, "folder"

    .line 110
    invoke-virtual {v0, p0, v1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 111
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "name"

    .line 112
    invoke-virtual {v0, p0, p1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 114
    :cond_0
    new-instance p0, Lcom/box/androidsdk/content/models/BoxFolder;

    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/models/BoxFolder;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-object p0
.end method


# virtual methods
.method public getAllowedSharedLinkAccessLevels()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/box/androidsdk/content/models/BoxSharedLink$Access;",
            ">;"
        }
    .end annotation

    .line 153
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxFolder;->mCachedAccessLevels:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "allowed_shared_link_access_levels"

    .line 156
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFolder;->getPropertyAsStringArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    return-object v0

    .line 160
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/box/androidsdk/content/models/BoxFolder;->mCachedAccessLevels:Ljava/util/ArrayList;

    .line 161
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 162
    iget-object v2, p0, Lcom/box/androidsdk/content/models/BoxFolder;->mCachedAccessLevels:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxFolder;->mCachedAccessLevels:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getContentCreatedAt()Ljava/util/Date;
    .locals 1

    .line 170
    invoke-super {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getContentCreatedAt()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getContentModifiedAt()Ljava/util/Date;
    .locals 1

    .line 180
    invoke-super {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getContentModifiedAt()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getItemCollection()Lcom/box/androidsdk/content/models/BoxIteratorItems;
    .locals 2

    .line 141
    const-class v0, Lcom/box/androidsdk/content/models/BoxIteratorItems;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxJsonObject;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "item_collection"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxFolder;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxIteratorItems;

    return-object v0
.end method

.method public getSize()Ljava/lang/Long;
    .locals 1

    .line 175
    invoke-super {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getSize()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getSyncState()Lcom/box/androidsdk/content/models/BoxFolder$SyncState;
    .locals 1

    const-string v0, "sync_state"

    .line 132
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFolder;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxFolder$SyncState;->fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFolder$SyncState;

    move-result-object v0

    return-object v0
.end method

.method public getUploadEmail()Lcom/box/androidsdk/content/models/BoxUploadEmail;
    .locals 2

    .line 123
    const-class v0, Lcom/box/androidsdk/content/models/BoxUploadEmail;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "folder_upload_email"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxFolder;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUploadEmail;

    return-object v0
.end method

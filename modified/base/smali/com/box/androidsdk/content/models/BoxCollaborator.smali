.class public abstract Lcom/box/androidsdk/content/models/BoxCollaborator;
.super Lcom/box/androidsdk/content/models/BoxEntity;
.source "BoxCollaborator.java"


# static fields
.field public static final FIELD_CREATED_AT:Ljava/lang/String; = "created_at"

.field public static final FIELD_MODIFIED_AT:Ljava/lang/String; = "modified_at"

.field public static final FIELD_NAME:Ljava/lang/String; = "name"

.field private static final serialVersionUID:J = 0x455385a835bc4697L


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method


# virtual methods
.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "created_at"

    .line 52
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaborator;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getModifiedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "modified_at"

    .line 61
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaborator;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    .line 43
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaborator;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

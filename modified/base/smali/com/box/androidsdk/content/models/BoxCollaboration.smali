.class public Lcom/box/androidsdk/content/models/BoxCollaboration;
.super Lcom/box/androidsdk/content/models/BoxEntity;
.source "BoxCollaboration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/models/BoxCollaboration$Role;,
        Lcom/box/androidsdk/content/models/BoxCollaboration$Status;
    }
.end annotation


# static fields
.field public static final ALL_FIELDS:[Ljava/lang/String;

.field public static final FIELD_ACCESSIBLE_BY:Ljava/lang/String; = "accessible_by"

.field public static final FIELD_ACKNOWLEDGED_AT:Ljava/lang/String; = "acknowledged_at"

.field public static final FIELD_CREATED_AT:Ljava/lang/String; = "created_at"

.field public static final FIELD_CREATED_BY:Ljava/lang/String; = "created_by"

.field public static final FIELD_EXPIRES_AT:Ljava/lang/String; = "expires_at"

.field public static final FIELD_ITEM:Ljava/lang/String; = "item"

.field public static final FIELD_MODIFIED_AT:Ljava/lang/String; = "modified_at"

.field public static final FIELD_ROLE:Ljava/lang/String; = "role"

.field public static final FIELD_STATUS:Ljava/lang/String; = "status"

.field public static final TYPE:Ljava/lang/String; = "collaboration"

.field private static final serialVersionUID:J = 0x70c53a24a2833d03L


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const-string v0, "type"

    const-string v1, "id"

    const-string v2, "created_by"

    const-string v3, "created_at"

    const-string v4, "modified_at"

    const-string v5, "expires_at"

    const-string v6, "status"

    const-string v7, "accessible_by"

    const-string v8, "role"

    const-string v9, "acknowledged_at"

    const-string v10, "item"

    .line 28
    filled-new-array/range {v0 .. v10}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/models/BoxCollaboration;->ALL_FIELDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 55
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method


# virtual methods
.method public getAccessibleBy()Lcom/box/androidsdk/content/models/BoxCollaborator;
    .locals 2

    .line 109
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator()Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "accessible_by"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxCollaborator;

    return-object v0
.end method

.method public getAcknowledgedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "acknowledged_at"

    .line 127
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "created_at"

    .line 73
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getCreatedBy()Lcom/box/androidsdk/content/models/BoxCollaborator;
    .locals 2

    .line 64
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator()Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "created_by"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxCollaborator;

    return-object v0
.end method

.method public getExpiresAt()Ljava/util/Date;
    .locals 1

    const-string v0, "expires_at"

    .line 91
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getItem()Lcom/box/androidsdk/content/models/BoxCollaborationItem;
    .locals 2

    .line 136
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator()Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "item"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxCollaborationItem;

    return-object v0
.end method

.method public getModifiedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "modified_at"

    .line 82
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getRole()Lcom/box/androidsdk/content/models/BoxCollaboration$Role;
    .locals 1

    const-string v0, "role"

    .line 118
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxCollaboration$Role;->fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxCollaboration$Role;

    move-result-object v0

    return-object v0
.end method

.method public getStatus()Lcom/box/androidsdk/content/models/BoxCollaboration$Status;
    .locals 1

    const-string v0, "status"

    .line 100
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxCollaboration;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxCollaboration$Status;->fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxCollaboration$Status;

    move-result-object v0

    return-object v0
.end method

.class public abstract Lcom/box/androidsdk/content/models/BoxItem;
.super Lcom/box/androidsdk/content/models/BoxEntity;
.source "BoxItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/models/BoxItem$Permission;
    }
.end annotation


# static fields
.field public static final FIELD_ALLOWED_SHARED_LINK_ACCESS_LEVELS:Ljava/lang/String; = "allowed_shared_link_access_levels"

.field public static final FIELD_COLLECTIONS:Ljava/lang/String; = "collections"

.field public static final FIELD_CREATED_AT:Ljava/lang/String; = "created_at"

.field public static final FIELD_CREATED_BY:Ljava/lang/String; = "created_by"

.field public static final FIELD_DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FIELD_ETAG:Ljava/lang/String; = "etag"

.field public static final FIELD_ITEM_STATUS:Ljava/lang/String; = "item_status"

.field public static final FIELD_MODIFIED_AT:Ljava/lang/String; = "modified_at"

.field public static final FIELD_MODIFIED_BY:Ljava/lang/String; = "modified_by"

.field public static final FIELD_NAME:Ljava/lang/String; = "name"

.field public static final FIELD_OWNED_BY:Ljava/lang/String; = "owned_by"

.field public static final FIELD_PARENT:Ljava/lang/String; = "parent"

.field public static final FIELD_PATH_COLLECTION:Ljava/lang/String; = "path_collection"

.field public static final FIELD_PERMISSIONS:Ljava/lang/String; = "permissions"

.field public static final FIELD_PURGED_AT:Ljava/lang/String; = "purged_at"

.field public static final FIELD_SEQUENCE_ID:Ljava/lang/String; = "sequence_id"

.field public static final FIELD_SHARED_LINK:Ljava/lang/String; = "shared_link"

.field public static final FIELD_SYNCED:Ljava/lang/String; = "synced"

.field public static final FIELD_TAGS:Ljava/lang/String; = "tags"

.field public static final FIELD_TRASHED_AT:Ljava/lang/String; = "trashed_at"

.field private static final serialVersionUID:J = 0x43abae8f5de612d6L


# instance fields
.field protected transient mPermissions:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet<",
            "Lcom/box/androidsdk/content/models/BoxItem$Permission;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 51
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>()V

    const/4 v0, 0x0

    .line 45
    iput-object v0, p0, Lcom/box/androidsdk/content/models/BoxItem;->mPermissions:Ljava/util/EnumSet;

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    const/4 p1, 0x0

    .line 45
    iput-object p1, p0, Lcom/box/androidsdk/content/models/BoxItem;->mPermissions:Ljava/util/EnumSet;

    return-void
.end method

.method public static createBoxItemFromJson(Lcom/eclipsesource/json/JsonObject;)Lcom/box/androidsdk/content/models/BoxItem;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 343
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEntity;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>()V

    .line 344
    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/models/BoxEntity;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    .line 345
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 346
    new-instance v0, Lcom/box/androidsdk/content/models/BoxFile;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxFile;-><init>()V

    .line 347
    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/models/BoxFile;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    return-object v0

    .line 349
    :cond_0
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "web_link"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 350
    new-instance v0, Lcom/box/androidsdk/content/models/BoxBookmark;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxBookmark;-><init>()V

    .line 351
    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/models/BoxBookmark;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    return-object v0

    .line 353
    :cond_1
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    new-instance v0, Lcom/box/androidsdk/content/models/BoxFolder;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxFolder;-><init>()V

    .line 355
    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/models/BoxFolder;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    return-object v0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method public static createBoxItemFromJson(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxItem;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 316
    new-instance v0, Lcom/box/androidsdk/content/models/BoxEntity;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>()V

    .line 317
    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/models/BoxEntity;->createFromJson(Ljava/lang/String;)V

    .line 318
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    new-instance v0, Lcom/box/androidsdk/content/models/BoxFile;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxFile;-><init>()V

    .line 320
    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/models/BoxFile;->createFromJson(Ljava/lang/String;)V

    return-object v0

    .line 322
    :cond_0
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "web_link"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 323
    new-instance v0, Lcom/box/androidsdk/content/models/BoxBookmark;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxBookmark;-><init>()V

    .line 324
    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/models/BoxBookmark;->createFromJson(Ljava/lang/String;)V

    return-object v0

    .line 326
    :cond_1
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 327
    new-instance v0, Lcom/box/androidsdk/content/models/BoxFolder;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxFolder;-><init>()V

    .line 328
    invoke-virtual {v0, p0}, Lcom/box/androidsdk/content/models/BoxFolder;->createFromJson(Ljava/lang/String;)V

    return-object v0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method private parsePathCollection(Lcom/eclipsesource/json/JsonObject;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/eclipsesource/json/JsonObject;",
            ")",
            "Ljava/util/List<",
            "Lcom/box/androidsdk/content/models/BoxFolder;",
            ">;"
        }
    .end annotation

    const-string v0, "total_count"

    .line 280
    invoke-virtual {p1, v0}, Lcom/eclipsesource/json/JsonObject;->get(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/eclipsesource/json/JsonValue;->asInt()I

    move-result v0

    .line 281
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const-string v0, "entries"

    .line 282
    invoke-virtual {p1, v0}, Lcom/eclipsesource/json/JsonObject;->get(Ljava/lang/String;)Lcom/eclipsesource/json/JsonValue;

    move-result-object p1

    invoke-virtual {p1}, Lcom/eclipsesource/json/JsonValue;->asArray()Lcom/eclipsesource/json/JsonArray;

    move-result-object p1

    .line 283
    invoke-virtual {p1}, Lcom/eclipsesource/json/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/eclipsesource/json/JsonValue;

    .line 284
    invoke-virtual {v0}, Lcom/eclipsesource/json/JsonValue;->asObject()Lcom/eclipsesource/json/JsonObject;

    move-result-object v0

    .line 285
    new-instance v2, Lcom/box/androidsdk/content/models/BoxFolder;

    invoke-direct {v2}, Lcom/box/androidsdk/content/models/BoxFolder;-><init>()V

    .line 286
    invoke-virtual {v2, v0}, Lcom/box/androidsdk/content/models/BoxFolder;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    .line 287
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private parseTags(Lcom/eclipsesource/json/JsonArray;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/eclipsesource/json/JsonArray;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 301
    invoke-virtual {p1}, Lcom/eclipsesource/json/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/eclipsesource/json/JsonValue;

    .line 302
    invoke-virtual {v1}, Lcom/eclipsesource/json/JsonValue;->asString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private parseUserInfo(Lcom/eclipsesource/json/JsonObject;)Lcom/box/androidsdk/content/models/BoxUser;
    .locals 1

    .line 294
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser;

    invoke-direct {v0}, Lcom/box/androidsdk/content/models/BoxUser;-><init>()V

    .line 295
    invoke-virtual {v0, p1}, Lcom/box/androidsdk/content/models/BoxUser;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    return-object v0
.end method


# virtual methods
.method public getAllowedSharedLinkAccessLevels()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/box/androidsdk/content/models/BoxSharedLink$Access;",
            ">;"
        }
    .end annotation

    const-string v0, "allowed_shared_link_access_levels"

    .line 214
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsStringArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 218
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 219
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 220
    invoke-static {v2}, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getCollections()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/box/androidsdk/content/models/BoxCollection;",
            ">;"
        }
    .end annotation

    .line 267
    const-class v0, Lcom/box/androidsdk/content/models/BoxCollection;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "collections"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsJsonObjectArray(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected getCommentCount()Ljava/lang/Long;
    .locals 1

    const-string v0, "comment_count"

    .line 276
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getContentCreatedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "content_created_at"

    .line 168
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method protected getContentModifiedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "content_modified_at"

    .line 177
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "created_at"

    .line 87
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getCreatedBy()Lcom/box/androidsdk/content/models/BoxUser;
    .locals 2

    .line 132
    const-class v0, Lcom/box/androidsdk/content/models/BoxUser;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "created_by"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUser;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    const-string v0, "description"

    .line 105
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    const-string v0, "etag"

    .line 69
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIsSynced()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "synced"

    .line 249
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getItemStatus()Ljava/lang/String;
    .locals 1

    const-string v0, "item_status"

    .line 240
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModifiedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "modified_at"

    .line 96
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getModifiedBy()Lcom/box/androidsdk/content/models/BoxUser;
    .locals 2

    .line 141
    const-class v0, Lcom/box/androidsdk/content/models/BoxUser;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "modified_by"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUser;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "name"

    .line 78
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOwnedBy()Lcom/box/androidsdk/content/models/BoxUser;
    .locals 2

    .line 186
    const-class v0, Lcom/box/androidsdk/content/models/BoxUser;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "owned_by"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUser;

    return-object v0
.end method

.method public getParent()Lcom/box/androidsdk/content/models/BoxFolder;
    .locals 2

    .line 231
    const-class v0, Lcom/box/androidsdk/content/models/BoxFolder;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "parent"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxFolder;

    return-object v0
.end method

.method public getPathCollection()Lcom/box/androidsdk/content/models/BoxIterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/box/androidsdk/content/models/BoxIterator<",
            "Lcom/box/androidsdk/content/models/BoxFolder;",
            ">;"
        }
    .end annotation

    .line 123
    const-class v0, Lcom/box/androidsdk/content/models/BoxIteratorBoxEntity;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxJsonObject;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "path_collection"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxIterator;

    return-object v0
.end method

.method public getPermissions()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/box/androidsdk/content/models/BoxItem$Permission;",
            ">;"
        }
    .end annotation

    .line 368
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxItem;->mPermissions:Ljava/util/EnumSet;

    if-nez v0, :cond_0

    .line 369
    invoke-virtual {p0}, Lcom/box/androidsdk/content/models/BoxItem;->parsePermissions()Ljava/util/EnumSet;

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxItem;->mPermissions:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getPurgedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "purged_at"

    .line 159
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getSequenceID()Ljava/lang/String;
    .locals 1

    const-string v0, "sequence_id"

    .line 204
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSharedLink()Lcom/box/androidsdk/content/models/BoxSharedLink;
    .locals 2

    .line 195
    const-class v0, Lcom/box/androidsdk/content/models/BoxSharedLink;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "shared_link"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxSharedLink;

    return-object v0
.end method

.method public getSize()Ljava/lang/Long;
    .locals 1

    const-string v0, "size"

    .line 114
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getTags()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "tags"

    .line 258
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsStringArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getTrashedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "trashed_at"

    .line 150
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method protected parsePermissions()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet<",
            "Lcom/box/androidsdk/content/models/BoxItem$Permission;",
            ">;"
        }
    .end annotation

    .line 375
    const-class v0, Lcom/box/androidsdk/content/models/BoxPermission;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "permissions"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxItem;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxPermission;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 379
    :cond_0
    invoke-virtual {v0}, Lcom/box/androidsdk/content/models/BoxPermission;->getPermissions()Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/box/androidsdk/content/models/BoxItem;->mPermissions:Ljava/util/EnumSet;

    .line 380
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxItem;->mPermissions:Ljava/util/EnumSet;

    return-object v0
.end method

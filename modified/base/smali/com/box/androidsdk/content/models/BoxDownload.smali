.class public Lcom/box/androidsdk/content/models/BoxDownload;
.super Lcom/box/androidsdk/content/models/BoxJsonObject;
.source "BoxDownload.java"


# static fields
.field private static final FIELD_CONTENT_LENGTH:Ljava/lang/String; = "content_length"

.field private static final FIELD_CONTENT_TYPE:Ljava/lang/String; = "content_type"

.field private static final FIELD_DATE:Ljava/lang/String; = "date"

.field private static final FIELD_END_RANGE:Ljava/lang/String; = "end_range"

.field private static final FIELD_EXPIRATION:Ljava/lang/String; = "expiration"

.field private static final FIELD_FILE_NAME:Ljava/lang/String; = "file_name"

.field private static final FIELD_START_RANGE:Ljava/lang/String; = "start_range"

.field private static final FIELD_TOTAL_RANGE:Ljava/lang/String; = "total_range"


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 34
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxJsonObject;-><init>()V

    .line 35
    invoke-static {p1}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    invoke-virtual {p0, p1}, Lcom/box/androidsdk/content/models/BoxDownload;->setFileName(Ljava/lang/String;)V

    :cond_0
    const-string p1, "content_length"

    .line 38
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/box/androidsdk/content/models/BoxDownload;->set(Ljava/lang/String;Ljava/lang/Long;)V

    .line 39
    invoke-static {p4}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    const-string p1, "content_type"

    .line 40
    invoke-virtual {p0, p1, p4}, Lcom/box/androidsdk/content/models/BoxDownload;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_1
    invoke-static {p5}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 43
    invoke-virtual {p0, p5}, Lcom/box/androidsdk/content/models/BoxDownload;->setContentRange(Ljava/lang/String;)V

    .line 45
    :cond_2
    invoke-static {p6}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    const-string p1, "date"

    .line 46
    invoke-virtual {p0, p1, p6}, Lcom/box/androidsdk/content/models/BoxDownload;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_3
    invoke-static {p7}, Lcom/box/androidsdk/content/utils/SdkUtils;->isEmptyString(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_4

    const-string p1, "expiration"

    .line 49
    invoke-virtual {p0, p1, p7}, Lcom/box/androidsdk/content/models/BoxDownload;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method private static final parseDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 0

    .line 160
    :try_start_0
    invoke-static {p0}, Lcom/box/androidsdk/content/utils/BoxDateFormat;->parseHeaderDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public getContentLength()Ljava/lang/Long;
    .locals 1

    const-string v0, "content_length"

    .line 105
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxDownload;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    const-string v0, "content_type"

    .line 114
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxDownload;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDate()Ljava/util/Date;
    .locals 1

    const-string v0, "date"

    .line 146
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxDownload;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxDownload;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getEndRange()Ljava/lang/Long;
    .locals 1

    const-string v0, "end_range"

    .line 130
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxDownload;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getExpiration()Ljava/util/Date;
    .locals 1

    const-string v0, "expiration"

    .line 154
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxDownload;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxDownload;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    const-string v0, "file_name"

    .line 87
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxDownload;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOutputFile()Ljava/io/File;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getStartRange()Ljava/lang/Long;
    .locals 1

    const-string v0, "start_range"

    .line 122
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxDownload;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getTotalRange()Ljava/lang/Long;
    .locals 1

    const-string v0, "total_range"

    .line 138
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxDownload;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected setContentRange(Ljava/lang/String;)V
    .locals 6

    const-string v0, "/"

    .line 71
    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const-string v1, "-"

    .line 72
    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const-string v2, "bytes"

    .line 73
    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    const-string v3, "start_range"

    add-int/lit8 v2, v2, 0x6

    .line 75
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0, v3, v2}, Lcom/box/androidsdk/content/models/BoxDownload;->set(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "end_range"

    add-int/lit8 v1, v1, 0x1

    .line 76
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/box/androidsdk/content/models/BoxDownload;->set(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "total_range"

    add-int/lit8 v0, v0, 0x1

    .line 77
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Lcom/box/androidsdk/content/models/BoxDownload;->set(Ljava/lang/String;Ljava/lang/Long;)V

    return-void
.end method

.method protected setFileName(Ljava/lang/String;)V
    .locals 5

    const-string v0, ";"

    .line 54
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 56
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p1, v1

    .line 57
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, "filename="

    .line 58
    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "\""

    .line 59
    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "\""

    .line 60
    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_0
    const/16 v3, 0x9

    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    const-string v3, "file_name"

    .line 64
    invoke-virtual {p0, v3, v2}, Lcom/box/androidsdk/content/models/BoxDownload;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.class public Lcom/box/androidsdk/content/models/BoxOrder;
.super Lcom/box/androidsdk/content/models/BoxJsonObject;
.source "BoxOrder.java"


# static fields
.field public static final FIELD_BY:Ljava/lang/String; = "by"

.field public static final FIELD_DIRECTION:Ljava/lang/String; = "direction"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxJsonObject;-><init>()V

    return-void
.end method


# virtual methods
.method public getBy()Ljava/lang/String;
    .locals 1

    const-string v0, "by"

    .line 15
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxOrder;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDirection()Ljava/lang/String;
    .locals 1

    const-string v0, "direction"

    .line 19
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxOrder;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

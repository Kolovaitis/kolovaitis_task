.class public final enum Lcom/box/androidsdk/content/models/BoxItem$Permission;
.super Ljava/lang/Enum;
.source "BoxItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/models/BoxItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Permission"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/models/BoxItem$Permission;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_COMMENT:Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_DELETE:Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_DOWNLOAD:Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_INVITE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_PREVIEW:Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_RENAME:Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_SET_SHARE_ACCESS:Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_SHARE:Lcom/box/androidsdk/content/models/BoxItem$Permission;

.field public static final enum CAN_UPLOAD:Lcom/box/androidsdk/content/models/BoxItem$Permission;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 391
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_PREVIEW"

    const-string v2, "can_preview"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_PREVIEW:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    .line 396
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_DOWNLOAD"

    const-string v2, "can_download"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_DOWNLOAD:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    .line 401
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_UPLOAD"

    const-string v2, "can_upload"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_UPLOAD:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    .line 406
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_INVITE_COLLABORATOR"

    const-string v2, "can_invite_collaborator"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_INVITE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    .line 411
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_RENAME"

    const-string v2, "can_rename"

    const/4 v7, 0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_RENAME:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    .line 416
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_DELETE"

    const-string v2, "can_delete"

    const/4 v8, 0x5

    invoke-direct {v0, v1, v8, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_DELETE:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    .line 421
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_SHARE"

    const-string v2, "can_share"

    const/4 v9, 0x6

    invoke-direct {v0, v1, v9, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_SHARE:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    .line 426
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_SET_SHARE_ACCESS"

    const-string v2, "can_set_share_access"

    const/4 v10, 0x7

    invoke-direct {v0, v1, v10, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_SET_SHARE_ACCESS:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    .line 431
    new-instance v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const-string v1, "CAN_COMMENT"

    const-string v2, "can_comment"

    const/16 v11, 0x8

    invoke-direct {v0, v1, v11, v2}, Lcom/box/androidsdk/content/models/BoxItem$Permission;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_COMMENT:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    const/16 v0, 0x9

    .line 386
    new-array v0, v0, [Lcom/box/androidsdk/content/models/BoxItem$Permission;

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_PREVIEW:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v3

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_DOWNLOAD:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v4

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_UPLOAD:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v5

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_INVITE_COLLABORATOR:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v6

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_RENAME:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v7

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_DELETE:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v8

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_SHARE:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v9

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_SET_SHARE_ACCESS:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v10

    sget-object v1, Lcom/box/androidsdk/content/models/BoxItem$Permission;->CAN_COMMENT:Lcom/box/androidsdk/content/models/BoxItem$Permission;

    aput-object v1, v0, v11

    sput-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->$VALUES:[Lcom/box/androidsdk/content/models/BoxItem$Permission;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 435
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 436
    iput-object p3, p0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxItem$Permission;
    .locals 6

    .line 440
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 441
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxItem$Permission;->values()[Lcom/box/androidsdk/content/models/BoxItem$Permission;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 442
    invoke-virtual {v4}, Lcom/box/androidsdk/content/models/BoxItem$Permission;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 447
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v1

    const-string p0, "No enum with text %s found"

    invoke-static {v2, p0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxItem$Permission;
    .locals 1

    .line 386
    const-class v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/models/BoxItem$Permission;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/models/BoxItem$Permission;
    .locals 1

    .line 386
    sget-object v0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->$VALUES:[Lcom/box/androidsdk/content/models/BoxItem$Permission;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/models/BoxItem$Permission;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/models/BoxItem$Permission;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 452
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxItem$Permission;->value:Ljava/lang/String;

    return-object v0
.end method

.class public final enum Lcom/box/androidsdk/content/models/BoxUser$Status;
.super Ljava/lang/Enum;
.source "BoxUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/models/BoxUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/models/BoxUser$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/models/BoxUser$Status;

.field public static final enum ACTIVE:Lcom/box/androidsdk/content/models/BoxUser$Status;

.field public static final enum CANNOT_DELETE_EDIT:Lcom/box/androidsdk/content/models/BoxUser$Status;

.field public static final enum CANNOT_DELETE_EDIT_UPLOAD:Lcom/box/androidsdk/content/models/BoxUser$Status;

.field public static final enum INACTIVE:Lcom/box/androidsdk/content/models/BoxUser$Status;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 341
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser$Status;

    const-string v1, "ACTIVE"

    const-string v2, "active"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/box/androidsdk/content/models/BoxUser$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Status;->ACTIVE:Lcom/box/androidsdk/content/models/BoxUser$Status;

    .line 346
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser$Status;

    const-string v1, "INACTIVE"

    const-string v2, "inactive"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/box/androidsdk/content/models/BoxUser$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Status;->INACTIVE:Lcom/box/androidsdk/content/models/BoxUser$Status;

    .line 351
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser$Status;

    const-string v1, "CANNOT_DELETE_EDIT"

    const-string v2, "cannot_delete_edit"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/box/androidsdk/content/models/BoxUser$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Status;->CANNOT_DELETE_EDIT:Lcom/box/androidsdk/content/models/BoxUser$Status;

    .line 356
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser$Status;

    const-string v1, "CANNOT_DELETE_EDIT_UPLOAD"

    const-string v2, "cannot_delete_edit_upload"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/box/androidsdk/content/models/BoxUser$Status;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Status;->CANNOT_DELETE_EDIT_UPLOAD:Lcom/box/androidsdk/content/models/BoxUser$Status;

    const/4 v0, 0x4

    .line 337
    new-array v0, v0, [Lcom/box/androidsdk/content/models/BoxUser$Status;

    sget-object v1, Lcom/box/androidsdk/content/models/BoxUser$Status;->ACTIVE:Lcom/box/androidsdk/content/models/BoxUser$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/box/androidsdk/content/models/BoxUser$Status;->INACTIVE:Lcom/box/androidsdk/content/models/BoxUser$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/box/androidsdk/content/models/BoxUser$Status;->CANNOT_DELETE_EDIT:Lcom/box/androidsdk/content/models/BoxUser$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/box/androidsdk/content/models/BoxUser$Status;->CANNOT_DELETE_EDIT_UPLOAD:Lcom/box/androidsdk/content/models/BoxUser$Status;

    aput-object v1, v0, v6

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Status;->$VALUES:[Lcom/box/androidsdk/content/models/BoxUser$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 360
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 361
    iput-object p3, p0, Lcom/box/androidsdk/content/models/BoxUser$Status;->mValue:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxUser$Status;
    .locals 6

    .line 365
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 366
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxUser$Status;->values()[Lcom/box/androidsdk/content/models/BoxUser$Status;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 367
    invoke-virtual {v4}, Lcom/box/androidsdk/content/models/BoxUser$Status;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 372
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v1

    const-string p0, "No enum with text %s found"

    invoke-static {v2, p0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxUser$Status;
    .locals 1

    .line 337
    const-class v0, Lcom/box/androidsdk/content/models/BoxUser$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/models/BoxUser$Status;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/models/BoxUser$Status;
    .locals 1

    .line 337
    sget-object v0, Lcom/box/androidsdk/content/models/BoxUser$Status;->$VALUES:[Lcom/box/androidsdk/content/models/BoxUser$Status;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/models/BoxUser$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/models/BoxUser$Status;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxUser$Status;->mValue:Ljava/lang/String;

    return-object v0
.end method

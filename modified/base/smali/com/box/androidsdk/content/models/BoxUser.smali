.class public Lcom/box/androidsdk/content/models/BoxUser;
.super Lcom/box/androidsdk/content/models/BoxCollaborator;
.source "BoxUser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/box/androidsdk/content/models/BoxUser$Status;,
        Lcom/box/androidsdk/content/models/BoxUser$Role;
    }
.end annotation


# static fields
.field public static final ALL_FIELDS:[Ljava/lang/String;

.field public static final FIELD_ADDRESS:Ljava/lang/String; = "address"

.field public static final FIELD_AVATAR_URL:Ljava/lang/String; = "avatar_url"

.field public static final FIELD_CAN_SEE_MANAGED_USERS:Ljava/lang/String; = "can_see_managed_users"

.field public static final FIELD_ENTERPRISE:Ljava/lang/String; = "enterprise"

.field public static final FIELD_HOSTNAME:Ljava/lang/String; = "hostname"

.field public static final FIELD_IS_EXEMPT_FROM_DEVICE_LIMITS:Ljava/lang/String; = "is_exempt_from_device_limits"

.field public static final FIELD_IS_EXEMPT_FROM_LOGIN_VERIFICATION:Ljava/lang/String; = "is_exempt_from_login_verification"

.field public static final FIELD_IS_EXTERNAL_COLLAB_RESTRICTED:Ljava/lang/String; = "is_external_collab_restricted"

.field public static final FIELD_IS_SYNC_ENABLED:Ljava/lang/String; = "is_sync_enabled"

.field public static final FIELD_JOB_TITLE:Ljava/lang/String; = "job_title"

.field public static final FIELD_LANGUAGE:Ljava/lang/String; = "language"

.field public static final FIELD_LOGIN:Ljava/lang/String; = "login"

.field public static final FIELD_MAX_UPLOAD_SIZE:Ljava/lang/String; = "max_upload_size"

.field public static final FIELD_MY_TAGS:Ljava/lang/String; = "my_tags"

.field public static final FIELD_PHONE:Ljava/lang/String; = "phone"

.field public static final FIELD_ROLE:Ljava/lang/String; = "role"

.field public static final FIELD_SPACE_AMOUNT:Ljava/lang/String; = "space_amount"

.field public static final FIELD_SPACE_USED:Ljava/lang/String; = "space_used"

.field public static final FIELD_STATUS:Ljava/lang/String; = "status"

.field public static final FIELD_TIMEZONE:Ljava/lang/String; = "timezone"

.field public static final FIELD_TRACKING_CODES:Ljava/lang/String; = "tracking_codes"

.field public static final TYPE:Ljava/lang/String; = "user"

.field private static final serialVersionUID:J = -0x7f581a875d6f7853L


# direct methods
.method static constructor <clinit>()V
    .locals 26

    const-string v0, "type"

    const-string v1, "id"

    const-string v2, "name"

    const-string v3, "login"

    const-string v4, "created_at"

    const-string v5, "modified_at"

    const-string v6, "role"

    const-string v7, "language"

    const-string v8, "timezone"

    const-string v9, "space_amount"

    const-string v10, "space_used"

    const-string v11, "max_upload_size"

    const-string v12, "tracking_codes"

    const-string v13, "can_see_managed_users"

    const-string v14, "is_sync_enabled"

    const-string v15, "is_external_collab_restricted"

    const-string v16, "status"

    const-string v17, "job_title"

    const-string v18, "phone"

    const-string v19, "address"

    const-string v20, "avatar_url"

    const-string v21, "is_exempt_from_device_limits"

    const-string v22, "is_exempt_from_login_verification"

    const-string v23, "enterprise"

    const-string v24, "hostname"

    const-string v25, "my_tags"

    .line 41
    filled-new-array/range {v0 .. v25}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser;->ALL_FIELDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 74
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxCollaborator;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 82
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxCollaborator;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method

.method public static createFromId(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxUser;
    .locals 2

    .line 93
    new-instance v0, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    const-string v1, "id"

    .line 94
    invoke-virtual {v0, v1, p0}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    const-string p0, "type"

    const-string v1, "user"

    .line 95
    invoke-virtual {v0, p0, v1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 96
    new-instance p0, Lcom/box/androidsdk/content/models/BoxUser;

    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxUser;-><init>()V

    .line 97
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->createFromJson(Lcom/eclipsesource/json/JsonObject;)V

    return-object p0
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 1

    const-string v0, "address"

    .line 197
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAvatarURL()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const-string v0, "avatar_url"

    .line 208
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCanSeeManagedUsers()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "can_see_managed_users"

    .line 226
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getEnterprise()Lcom/box/androidsdk/content/models/BoxEnterprise;
    .locals 2

    .line 271
    const-class v0, Lcom/box/androidsdk/content/models/BoxEnterprise;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxJsonObject;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "enterprise"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxEnterprise;

    return-object v0
.end method

.method public getHostname()Ljava/lang/String;
    .locals 1

    const-string v0, "hostname"

    .line 280
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIsExemptFromDeviceLimits()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "is_exempt_from_device_limits"

    .line 253
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getIsExemptFromLoginVerification()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "is_exempt_from_login_verification"

    .line 262
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getIsExternalCollabRestricted()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "is_external_collab_restricted"

    .line 244
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getIsSyncEnabled()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "is_sync_enabled"

    .line 235
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getJobTitle()Ljava/lang/String;
    .locals 1

    const-string v0, "job_title"

    .line 179
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    const-string v0, "language"

    .line 125
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLogin()Ljava/lang/String;
    .locals 1

    const-string v0, "login"

    .line 107
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxUploadSize()Ljava/lang/Long;
    .locals 1

    const-string v0, "max_upload_size"

    .line 161
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getMyTags()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "my_tags"

    .line 289
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsStringArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getPhone()Ljava/lang/String;
    .locals 1

    const-string v0, "phone"

    .line 188
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRole()Lcom/box/androidsdk/content/models/BoxUser$Role;
    .locals 1

    const-string v0, "role"

    .line 116
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxUser$Role;->fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxUser$Role;

    move-result-object v0

    return-object v0
.end method

.method public getSpaceAmount()Ljava/lang/Long;
    .locals 1

    const-string v0, "space_amount"

    .line 143
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getSpaceUsed()Ljava/lang/Long;
    .locals 1

    const-string v0, "space_used"

    .line 152
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getStatus()Lcom/box/androidsdk/content/models/BoxUser$Status;
    .locals 1

    const-string v0, "status"

    .line 170
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxUser$Status;->fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxUser$Status;

    move-result-object v0

    return-object v0
.end method

.method public getTimezone()Ljava/lang/String;
    .locals 1

    const-string v0, "timezone"

    .line 134
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTrackingCodes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "tracking_codes"

    .line 217
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxUser;->getPropertyAsStringArray(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/box/androidsdk/content/models/BoxComment;
.super Lcom/box/androidsdk/content/models/BoxEntity;
.source "BoxComment.java"


# static fields
.field public static final ALL_FIELDS:[Ljava/lang/String;

.field public static final FIELD_CREATED_AT:Ljava/lang/String; = "created_at"

.field public static final FIELD_CREATED_BY:Ljava/lang/String; = "created_by"

.field public static final FIELD_IS_REPLY_COMMENT:Ljava/lang/String; = "is_reply_comment"

.field public static final FIELD_ITEM:Ljava/lang/String; = "item"

.field public static final FIELD_MESSAGE:Ljava/lang/String; = "message"

.field public static final FIELD_MODIFIED_AT:Ljava/lang/String; = "modified_at"

.field public static final FIELD_TAGGED_MESSAGE:Ljava/lang/String; = "tagged_message"

.field public static final TYPE:Ljava/lang/String; = "comment"

.field private static final serialVersionUID:J = 0x7b26ba22de2ed01fL


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const-string v0, "type"

    const-string v1, "id"

    const-string v2, "is_reply_comment"

    const-string v3, "message"

    const-string v4, "tagged_message"

    const-string v5, "created_by"

    const-string v6, "created_at"

    const-string v7, "item"

    const-string v8, "modified_at"

    .line 24
    filled-new-array/range {v0 .. v8}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/models/BoxComment;->ALL_FIELDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxEntity;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method


# virtual methods
.method public getCreatedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "created_at"

    .line 95
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxComment;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getCreatedBy()Lcom/box/androidsdk/content/models/BoxUser;
    .locals 2

    .line 86
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator()Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "created_by"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxComment;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxUser;

    return-object v0
.end method

.method public getIsReplyComment()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "is_reply_comment"

    .line 59
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxComment;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getItem()Lcom/box/androidsdk/content/models/BoxItem;
    .locals 2

    .line 105
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxEntity;->getBoxJsonObjectCreator()Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "item"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxComment;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxItem;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    const-string v0, "message"

    .line 68
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxComment;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModifiedAt()Ljava/util/Date;
    .locals 1

    const-string v0, "modified_at"

    .line 114
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxComment;->getPropertyAsDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getTaggedMessage()Ljava/lang/String;
    .locals 1

    const-string v0, "tagged_message"

    .line 77
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxComment;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

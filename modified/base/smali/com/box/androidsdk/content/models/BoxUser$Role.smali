.class public final enum Lcom/box/androidsdk/content/models/BoxUser$Role;
.super Ljava/lang/Enum;
.source "BoxUser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/models/BoxUser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Role"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/models/BoxUser$Role;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/models/BoxUser$Role;

.field public static final enum ADMIN:Lcom/box/androidsdk/content/models/BoxUser$Role;

.field public static final enum COADMIN:Lcom/box/androidsdk/content/models/BoxUser$Role;

.field public static final enum USER:Lcom/box/androidsdk/content/models/BoxUser$Role;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 299
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser$Role;

    const-string v1, "ADMIN"

    const-string v2, "admin"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/box/androidsdk/content/models/BoxUser$Role;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Role;->ADMIN:Lcom/box/androidsdk/content/models/BoxUser$Role;

    .line 304
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser$Role;

    const-string v1, "COADMIN"

    const-string v2, "coadmin"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/box/androidsdk/content/models/BoxUser$Role;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Role;->COADMIN:Lcom/box/androidsdk/content/models/BoxUser$Role;

    .line 309
    new-instance v0, Lcom/box/androidsdk/content/models/BoxUser$Role;

    const-string v1, "USER"

    const-string v2, "user"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/box/androidsdk/content/models/BoxUser$Role;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Role;->USER:Lcom/box/androidsdk/content/models/BoxUser$Role;

    const/4 v0, 0x3

    .line 295
    new-array v0, v0, [Lcom/box/androidsdk/content/models/BoxUser$Role;

    sget-object v1, Lcom/box/androidsdk/content/models/BoxUser$Role;->ADMIN:Lcom/box/androidsdk/content/models/BoxUser$Role;

    aput-object v1, v0, v3

    sget-object v1, Lcom/box/androidsdk/content/models/BoxUser$Role;->COADMIN:Lcom/box/androidsdk/content/models/BoxUser$Role;

    aput-object v1, v0, v4

    sget-object v1, Lcom/box/androidsdk/content/models/BoxUser$Role;->USER:Lcom/box/androidsdk/content/models/BoxUser$Role;

    aput-object v1, v0, v5

    sput-object v0, Lcom/box/androidsdk/content/models/BoxUser$Role;->$VALUES:[Lcom/box/androidsdk/content/models/BoxUser$Role;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 313
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 314
    iput-object p3, p0, Lcom/box/androidsdk/content/models/BoxUser$Role;->mValue:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxUser$Role;
    .locals 6

    .line 318
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 319
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxUser$Role;->values()[Lcom/box/androidsdk/content/models/BoxUser$Role;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 320
    invoke-virtual {v4}, Lcom/box/androidsdk/content/models/BoxUser$Role;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 325
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v1

    const-string p0, "No enum with text %s found"

    invoke-static {v2, p0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxUser$Role;
    .locals 1

    .line 295
    const-class v0, Lcom/box/androidsdk/content/models/BoxUser$Role;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/models/BoxUser$Role;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/models/BoxUser$Role;
    .locals 1

    .line 295
    sget-object v0, Lcom/box/androidsdk/content/models/BoxUser$Role;->$VALUES:[Lcom/box/androidsdk/content/models/BoxUser$Role;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/models/BoxUser$Role;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/models/BoxUser$Role;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 330
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxUser$Role;->mValue:Ljava/lang/String;

    return-object v0
.end method

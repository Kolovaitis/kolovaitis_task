.class public Lcom/box/androidsdk/content/models/BoxRepresentation$BoxRepPropertiesMap;
.super Lcom/box/androidsdk/content/models/BoxMap;
.source "BoxRepresentation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/models/BoxRepresentation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BoxRepPropertiesMap"
.end annotation


# static fields
.field public static final FIELD_PROPERTIES_DIMENSIONS:Ljava/lang/String; = "dimensions"

.field public static final FIELD_PROPERTIES_PAGED:Ljava/lang/String; = "paged"

.field public static final FIELD_PROPERTIES_THUMB:Ljava/lang/String; = "thumb"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxMap;-><init>()V

    return-void
.end method


# virtual methods
.method public getDimension()Ljava/lang/String;
    .locals 1

    const-string v0, "dimensions"

    .line 125
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxRepresentation$BoxRepPropertiesMap;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPaged()Z
    .locals 2

    const-string v0, "paged"

    .line 115
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxRepresentation$BoxRepPropertiesMap;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 116
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isThumb()Z
    .locals 2

    const-string v0, "thumb"

    .line 120
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxRepresentation$BoxRepPropertiesMap;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 121
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

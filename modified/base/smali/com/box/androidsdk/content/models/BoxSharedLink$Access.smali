.class public final enum Lcom/box/androidsdk/content/models/BoxSharedLink$Access;
.super Ljava/lang/Enum;
.source "BoxSharedLink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/androidsdk/content/models/BoxSharedLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Access"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/box/androidsdk/content/models/BoxSharedLink$Access;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

.field public static final enum COLLABORATORS:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

.field public static final enum COMPANY:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

.field public static final enum DEFAULT:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

.field public static final enum OPEN:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 191
    new-instance v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    const-string v1, "DEFAULT"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->DEFAULT:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    .line 196
    new-instance v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    const-string v1, "OPEN"

    const-string v3, "open"

    const/4 v4, 0x1

    invoke-direct {v0, v1, v4, v3}, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->OPEN:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    .line 201
    new-instance v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    const-string v1, "COMPANY"

    const-string v3, "company"

    const/4 v5, 0x2

    invoke-direct {v0, v1, v5, v3}, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->COMPANY:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    .line 206
    new-instance v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    const-string v1, "COLLABORATORS"

    const-string v3, "collaborators"

    const/4 v6, 0x3

    invoke-direct {v0, v1, v6, v3}, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->COLLABORATORS:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    const/4 v0, 0x4

    .line 187
    new-array v0, v0, [Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    sget-object v1, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->DEFAULT:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    aput-object v1, v0, v2

    sget-object v1, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->OPEN:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    aput-object v1, v0, v4

    sget-object v1, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->COMPANY:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    aput-object v1, v0, v5

    sget-object v1, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->COLLABORATORS:Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    aput-object v1, v0, v6

    sput-object v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->$VALUES:[Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 221
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 222
    iput-object p3, p0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->mValue:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxSharedLink$Access;
    .locals 6

    .line 211
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 212
    invoke-static {}, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->values()[Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 213
    invoke-virtual {v4}, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 218
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v1

    const-string p0, "No enum with text %s found"

    invoke-static {v2, p0, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxSharedLink$Access;
    .locals 1

    .line 187
    const-class v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    return-object p0
.end method

.method public static values()[Lcom/box/androidsdk/content/models/BoxSharedLink$Access;
    .locals 1

    .line 187
    sget-object v0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->$VALUES:[Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    invoke-virtual {v0}, [Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/box/androidsdk/content/models/BoxSharedLink$Access;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/box/androidsdk/content/models/BoxSharedLink$Access;->mValue:Ljava/lang/String;

    return-object v0
.end method

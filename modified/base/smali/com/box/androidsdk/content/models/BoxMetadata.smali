.class public Lcom/box/androidsdk/content/models/BoxMetadata;
.super Lcom/box/androidsdk/content/models/BoxJsonObject;
.source "BoxMetadata.java"


# static fields
.field public static final FIELD_PARENT:Ljava/lang/String; = "parent"

.field public static final FIELD_SCOPE:Ljava/lang/String; = "scope"

.field public static final FIELD_TEMPLATE:Ljava/lang/String; = "template"


# instance fields
.field private mMetadataKeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxJsonObject;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxJsonObject;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method


# virtual methods
.method public getParent()Ljava/lang/String;
    .locals 1

    const-string v0, "parent"

    .line 54
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxMetadata;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getScope()Ljava/lang/String;
    .locals 1

    const-string v0, "scope"

    .line 72
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxMetadata;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTemplate()Ljava/lang/String;
    .locals 1

    const-string v0, "template"

    .line 63
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxMetadata;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/box/androidsdk/content/models/BoxFile;
.super Lcom/box/androidsdk/content/models/BoxCollaborationItem;
.source "BoxFile.java"


# static fields
.field public static final ALL_FIELDS:[Ljava/lang/String;

.field public static final FIELD_COMMENT_COUNT:Ljava/lang/String; = "comment_count"

.field public static final FIELD_CONTENT_CREATED_AT:Ljava/lang/String; = "content_created_at"

.field public static final FIELD_CONTENT_MODIFIED_AT:Ljava/lang/String; = "content_modified_at"

.field public static final FIELD_EXTENSION:Ljava/lang/String; = "extension"

.field public static final FIELD_FILE_VERSION:Ljava/lang/String; = "file_version"

.field public static final FIELD_IS_PACKAGE:Ljava/lang/String; = "is_package"

.field public static final FIELD_REPRESENTATIONS:Ljava/lang/String; = "representations"

.field public static final FIELD_SHA1:Ljava/lang/String; = "sha1"

.field public static final FIELD_SIZE:Ljava/lang/String; = "size"

.field public static final FIELD_VERSION_NUMBER:Ljava/lang/String; = "version_number"

.field public static final TYPE:Ljava/lang/String; = "file"

.field private static final serialVersionUID:J = -0x41ae1a0be9cd65ffL


# direct methods
.method static constructor <clinit>()V
    .locals 32

    const-string v0, "type"

    const-string v1, "id"

    const-string v2, "file_version"

    const-string v3, "sequence_id"

    const-string v4, "etag"

    const-string v5, "sha1"

    const-string v6, "name"

    const-string v7, "created_at"

    const-string v8, "modified_at"

    const-string v9, "description"

    const-string v10, "size"

    const-string v11, "path_collection"

    const-string v12, "created_by"

    const-string v13, "modified_by"

    const-string v14, "trashed_at"

    const-string v15, "purged_at"

    const-string v16, "content_created_at"

    const-string v17, "content_modified_at"

    const-string v18, "owned_by"

    const-string v19, "shared_link"

    const-string v20, "parent"

    const-string v21, "item_status"

    const-string v22, "version_number"

    const-string v23, "comment_count"

    const-string v24, "permissions"

    const-string v25, "extension"

    const-string v26, "is_package"

    const-string v27, "collections"

    const-string v28, "has_collaborations"

    const-string v29, "can_non_owners_invite"

    const-string v30, "is_externally_owned"

    const-string v31, "allowed_invitee_roles"

    .line 32
    filled-new-array/range {v0 .. v31}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/models/BoxFile;->ALL_FIELDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 71
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 81
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method

.method public static createFromId(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFile;
    .locals 1

    const/4 v0, 0x0

    .line 92
    invoke-static {p0, v0}, Lcom/box/androidsdk/content/models/BoxFile;->createFromIdAndName(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFile;

    move-result-object p0

    return-object p0
.end method

.method public static createFromIdAndName(Ljava/lang/String;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxFile;
    .locals 2

    .line 104
    new-instance v0, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    const-string v1, "id"

    .line 105
    invoke-virtual {v0, v1, p0}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    const-string p0, "type"

    const-string v1, "file"

    .line 106
    invoke-virtual {v0, p0, v1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 107
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const-string p0, "name"

    .line 108
    invoke-virtual {v0, p0, p1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 110
    :cond_0
    new-instance p0, Lcom/box/androidsdk/content/models/BoxFile;

    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/models/BoxFile;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-object p0
.end method


# virtual methods
.method public getCommentCount()Ljava/lang/Long;
    .locals 1

    .line 173
    invoke-super {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getCommentCount()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getContentCreatedAt()Ljava/util/Date;
    .locals 1

    .line 158
    invoke-super {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getContentCreatedAt()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getContentModifiedAt()Ljava/util/Date;
    .locals 1

    .line 168
    invoke-super {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getContentModifiedAt()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getExtension()Ljava/lang/String;
    .locals 1

    const-string v0, "extension"

    .line 144
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFile;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileVersion()Lcom/box/androidsdk/content/models/BoxFileVersion;
    .locals 2

    .line 117
    const-class v0, Lcom/box/androidsdk/content/models/BoxFileVersion;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxJsonObject;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "file_version"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxFile;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxFileVersion;

    return-object v0
.end method

.method public getIsPackage()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "is_package"

    .line 153
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFile;->getPropertyAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getRepresentations()Lcom/box/androidsdk/content/models/BoxIteratorRepresentations;
    .locals 2

    .line 181
    const-class v0, Lcom/box/androidsdk/content/models/BoxIteratorRepresentations;

    invoke-static {v0}, Lcom/box/androidsdk/content/models/BoxJsonObject;->getBoxJsonObjectCreator(Ljava/lang/Class;)Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;

    move-result-object v0

    const-string v1, "representations"

    invoke-virtual {p0, v0, v1}, Lcom/box/androidsdk/content/models/BoxFile;->getPropertyAsJsonObject(Lcom/box/androidsdk/content/models/BoxJsonObject$BoxJsonObjectCreator;Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxJsonObject;

    move-result-object v0

    check-cast v0, Lcom/box/androidsdk/content/models/BoxIteratorRepresentations;

    return-object v0
.end method

.method public getSha1()Ljava/lang/String;
    .locals 1

    const-string v0, "sha1"

    .line 126
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFile;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSize()Ljava/lang/Long;
    .locals 1

    .line 163
    invoke-super {p0}, Lcom/box/androidsdk/content/models/BoxCollaborationItem;->getSize()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getVersionNumber()Ljava/lang/String;
    .locals 1

    const-string v0, "version_number"

    .line 135
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxFile;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/box/androidsdk/content/models/BoxBookmark;
.super Lcom/box/androidsdk/content/models/BoxItem;
.source "BoxBookmark.java"


# static fields
.field public static final ALL_FIELDS:[Ljava/lang/String;

.field public static final FIELD_COMMENT_COUNT:Ljava/lang/String; = "comment_count"

.field public static final FIELD_URL:Ljava/lang/String; = "url"

.field public static final TYPE:Ljava/lang/String; = "web_link"

.field private static final serialVersionUID:J = 0x247baa1c966857f2L


# direct methods
.method static constructor <clinit>()V
    .locals 20

    const-string v0, "type"

    const-string v1, "id"

    const-string v2, "sequence_id"

    const-string v3, "etag"

    const-string v4, "name"

    const-string v5, "url"

    const-string v6, "created_at"

    const-string v7, "modified_at"

    const-string v8, "description"

    const-string v9, "path_collection"

    const-string v10, "created_by"

    const-string v11, "modified_by"

    const-string v12, "trashed_at"

    const-string v13, "purged_at"

    const-string v14, "owned_by"

    const-string v15, "shared_link"

    const-string v16, "parent"

    const-string v17, "item_status"

    const-string v18, "permissions"

    const-string v19, "comment_count"

    .line 17
    filled-new-array/range {v0 .. v19}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/box/androidsdk/content/models/BoxBookmark;->ALL_FIELDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/box/androidsdk/content/models/BoxItem;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/eclipsesource/json/JsonObject;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1}, Lcom/box/androidsdk/content/models/BoxItem;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-void
.end method

.method public static createFromId(Ljava/lang/String;)Lcom/box/androidsdk/content/models/BoxBookmark;
    .locals 2

    .line 64
    new-instance v0, Lcom/eclipsesource/json/JsonObject;

    invoke-direct {v0}, Lcom/eclipsesource/json/JsonObject;-><init>()V

    const-string v1, "id"

    .line 65
    invoke-virtual {v0, v1, p0}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    const-string p0, "type"

    const-string v1, "web_link"

    .line 66
    invoke-virtual {v0, p0, v1}, Lcom/eclipsesource/json/JsonObject;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/eclipsesource/json/JsonObject;

    .line 67
    new-instance p0, Lcom/box/androidsdk/content/models/BoxBookmark;

    invoke-direct {p0, v0}, Lcom/box/androidsdk/content/models/BoxBookmark;-><init>(Lcom/eclipsesource/json/JsonObject;)V

    return-object p0
.end method


# virtual methods
.method public getSize()Ljava/lang/Long;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    const-string v0, "url"

    .line 76
    invoke-virtual {p0, v0}, Lcom/box/androidsdk/content/models/BoxBookmark;->getPropertyAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

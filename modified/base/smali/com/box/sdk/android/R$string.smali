.class public final Lcom/box/sdk/android/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/sdk/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final blocked_ip_error_message:I = 0x7f0e02b5

.field public static final boxsdk_Authenticating:I = 0x7f0e02b8

.field public static final boxsdk_Authentication_fail:I = 0x7f0e02b9

.field public static final boxsdk_Authentication_fail_forbidden:I = 0x7f0e02ba

.field public static final boxsdk_Authentication_fail_url_mismatch:I = 0x7f0e02bb

.field public static final boxsdk_Go_back:I = 0x7f0e02bc

.field public static final boxsdk_Please_wait:I = 0x7f0e02bd

.field public static final boxsdk_Security_Warning:I = 0x7f0e02be

.field public static final boxsdk_Select_an_account_to_use:I = 0x7f0e02bf

.field public static final boxsdk_There_are_problems_with_the_security_certificate_for_this_site:I = 0x7f0e02c0

.field public static final boxsdk_Use_a_different_account:I = 0x7f0e02c1

.field public static final boxsdk_alert_dialog_cancel:I = 0x7f0e02c2

.field public static final boxsdk_alert_dialog_ok:I = 0x7f0e02c3

.field public static final boxsdk_alert_dialog_password:I = 0x7f0e02c4

.field public static final boxsdk_alert_dialog_text_entry:I = 0x7f0e02c5

.field public static final boxsdk_alert_dialog_username:I = 0x7f0e02c6

.field public static final boxsdk_box_app_signature:I = 0x7f0e02c7

.field public static final boxsdk_button_ok:I = 0x7f0e02c8

.field public static final boxsdk_button_okay:I = 0x7f0e02c9

.field public static final boxsdk_bytes:I = 0x7f0e02ca

.field public static final boxsdk_common_name:I = 0x7f0e02cb

.field public static final boxsdk_details:I = 0x7f0e02cc

.field public static final boxsdk_error_fatal_refresh:I = 0x7f0e02cd

.field public static final boxsdk_error_network_connection:I = 0x7f0e02ce

.field public static final boxsdk_error_terms_of_service:I = 0x7f0e02cf

.field public static final boxsdk_expires_on:I = 0x7f0e02d0

.field public static final boxsdk_gigabytes:I = 0x7f0e02d1

.field public static final boxsdk_issued_by:I = 0x7f0e02d2

.field public static final boxsdk_issued_on:I = 0x7f0e02d3

.field public static final boxsdk_issued_to:I = 0x7f0e02d4

.field public static final boxsdk_kilobytes:I = 0x7f0e02d5

.field public static final boxsdk_megabytes:I = 0x7f0e02d6

.field public static final boxsdk_no_offline_access:I = 0x7f0e02d7

.field public static final boxsdk_no_offline_access_detail:I = 0x7f0e02d8

.field public static final boxsdk_no_offline_access_todo:I = 0x7f0e02d9

.field public static final boxsdk_org_name:I = 0x7f0e02da

.field public static final boxsdk_org_unit:I = 0x7f0e02db

.field public static final boxsdk_ssl_error_details:I = 0x7f0e02dc

.field public static final boxsdk_ssl_error_warning_DATE_INVALID:I = 0x7f0e02dd

.field public static final boxsdk_ssl_error_warning_EXPIRED:I = 0x7f0e02de

.field public static final boxsdk_ssl_error_warning_ID_MISMATCH:I = 0x7f0e02df

.field public static final boxsdk_ssl_error_warning_INVALID:I = 0x7f0e02e0

.field public static final boxsdk_ssl_error_warning_NOT_YET_VALID:I = 0x7f0e02e1

.field public static final boxsdk_ssl_error_warning_UNTRUSTED:I = 0x7f0e02e2

.field public static final boxsdk_ssl_should_not_proceed:I = 0x7f0e02e3

.field public static final boxsdk_terabytes:I = 0x7f0e02e4

.field public static final boxsdk_unable_to_connect:I = 0x7f0e02e5

.field public static final boxsdk_unable_to_connect_detail:I = 0x7f0e02e6

.field public static final boxsdk_unable_to_connect_todo:I = 0x7f0e02e7

.field public static final boxsdk_validity_period:I = 0x7f0e02e8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

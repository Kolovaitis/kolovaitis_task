.class public final Lcom/box/sdk/android/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/box/sdk/android/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final body:I = 0x7f080073

.field public static final box_account_description:I = 0x7f080076

.field public static final box_account_initials:I = 0x7f080077

.field public static final box_account_title:I = 0x7f080078

.field public static final box_avatar_image:I = 0x7f080079

.field public static final box_avatar_initials:I = 0x7f08007a

.field public static final boxsdk_accounts_list:I = 0x7f08007d

.field public static final by_common:I = 0x7f0800a9

.field public static final by_common_header:I = 0x7f0800aa

.field public static final by_org:I = 0x7f0800ab

.field public static final by_org_header:I = 0x7f0800ac

.field public static final by_org_unit:I = 0x7f0800ad

.field public static final by_org_unit_header:I = 0x7f0800ae

.field public static final expires_on:I = 0x7f08013b

.field public static final expires_on_header:I = 0x7f08013c

.field public static final issued_by_header:I = 0x7f0801ad

.field public static final issued_on:I = 0x7f0801ae

.field public static final issued_on_header:I = 0x7f0801af

.field public static final issued_to_header:I = 0x7f0801b0

.field public static final oauth_container:I = 0x7f080231

.field public static final oauthview:I = 0x7f080232

.field public static final ok:I = 0x7f080233

.field public static final password_edit:I = 0x7f080257

.field public static final password_view:I = 0x7f080259

.field public static final placeholder:I = 0x7f080263

.field public static final stop_screen_txt:I = 0x7f08030f

.field public static final title_separator:I = 0x7f08033f

.field public static final to_common:I = 0x7f080342

.field public static final to_common_header:I = 0x7f080343

.field public static final to_org:I = 0x7f080344

.field public static final to_org_header:I = 0x7f080345

.field public static final to_org_unit:I = 0x7f080346

.field public static final to_org_unit_header:I = 0x7f080347

.field public static final username_edit:I = 0x7f080381

.field public static final username_view:I = 0x7f080382

.field public static final validity_header:I = 0x7f080383


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

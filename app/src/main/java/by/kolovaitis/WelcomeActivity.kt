package by.kolovaitis


import android.animation.LayoutTransition
import android.content.Intent
import android.os.Bundle
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import by.kolovaitis.welcome.DescriptionFragment
import by.kolovaitis.welcome.HelloFragment
import by.kolovaitis.welcome.LayoutFragment
import by.kolovaitis.welcome.ThemeFragment
import kotlinx.android.synthetic.main.activity_welcome.*


class WelcomeActivity : AppCompatActivity() {
    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int = 4
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> HelloFragment()
                1 -> DescriptionFragment()
                2 -> ThemeFragment()
                3 -> LayoutFragment()
                else -> MainFragment()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        welcomePager.adapter = ScreenSlidePagerAdapter(supportFragmentManager)

        welcomePager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                (radioGroup.getChildAt(position) as RadioButton).isChecked = true
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        next.setOnClickListener {

            if (welcomePager.currentItem < welcomePager.childCount )
                welcomePager.currentItem++
            else {
                setResult(RESULT_OK)
                finish()
            }
            (radioGroup.getChildAt(welcomePager.currentItem) as RadioButton).isChecked = true
        }
        radioGroup.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { radioGroup, i ->
            val index = radioGroup.indexOfChild(findViewById(i))
            if (index != welcomePager.currentItem) {
                welcomePager.currentItem = index
            }
        })


    }

    override fun onBackPressed() {
        if(welcomePager.currentItem>0){
            welcomePager.currentItem--
        }
    }


}

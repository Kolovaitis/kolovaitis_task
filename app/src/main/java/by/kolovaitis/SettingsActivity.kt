package by.kolovaitis

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationManagerCompat
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.yandex.metrica.YandexMetrica


val CUSTOM_NOTIFICATION = 101
val STANDARD_NOTIFICATION = 102

class SettingsActivity : AppCompatActivity() {

    val model: AppsViewModel by viewModels()

    val SETTINGS_ACTIVITY_TAG = "Settings activity"
    private val preferencesListener = SharedPreferencesListener(this)
    lateinit var sharedPreferences: SharedPreferences;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        val eventParameters = "{\"OnCreate()\":\"create\"}"
        YandexMetrica.reportEvent(SETTINGS_ACTIVITY_TAG, eventParameters)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        val settingsFragment = SettingsFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.settings,
                settingsFragment
            )
            .commit()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.activity_in_main,R.anim.activity_out_right_top)
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            val myPref =
                findPreference(getString(R.string.refresh_button)) as Preference?
            myPref!!.onPreferenceClickListener = Preference.OnPreferenceClickListener {
                //open browser or intent here
                Log.d("AlarmReceiver", "PreferenceClick")

                activity?.startService(
                    Intent(activity, ImageLoadService::class.java)
                )
                true
            }

            val standardNotification =
                findPreference(getString(R.string.push_standard)) as Preference?
            standardNotification!!.onPreferenceClickListener =
                Preference.OnPreferenceClickListener {
                    pushStandard()
                    true
                }
            val customNotification =
                findPreference(getString(R.string.push_custom)) as Preference?
            customNotification!!.onPreferenceClickListener =
                Preference.OnPreferenceClickListener {
                    pushLove()
                    true
                }


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationManager =
                    NotificationManagerCompat.from(context!!)

                val name: CharSequence = getString(R.string.app_name)

                val description = getString(R.string.developer_name)

                val importance = NotificationManager.IMPORTANCE_LOW
                val mChannel = NotificationChannel(getString(R.string.channel_id), name, importance)
                mChannel.description = description
                notificationManager.createNotificationChannel(mChannel)
            }


        }

        private fun pushLove() {
            var notificationIntent: Intent = Intent(context, LauncherActivity::class.java)
            var contentIntent = PendingIntent.getActivity(
                context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
            )
            val builder = Notification.Builder(context)
                .setContentTitle("What is love?")
                .setContentText("Baby don't hurt me")
                .setSmallIcon(R.drawable.ic_love)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_love))
                .setContentIntent(contentIntent)
                .setAutoCancel(true)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                builder
                    .setColor(context?.getColor(R.color.colorAccent)!!)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(getString(R.string.channel_id))
            }
            val notificationManager =
                NotificationManagerCompat.from(context!!)
            notificationManager.notify(CUSTOM_NOTIFICATION, builder.build())
        }

        private fun pushStandard() {
            var notificationIntent: Intent = Intent(context, LauncherActivity::class.java)
            var contentIntent = PendingIntent.getActivity(
                context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
            )

            val builder = Notification.Builder(context)
                .setContentTitle("Заголовок")
                .setContentText("Текст")
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_info)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_info))
                .setAutoCancel(true)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.setChannelId(getString(R.string.channel_id))
            }
            val notificationManager =
                NotificationManagerCompat.from(context!!)
            notificationManager.notify(STANDARD_NOTIFICATION, builder.build())

        }


    }

    override fun onStart() {
        super.onStart()
        sharedPreferences.registerOnSharedPreferenceChangeListener(preferencesListener)
    }

    override fun onStop() {
        super.onStop()
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferencesListener)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
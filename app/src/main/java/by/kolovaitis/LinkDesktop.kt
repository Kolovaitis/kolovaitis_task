package by.kolovaitis

import android.graphics.drawable.Drawable


class LinkDesktop(var _label: String, var _image: Drawable?, var url: String, index: Int) :
    DesktopItem(index) {
    override var image: Drawable?
        get() = _image
        set(value) {
            _image = value
        }


    override var label: String
        get() = _label
        set(value) {}


}
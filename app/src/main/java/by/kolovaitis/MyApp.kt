package by.kolovaitis

import android.app.Application
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import com.yandex.metrica.push.YandexMetricaPush


class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        // Creating an extended library configuration.
        val config = YandexMetricaConfig.newConfigBuilder("694d4cd9-80b9-45f2-a415-e60d17216cb3")
            // Setting up the configuration. For example, to enable logging.
            .withLogs().build();
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(getApplicationContext(), config);
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this);
        YandexMetricaPush.init(getApplicationContext());
    }
}
package by.kolovaitis


import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.list.ActivityStarter
import java.util.*


class MainGridAdapter(
    val context: Activity,
    val activityStarter: ActivityStarter
) :
    RecyclerView.Adapter<MainGridAdapter.MyViewHolder>() {
    private var currentApps: Array<DesktopItem?> = arrayOfNulls<DesktopItem>(MAX_COUNT)

    fun wasChanged(newApps: List<DesktopItem>?) {
        if (newApps != null) {
            for(i in 0..MAX_COUNT-1){
                currentApps[i]=null
            }
            newApps.forEach {
                currentApps[it.index]=it
            }
            notifyDataSetChanged()
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MainGridAdapter.MyViewHolder {
        // create a new view
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.grid_card_view, parent, false) as CardView
        context.registerForContextMenu(cardView)
        return MyViewHolder(cardView)
    }

    fun move(index1: Int, index2: Int) {
        currentApps[index1] =
            currentApps[index2].also { currentApps[index2] = currentApps[index1] }
        currentApps[index1]?.index=index1
        currentApps[index2]?.index=index2
        if(index1>index2) {
            notifyItemMoved(index1, index2)
            notifyItemMoved(index2 + 1, index1)
        }else{
            notifyItemMoved(index2, index1)
            notifyItemMoved(index1 + 1, index2)
        }
    }


    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        var current = currentApps[position]

        if (current != null) {
            holder.cardView.children.forEach { it.visibility = View.VISIBLE }

            holder.cardView.findViewById<ImageView>(R.id.imageView)
                .setImageDrawable(current!!.image)
            holder.cardView.findViewById<TextView>(R.id.label).text =
                current!!.label

            holder.cardView.tag = current

            //text = myDataset[position]
            holder.cardView.setOnClickListener {
                activityStarter.startDesktop(current!!)

            }
        } else {
            holder.cardView.children.forEach { it.visibility = View.INVISIBLE }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = 9


}
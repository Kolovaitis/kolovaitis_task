package by.kolovaitis

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat.recreate
import by.kolovaitis.wallpaper.Wallpaper

class SharedPreferencesListener(val context: Activity) : SharedPreferences.OnSharedPreferenceChangeListener {
    override fun onSharedPreferenceChanged(p0: SharedPreferences?, p1: String?) {
        when (p1) {
            "theme" -> {
                AppCompatDelegate.setDefaultNightMode(
                    if (p0!!.getBoolean("theme", false)) {
                        AppCompatDelegate.MODE_NIGHT_YES
                    } else {
                        AppCompatDelegate.MODE_NIGHT_NO
                    }

                )
            }
            Wallpaper.PREFERENCES_KEY -> {
                Log.d("AlarmReceiver", "preferences")
                Wallpaper.changeSite(p0!!.getString(Wallpaper.PREFERENCES_KEY, Wallpaper.NONE),context)

            }
            Wallpaper.PREFERENCES_TIME -> {
                Wallpaper.changeInterval(p0!!.getString(Wallpaper.PREFERENCES_TIME, "15").toInt(), context)

            }

        }

    }
}
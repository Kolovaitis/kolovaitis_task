package by.kolovaitis

import android.graphics.drawable.Drawable

data class App(val label:String, val packageName:String, val icon:Drawable, val installDate:Long, var launchCount:Int=0, var lastLaunch:Long?=null, val isSystem:Boolean) {

}

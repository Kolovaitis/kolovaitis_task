package by.kolovaitis

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import by.kolovaitis.wallpaper.Wallpaper

class ImageLoadService : Service() {

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Thread {
            Log.d("AlarmReceiver", "ImageLoadService")
            Wallpaper.refresh(this)
            stopSelf()
        }.start()
        return super.onStartCommand(intent, flags, startId)
    }
}

package by.kolovaitis

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.activity.viewModels
import com.yandex.metrica.YandexMetrica

class AppsChangedReceiver(val model: AppsViewModel) : BroadcastReceiver() {
    val APPS_CHANGE_TAG = "AppsChangeReceiver"
    override fun onReceive(p0: Context?, p1: Intent?) {
        val eventParameters = "{\"List of apps was changed\":\"reload...\"}"
        YandexMetrica.reportEvent(APPS_CHANGE_TAG, eventParameters)
        Log.d("AppsChangedReceiver", "receive")
        model.loadApps()
    }
}
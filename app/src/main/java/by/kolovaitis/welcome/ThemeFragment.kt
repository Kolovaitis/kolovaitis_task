package by.kolovaitis.welcome

import android.R.attr.button
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import by.kolovaitis.R
import by.kolovaitis.WelcomeActivity

import kotlinx.android.synthetic.main.fragment_theme.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ThemeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ThemeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var sharedPreferences: SharedPreferences
    override fun onStart() {
        super.onStart()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        themeChoosed(sharedPreferences.getBoolean("theme", false))

        light_mode_touch.setOnClickListener {
            themeChoosed(false)
        }
        dark_mode_touch.setOnClickListener {
            themeChoosed(true)
        }

    }

    private fun themeChoosed(theme: Boolean) {

        if (sharedPreferences.getBoolean("theme", !theme) != theme) {

                (activity as WelcomeActivity).runOnUiThread {
                    AppCompatDelegate.setDefaultNightMode(
                        if (theme) {
                            AppCompatDelegate.MODE_NIGHT_YES
                        } else {
                            AppCompatDelegate.MODE_NIGHT_NO
                        }
                    )
                }


        }
        sharedPreferences.edit().putBoolean("theme", theme).apply()
        dark_mode_button.isChecked = theme
        light_mode_button.isChecked = !theme
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_theme, container, false)
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ThemeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ThemeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

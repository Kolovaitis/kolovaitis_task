package by.kolovaitis.welcome

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import by.kolovaitis.R
import kotlinx.android.synthetic.main.fragment_layout.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LayoutFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LayoutFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onStart() {
        super.onStart()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        layoutChoosed(
            when (sharedPreferences.getString("layout", getString(R.string.standard_layout))) {
                getString(R.string.standard_layout) -> true
                else -> false
            }
        )


        standard_touch?.setOnClickListener {
            layoutChoosed(true)
        }
        extended_touch?.setOnClickListener {
            layoutChoosed(false)
        }
    }
    override fun onPause() {
        super.onPause()
    }

    private fun layoutChoosed(layout: Boolean) {
        standard_layout_button?.isChecked = layout
        extended_layout_button?.isChecked = !layout
        sharedPreferences.edit().putString(
            "layout", if (layout) {
                getString(R.string.standard_layout)
            } else {
                getString(R.string.extended_layout)
            }
        ).apply()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_layout, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment LayoutFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            LayoutFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

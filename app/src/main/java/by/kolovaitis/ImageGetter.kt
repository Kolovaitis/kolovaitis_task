package by.kolovaitis

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import com.android.volley.toolbox.Volley
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

object ImageGetter {
    var queue: RequestQueue? = null
    fun getImage(
        url: String, context: Context, linkDesktop: LinkDesktop,
        liveData: MutableLiveData<List<DesktopItem>>
    ) {
        val file =
            File(context.getCacheDir(), url)
        if (file.exists()) {
            linkDesktop.image = BitmapDrawable(BitmapFactory.decodeFile(file.absolutePath))
        } else {

            if (queue == null) {
                queue = Volley.newRequestQueue(context)
            }
            val resultDrawable = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                context.getDrawable(R.drawable.ic_null_internet)
            } else {
                ColorDrawable()
            }
            linkDesktop.image = resultDrawable

            var imageRequest: ImageRequest? = ImageRequest(
                "https://favicon.yandex.net/favicon/$url?size=32",  // Image URL
                Response.Listener { response ->
                    linkDesktop.image = BitmapDrawable(response)
                    liveData.value = liveData.value
                    file.createNewFile()
                    val bos = ByteArrayOutputStream()
                    response.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
                    val bitmapdata = bos.toByteArray()
                    val fos = FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();

                },
                32,
                32,
                ImageView.ScaleType.CENTER_CROP,
                Bitmap.Config.RGB_565,
                Response.ErrorListener { error ->
                    Toast.makeText(
                        context,
                        context.getString(R.string.problem_loading_image),
                        Toast.LENGTH_LONG
                    ).show()
                }
            )
            queue?.add(imageRequest)
        }
    }
}

package by.kolovaitis

import android.graphics.drawable.Drawable

abstract class DesktopItem(var index:Int) {
    abstract var image: Drawable?
    abstract var label: String

}
package by.kolovaitis

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast
import com.yandex.metrica.push.YandexMetricaPush;
class SilentPushReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        // Extract push message payload from your push message.
        val payload = intent.getStringExtra(YandexMetricaPush.EXTRA_PAYLOAD);
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        sharedPreferences.edit().putString("silent_push", payload).apply()
       
    }
}
package by.kolovaitis.content

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import android.text.TextUtils
import android.util.Log


class AppsContentProvider : ContentProvider() {
    companion object {

        private const val APP_LIST = 1
        private const val APP_PACKAGE = 2
        private const val APP_LAST = 3
        private const val DESKTOP_LIST = 4

        private val URI_MATCHER: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            URI_MATCHER.addURI(MyContract.AUTHORITY, "apps", APP_LIST)
            URI_MATCHER.addURI(MyContract.AUTHORITY, "apps/*", APP_PACKAGE)
            URI_MATCHER.addURI(MyContract.AUTHORITY, "appslast", APP_LAST)
            URI_MATCHER.addURI(MyContract.AUTHORITY, "desktop", DESKTOP_LIST)

        }
    }

    private lateinit var helper: MySQLiteOpenHelper

    override fun onCreate(): Boolean {
        helper = MySQLiteOpenHelper(context)
        return true
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val matchedUri = URI_MATCHER.match(uri)
        if (matchedUri == DESKTOP_LIST) {
            val db = helper.writableDatabase
            val sql =
                "SELECT * FROM ${DbConstants.TABLE_DESKTOP} WHERE ${DbConstants.COL_INDEX_DESKTOP} = ?"
            val cursor: Cursor = db.rawQuery(
                sql,
                arrayOf(values?.getAsInteger(DbConstants.COL_INDEX_DESKTOP).toString())
            )
            return if (cursor == null || !cursor.moveToFirst()) { //Insert new
                val id = helper.writableDatabase.insert(DbConstants.TABLE_DESKTOP, null, values)
                getUriForId(id, uri)
            } else { //Update


                db.update(
                    DbConstants.TABLE_DESKTOP,
                    values,
                    "${DbConstants.COL_INDEX_DESKTOP}= ?",
                    arrayOf(values?.getAsInteger(DbConstants.COL_INDEX_DESKTOP).toString())

                )


                uri
            }


        }
        if (matchedUri != APP_LIST) {
            throw IllegalArgumentException("Unsupported URI for insertion: $uri")
        }
        val db = helper.writableDatabase
        val sql = "SELECT * FROM ${DbConstants.TABLE_APPS} WHERE ${DbConstants.COL_PACKAGE} = ?"
        val cursor: Cursor = db.rawQuery(
            sql,
            arrayOf(values?.getAsString(DbConstants.COL_PACKAGE))
        )
        return if (cursor == null || !cursor.moveToFirst()) { //Insert new
            val id = helper.writableDatabase.insert(DbConstants.TABLE_APPS, null, values)
            Log.d("AppsContentProvider", "Create ${values?.getAsString(DbConstants.COL_PACKAGE)}")
            getUriForId(id, uri)
        } else { //Update
            val cv = ContentValues()
            cv.put(
                DbConstants.COL_LAST_START,
                values?.getAsString(DbConstants.COL_LAST_START)
            )
            cv.put(
                DbConstants.COL_COUNT,
                values?.getAsInteger(DbConstants.COL_COUNT)
            )
            cv.put(
                DbConstants.COL_LABEL,
                values?.getAsString(DbConstants.COL_LABEL)
            )

            db.update(
                DbConstants.TABLE_APPS,
                cv,
                "${DbConstants.COL_PACKAGE}= ?",
                arrayOf(values?.getAsString(DbConstants.COL_PACKAGE))

            );
            Log.d("AppsContentProvider", "Update ${values?.getAsString(DbConstants.COL_PACKAGE)}")

            uri
        }


    }


    private fun getUriForId(id: Long, uri: Uri): Uri {
        if (id > 0) {
            return ContentUris.withAppendedId(uri, id)
        }
        throw SQLException("Problem while inserting into uri: $uri")
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        val db = helper.readableDatabase
        val builder = SQLiteQueryBuilder()
        builder.tables = DbConstants.TABLE_APPS
        when (URI_MATCHER.match(uri)) {
            APP_LIST -> {
                return builder.query(
                    db,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
                )
            }
            APP_PACKAGE -> {
                return builder.query(
                    db,
                    projection,
                    DbConstants.COL_PACKAGE + "=?",
                    arrayOf(uri.lastPathSegment),
                    null,
                    null,
                    sortOrder
                )
            }
            APP_LAST -> {
                val cursor = builder.query(
                    db,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
                )
                var maxLastStart: Long = 0
                cursor.moveToFirst()
                for (i in (1..cursor.count)) {
                    if (cursor.getLong(cursor.getColumnIndex(DbConstants.COL_LAST_START)) > maxLastStart) {
                        maxLastStart =
                            cursor.getLong(cursor.getColumnIndex(DbConstants.COL_LAST_START))
                    }
                    cursor.moveToNext()
                }
                return builder.query(
                    db,
                    projection,
                    DbConstants.COL_LAST_START + "=?",
                    arrayOf(maxLastStart.toString()),
                    null,
                    null,
                    sortOrder
                )

            }
            DESKTOP_LIST -> {
                builder.tables = DbConstants.TABLE_DESKTOP
                return builder.query(
                    db,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
                )
            }
            else -> throw IllegalArgumentException("Unsupported uri: $uri")
        }

    }


    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        val db = helper.writableDatabase
        var updateCount = 0
        when (URI_MATCHER.match(uri)) {
            APP_LIST -> updateCount =
                db.update(DbConstants.TABLE_APPS, values, selection, selectionArgs)
            DESKTOP_LIST -> updateCount =
                db.update(DbConstants.TABLE_DESKTOP, values, selection, selectionArgs)
            APP_PACKAGE -> {
                var where = "${DbConstants.COL_PACKAGE} = ${uri.lastPathSegment}"
                if (!TextUtils.isEmpty(selection)) {
                    where += " AND $selection"
                }
                updateCount = db.update(DbConstants.TABLE_APPS, values, where, selectionArgs)
            }
            else -> throw IllegalArgumentException("Unsupported URI: $uri")
        }
        return updateCount
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        val db = helper.writableDatabase
        var delCount = 0
        when (URI_MATCHER.match(uri)) {
            DESKTOP_LIST -> delCount = db.delete(DbConstants.TABLE_DESKTOP, selection, selectionArgs)
            APP_LIST -> delCount = db.delete(DbConstants.TABLE_APPS, selection, selectionArgs)
            APP_PACKAGE -> {
                var where = "${DbConstants.COL_PACKAGE} = ${uri.lastPathSegment}"
                if (!TextUtils.isEmpty(selection)) {
                    where += " AND $selection"
                }
                delCount = db.delete(DbConstants.TABLE_APPS, where, selectionArgs)
            }
            else -> throw IllegalArgumentException("Unsupported URI: $uri")
        }
        return delCount
    }

    override fun getType(uri: Uri): String? {
        return when (URI_MATCHER.match(uri)) {
            APP_LIST -> MyContract.Apps.CONTENT_TYPE
            DESKTOP_LIST -> MyContract.Apps.CONTENT_TYPE
            APP_PACKAGE -> MyContract.Apps.CONTENT_ITEM_TYPE
            else -> throw IllegalArgumentException("Unsupported URI: $uri")
        }
    }

}
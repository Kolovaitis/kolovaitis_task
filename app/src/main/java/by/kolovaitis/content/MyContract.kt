package by.kolovaitis.content

import android.content.ContentResolver

object MyContract {

    const val AUTHORITY = "by.kolovaitis.provider"

    object Apps {

        const val CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.by.kolovaitis.provider_apps"
        const val CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.by.kolovaitis.provider_apps"
    }


}
package by.kolovaitis.profile

import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.R


class ContactsAdapter(private val myDataset: List<Contact>, val color: Int) :
    RecyclerView.Adapter<ContactsAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ContactsAdapter.MyViewHolder {
        // create a new view
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.linear_card_view, parent, false) as CardView

        return MyViewHolder(cardView)
    }
    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.cardView.findViewById<TextView>(R.id.textViewName).text = myDataset[position].title
        holder.cardView.findViewById<TextView>(R.id.textViewDescription).text = myDataset[position].subtitle
        holder.cardView.findViewById<ImageView>(R.id.imageView).setImageDrawable(myDataset[position].icon)
        holder.cardView.findViewById<ImageView>(R.id.imageView).setColorFilter(color)
        holder.cardView.setOnClickListener(myDataset[position].onClickListener)

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size
}
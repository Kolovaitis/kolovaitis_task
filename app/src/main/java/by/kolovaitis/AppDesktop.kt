package by.kolovaitis

import android.graphics.drawable.Drawable
import by.kolovaitis.list.ActivityStarter

class AppDesktop(val app: App, index:Int) : DesktopItem(index) {


    override var image: Drawable?
        get() = app.icon
        set(value) {}
    override var label: String
        get() = app.label
        set(value) {}





}
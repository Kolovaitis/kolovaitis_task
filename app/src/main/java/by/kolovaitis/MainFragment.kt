package by.kolovaitis

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.fragment_main.*
import java.lang.Exception


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
val COL_COUNT = 3

/**
 * A simple [Fragment] subclass.
 * Use the [MainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainFragment : Fragment() {
    private val model: AppsViewModel by activityViewModels()
    lateinit var addButtonOnClick: View.OnClickListener
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    inner class MyCallback(val mAdapter: MainGridAdapter) : ItemTouchHelper.Callback() {


        override fun isLongPressDragEnabled(): Boolean {
            return true
        }

        override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
            val dragFlags =
                ItemTouchHelper.UP or ItemTouchHelper.DOWN or ItemTouchHelper.START or ItemTouchHelper.END
            val swipeFlags = 0
            return makeMovementFlags(
                dragFlags,
                swipeFlags
            )
        }

        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: ViewHolder,
            target: ViewHolder
        ): Boolean {
            if (model.moveDesktop(viewHolder.adapterPosition, target.adapterPosition))
                mAdapter.move(viewHolder.adapterPosition, target.adapterPosition)
            return true
        }

        override fun isItemViewSwipeEnabled(): Boolean {
            return true
        }


        override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewManager = GridLayoutManager(context, COL_COUNT)
        val viewAdapter = MainGridAdapter(
            activity as LauncherActivity,
            (activity as LauncherActivity).activityStarter
        )
        gridmenu.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        viewAdapter.wasChanged(model.getDesktopApps().value)
        model.getDesktopApps().observe(viewLifecycleOwner, Observer<List<DesktopItem>> { items ->
            viewAdapter.wasChanged(items)
            progressBarDesktop.visibility = View.GONE
        })
        val touchHelper = ItemTouchHelper(MyCallback(viewAdapter))
        touchHelper.attachToRecyclerView(gridmenu)

    }

    fun addButtonOnClick(onClickListener: View.OnClickListener) {
        addButtonOnClick = onClickListener
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment MainFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MainFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

package by.kolovaitis.list

import by.kolovaitis.App
import by.kolovaitis.DesktopItem

interface ActivityStarter {
fun startActivity(app: App)
    fun startDesktop(desktopItem: DesktopItem)
}

package by.kolovaitis.list

import android.app.Activity
import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import by.kolovaitis.App
import by.kolovaitis.R

class GridAdapter(
    val context: Activity,
    val activityStarter: ActivityStarter
) :
    RecyclerView.Adapter<GridAdapter.MyViewHolder>() {
    private var currentApps: MutableList<App> = mutableListOf()

    fun wasChanged(newApps: List<App>?) {
        if (newApps != null) {
            if (currentApps.isEmpty()) {
                currentApps = newApps.toMutableList()
                notifyDataSetChanged()
            } else {

                for (i in newApps.indices) {
                    var pastIndex = -1
                    for (j in currentApps.indices) {
                        if (newApps[i].packageName == currentApps[j].packageName) {
                            pastIndex = j
                            break
                        }
                    }

                    if (pastIndex == -1) {
                        Log.d("GridAdapterMy", newApps[i].label + " $i size ${currentApps.size}")
                        currentApps.add(i, newApps[i])
                        notifyItemInserted(i)
                    } else {

                        currentApps.add(i, newApps[i])
                        notifyItemMoved(pastIndex, i)
                        for (j in i + 1 until currentApps.size) {
                            if (newApps[i].packageName == currentApps[j].packageName) {
                                currentApps.removeAt(j)
                                break

                            }
                        }
                    }
                }
                while (currentApps.size > newApps.size) {
                    currentApps.removeAt(currentApps.size - 1)
                }
            }
        }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(val cardView: CardView) : RecyclerView.ViewHolder(cardView)


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GridAdapter.MyViewHolder {
        // create a new view
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.grid_card_view, parent, false) as CardView
        context.registerForContextMenu(cardView)
        return MyViewHolder(cardView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        holder.cardView.findViewById<ImageView>(R.id.imageView)
            .setImageDrawable(currentApps[position].icon)
        holder.cardView.findViewById<TextView>(R.id.label).text =
            currentApps[position].label

        holder.cardView.tag = currentApps[position]

        //text = myDataset[position]
        holder.cardView.setOnClickListener {
            activityStarter.startActivity(currentApps[position])

        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = currentApps.size
}